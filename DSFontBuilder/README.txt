Ultima VI Project
Dungeon Siege Font Builder
Presented by Archon (www.u6archon.com)


PURPOSE:
---------------------------------------------------------
To easily create fonts usable from within Dungeon Siege.



REQUIREMENTS:
---------------------------------------------------------
Dungeon Siege Toolkit (DSTK) (www.dungeonsiege.com)
Dungeon Siege (www.dungeonsiege.com)
Windows 9x/2000/XP (tested on 2000 and XP)



INSTALLATION:
---------------------------------------------------------
To install, run the setup program.  A shortcut should be installed in the start menu and the following directory structure created:

DSFontBuilder

RUNNING:
---------------------------------------------------------
To launch the UI Designer, double+click U6PID.exe in the root directory or launch from the Dungeon Siege Toolkit folder on the start menu.


HOW TO USE:
---------------------------------------------------------



SUPPORT:
---------------------------------------------------------
A forum for questions has been set up at http://dynamic6.gamespy.com/~archon/phpBB2


CREDITS:
---------------------------------------------------------

Thanks go out to the following:

  Jesse Strachman - Ultima 6 Project (www.u6archon.com)
  Shaddock Heath - Ultima 6 Project (www.u6archon.com)
  Tiberius Moongazer - Ultima V: Lazarus (www.u5lazarus.com)

Dungeon Siege Community:

  Gas Powered Games (www.dungeonsiege.com)
  Unkle Ernsie and all the folks on Siegworks.org (www.siegeworks.org)
  GameEditing.net (site no longer active)
  Planet Dungeon Siege (www.planetdungeonsiege.com)
  Siege Network (www.siegenetwork.com)
  Dungeon Siege Heaven (ds.heavengames.com)

Delphi Resources:

  Borland International (www.borland.com)
  JVCL (Project Jedi) Component Team (jvcl.sourceforge.net)
  Mike Lischke for the GraphicEx library (www.delphi-gems.com)



DISCLAIMER
---------------------------------------------------------
Dungeon Siege is a registered trademark of Gas Powered Games, Inc. and Microsoft, Inc.
