object frmFontBuilder: TfrmFontBuilder
  Left = 179
  Top = 276
  BorderStyle = bsDialog
  Caption = 'Dungeon Siege Font Builder '
  ClientHeight = 324
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 288
    Top = 23
    Width = 256
    Height = 256
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 24
    Height = 13
    Caption = 'Font:'
  end
  object Label2: TLabel
    Left = 224
    Top = 8
    Width = 27
    Height = 13
    Caption = 'Pitch:'
  end
  object Button1: TButton
    Left = 456
    Top = 287
    Width = 89
    Height = 26
    Caption = 'Save Font'
    TabOrder = 0
    OnClick = Button1Click
  end
  object JvFontComboBox1: TJvFontComboBox
    Left = 8
    Top = 24
    Width = 201
    Height = 22
    FontName = '@Arial Unicode MS'
    ItemIndex = 0
    Options = [foTrueTypeOnly]
    Sorted = True
    TabOrder = 1
    OnChange = FontSettingChanged
  end
  object cmbSize: TComboBox
    Left = 224
    Top = 24
    Width = 41
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 2
    Text = '8'
    OnChange = FontSettingChanged
    Items.Strings = (
      '8'
      '9'
      '10'
      '11'
      '12'
      '14'
      '18'
      '20'
      '24'
      '32')
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 56
    Width = 273
    Height = 153
    Caption = 'Font Style:'
    TabOrder = 3
    object Label5: TLabel
      Left = 172
      Top = 17
      Width = 93
      Height = 13
      Caption = '(Not recommended)'
    end
    object Label6: TLabel
      Left = 172
      Top = 41
      Width = 93
      Height = 13
      Caption = '(Not recommended)'
    end
    object Label4: TLabel
      Left = 8
      Top = 96
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Foreground Color:'
    end
    object Label3: TLabel
      Left = 6
      Top = 124
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Background Color:'
    end
    object chkBold: TCheckBox
      Left = 8
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Bold'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = FontSettingChanged
    end
    object chkItalic: TCheckBox
      Left = 8
      Top = 40
      Width = 73
      Height = 17
      Caption = 'Italic'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 1
      OnClick = FontSettingChanged
    end
    object chkUnderline: TCheckBox
      Left = 88
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Underline'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = FontSettingChanged
    end
    object chkStrikethrough: TCheckBox
      Left = 88
      Top = 40
      Width = 85
      Height = 17
      Caption = 'Strikethrough'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsStrikeOut]
      ParentFont = False
      TabOrder = 3
      OnClick = FontSettingChanged
    end
    object chkAntialias: TCheckBox
      Left = 8
      Top = 72
      Width = 257
      Height = 17
      Caption = 'Antialias (Only works for certain fonts/OSs)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = FontSettingChanged
    end
    object JvColorButton2: TJvColorButton
      Left = 96
      Top = 93
      Width = 49
      Height = 22
      OtherCaption = '&Other...'
      Options = []
      Color = clWhite
      OnChange = FontSettingChanged
    end
    object JvColorButton1: TJvColorButton
      Left = 96
      Top = 120
      Width = 49
      Height = 22
      OtherCaption = '&Other...'
      Options = []
      OnChange = FontSettingChanged
    end
  end
  object dlgSave: TSaveDialog
    DefaultExt = 'raw'
    Filter = '*.raw|*.raw|All Files (*.*)|*.*'
    Left = 176
    Top = 240
  end
end
