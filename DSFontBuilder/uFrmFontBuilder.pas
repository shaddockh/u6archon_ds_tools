unit uFrmFontBuilder;
(*

 Copyright (C) 2004  Jesse Strachman, T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmFontBuilder.pas,v $
 * Author(s):  Jesse (archon), Shaddock (archon)
 *
 * @copyright (C) 2004 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Main interface for the DS Font Builder
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.11 $              $Date: 2004-02-25 02:07:42 $
 *----------------------------------------------------------------------------
 *
 */

 Updated 2004-02-23   by Dan
 -Uglified the code by a factor of 3.7
 -Fixed anti-aliasing on the alpha chan if selected.
 -Outputs .raw files instead of 3vil tga.
 -Shows a save dialog instead of saving to app dir

 2004-02-24 Shaddock (Archon)
 - Merged Dan's changes in with the official branch
 - Fixed an issue with the dot being rendered off the edge of the texture.
 - Consolidated the Font Setting Event Handlers
 - Added 24pt and 32pt Font Settings

*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, JvCombobox, JvColorCombo, JvComponent,
  JvColorBox, JvColorBtn, ShellApi, RawFormat;

type
  TfrmFontBuilder = class(TForm)
    Image1: TImage;
    Button1: TButton;
    JvFontComboBox1: TJvFontComboBox;
    cmbSize: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    chkBold: TCheckBox;
    chkItalic: TCheckBox;
    chkUnderline: TCheckBox;
    chkStrikethrough: TCheckBox;
    chkAntialias: TCheckBox;
    Label4: TLabel;
    JvColorButton2: TJvColorButton;
    JvColorButton1: TJvColorButton;
    Label3: TLabel;
    dlgSave: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FontSettingChanged(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    procedure RenderFont(SaveToFile: boolean);
    procedure FontToBitmap(bmp: TBitmap);
  end;

var
  frmFontBuilder    : TfrmFontBuilder;

const
  DOTOFFSET         = 1;                //# of pixels between right edge of letter and dot
  TITLE             = 'Dungeon Siege Font Builder';
  VERSION           = 'v. 0.2';
implementation

{$R *.DFM}

function MakeItAString(I: Longint): string;

{ Convert any integer type to a string }
var
  S                 : string[11];
begin
  Str(I, S);
  Result := S;
end;

procedure TfrmFontBuilder.Button1Click(Sender: TObject);
begin
  RenderFont(true);
end;

procedure TfrmFontBuilder.FontToBitmap(bmp: TBitmap);
var
  iChar             : integer;
  iRow              : integer;
  ixPos             : integer;
  iyPos             : integer;
  ivertInterval     : integer;
  lf                : TLogFont;
begin
  bmp.PixelFormat := pf32bit;
  bmp.Canvas.Font.Name := jvFontComboBox1.FontName;
  bmp.Canvas.Font.Pitch := fpVariable;
  bmp.Canvas.Font.Size := strToIntDef(cmbSize.Text, 8);

  bmp.Width := 256;
  bmp.Height := 256;

  // set font style
  if chkBold.Checked then
    bmp.Canvas.Font.Style := bmp.Canvas.Font.Style + [fsBold];
  if chkItalic.Checked then
    bmp.Canvas.Font.Style := bmp.Canvas.Font.Style + [fsItalic];
  if chkUnderline.Checked then
    bmp.Canvas.Font.Style := bmp.Canvas.Font.Style + [fsUnderline];
  if chkStrikethrough.Checked then
    bmp.Canvas.Font.Style := bmp.Canvas.Font.Style + [fsStrikeout];

  if chkAntialias.Checked then
  begin
    GetObject(bmp.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
    lf.lfQuality := ANTIALIASED_QUALITY;
    bmp.Canvas.Font.Handle := CreateFontIndirect(lf);
  end;

  iRow := 0;
  iyPos := 0;
  ivertInterval := abs(bmp.Canvas.Font.Height) +
    round(bmp.Canvas.Font.Size / 3) + 1;
  ixPos := 0;

  for iChar := 32 to 255 do
  begin
    if ixPos + bmp.Canvas.TextWidth(Chr(iChar)) + DOTOFFSET > 256 then
    begin
      iRow := iRow + 1;
      iyPos := (iRow * ivertInterval);
      ixPos := 0;
    end;

    bmp.Canvas.TextOut(ixPos, iyPos, Chr(iChar));
    ixPos := ixPos + bmp.Canvas.TextWidth(Chr(iChar)) + DOTOFFSET + 1;
  end;
end;

procedure TfrmFontBuilder.RenderFont(SaveToFile: boolean);
type
  PRGBQuadArray = ^TRGBQuadArray;
  TRGBQuadArray = array[WORD] of TRGBQuad;

var
  iChar             : integer;
  iRow              : integer;
  ixPos             : integer;
  iyPos             : integer;
  ivertInterval     : integer;

  basename          : string;
  P                 : PRGBQuadArray;
  Ptl               : PRGBQuadArray;
  y                 : integer;
  x                 : integer;

  bmp               : TRawGraphic;
  bmp_alpha         : TBitmap;
  sl                : TStringlist;
  fntName           : string;
begin
  //Create the base filename - this follows DS naming conventions
  basename := 'b_gui_fnt_' + cmbSize.text + 'p_' + jvFontComboBox1.FontName;
  basename := StringReplace(basename, ' ', '-', [rfReplaceAll]);

  dlgSave.FileName := basename + '.raw';

  if SaveToFile then
  begin
    if not dlgSave.Execute then Exit;
  end;

  bmp := nil;
  bmp_alpha := nil;
  try
    bmp := TRawGraphic.Create;
    bmp.Canvas.Brush.Color := JvColorButton1.Color;
    bmp.Canvas.FillRect(bmp.Canvas.ClipRect);
    bmp.Canvas.Font.Color := JvColorButton2.Color;

    bmp_alpha := TBitmap.Create;
    bmp_alpha.Canvas.Brush.Color := clBlack;
    bmp_alpha.Canvas.FillRect(bmp.Canvas.ClipRect);
    bmp_alpha.Canvas.Font.Color := clWhite;

    FontToBitmap(bmp);

    if SaveToFile then
    begin
      FontToBitmap(bmp_alpha);

      // Set up alpha
      for y := 0 to bmp.Height - 1 do
      begin
        P := bmp.ScanLine[y];
        Ptl := bmp_alpha.ScanLine[y];
        for x := 0 to bmp.Width - 1 do
        begin
          p[x].rgbReserved := (Ptl[x].rgbBlue + Ptl[x].rgbGreen + Ptl[x].rgbRed) div 3;
        end;
      end;
    end;

    ivertInterval := abs(bmp.Canvas.Font.Height) +
      round(bmp.Canvas.Font.Size / 3) + 1;

    { Print dots and save to file }

    iRow := 0;
    iyPos := 0;
    ixPos := 0;

    //Set our purple hook point color
    bmp.Canvas.Pen.Color := $00FF00FF;

    for iChar := 32 to 255 do
    begin
      if ixPos + bmp.Canvas.TextWidth(Chr(iChar)) + DOTOFFSET > 256 then
      begin
        iRow := iRow + 1;
        iyPos := (iRow * ivertInterval);
        ixPos := 0;
      end;

      bmp.Canvas.MoveTo(ixPos + bmp.Canvas.TextWidth(Chr(iChar)), iyPos);
      bmp.Canvas.LineTo(ixPos + bmp.Canvas.TextWidth(Chr(iChar)), iyPos + 1);

      ixPos := ixPos + bmp.Canvas.TextWidth(Chr(iChar)) + DOTOFFSET + 1;
    end;

    //Display our handiwork
    Image1.Canvas.Draw(0, 0, bmp);

    //Save the file.
    if SaveToFile then
    begin
      bmp.SaveToFile(dlgSave.FileName);

      sl := nil;
      try
        sl := TStringlist.create;
        fntName := cmbSize.text + 'p_' + jvFontComboBox1.FontName;
        fntName := StringReplace(fntName, ' ', '-', [rfReplaceAll]);
        sl.add('//Place this file in: \bits\ui\fonts\');
        sl.add('//and place the associated texture file in: \bits\art\bitmaps\gui\fonts\');
        sl.add('[t:font,n:' + 'b_gui_fnt_' + fntName + ']');
        sl.add('{');
        sl.add(#9 + 'endrange = 256;');
        sl.add(#9 + 'height = ' + MakeItAString(ivertInterval) + ';');
        sl.add(#9 + 'startrange = 32;');
        sl.add(#9 + 'texture = ' + basename + ';');
        sl.add('}');
        sl.SaveToFile(ExtractFilePath(dlgSave.FileName) + 'font_' + fntName + '.gas');

        MessageDlg('Font has been saved to: ' + #13#10 +
          dlgSave.FileName + #13#10 +
          ExtractFilePath(dlgSave.FileName) + 'font_' + fntName + '.gas',
          mtInformation, [mbOK], 0);

        sl.LoadFromFile(ExtractFilePath(application.exename) + 'build.tem');
        sl.text := stringreplace(sl.text, '<FONTNAME>', JvFontComboBox1.FontName, [rfIgnoreCase, rfReplaceAll]);
        sl.text := stringreplace(sl.text, '<FONTPITCH>', cmbSize.text, [rfIgnoreCase, rfReplaceAll]);
        if chkBold.checked then
          sl.text := stringreplace(sl.text, '<BOLD>', 'Yes', [rfIgnoreCase, rfReplaceAll])
        else
          sl.text := stringreplace(sl.text, '<BOLD>', 'No', [rfIgnoreCase, rfReplaceAll]);

        if chkItalic.checked then
          sl.text := stringreplace(sl.text, '<ITALIC>', 'Yes', [rfIgnoreCase, rfReplaceAll])
        else
          sl.text := stringreplace(sl.text, '<ITALIC>', 'No', [rfIgnoreCase, rfReplaceAll]);

        if chkUnderline.checked then
          sl.text := stringreplace(sl.text, '<UNDERLINE>', 'Yes', [rfIgnoreCase, rfReplaceAll])
        else
          sl.text := stringreplace(sl.text, '<UNDERLINE>', 'No', [rfIgnoreCase, rfReplaceAll]);

        if chkStrikethrough.checked then
          sl.text := stringreplace(sl.text, '<STRIKETHROUGH>', 'Yes', [rfIgnoreCase, rfReplaceAll])
        else
          sl.text := stringreplace(sl.text, '<STRIKETHROUGH>', 'No', [rfIgnoreCase, rfReplaceAll]);

        if chkAntialias.checked then
          sl.text := stringreplace(sl.text, '<ANTIALIAS>', 'Yes', [rfIgnoreCase, rfReplaceAll])
        else
          sl.text := stringreplace(sl.text, '<ANTIALIAS>', 'No', [rfIgnoreCase, rfReplaceAll]);

        sl.text := stringreplace(sl.text, '<OUTPUTPATH>', ExtractFilePath(dlgSave.FileName), [rfIgnoreCase, rfReplaceAll]);
        sl.text := stringreplace(sl.text, '<FILE1>', ExtractFilename(dlgSave.FileName), [rfIgnoreCase, rfReplaceAll]);
        sl.text := stringreplace(sl.text, '<FILE2>', 'font_' + fntName + '.gas', [rfIgnoreCase, rfReplaceAll]);

        sl.SaveToFile(ChangeFileExt(dlgSave.FileName, '.txt'));

        ShellExecute(handle, 'open', pchar(ChangeFileExt(dlgSave.FileName, '.txt')), '', '', 0);
      finally
        if assigned(sl) then freeAndNil(sl);
      end;

    end;
  finally
    if assigned(bmp) then FreeAndNil(bmp);
    if assigned(bmp) then FreeAndNil(bmp_alpha);
  end;
end;

procedure TfrmFontBuilder.FormCreate(Sender: TObject);
begin
  Caption := TITLE + ' (' + VERSION + ')';
  RenderFont(False);
end;

procedure TfrmFontBuilder.FontSettingChanged(Sender: TObject);
begin
  RenderFont(False);
end;

end.

