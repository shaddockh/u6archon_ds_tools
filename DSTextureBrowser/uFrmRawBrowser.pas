unit uFrmRawBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvListBox, JvDriveCtrls, ExtCtrls, JvBaseThumbnail,
  JvThumbviews,uRawRapiReader,graphicex, ComCtrls, ShellCtrls, ToolWin;

type
  TForm1 = class(TForm)
    JvThumbView1: TJvThumbView;
    Splitter1: TSplitter;
    ShellTreeView1: TShellTreeView;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    trkThumbSize: TTrackBar;
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure trkThumbSizeChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1             : TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  msg               : Cardinal;
  code              : Cardinal;
  i, n              : Integer;
  l : hwnd;
begin
  l := ChildWindowFromPoint(jvThumbView1.Handle, jvThumbView1.ScreenToClient(Mouse.CursorPos));
  //caption := intToStr(l);
  if  l <> 0 then
  begin
    Handled := true;
    if ssShift in Shift then
      msg := WM_HSCROLL
    else
      msg := WM_VSCROLL;

    if WheelDelta > 0 then
      code := SB_LINEUP
    else
      code := SB_LINEDOWN;

    n := Mouse.WheelScrollLines;
    for i := 1 to n do
      jvThumbView1.Perform(msg, code, 0);
    jvThumbView1.Perform(msg, SB_ENDSCROLL, 0);
  end;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
        FileFormatList.RegisterFileFormat('raw', 'Raw', 'GPG Raw', [ftRaster], true, true, TDSRawGraphic);
end;

procedure TForm1.ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
begin
JvThumbView1.Directory := ShellTreeView1.Path;
end;

procedure TForm1.trkThumbSizeChange(Sender: TObject);
begin
jvThumbView1.MaxHeight := trkThumbSize.Position;
jvThumbView1.MaxWidth := trkThumbSize.Position;
//jvThumbView1.VertScrollBar.Increment := trkThumbSize.Position;
end;

end.

