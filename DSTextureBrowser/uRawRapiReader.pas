(*

 Copyright (C) 2003  T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uRawRapiReader.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Reader class for RAW files.
 *
 * Thanks non` for the insight into the RAW files.
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.1 $              $Date: 2004-02-25 02:26:19 $
 *----------------------------------------------------------------------------
 *
 */
*)
unit uRawRapiReader;
// Thanks non` for the insight into the raw files!
interface
uses sysutils, classes, graphics, graphicex;
type
  TDSRawGraphic = class(TGraphicExGraphic)
  public
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;
  end;

implementation
type
  TDSRawFileHeader = record
    case boolean of
      true:
      (
        m_HeaderMagic: array[0..3] of char; // special magic number
        m_Format: array[0..3] of char;  // format of bits
        m_Flags: word;                  // any special flags (for future expansion)
        m_SurfaceCount: word;           // total surfaces stored (for mip maps), always >= 1
        m_Width: word;                  // width of surface 0
        m_Height: word;                 // height of surface 0
        );
      false: (Data: array[0..13] of char);
  end;

const
  HEADER_MAGIC      = 'ipaR';
  FORMAT_UNKOWN     = '0';
  FORMAT_ARGB_8888  = '8888';

procedure TDSRawGraphic.LoadFromStream(Stream: TStream);
var
  header            : TDSRawFileHeader;
  P                 : PByteArray;
  rowsize           : integer;
begin
  Stream.Read(header, sizeof(header));
  if header.m_HeaderMagic <> HEADER_MAGIC then raise exception.create('Invalid Raw file');
  if header.m_Format <> FORMAT_ARGB_8888 then raise Exception.create('Unknown format for raw file');
  width := header.m_width;
  height := header.m_height;
  PixelFormat := pf32bit;
  rowsize := header.m_width * 4;

  //Raw textures are upside down, so start filling in from the bottom of the bitmap
  //Optimization tip gleaned from Dan's unofficial modification to the font builder
  P := Scanline[Height -1];
  Stream.Read(p^, rowsize * height );

end;

procedure TDSRawGraphic.SaveToStream(Stream: TStream);
var
  header            : TDSRawFileHeader;
  P                 : PByteArray;
  rowsize           : integer;
begin
  header.m_HeaderMagic := HEADER_MAGIC;
  header.m_Format := FORMAT_ARGB_8888;
  header.m_Flags := 0;
  header.m_SurfaceCount := 1;
  header.m_Width := width;
  header.m_Height := height;
  Stream.Write(Header.data, sizeof(Header));
  rowsize := header.m_width * 4;

  //Raw textures are upside down, so start filling in from the bottom of the bitmap
  //Optimization tip gleaned from Dan's unofficial modification to the font builder
  P := ScanLine[Height - 1];
  Stream.Write(P^, rowsize * Height);

end;
end.

