(*

 Copyright (C) 2003  T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: DSGuiComponents.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * UI components that can be placed
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.19 $              $Date: 2004-02-25 14:16:21 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit DSGuiComponents;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls,
  ExtCtrls, forms, graphics, jansticker, stdCtrls,
  uGasParser, contnrs, graphicex, uRawRapiReader;

type

  TDSTextureMode = (Clamp, Tile);
  TDSCommonTemplate = (ctNone, ctBrowntrim, ctButton_4, ctButton_5, ctButton_down, ctButton_inset, ctButton_stop, ctButton_subtab, ctButton_up, ctCpbox, ctCpbox_thin, ctCpbox_thin_dark, ctCpbox_wide, ctCrystal, ctDbldwn, ctDblup, ctDown, ctGraytrim, ctJbox, ctLeft, ctMin, ctMid, ctMax, ctReplay, ctRight, ctTrack, ctUp, ctWoodbox, ctX, ctX_button);
  TDSDockbarType = (dockbar_screen_x_switch_y, dockbar_screen_y);
  TDSJustify = (jsLeft, jsCenter, jsRight);
  TDSJustifyY = (jsyCenter, jsyTop, jsyBottom);
  TDSEdge = (edgeLeft, edgeTop, edgeRight, edgeBottom);
  TDSDesignerColor = (dcBlack, dcMaroon, dcGreen, dcOlive, dcNavy, dcPurple, dcTeal, dcGray, dcSilver, dcRed, dcLime, dcYellow, dcBlue, dcFuchsia, dcAqua, dcWhite);
  TDSSlotTypes = (slotAmulet, slotArmour, slotBoots, slotGauntlets, slotHelmet, slotMelee_Weapon, slotRanged_Weapon, slotRing, slotShield, slotSpellbook, slotPicture, slotSpell, slotScroll);
  TDSGridTypes = (gridInventory);
  TDSLBElementTypes = (lbetText);
  TDSSlideAxis = (slaxHorizontal, slaxVertical);
  TDSPopAlignment = (popDownLeft);

  TDSInterface = class(TPanel)
  protected
    Fisinterface: boolean;
    fintended_resolution_height: integer;
    fintended_resolution_width: integer;
    fdisable_camera: boolean;
    fmodal: boolean;
    FPassiveModal: boolean;
    //    fskrit_block: TStringlist;
    fcentered: string;
    FInterfaceName: string;
    FCode: string;
    FOnHeirarchyChange: TNotifyEvent;
    FReferenceBitmap: TBitmap;
    FDesignerReferenceImage: string;
    FDesignerRefImageX: integer;
    FDesignerRefImageY: integer;
    PropertyXRef: TSTringlist;
    //procedure SetSkritBlock(Value: TStringlist);
    procedure SetIntendedResolutionWidth(Value: integer);
    procedure SetIntendedResolutionHeight(Value: integer);
    procedure SetInterfaceName(Value: string);
    procedure SetDesignerReferenceImage(Value: string);
    procedure SetDesignerRefImgX(Value: integer);
    procedure SetDesignerRefImgY(Value: integer);
    procedure Paint; override;

  public
    constructor create(AOwner: TComponent); override;
    destructor destroy; override;
    function GenerateInterface: string;
    procedure SaveToStream(var Stream: TStream);
    procedure LoadFromStream(var Stream: TStream);
    procedure ImportFromGas(Gas: string);
    procedure Clear;
    procedure InsertPropertyTemplate(name: string; value: string = '');
    Procedure SetRenderComponentTextures(render : boolean);
  published
    property isinterface: boolean read Fisinterface write Fisinterface;
    property intended_resolution_height: integer read fintended_resolution_height write SetIntendedresolutionheight;
    property intended_resolution_width: integer read fintended_resolution_width write SetIntendedresolutionwidth;
    property disable_camera: boolean read fdisable_camera write fdisable_camera;
    property modal: boolean read fmodal write fmodal;
    property passive_modal: boolean read fPassiveModal write fPassiveModal;
    //    property skrit_block: TStringlist read fskrit_block write SetSkritblock;
    property centered: string read fcentered write fcentered;
    property interface_name: string read FInterfaceName write SetInterfaceName;
    property code: string read FCode write FCode;
    property designer_ref_img: string read FDesignerReferenceImage write SetDesignerReferenceImage;
    property designer_ref_img_x: integer read FDesignerRefImageX write SetDesignerRefImgX;
    property designer_ref_img_y: integer read FDesignerRefImageY write SetDesignerRefImgY;
    property OnHeirarchyChange: TNotifyEvent read FOnHeirarchyChange write FOnHeirarchyChange;
  end;

  TDSBaseUIControl = class;
  TElementSelectionProc = procedure(Sender: TDSBaseUIControl; Button: TMouseButton; Shift: TShiftState; X, Y: Integer) of object;
  TDSBaseUIControl = class(TShape)
  //  TDSBaseUIControl = class(TCustomSMPanel)
    //TDSBaseUIControl = class(TPanel)
  private
    { Private declarations }
  protected
    { Protected declarations }
    FGroup: string;
    FDrawOrder: integer;
    FTemplateName: string;
    fdock_group: string;
    fCode: TStringlist;
    FOnSelected: TElementSelectionProc;
    FUnselected: TNotifyEvent;
    FOnHeirarchyChange: TNotifyEvent;
    FOnChange: TNotifyEvent;
    FOnBoundsChange: TNotifyEvent;
    FElementName: string;
    FTexture: string;
    FUVCoords: string;
    FTextureMode: TDSTextureMode;
    FCommonControl: Boolean;
    FCommonTemplate: TDSCommonTemplate;
    FAlpha: Double;
    FPassthrough: boolean;
    FIsVisible: boolean;
    FIsBottomAnchor: boolean;
    FBottomAnchor: integer;
    FIsRightAnchor: boolean;
    FRightAnchor: integer;
    FIsTopAnchor: boolean;
    FTopAnchor: integer;
    FParentElement: string;
    FIndex: integer;
    FSelected: boolean;
    FSizer: TjanStickSizer;
    FHiddenProperties: string;
    FRolloverHelp: string;
    PropertyXRef: TSTringlist;
    FDesignerColor: TDSDesignerColor;
    FDesignerPattern: TBrushStyle;
    FConsumable: boolean;
    FType: string;
    FTextureBitmap: TBitmap;
    FTextureLoaded: boolean;
    FUVLeft: double;
    FUVTop: double;
    FUVRight: double;
    FUVBottom: double;

    function getRight: integer;
    procedure setright(value: integer);

    function getLeft: integer;
    procedure setLeft(value: integer);
    function getTop: integer;
    procedure setTop(value: integer);
    function getWidth: integer;
    procedure setWidth(value: integer);
    function getHeight: integer;
    procedure setHeight(value: integer);

    function getBottom: integer;

    procedure CMMouseLeave(var Msg: TMessage); message CM_MouseLeave;
    procedure CMMouseEnter(var Msg: TMessage); message CM_MouseEnter;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure CMInvalidate(var Message: TMessage); message CM_CHANGED;
    procedure SetCode(Value: TStringlist);
    procedure setbottom(value: integer);
    procedure SetElementName(value: string);
    procedure SetParentElement(value: string);
    procedure SetSelected(value: boolean);
    procedure SetGroup(value: string);
    procedure SetVisible(value: boolean);
    function GetVisible: boolean;
    procedure SetTexture(Value: string);
    procedure SetUVCoords(Value: string);
    procedure SetDesignerColor(value: TDSDesignerColor);
    procedure SetDesignerPattern(value: TBrushStyle);

    procedure RepositionSizer;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure UpdateComponentTexture;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); virtual;
    function BuildParserComponent: TGasInterestItem; virtual;

    procedure InsertPropertyTemplate(name: string; value: string = ''); virtual;
    procedure Paint; override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure SetBounds(ALeft: integer; ATop: integer; AWidth: integer; AHeight: integer); override;
    destructor destroy; override;

    property TemplateName: string read FTemplateName write FTemplateName;
    property Selected: boolean read FSelected write SetSelected;
    property HiddenProperties: string read FHiddenProperties write FHiddenProperties;
    property Sizer: TjanStickSizer read FSizer;
    property type_: string read FType write FType;

  published

    { Published declarations }
    property Right: integer read getRight write setright;
    property Bottom: integer read getBottom write setbottom;
    property Left: integer read getLeft write setLeft;
    property Top: integer read getTop write setTop;
    property Width: integer read getWidth write setWidth;
    property Height: integer read getHeight write setHeight;
    property group: string read FGroup write SetGroup;
    property Code: TStringlist read FCode write SetCode;

    property draw_order: integer read FDrawOrder write FDrawOrder;

    property dock_group: string read fdock_group write fdock_group;
    property OnSelect: TElementSelectionProc read FOnSelected write FOnSelected;
    //property OnCtrlSelect: TNotifyEvent read FOnCtrlSelected write FOnCtrlSelected;
    property OnUnSelect: TNotifyEvent read FUnselected write FUnselected;
    property OnHeirarchyChange: TNotifyEvent read FOnHeirarchyChange write FOnHeirarchyChange;
    property Name_: string read FElementName write SetElementName;
    property texture: string read FTexture write SetTexture;
    property uvcoords: string read FUVCoords write SetUVCoords;
    property wrap_mode: TDSTextureMode read FTextureMode write FTextureMode;
    property common_control: boolean read FCommonControl write FCommonControl;
    property common_template: TDSCommonTemplate read FCommonTemplate write FCommonTemplate;
    property alpha: double read FAlpha write FAlpha;
    property pass_through: boolean read FPassthrough write FPassthrough;
    property Visible_: boolean read FIsVisible write FIsVisible;
    property is_bottom_anchor: boolean read FIsBottomAnchor write FIsBottomAnchor;

    property is_right_anchor: boolean read FIsRightAnchor write FIsRightAnchor;

    property is_top_anchor: boolean read FIsTopAnchor write FIsTopAnchor;

    property bottom_anchor: integer read FBottomAnchor write FBottomAnchor;
    property right_anchor: integer read FRightanchor write FRightAnchor;
    property top_anchor: integer read FTopAnchor write FTopAnchor;

    property ParentElement: string read FParentElement write SetParentElement;
    property index: integer read FIndex write FIndex;
    property Visible: boolean read GetVisible write SetVisible;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnBoundsChange: TNotifyEvent read FOnBoundsChange write FOnBoundsChange;
    property rollover_help: string read FRolloverHelp write FRolloverHelp;

    property designer_color: TDSDesignerColor read FDesignerColor write SetDesignerColor;
    property designer_pattern: TBrushStyle read FDesignerPattern write SetDesignerPattern;
    property consumable: boolean read fconsumable write fconsumable;
  end;

  TDSWindow = class(TDSBaseUIControl)
  protected
    Fdisable_color: string;
    FLeftRightButton: boolean;
    FCenterX: boolean;
    FTopmost: boolean;
    FNormalizeResize: boolean;
    FBackgroundColor: string;
    FBackgroundFill: boolean;
    FAllowUserPress: boolean;
    FTag: integer;
    FBorderColor: string;
    FHasBorder: boolean;
    FHelpText: string;
    FHelpToolTip: boolean;
    FHelpTextBox: string;

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property disable_color: string read fdisable_color write Fdisable_color;
    property leftright_button: boolean read FLeftRightButton write FLeftRightButton;
    property center_x: boolean read Fcenterx write fcenterx;
    property topmost: boolean read ftopmost write ftopmost;
    property normalize_resize: boolean read FNormalizeResize write FNormalizeResize;
    property background_color: string read FBackgroundColor write FBackgroundColor;
    property background_fill: boolean read FBackgroundFill write FBackgroundFill;
    property allow_user_press: boolean read FAllowUserPress write FAllowUserPress;
    property tag_: integer read Ftag write FTag;
    property border_color: string read FBorderColor write FBorderColor;
    property has_border: boolean read FHasBorder write FHasBorder;

    property help_text: string read FHelpText write FHelpText;
    property help_tool_tip: boolean read FHelpToolTip write FHelpToolTip;
    property help_text_box: string read FHelpTextBox write FHelpTextBox;

  end;

  TDSButton = class(TDSWindow)
  private
    FRepeatRate: double;
    FRepeater: boolean;
    FIgnoreItems: boolean;
    FFontType: string;
    FText: string;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;

  published

    property repeat_rate: double read fRepeatRate write FRepeatRate;
    property repeater: boolean read FRepeater write FRepeater;
    property ignore_items: boolean read FIgnoreItems write FIgnoreItems;
    property font_type: string read FFontType write FFontType;
    property text: string read FText write FText;

  end;

  TDSRadioButton = class(TDSWindow)
  private
    FRadioGroup: string;
    FPopup: boolean;
    FClickDelay: double;
    FSendUncheckToSelf: boolean;
    FIgnoreItems: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property radio_group: string read FRadioGroup write FRadioGroup;
    property popup: boolean read FPopup write FPopup;
    property click_delay: double read FClickDelay write FClickDelay;
    property send_uncheck_to_self: boolean read FSendUncheckToSelf write FSendUncheckToSelf;
    property ignore_items: boolean read FIgnoreItems write FIgnoreItems;

  end;

  TDSCheckbox = class(TDSWindow)
  private
    FPopup: boolean;

  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;

  published
    property popup: boolean read FPopup write FPopup;

  end;

  TDSListBox = class(TDSWindow)
  private
    FFontType: string;
    FTextcolor: string;
    FText: string;
    FRolloverSelect: boolean;
    FPermanentFocus: boolean;
    FLeftIndent: integer;
    FElementType: TDSLBElementTypes;
    FFontColor: string;
    FSelectionColor: string;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;

  published
    property font_type: string read FFontType write FFontType;
    property text_color: string read FTextColor write FTextColor;
    property text: string read FText write FText;
    property rollover_select: boolean read FRolloverSelect write FRolloverSelect;
    property permanent_focus: boolean read FPermanentFocus write FPermanentFocus;
    property left_indent: integer read FLeftIndent write FLeftIndent;
    property element_type: TDSLBElementTypes read FElementType write FElementType;
    property font_color: string read ffontcolor write ffontcolor;
    property selection_color: string read fselectioncolor write fselectioncolor;
  end;

  TDSDialogBox = class(TDSWindow)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;

  published
  end;

  TDSTextBox = class(TDSWindow)
  private
    FCenterHeight: boolean;
    FFixedLocation: boolean;
    FFontType: string;
    FHasSlider: boolean;
    FJustify: TDSJustify;
    FMaxHeight: integer;
    FMaxWidth: integer;
    FScrollRate: double;
    FStretchX: boolean;
    FText: string;
    ffontcolor: string;
    ffontsize: integer;
    FShowFirstLine: boolean;
    FLocFont: string;
    FTextColor: string;
    FFont1024x768: string;
    FBorderPadding: integer;
    FJustifyY: TDSJustifyY;
    FTruncate: boolean;

  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;

  published
    property center_height: boolean read FCenterHeight write FCenterHeight;
    property fixed_location: boolean read FFixedLocation write FFixedLocation;
    property font_type: string read FFontType write FFontType;
    property has_slider: boolean read FHasSlider write FHasSlider;
    property justify: TDSJustify read FJustify write FJustify;
    property max_height: integer read FMaxHeight write FMaxHeight;
    property max_width: integer read FMaxWidth write FMaxWidth;
    property scroll_rate: double read FScrollrate write FScrollrate;
    property stretch_x: boolean read FStretchX write FStretchX;
    property text: string read FText write FText;
    property font_color: string read ffontcolor write ffontcolor;
    property font_size: integer read ffontsize write ffontsize;

    property show_first_line: boolean read FShowFirstLine write FShowFirstLine;
    property loc_font: string read FLocFont write FLocFont;
    property text_color: string read FTextColor write FTextColor;
    property font_1024x768: string read FFont1024x768 write FFont1024x768;
    property border_padding: integer read FBorderPadding write FBorderPadding;
    property justify_y: TDSJustifyY read FJustifyY write FJustifyY;
    property truncate: boolean read FTruncate write FTruncate;

  end;

  TDSText = class(TDSWindow)
  private

    ffontcolor: string;
    ffontsize: integer;
    ffonttype: string;
    fjustify: TDSJustify;
    ftext: string;
    FTruncate: boolean;
    FNormalizeResize: boolean;
    FAutoSize: boolean;
    FFont1024x768: string;
    FTextColor: string;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published

    property font_color: string read ffontcolor write ffontcolor;
    property font_size: integer read ffontsize write ffontsize;
    property font_type: string read ffonttype write ffonttype;
    property justify: TDSJustify read FJustify write FJustify;
    property text: string read FText write FText;
    property truncate: boolean read FTruncate write FTruncate;
    property normalize_resize: boolean read FNormalizeResize write FNormalizeResize;
    property auto_size: boolean read FAutoSize write FAutoSize;
    property font_1024x768: string read FFont1024x768 write FFont1024x768;
    property text_color: string read FTextColor write FTextColor;
  end;

  TDSItemSlot = class(TDSBaseUIControl)
  private
  protected
    factivate_color: string;
    fborder_color: string;
    fis_equip_slot: boolean;
    FSlotType: TDSSlotTypes;
    FAcceptInput: boolean;
    FAllowInput: boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property activate_color: string read factivate_color write Factivate_color;
    property border_color: string read fborder_color write Fborder_color;
    property is_equip_slot: boolean read fis_equip_slot write Fis_equip_slot;
    property slot_type: TDSSlotTypes read FSlotType write FSlotType;
    property accept_input: boolean read FAcceptInput write FAcceptInput;
    property allow_input: boolean read FAllowInput write FAllowInput;
  end;

  TDSStatusBar = class(TDSBaseUIControl)
  private
    FDynamicEdge: TDSEdge;
    FDrawAdvance: boolean;
    FAdvanceColor: string;
    FAdvanceDuration: Double;
    FTopmost: boolean;
    FUVMap: boolean;
    FDrawOutline: boolean;
    FBorderColor: string;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property dynamic_edge: TDSEdge read FDynamicEdge write FDynamicEdge;
    property draw_advance: boolean read FDrawAdvance write FDrawAdvance;
    property advance_color: string read FAdvanceColor write FAdvanceColor;
    property advance_duration: double read FAdvanceDuration write FAdvanceDuration;
    property topmost: boolean read FTopmost write FTopmost;
    property uv_map: boolean read FUVMap write FUVMap;
    property draw_outline: boolean read FDrawOutline write FDrawOutline;
    property border_color: string read FBorderColor write FBorderColor;

  end;

  TDSDockbar = class(TDSBaseUIControl)
  private
    fStretchY: boolean;
    FStretchX: boolean;
    FDockbarType: TDSDockbarType;
    FDragDelay: double;
    FDragX: integer;
    FDragY: integer;
    FDraggable: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property dockbar_type: TDSDockbarType read FDockbartype write FDockbartype;
    property stretch_x: boolean read FStretchX write FStretchX;
    property stretch_y: boolean read FStretchY write FStretchY;
    property drag_delay: double read FDragDelay write FDragDelay;
    property drag_x: integer read FDragX write FDragX;
    property drag_y: integer read FDragY write FDragY;
    property draggable: boolean read FDraggable write FDraggable;

  end;

  TDSEditBox = class(TDSWindow)
  private
    FClearSelect: boolean;
    FFontColor: string;
    FFontSize: integer;
    FFontType: string;
    FMaxStringSize: integer;
    FPermanentFocus: boolean;
    FExcludedChars: string;
    FAllowedChars: string;
    FMaxPixels: integer;
    FHasPixelLimit: boolean;
    FAllowIme: boolean;
    FAsciiHighBound: integer;
    FAsciiLowBound: integer;
    FRestrictInput: boolean;
    FText: string;
    FPromptFlash: double;
    FTabStop: integer;
    FHiddenText: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property clear_select: boolean read FClearSelect write FClearSelect;
    property font_color: string read FFontColor write FFontColor;
    property font_size: integer read FFontSize write FFontSize;
    property font_type: string read FFontType write FFontType;
    property max_string_size: integer read FMaxStringSize write FMaxStringSize;
    property permanent_focus: boolean read FPermanentFocus write FPermanentFocus;
    property excluded_chars: string read FExcludedChars write FExcludedChars;
    property allowed_chars: string read FAllowedChars write FAllowedChars;
    property max_pixels: integer read FMaxPixels write FMaxPixels;
    property has_pixel_limit: boolean read FHasPixelLimit write FHasPixelLimit;
    property allow_ime: boolean read FAllowIme write FAllowIme;
    property ascii_high_bound: integer read FAsciiHighBound write FAsciiHighBound;
    property ascii_low_bound: integer read FAsciiLowBound write FAsciiLowBound;
    property restrict_input: boolean read FRestrictInput write FRestrictInput;
    property text: string read FText write FText;
    property prompt_flash: double read FPromptFlash write FPromptFlash;
    property tab_stop: integer read FTabStop write FTabStop;
    property hidden_text: boolean read FHiddenText write FHiddenText;
  end;

  TDSChatBox = class(TDSWindow)
  private
    FFontColor: string;
    FFontSize: integer;
    FFontType: string;
    FHasSlider: boolean;
    FLineTimeout: double;
    FMaxStringSize: integer;
    FScrollDown: boolean;
    FText: string;
    FDeleteTimeouts: boolean;
    FMaxHistory: integer;
    FRetainScrollPosition: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published

    property font_color: string read FFontColor write FFontColor;
    property font_size: integer read FFontSize write FFontSize;
    property font_type: string read FFontType write FFontType;
    property has_slider: boolean read FHasSlider write FHasSlider;
    property line_timeout: double read FLineTimeout write FLineTimeout;
    property max_string_size: integer read FMaxStringSize write FMaxStringSize;
    property scroll_down: boolean read FScrollDown write FScrollDown;
    property text: string read FText write FText;
    property delete_timeouts: boolean read FDeleteTimeouts write FDeleteTimeouts;
    property max_history: integer read FMaxHistory write FMaxHistory;
    property retain_scroll_position: boolean read FRetainScrollPosition write FRetainScrollPosition;

  end;

  TDSComboBox = class(TDSWindow)
  private
    FFontType: string;
    FTextColor: string;
    FHasBackground: boolean;
    FAutoSize: boolean;
    FMaxVisibleElements: integer;
    FShowText: boolean;
    FTruncate: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property font_type: string read FFontType write FFontType;
    property text_color: string read FTextColor write FTextColor;
    property has_background: boolean read FHasBackground write FHasBackground;
    property auto_size: boolean read FAutoSize write FAutoSize;
    property max_visible_elements: integer read FMaxVisibleElements write FMaxVisibleElements;
    property show_text: boolean read FShowText write FShowText;
    property truncate: boolean read FTruncate write FTruncate;
  end;

  TDSGridBox = class(TDSWindow)
  private
    FBoxHeight: integer;
    FBoxWidth: integer;
    FColumns: integer;
    FGridType: TDSGridTypes;
    FRows: integer;
    FAutoPlaceOnly: boolean;
    FConsumesItems: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property box_height: integer read FBoxHeight write FBoxHeight;
    property box_width: integer read FBoxWidth write FBoxWidth;
    property columns: integer read FColumns write FColumns;
    property grid_type: TDSGridTypes read FGridType write FGridType;
    property rows: integer read FRows write FRows;
    property autoplace_only: boolean read FAutoPlaceOnly write FAutoPlaceOnly;
    property consumes_items: boolean read FConsumesItems write FConsumesItems;
  end;

  TDSSlider = class(TDSBaseUIControl)
  private
    FslideAxis: TDSSlideAxis;
    FMinValue: integer;
    FMaxValue: integer;
    FStepValue: integer;
    FDynamicButton: boolean;
    FHasScrollButtons: boolean;
    FEnabled: boolean;
    FShowPopupValue: boolean;
    FPopupVAlueFont: string;
    FDefaultValue: integer;
    FRepeaterButtons: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property slide_axis: TDSSlideAxis read FSlideAxis write FSlideAxis;
    property min_value: integer read FMinValue write FMinValue;
    property max_value: integer read FMaxValue write FMaxValue;
    property step_value: integer read FStepValue write FStepVAlue;
    property dynamic_button: boolean read FDynamicButton write FDynamicButton;
    property has_scroll_buttons: boolean read FHasScrollButtons write FHasScrollButtons;
    property enabled_: boolean read FEnabled write FEnabled;
    property show_popup_value: boolean read FShowPopupValue write FShowPopupValue;
    property popup_value_font: string read FPopupValueFont write FPopupValueFont;
    property default_value: integer read FDefaultValue write FDefaultValue;

    property repeater_buttons: boolean read FRepeaterButtons write FRepeaterButtons;
  end;

  TDSTab = class(TDSBaseUIControl)
  private
    FRadioGroup: string;
    FEnabled: boolean;
    FColumn: integer;
    FRow: integer;
    FDisableColor: string;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property radio_group: string read FRadioGroup write FRadioGroup;
    property enabled_: boolean read FEnabled write FEnabled;
    property column: integer read FColumn write FColumn;
    property row: integer read FRow write FRow;
    property disable_color: string read FDisableColor write FDisableColor;
  end;

  TDSInfoSlot = class(TDSBaseUIControl)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
  end;

  TDSListener = class(TDSBaseUIControl)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
  end;

  TDSPopupMenu = class(TDSBaseUIControl)
  private
    FElementHeight: integer;
    FAutoSize: boolean;
    FFontType: string;
    FAlignment: TDSPopAlignment;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property element_height: integer read FElementHeight write FElementHeight;
    property auto_size: boolean read FAutoSize write FAutoSize;
    property font_type: string read FFontType write FFontType;
    property alignment: TDSPopAlignment read FAlignment write FAlignment;

  end;

  TDSListReport = class(TDSBaseUIControl)
  private
    FElementType: TDSLBElementTypes;
    FFontcolor: string;
    FFontSize: integer;
    FFontType: string;
    FSelectionColor: string;
    FCustomColumnSort: boolean;
    FDualColumnSort: boolean;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
    property element_type: TDSLBElementTypes read FElementType write FElementType;
    property font_color: string read FFontColor write FFontColor;
    property font_size: integer read FFontSize write FFontSize;
    property font_type: string read FFontType write FFontType;
    property selection_color: string read fselectioncolor write fselectioncolor;
    property custom_column_sort: boolean read FCustomColumnSort write FCustomColumnSort;
    property dual_column_sort: boolean read FDualColumnSort write FDualColumnSort;
  end;

  TDSRolloverTexture = class(TDSBaseUIControl)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
  end;

  TDSSelectionTexture = class(TDSBaseUIControl)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
  end;
  TDSSelectionBox = class(TDSBaseUIControl)
  private
  protected

  public
    constructor Create(AOwner: TComponent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure LoadComponentFromParser(Parser: TGasInterestItem); override;
    function BuildParserComponent: TGasInterestItem; override;
  published
  end;

procedure Register;
function CloneUIControl(Control: TDSBaseUIControl): TDSBaseUIControl;
function CreateUIControlFromTemplateName(TemplateName: string; AOwner: TComponent): TDSBaseUIControl;
function GetTextureFilename(texture: string): string;
function ShouldComponentizeTemplate(TemplateName: string): boolean;
var
  Texture_Cache_Root: string;
  use_component_textures: boolean;
  TexturePathXref   : TStringlist;
  UnknownTextureList: TStringlist;
  ParseWarningsList : TStringlist;

const
  EXCLUDE_CHILDREN_COMPONENT_TYPES = 'SELECTION_TEXTURE,ROLLOVER_TEXTURE,SELECTION_BOX,SLIDER_BUTTON,SLIDER';

implementation

procedure GetDirectoryFilesRecursive(DirPath: string; FilePattern: string; var
  DirList: TStringList; ExitAfterFirstFound: boolean = false);
var
  SR                : tSearchRec;
  Temp              : string;
  Status            : integer;
  TempDirList       : TStringlist;
  i                 : integer;
begin
  if ExitAfterFirstFound and (DirList.count > 0) then
    exit;
  TempDirList := TStringlist.create;
  if ExtractFilename(FilePattern) <> FilePattern then
  begin
    Dirpath := DirPath + ExtractFilePath(FilePAttern);
    FilePattern := Extractfilename(FilePattern);
  end;

  if DirPath[Length(DirPath)] = '\' then
    DirPath := copy(DirPath, 1, length(DirPath) - 1);

  //First, get the directories
  Temp := DirPath + '\*.*';
  Status := FindFirst(temp, faDirectory, SR);
  while Status = 0 do
  begin
    if ((SR.Attr and faDirectory) <> 0) then
    begin
      if (SR.name <> '.') and (SR.name <> '..') then
        TempDirList.add(DirPath + '\' + SR.name);
    end;
    Status := FindNext(SR);
  end;
  sysutils.findclose(SR);

  temp := DirPath + '\' + FilePattern;
  Status := FindFirst(temp, faAnyFile, SR);
  while Status = 0 do
  begin
    if ((SR.Attr and faDirectory) <> 0) then
    begin
      if (SR.name <> '.') and (SR.name <> '..') then
        TempDirList.add(DirPath + '\' + SR.name);
    end
    else
    begin
      DirList.Add(DirPAth + '\' + sr.name);
    end;
    if ExitAfterFirstFound and (DirList.count > 0) then
      break;
    Status := FindNext(SR);
  end;
  sysutils.findclose(SR);

  if ExitAfterFirstFound and (DirList.count > 0) then
  begin
    TempDirList.free;
    exit;
  end;
  for i := TempDirlist.Count - 1 downto 0 do
    GetDirectoryFilesRecursive(TempDirList.strings[i], FilePattern, DirList,
      exitAfterFirstFound);
  TempDirList.free;

end;

function FindFirstFileRecursive(path: string; filename: string): string;
var
  s                 : TStringlist;
begin
  s := TStringlist.create;
  GetDirectoryFilesRecursive(path, filename, s, true);
  if s.Count > 0 then
    result := s.Strings[0]
  else
    result := '';
  s.free;

end;

function GetTextureFilename(texture: string): string;
var
  dirpath           : string;
  fullpath          : string;
begin

  if TexturePathXref.IndexOfName(texture) = -1 then
  begin
    DirPath := FindFirstFileRecursive(texture_cache_root, texture + '.*');
    if dirPath <> '' then TexturePathXref.Values[texture] := ExtractFilePath(dirpath);
    dirpath := ExtractFilePath(dirpath);
  end
  else
    DirPath := TexturePathXref.Values[texture];

  FullPath := dirpath + '\' + texture + '.raw';
  if not fileExists(dirpath + '\' + texture + '.raw') then
    FullPath := dirpath + '\' + texture + '.psd';
  if FileExists(FullPath) then
    result := fullpath
  else
    result := '';
end;

function CloneUIControl(Control: TDSBaseUIControl): TDSBaseUIControl;
begin
  if Control is TDSButton then
    result := TDSButton.create(Control.Owner)
  else if Control is TDSCheckbox then
    result := TDSCheckbox.create(Control.Owner)
  else if Control is TDSDialogBox then
    result := TDSDialogBox.create(Control.Owner)
  else if Control is TDSDockBar then
    result := TDSDockBar.create(Control.Owner)
  else if Control is TDSItemSlot then
    result := TDSItemSlot.create(Control.Owner)
  else if Control is TDSListBox then
    result := TDSListBox.create(Control.Owner)
  else if Control is TDSRadioButton then
    result := TDSRadioButton.create(Control.Owner)
  else if Control is TDSStatusBar then
    result := TDSStatusBar.create(Control.Owner)
  else if Control is TDSText then
    result := TDSTExt.create(Control.Owner)
  else if Control is TDSTextBox then
    result := TDSTextBox.create(Control.Owner)
  else if control is TDSEditBox then
    result := TDSEditBox.Create(Control.owner)
  else if control is TDSChatBox then
    result := TDSChatBox.create(Control.owner)
  else if control is TDSCombobox then
    result := TDSComboBox.create(Control.Owner)
  else if control is TDSGridBox then
    result := TDSGridBox.create(Control.Owner)
  else if control is TDSSlider then
    result := TDSSlider.create(Control.owner)
  else if control is TDSTab then
    result := TDSTab.create(Control.Owner)
  else if control is TDSInfoSlot then
    result := TDSInfoSlot.create(Control.Owner)
  else if control is TDSListener then
    result := TDSListener.create(Control.Owner)
  else if control is TDSPopupmenu then
    result := TDSPopupMenu.create(Control.Owner)
  else if control is TDSListReport then
    result := TDSListReport.create(Control.Owner)
  else if control is TDSRolloverTexture then
    result := TDSRolloverTexture.create(Control.Owner)
  else if control is TDSSelectionTexture then
    result := TDSSelectionTexture.create(Control.Owner)
  else if control is TDSSelectionBox then
    result := TDSSelectionBox.create(Control.Owner)

  else if Control is TDSWindow then
    result := TDSWindow.create(Control.Owner)
  else
    result := TDSBaseUIControl.create(Control.Owner);

  Control.AssignTo(result);

end;

function CreateUIControlFromTemplateName(TemplateName: string; AOwner: TComponent): TDSBaseUIControl;
begin
  if comparetext(TemplateName, 'window') = 0 then
    result := TDSWindow.create(AOwner)
  else if comparetext(TemplateName, 'button') = 0 then
    result := TDSButton.create(AOwner)
  else if comparetext(TemplateName, 'checkbox') = 0 then
    result := TDSCheckbox.create(AOwner)
  else if comparetext(TemplateName, 'dialog_box') = 0 then
    result := TDSDialogBox.create(AOwner)
  else if comparetext(TemplateName, 'dockbar') = 0 then
    result := TDSDockbar.create(AOwner)
  else if comparetext(TemplateName, 'itemslot') = 0 then
    result := TDSItemSlot.create(AOwner)
  else if comparetext(TemplateName, 'listbox') = 0 then
    result := TDSListbox.create(AOwner)
  else if comparetext(TemplateName, 'radio_button') = 0 then
    result := TDSRadiobutton.create(AOwner)
  else if comparetext(TemplateName, 'status_bar') = 0 then
    result := TDSStatusBar.create(AOwner)
  else if comparetext(TemplateName, 'text') = 0 then
    result := TDSText.create(AOwner)
  else if comparetext(TemplateName, 'text_box') = 0 then
    result := TDSTextbox.create(AOwner)
  else if comparetext(TemplateName, 'edit_box') = 0 then
    result := TDSEditBox.create(Aowner)
  else if comparetext(TemplateName, 'chat_box') = 0 then
    result := TDSChatBox.create(Aowner)
  else if comparetext(TemplateNAme, 'combo_box') = 0 then
    result := TDSComboBox.create(Aowner)
  else if comparetext(TemplateName, 'gridbox') = 0 then
    result := TDSGridbox.create(AOwner)
  else if comparetext(Templatename, 'slider') = 0 then
    result := TDSSlider.create(AOwner)
  else if comparetext(Templatename, 'tab') = 0 then
    result := TDSTab.create(AOwner)
  else if comparetext(TemplateName, 'infoslot') = 0 then
    result := TDSInfoSlot.create(AOwner)
  else if comparetext(TemplateName, 'listener') = 0 then
    result := TDSlistener.create(AOwner)
  else if comparetext(TemplateName, 'popupmenu') = 0 then
    result := TDSPopupMenu.create(AOwner)
  else if comparetext(TemplateName, 'listreport') = 0 then
    result := TDSListReport.create(AOwner)
  else if comparetext(TemplateName, 'rollover_texture') = 0 then
    result := TDSRolloverTexture.create(AOwner)
  else if comparetext(TemplateName, 'selection_texture') = 0 then
    result := TDSSelectionTexture.create(AOwner)
  else if comparetext(TemplateName, 'selection_box') = 0 then
    result := TDSSelectionBox.create(AOwner)

    //  else result := TDSWindow.Create(aowner);
  else
  begin
    ParseWarningsList.Add('Unknown Control: ' + templatename + ' .. adding as UIWindow');
    result := TDSWindow.Create(AOwner);
  end;


end;

function TextureModeToString(Mode: TDSTextureMode): string;
begin
  case Mode of
    Clamp: result := 'Clamp';
    Tile: result := 'Tiled';
  end;
end;

function StringToTextureModeDef(Mode: string; default: TDSTextureMode): TDSTextureMode;
begin
  if CompareText(Mode, 'clamp') = 0 then
    result := Clamp
  else if CompareText(Mode, 'Tiled') = 0 then
    result := Tile
  else if trim(mode) = '' then
    result := default
  else
    ParseWarningsList.Add('Unknown texture mode value: ' + mode);


end;

function DockbarTypeToString(value: TDSDockbarType): string;
begin
  case Value of
    dockbar_screen_x_switch_y: result := 'dockbar_screen_x_switch_y';
    dockbar_screen_y: result := 'dockbar_screen_y';
  end;
end;

function StringToDockbarTypeDef(value: string; default: TDSDockbarType): TDSDockbarType;
begin
  if comparetext(value, 'dockbar_screen_x_switch_y') = 0 then
    result := dockbar_screen_x_switch_y
  else if comparetext(value, 'dockbar_screen_y') = 0 then
    result := dockbar_screen_y
  else if trim(value) = '' then
    result := default
  else
    ParseWarningsList.Add('Unkown Dockbar Type: ' + value);
end;

function JustifyToString(value: TDSJustify): string;
begin
  case Value of
    jsLeft: result := 'Left';
    jsCenter: result := 'Center';
    jsRight: result := 'Right';
  end;
end;

function StringToJustifyDef(value: string; default: TDSJustify): TDSJustify;
begin
  if comparetext(value, 'left') = 0 then
    result := jsLeft
  else if comparetext(value, 'center') = 0 then
    result := jsCenter
  else if comparetext(value, 'right') = 0 then
    result := jsRight
  else if trim(value) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown justify value: ' + value);
end;

function JustifyYToString(value: TDSJustifyY): string;
begin
  case Value of
    jsyTop: result := 'top';
    jsyCenter: result := 'center';
    jsyBottom: result := 'bottom';
  end;
end;

function StringToJustifyYDef(value: string; default: TDSJustifyY): TDSJustifyY;
begin
  if comparetext(value, 'top') = 0 then
    result := jsyTop
  else if comparetext(value, 'center') = 0 then
    result := jsyCenter
  else if comparetext(value, 'bottom') = 0 then
    result := jsyBottom
  else if trim(value) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown justify Y value: ' + value);
end;

function EdgeToString(value: TDSEdge): string;
begin
  case Value of
    edgeLeft: result := 'Left';
    edgeTop: result := 'Top';
    edgeRight: result := 'Right';
    edgeBottom: result := 'Bottom';
  end;
end;

function StringToEdgeDef(value: string; default: TDSEdge): TDSEdge;
begin
  if comparetext(value, 'left') = 0 then
    result := edgeLeft
  else if comparetext(value, 'top') = 0 then
    result := edgeTop
  else if comparetext(value, 'right') = 0 then
    result := edgeRight
  else if comparetext(value, 'bottom') = 0 then
    result := edgeBottom
  else if trim(value) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown Edge value: ' + value);

end;

function CommonTemplateToString(Template: TDSCommonTemplate): string;
begin
  case Template of
    ctNone: result := '';
    ctButton_4: result := 'button_4';
    ctX: result := 'x';
    ctX_button: result := 'x_button';
    ctCpbox: result := 'cpbox';
    ctReplay: result := 'replay';
    ctButton_stop: result := 'button_stop';
    ctJbox: result := 'jbox';
    ctCrystal: result := 'crystal';
    ctButton_5: result := 'button_5';
    ctCpbox_wide: result := 'cpbox_wide';
    ctUp: result := 'up';
    ctDown: result := 'down';
    ctTrack: result := 'track';
    ctDblup: result := 'dblup';
    ctDbldwn: result := 'dbldwn';
    ctCpbox_thin: result := 'cpbox_thin';
    ctButton_down: result := 'button_down';
    ctButton_up: result := 'button_up';
    ctCpbox_thin_dark: result := 'cpbox_thin_dark';
    ctWoodbox: result := 'woodbox';
    ctLeft: result := 'left';
    ctRight: result := 'right';
    ctGraytrim: result := 'graytrim';
    ctBrowntrim: result := 'browntrim';
    ctMin: result := 'min';
    ctMid: result := 'mid';
    ctMax: result := 'max';
    ctButton_inset: result := 'button_inset';
    ctButton_subtab: result := 'button_subtab';
  end;
end;

function StringToCommonTemplateDef(Template: string; default: TDSCommonTemplate): TDSCommonTemplate;
begin
  if comparetext(template, 'button_4') = 0 then
    result := ctButton_4
  else if comparetext(template, 'x') = 0 then
    result := ctX
  else if comparetext(template, 'x_button') = 0 then
    result := ctX_button
  else if comparetext(template, 'cpbox') = 0 then
    result := ctCpbox
  else if comparetext(template, 'replay') = 0 then
    result := ctReplay
  else if comparetext(template, 'button_stop') = 0 then
    result := ctButton_stop
  else if comparetext(template, 'jbox') = 0 then
    result := ctJbox
  else if comparetext(template, 'crystal') = 0 then
    result := ctCrystal
  else if comparetext(template, 'button_5') = 0 then
    result := ctButton_5
  else if comparetext(template, 'cpbox_wide') = 0 then
    result := ctCpbox_wide
  else if comparetext(template, 'up') = 0 then
    result := ctUp
  else if comparetext(template, 'down') = 0 then
    result := ctDown
  else if comparetext(template, 'track') = 0 then
    result := ctTrack
  else if comparetext(template, 'dblup') = 0 then
    result := ctDblup
  else if comparetext(template, 'dbldwn') = 0 then
    result := ctDbldwn
  else if comparetext(template, 'cpbox_thin') = 0 then
    result := ctCpbox_thin
  else if comparetext(template, 'button_down') = 0 then
    result := ctButton_down
  else if comparetext(template, 'button_up') = 0 then
    result := ctButton_up
  else if comparetext(template, 'cpbox_thin_dark') = 0 then
    result := ctCpbox_thin_dark
  else if comparetext(template, 'woodbox') = 0 then
    result := ctWoodbox
  else if comparetext(template, 'left') = 0 then
    result := ctLeft
  else if comparetext(template, 'right') = 0 then
    result := ctRight
  else if comparetext(template, 'graytrim') = 0 then
    result := ctGrayTrim
  else if comparetext(template, 'browntrim') = 0 then
    result := ctBrownTrim
  else if comparetext(template, 'min') = 0 then
    result := ctMin
  else if comparetext(template, 'mid') = 0 then
    result := ctMid
  else if comparetext(template, 'max') = 0 then
    result := ctMax
  else if comparetext(template, 'button_inset') = 0 then
    result := ctButton_inset
  else if comparetext(template, 'button_subtab') = 0 then
    result := ctButton_subtab
  else if comparetext(template, 'none') = 0 then
    result := ctNone
  else if trim(template) = '' then
    result := default

  else
    ParseWarningsList.Add('unknown Common Template value: ' + template);
end;

function GridTypeToString(GridType: TDSGridTypes): string;
begin
  case GridType of
    gridInventory: result := 'inventory';
  end;
end;

function StringToGridTypeDef(GridType: string; default: TDSGridTypes): TDSGridTypes;
begin
  if comparetext(GridType, 'inventory') = 0 then
    result := gridInventory
  else if trim(GridType) = '' then
    result := default

  else
    ParseWarningsList.Add('unknown Grid Type: ' + gridtype);
end;

function SlideAxisToString(value: TDSSlideAxis): string;
begin
  case value of
    slaxHorizontal: result := 'horizontal';
    slaxVertical: result := 'vertical';
  end;
end;

function StringToSlideAxisDef(value: string; default: TDSSlideAxis): TDSSlideAxis;
begin
  if comparetext(Value, 'horizontal') = 0 then
    result := slaxHorizontal
  else if comparetext(value, 'vertical') = 0 then
    result := slaxVertical
  else if trim(value) = '' then
    result := default

  else
    ParseWarningsList.Add('unknown Slider Axis Type: ' + value);
end;

function PopAlignmentToString(value: TDSPopAlignment): string;
begin
  case value of
    popDownLeft: result := 'down_left';
  end;
end;

function StringToPopAlignmentDef(value: string; default: TDSPopAlignment): TDSPopAlignment;
begin
  if comparetext(Value, 'down_left') = 0 then
    result := popDownLeft
  else if trim(value) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown popup aligment: ' + value);
end;

function LBElementTypeToString(ElementType: TDSLBElementTypes): string;
begin
  case ElementType of
    lbetText: result := 'text';
  end;
end;

function StringToLBElementTypeDef(ElementType: string; default: TDSLBElementTypes): TDSLBElementTypes;
begin
  if comparetext(ElementType, 'text') = 0 then
    result := lbetText
  else if trim(ElementType) = '' then
    result := default

  else
    ParseWarningsList.Add('unknown Listbox Element Type: ' + ElementType);
end;

function DesignerColorToString(dc: TDSDesignerColor): string;
begin
  case dc of
    dcBlack: result := 'black';
    dcMaroon: result := 'maroon';
    dcGreen: result := 'green';
    dcOlive: result := 'olive';
    dcNavy: result := 'navy';
    dcPurple: result := 'purple';
    dcTeal: result := 'teal';
    dcGray: result := 'gray';
    dcSilver: result := 'silver';
    dcRed: result := 'red';
    dcLime: result := 'lime';
    dcYellow: result := 'yellow';
    dcBlue: result := 'blue';
    dcFuchsia: result := 'fuchsia';
    dcAqua: result := 'aqua';
    dcWhite: result := 'white';
  end;

end;

function StringToDesignerColorDef(dc: string; default: TDSDesignerColor): TDSDesignerColor;
begin

  if comparetext(dc, 'black') = 0 then
    result := dcBlack
  else if comparetext(dc, 'maroon') = 0 then
    result := dcMaroon
  else if comparetext(dc, 'green') = 0 then
    result := dcGreen
  else if comparetext(dc, 'olive') = 0 then
    result := dcOlive
  else if comparetext(dc, 'navy') = 0 then
    result := dcNavy
  else if comparetext(dc, 'purple') = 0 then
    result := dcPurple
  else if comparetext(dc, 'teal') = 0 then
    result := dcTeal
  else if comparetext(dc, 'gray') = 0 then
    result := dcGray
  else if comparetext(dc, 'silver') = 0 then
    result := dcSilver
  else if comparetext(dc, 'red') = 0 then
    result := dcRed
  else if comparetext(dc, 'lime') = 0 then
    result := dcLime
  else if comparetext(dc, 'yellow') = 0 then
    result := dcYellow
  else if comparetext(dc, 'blue') = 0 then
    result := dcBlue
  else if comparetext(dc, 'fuchsia') = 0 then
    result := dcFuchsia
  else if comparetext(dc, 'aqua') = 0 then
    result := dcAqua
  else if comparetext(dc, 'white') = 0 then
    result := dcWhite
  else if trim(dc) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown color value: ' + dc);

end;

function DesignerPatternToString(dp: TBrushStyle): string;
begin

  case dp of
    bsSolid: result := 'solid';
    bsClear: result := 'clear';
    bsHorizontal: result := 'horizontal';
    bsVertical: result := 'vertical';
    bsFDiagonal: result := 'fdiagonal';
    bsBDiagonal: result := 'bdiagonal';
    bsCross: result := 'cross';
    bsDiagCross: result := 'diagcross';
  end;
end;

function StringToDesignerPatternDef(dp: string; default: TBrushStyle): TBrushStyle;
begin
  if comparetext(dp, 'solid') = 0 then
    result := bsSolid
  else if comparetext(dp, 'clear') = 0 then
    result := bsClear
  else if comparetext(dp, 'horizontal') = 0 then
    result := bsHorizontal
  else if comparetext(dp, 'vertical') = 0 then
    result := bsVertical
  else if comparetext(dp, 'fdiagonal') = 0 then
    result := bsFDiagonal
  else if comparetext(dp, 'bdiagonal') = 0 then
    result := bsBDiagonal
  else if comparetext(dp, 'cross') = 0 then
    result := bscross
  else if comparetext(dp, 'diagcross') = 0 then
    result := bsdiagcross
  else if trim(dp) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown pattern value: ' + dp);
end;

function SlotTypeToString(slot: TDSSlotTypes): string;
begin

  case slot of
    slotAmulet: result := 'amulet';
    slotArmour: result := 'armor';      //? spelling ?
    slotGauntlets: result := 'gauntlets';
    slotHelmet: result := 'helmet';
    slotMelee_Weapon: result := 'melee_weapon';
    slotRanged_Weapon: result := 'ranged_weapon';
    slotRing: result := 'ring';
    slotShield: result := 'shield';
    slotSpellbook: result := 'spellbook';
    slotPicture: result := 'picture';
    slotSpell: result := 'spell';
    slotScroll: result := 'scroll';
    slotBoots: result := 'boots';
  end;
end;

function StringToSlotTypeDef(slot: string; default: TDSSlotTypes): TDSSlotTypes;
begin
  if comparetext(slot, 'amulet') = 0 then
    result := slotAmulet
  else if comparetext(slot, 'armor') = 0 then
    result := slotArmour
  else if comparetext(slot, 'gauntlets') = 0 then
    result := slotGauntlets
  else if comparetext(slot, 'helmet') = 0 then
    result := slothelmet
  else if comparetext(slot, 'melee_weapon') = 0 then
    result := slotMelee_Weapon
  else if comparetext(slot, 'ranged_weapon') = 0 then
    result := slotRanged_Weapon
  else if comparetext(slot, 'ring') = 0 then
    result := slotRing
  else if comparetext(slot, 'shield') = 0 then
    result := slotShield
  else if comparetext(slot, 'spellbook') = 0 then
    result := slotspellbook
  else if comparetext(slot, 'picture') = 0 then
    result := slotPicture
  else if comparetext(slot, 'spell') = 0 then
    result := slotSpell
  else if comparetext(slot, 'scroll') = 0 then
    result := slotScroll
  else if comparetext(slot, 'boots') = 0 then
    result := slotBoots
  else if trim(slot) = '' then
    result := default
  else
    ParseWarningsList.Add('unknown slot type: ' + slot);
end;

procedure SetUIBrush(var Control: TDSBaseUIControl);
begin
  if Control is TDSButton then
  begin
    Control.Brush.Color := clGray;
    Control.Brush.Style := bsSolid;
    Control.designer_color := dcGray;
    Control.designer_pattern := bsSolid;
  end
  else if Control is TDSCheckbox then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsSolid;
    Control.Shape := stRoundRect;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsSolid;

  end
  else if Control is TDSDialogBox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsCross;

  end
  else if Control is TDSDockbar then
  begin
    Control.Brush.Color := clsilver;
    Control.Brush.Style := bsDiagCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsDiagCross;

  end
  else if Control is TDSItemslot then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else if Control is TDSListbox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

  end
  else if Control is TDSRadiobutton then
  begin
    Control.Brush.Color := clGray;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcGray;
    Control.designer_pattern := bsSolid;

    Control.Shape := stRoundRect;
  end
  else if Control is TDSStatusBar then
  begin
    Control.Brush.Color := clBlue;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcBlue;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 2;
  end
  else if Control is TDSText then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

  end
  else if Control is TDSTextbox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 3;
  end

  else if control is TDSEditBox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 3;
  end
  else if control is TDSChatBox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 3;
  end

  else if control is TDSCombobox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsSolid;

  end
  else if control is TDSGridBox then
  begin
    Control.Brush.Color := clWhite;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcWhite;
    Control.designer_pattern := bsCross;

    Control.pen.width := 3;
  end
  else if control is TDSSlider then
  begin
    Control.Brush.Color := clBlue;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcBlue;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 2;
  end
  else if control is TDSTab then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else if control is TDSInfoSlot then
  begin
    Control.Brush.Color := clBlue;
    Control.Brush.Style := bsSolid;

    Control.designer_color := dcBlue;
    Control.designer_pattern := bsSolid;

    Control.Pen.Width := 2;
  end
  else if Control is TDSListener then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else if Control is TDSPopupmenu then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else if Control is TDSListReport then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else if Control is TDSRolloverTexture then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsClear;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsClear;

  end
  else if Control is TDSSelectionTexture then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsClear;
    Control.designer_color := dcSilver;
    Control.designer_pattern := bsClear;

  end
  else if Control is TDSSelectionBox then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsClear;
    Control.designer_color := dcSilver;
    Control.designer_pattern := bsClear;

  end

  else if Control is TDSWindow then
  begin
    Control.Brush.Color := clSilver;
    Control.Brush.Style := bsCross;

    Control.designer_color := dcSilver;
    Control.designer_pattern := bsCross;

  end
  else
    ParseWarningsList.Add('Could not set brush for ' + control.ClassName);

end;

function ShouldComponentizeTemplate(TemplateName: string): boolean;
var
  sl                : TStringlist;
begin
  sl := nil;
  try
    sl := TStringlist.create;
    sl.commatext := EXCLUDE_CHILDREN_COMPONENT_TYPES;
    if sl.IndexOf(TemplateName) = -1 then
      result := true
    else
      result := false;
  finally
    if assigned(sl) then freeAndNil(sl);
  end;

end;
/////////////////////////////////////////////////////

procedure TDSInterface.SaveToStream(var Stream: TStream);
begin
  SetFocus;
  stream.WriteComponent(self);
end;

procedure TDSInterface.LoadFromStream(var Stream: TStream);
begin
  Clear;
  stream.ReadComponent(self);
end;

constructor TDSInterface.create(AOwner: TComponent);
begin
  inherited create(AOwner);
  intended_resolution_height := 480;
  intended_resolution_width := 640;
  Fisinterface := true;
  FInterfaceName := 'Untitled';
  FReferenceBitmap := TBitmap.create;

  FDesignerRefImageX := 0;
  FDesignerRefImageY := 0;
  FDesignerReferenceImage := '';
  PropertyXRef := TStringlist.create;

  PropertyXRef.Values['interface_name'] := '#interfacename#'; 
  PropertyXref.Values['isinterface'] := '#isinterface#';
  PropertyXref.Values['intended_resolution_width'] := '#reswidth#';
  PropertyXRef.Values['intended_resolution_height'] := '#resheight#';
  PropertyXRef.Values['disable_camera'] := '#disablecamera#';
  PropertyXRef.Values['modal'] := '#modal#';
  PropertyXRef.Values['passive_modal'] := '#passivemodal#';
  PropertyXRef.Values['centered'] := '#centered#';
  PropertyXRef.Values['designer_ref_img'] := '#designerrefimg#';
  PropertyXRef.Values['designer_ref_img_x'] := '#designerrefimgx#';
  PropertyXRef.Values['designer_ref_img_y'] := '#designerrefimgy#';

  //  fskrit_block := TStringlist.create;

end;

procedure TDSInterface.InsertPropertyTemplate(name: string; value: string = '');
var
  Parser            : TGasInterestItem;
  ParentParser      : TGasInterestItem;
begin

  if PropertyXref.IndexOfName(name) <> -1 then
  begin
    ParentParser := nil;
    try
      ParentParser := TGasInterestItem.create;
      try
        ParentParser.ParseGas(stringreplace(code, '#interfacename#', interface_name, [rfReplaceAll, rfIgnoreCase]));

        //For some reason, we are one down
        Parser := ParentParser.ChildBlocks.Items[0];

        if comparetext(name,'interface_name') = 0 then
          Parser.Name := value
        else
          Parser.SetProperty(name, value);

        code := ParentParser.BuildGas();

      except                            //we want to ignore any parsing errors here
        ;
      end;
    finally
      ParentParser.free;
    end;
  end;

end;
Procedure TDSInterface.SetRenderComponentTextures(render : boolean);
var
  i  : integer;
  ctrl : TControl;
begin
    use_component_textures := render;
    UnknownTextureList.Clear;
    for i := 0 to ComponentCount - 1 do
    begin
      ctrl := controls[i];
      if ctrl is TDSBaseUIControl then
        TDSBaseUIControl(ctrl).UpdateComponentTexture;
    end;
end;

procedure TDSInterface.Paint;
begin
  inherited;
  Canvas.Draw(designer_ref_img_x, designer_ref_img_y, FReferenceBitmap);
end;

procedure TDSInterface.ImportFromGas(Gas: string);
var
  i                 : integer;
  x                 : integer;
  Parser            : TGasInterestItem;
  Control           : TDSBaseUIControl;
  sl, sl2           : TSTringlist;
  previsspace       : boolean;
  ControlList       : TObjectList;

  InterfaceSection  : TGasInterestItem;
  CurrentChildBlock : TGasInterestItem;
begin
  Clear;

  Parser := nil;
  ControlList := TObjectList.Create(true);
  sl := nil;
  try
    Parser := TGasInterestItem.Create;
    Parser.ParseGas(Gas);

    if Parser.ChildBlocks.Count = 1 then
    begin
      InterfaceSection := parser.ChildBlocks.Items[0];

      for i := 0 to InterfaceSection.ChildBlocks.Count - 1 do
      begin
        CurrentChildBlock := InterfaceSection.ChildBlocks.Items[i];
        if CurrentChildBlock.ComponentType = gctTemplateBlock then
        begin

          if not ShouldComponentizeTemplate(CurrentChildBlock.ComponentTemplate) then continue;
          //Load up children first
          if CurrentChildBlock.ChildBlocks.Count > 0 then
          begin
            for x := 0 to CurrentChildBlock.ChildBlocks.Count - 1 do
            begin
              if CurrentChildBlock.ChildBlocks.Items[x].ComponentType = gctTemplateBlock then
              begin
                if not ShouldComponentizeTemplate(CurrentChildBlock.ChildBlocks.Items[x].ComponentTemplate) then continue;
                Control := CreateUIControlFromTemplateName(CurrentChildBlock.ChildBlocks.Items[x].ComponentTemplate, self);
                ControlList.Add(Control);
                Control.LoadComponentFromParser(CurrentChildBlock.ChildBlocks.Items[x]);
              end;
            end;
          end;

          //Now load up the parent
          Control := CreateUIControlFromTemplateName(CurrentChildBlock.ComponentTemplate, self);
          ControlList.Add(Control);
          Control.LoadComponentFromParser(CurrentChildBlock);

        end;
      end;

      for i := 0 to ControlList.Count - 1 do
        InsertControl(TControl(ControlList.Items[i]));
      ControlList.OwnsObjects := false;

      FInterfaceName := InterfaceSection.Name;
      intended_resolution_height := InterfaceSection.GetProperty('intended_resolution_height', intended_resolution_height);
      intended_resolution_width := InterfaceSection.GetProperty('intended_resolution_width', intended_resolution_width);
      isinterface := InterfaceSection.GetProperty('interface', isinterface);
      disable_camera := InterfaceSection.GetProperty('disable_camera', disable_camera);
      modal := InterfaceSection.GetProperty('modal', modal);
      passive_modal := InterfaceSection.GetProperty('passive_modal', modal);
      centered := InterfaceSection.GetProperty('centered', '');
      designer_ref_img := InterfaceSection.GetProperty('designer_ref_img', '');
      designer_ref_img_x := InterfaceSection.GetProperty('designer_ref_img_x', designer_ref_img_x);
      designer_ref_img_y := InterfaceSection.GetProperty('designer_ref_img_y', designer_ref_img_y);

      //clean up the code
      sl := TStringlist.Create;
      sl2 := TStringlist.create;
      sl.text := Parser.BuildGas(true, -1, '', false, false, 0, '', false, true);

      PrevIsSpace := false;
      for i := 0 to sl.Count - 1 do
      begin
        if (PrevIsSpace = true) and (trim(sl.Strings[i]) = '') then
        begin
          // don't add
        end
        else
        begin
          sl2.add(trimRight(sl.strings[i]));

        end;
        if trim(sl.strings[i]) = '' then
          prevIsSpace := true
        else
          prevIsSpace := false;
      end;

      FCode := sl2.text;

    end;
  finally
    if assigned(Parser) then freeAndNil(Parser);

    if assigned(sl) then freeandnil(sl);
    if assigned(sl2) then freeandnil(sl2);
    if Assigned(ControlList) then freeAndNil(ControlList);
  end;

end;

destructor TDSInterface.destroy;
begin
  Clear;
  FReferenceBitmap.Free;
  PropertyXref.free;
  inherited destroy;
end;

procedure TDSInterface.Clear;
var
  i                 : integer;
  ctrl              : TControl;
  UIControls        : TObjectlist;
begin
  UIControls := nil;
  try
    uicontrols := TObjectlist.create;
    for i := ComponentCount - 1 downto 0 do
    begin
      ctrl := controls[i];
      if ctrl is TDSBaseUIControl then
      begin
        RemoveControl(ctrl);
        UIControls.add(ctrl);
      end;
    end;

    UIControls.Clear;
  finally
    if assigned(UIControls) then freeandnil(uicontrols);
  end;
  finterfacename := '';
  fintended_resolution_height := 480;
  fintended_resolution_width := 640;
  fisinterface := true;
  fmodal := false;
  fpassivemodal := false;
  fdisable_camera := false;
  fcentered := '';
  fcode := '';
  FDesignerRefImageX := 0;
  FDesignerRefImageY := 0;
  FDesignerReferenceImage := '';

  FreeAndNil(FReferenceBitmap);
  FReferenceBitmap := TBitmap.create;

  Invalidate;

end;

function TDSInterface.GenerateInterface: string;
var
  sl                : TSTringlist;
  ChildComponentCode: TStringlist;
  i, x              : integer;
  ElementList       : TStringlist;
  InprogressComponentBuild: TStringlist;

  ParentControl     : TDSBaseUIControl;
  ChildControl      : TDSBaseUIControl;

  ParentParser      : TGasInterestItem;
  Parser            : TGasInterestItem;
  ChildParser       : TGasInterestItem;

begin
  sl := nil;
  ChildComponentCode := nil;
  ElementList := nil;
  InProgressComponentBuild := nil;
  try

    ParentParser := TGasInterestItem.create;
    ParentParser.ParseGas(stringreplace(code, '#interfacename#', interface_name, [rfReplaceAll, rfIgnoreCase]));

    //For some reason, we are one down
    Parser := ParentParser.ChildBlocks.Items[0];

    Parser.SetProperty('interface', isinterface, false);
    Parser.SetProperty('intended_resolution_width', intended_resolution_width, false);
    Parser.SetProperty('intended_resolution_height', intended_resolution_height, false);
    Parser.SetProperty('disable_camera', disable_camera, false);
    Parser.SetProperty('modal', modal, false);
    Parser.SetProperty('passive_modal', passive_modal, false);
    Parser.SetProperty('centered', centered, false);
    Parser.SetProperty('designer_ref_img', designer_ref_img, false);
    Parser.SetProperty('designer_ref_img_x', designer_ref_img_x, false);
    Parser.SetProperty('designer_ref_img_y', designer_ref_img_y, false);

    //First grab the list of controls to be saved
    ElementList := TStringList.create;
    for i := 0 to controlCount - 1 do
      if controls[i] is TDSBaseUIControl then
        ElementList.AddObject(TDSBaseUIControl(controls[i]).Name_, controls[i]);

    for i := 0 to ElementList.Count - 1 do
    begin
      ParentControl := TDSBaseUIControl(ElementList.Objects[i]);
      if trim(ParentControl.ParentElement) = '' then
      begin
        ChildParser := ParentControl.BuildParserComponent;
        Parser.InsertChildBlock(ChildParser);

        for x := 0 to ElementList.Count - 1 do
        begin
          ChildControl := TDSBaseUiControl(ElementList.Objects[x]);
          if trim(ChildControl.ParentElement) = '' then continue;
          if CompareText(ParentControl.Name_, ChildControl.ParentElement) = 0 then
            ChildParser.InsertChildBlock(ChildControl.BuildParserComponent);
        end;
      end;
    end;

    result := ParentParser.BuildGas;

  finally
    if Assigned(ParentParser) then freeAndNil(ParentParser);
    if assigned(sl) then freeAndNil(sl);
    if assigned(ChildComponentCode) then freeAndNil(ChildComponentCode);
    if Assigned(ElementList) then freeAndNil(ElementList);
    if Assigned(InProgressComponentBuild) then freeAndNil(InProgressComponentBuild);

  end;

end;

procedure TDSInterface.SetIntendedResolutionWidth(Value: integer);
begin
  fintended_resolution_width := value;
  width := value;
end;

procedure TDSInterface.SetIntendedResolutionHeight(Value: integer);
begin
  fintended_resolution_height := value;
  height := value;
end;

procedure TDSInterface.SetInterfaceName(Value: string);
begin
  FInterfaceName := value;
  if Assigned(FOnHeirarchyChange) then OnHeirarchyChange(self);

end;

procedure TDSInterface.SetDesignerReferenceImage(Value: string);
begin
  try
    FDesignerReferenceImage := value;
    if FileExists(value) then
      FReferenceBitmap.LoadFromFile(Value)
    else
    begin
      FreeAndNil(FReferenceBitmap);
      FReferenceBitmap := TBitmap.create;
    end;
  except
    FreeAndNil(FReferenceBitmap);
    FReferenceBitmap := TBitmap.create;
  end;
  invalidate;
end;

procedure TDSInterface.SetDesignerRefImgX(Value: integer);
begin
  FDesignerRefImageX := value;
  invalidate;
end;

procedure TDSInterface.SetDesignerRefImgY(Value: integer);
begin
  FDesignerRefImageY := value;
  invalidate;
end;

//////////////////////////////////////////////////////////////

constructor TDSBaseUIControl.create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FTextureBitmap := TBitmap.Create;
  SetUIBrush(self);

  FTemplateName := 'window';
  FCode := TStringlist.create;
  PropertyXRef := TStringlist.create;
  FTexture := 'None';
  FTextureMode := Clamp;
  FUVCoords := '0.0, 0.0, 1.0, 1.0';
  FAlpha := 1;
  FCommonControl := false;
  FPassthrough := false;
  FIsVisible := true;
  width := 20;
  height := 20;

  PropertyXref.Values['texture'] := '#texture#';
  PropertyXref.Values['name'] := '#name#';
  //  PropertyXref.Values['template'] := '#template#';
  PropertyXref.Values['rect'] := '#boundsrect#';
  PropertyXref.Values['draw_order'] := '#draworder#';
  PropertyXref.Values['group'] := '#group#';
  PropertyXref.Values['dock_group'] := '#dockgroup#';
  PropertyXref.Values['wrap_mode'] := '#texturemode#';
  PropertyXref.Values['uvcoords'] := '#uvcoords#';
  PropertyXref.Values['common_control'] := '#commoncontrol#';
  PropertyXref.Values['alpha'] := '#alpha#';
  PropertyXref.Values['common_template'] := '#commontemplate#';
  PropertyXref.Values['pass_through'] := '#passthrough#';
  PropertyXref.Values['visible'] := '#isvisible#';
  PropertyXref.Values['is_bottom_anchor'] := '#isbottomanchor#';
  PropertyXRef.Values['is_top_anchor'] := '#istopanchor#';
  PropertyXRef.Values['is_right_anchor'] := '#isrightanchor#';
  PropertyXref.Values['index'] := '#index#';
  PropertyXRef.Values['rollover_help'] := '#rolloverhelp#';
  propertyXRef.Values['designer_color'] := '#designercolor#';
  propertyXRef.Values['designer_pattern'] := '#designerpattern#';
  PropertyXref.Values['consumable'] := '#consumable#';
  PropertyXref.Values['type'] := '#type#';


  inherited ShowHint := true;
end;

procedure TDSBaseUIControl.InsertPropertyTemplate(name: string; value: string = '');
var
  Parser            : TGasInterestItem;
  ParentParser      : TGasInterestItem;
begin

  //first check the overrides.
  //rect and visible are overrides
  if comparetext(name, 'isvisible') = 0 then name := 'visible';

  if comparetext(name, 'left') = 0 then
    name := 'rect'
  else if comparetext(name, 'right') = 0 then
    name := 'rect'
  else if comparetext(name, 'top') = 0 then
    name := 'rect'
  else if comparetext(name, 'bottom') = 0 then
    name := 'rect';
  if (comparetext(name, 'rect') = 0) then
    value := intToStr(Left) + ',' + intToStr(top) + ',' + intToStr(right) + ',' + intToStr(Bottom);

  if PropertyXref.IndexOfName(name) <> -1 then
  begin
    try
      ParentParser := TGasInterestItem.create;
      try
        ParentParser.ParseGas(stringreplace(Code.text, '#name#', name_, [rfReplaceAll, rfIgnoreCase]));

        //For some reason, we are one down
        Parser := ParentParser.ChildBlocks.Items[0];

        if comparetext(name,'name') = 0 then
          Parser.Name := value
        else
          Parser.SetProperty(name, value);
        Code.text := ParentParser.BuildGas();
      except                            //we want to ignore any parsing errors here.
        ;
      end;
    finally
      ParentParser.free;
    end;
  end;

end;

procedure TDSBaseUIControl.LoadComponentFromParser(Parser: TGasInterestItem);
var
  bounds            : string;
  sl                : TStringlist;
begin
  Name_ := Parser.Name;
  //Parser.Name := '#name#';
  TemplateName := Parser.ComponentTemplate;
  if Parser.Parent = nil then
    ParentElement := ''
  else
  begin
    if Parser.Parent.ComponentType = gctComponentBlock then //parent can't be a component..must be a template
      ParentElement := ''
    else
      ParentElement := Parser.Parent.Name;
  end;

  Group := Parser.GetProperty('group', '');

  //parse bounds
  bounds := Parser.GetProperty('rect', '');
  if bounds <> '' then
  begin
    sl := nil;
    try
      sl := TStringlist.create;
      sl.commatext := bounds;
      if sl.count = 4 then
      begin
        left := StrToIntDef(sl.strings[0], left);
        top := StrToIntDef(sl.strings[1], top);
        right := StrToIntDef(sl.strings[2], right);
        bottom := StrToIntDef(sl.strings[3], bottom);
      end;
    finally
      sl.free;
    end;

  end;

  Draw_Order := Parser.GetProperty('draw_order', Draw_Order);
  Dock_Group := Parser.GetProperty('dock_group', '');
  Texture := Parser.GetProperty('texture', '');
  UVCoords := Parser.GetProperty('uvcoords', '');
  wrap_mode := StringToTextureModeDef(Parser.GetProperty('wrap_mode', ''), wrap_mode);
  common_control := Parser.GetProperty('common_control', Common_Control);
  common_template := StringToCommonTemplateDef(Parser.GetProperty('common_template', ''), Common_Template);
  Alpha := Parser.GetProperty('alpha', alpha);
  Pass_through := Parser.GetProperty('Pass_through', Pass_through);
  Visible_ := Parser.GetProperty('visible', Visible_);
  is_Bottom_Anchor := Parser.GetProperty('is_bottom_anchor', is_bottom_anchor);
  is_Right_Anchor := Parser.GetProperty('is_right_anchor', is_right_anchor);
  is_top_Anchor := Parser.GetProperty('is_top_anchor', is_top_anchor);
  Index := Parser.GetProperty('index', index);
  Rollover_help := Parser.GetProperty('rollover_help', '');
  designer_color := StringToDesignerColorDef(Parser.GetProperty('designer_color', ''), designer_color);
  designer_pattern := StringToDesignerPatternDef(Parser.GetProperty('designer_pattern', ''), designer_pattern);
  consumable := Parser.GetProperty('consumable', consumable);
  type_ := Parser.GetProperty('type', '');

  if Parser.Depth > 2 then
    Code.text := Parser.BuildGas()
  else
    Code.text := Parser.BuildGas(true, -1, '', false, true, 1, EXCLUDE_CHILDREN_COMPONENT_TYPES, true, true);

end;

procedure TDSBaseUIControl.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSBaseUIControl;
begin
  if Dest is TDSBaseUIControl then
  begin
    NewDest := TDSBaseUIControl(Dest);
    NewDest.Left := Left;
    NewDest.Top := Top;

    NewDest.Right := Right;
    NewDest.Bottom := Bottom;
    NewDest.Group := group;
    NewDest.Code.text := Code.Text;
    NewDest.Draw_Order := Draw_Order;
    NewDest.Dock_Group := Dock_Group;
    NewDest.Texture := Texture;
    NewDest.UVCoords := UVCoords;
    NewDest.wrap_mode := wrap_mode;
    NewDest.Common_Control := Common_Control;
    NewDest.Common_Template := Common_Template;
    NewDest.Alpha := Alpha;
    NewDest.Pass_Through := Pass_Through;
    NewDest.Visible_ := Visible_;
    NewDest.is_Bottom_Anchor := Is_Bottom_Anchor;
    NewDest.Bottom_Anchor := Bottom_Anchor;
    NewDest.is_Right_Anchor := Is_Right_Anchor;
    NewDest.Right_Anchor := Right_Anchor;
    NewDest.is_top_anchor := is_top_anchor;
    NewDest.top_anchor := top_anchor;
    NewDest.ParentElement := ParentElement;
    NewDest.Index := Index;
    NewDest.rollover_help := rollover_help;
    NewDest.designer_color := designer_color;
    NewDest.designer_pattern := designer_pattern;
    NewDest.consumable := consumable;
    NewDest.type_ := type_
  end;

end;

destructor TDSBaseUIControl.destroy;
begin
  FTextureBitmap.Free;
  FCode.free;
  PropertyXref.Free;
  if assigned(FSizer) then freeAndnil(Fsizer);
  inherited destroy;
end;

procedure TDSBaseUIControl.RepositionSizer;
var
  R                 : TRect;
begin
  if assigned(fsizer) then
  begin
    // set the size and position
    R := BoundsRect;
    InflateRect(R, 2, 2);
    FSizer.BoundsRect := R;
  end;
end;

function TDSBaseUIControl.getRight: integer;
begin
  result := left + width;
end;

function TDSBaseUIControl.getBottom: integer;
begin
  result := top + height;
end;

procedure TDSBaseUIControl.setbottom(value: integer);
begin
  setbounds(left, top, width, value - top);
  if Assigned(FOnChange) then OnChange(self);
end;

procedure TDSBaseUIControl.setright(value: integer);
begin
  setbounds(left, top, value - left, height);

  if Assigned(FOnChange) then OnChange(self);
end;

function TDSBaseUIControl.getLeft: integer;
begin
  result := inherited Left;
end;

procedure TDSBaseUIControl.SetBounds(ALeft: integer; ATop: integer; AWidth: integer; AHeight: integer);
begin
  if (ALeft <> left) or (ATop <> top) or (AWidth <> width) or (AHeight <> height) then
  begin
    inherited;
    RepositionSizer;

    if Assigned(FOnChange) then OnChange(self);
    if assigned(FOnBoundsChange) then OnBoundsChange(self);
  end
  else
    inherited;

end;

procedure TDSBaseUIControl.setLeft(value: integer);
begin
  inherited Left := value;
  RepositionSizer;
  if Assigned(FOnChange) then OnChange(self);

end;

function TDSBaseUIControl.getTop: integer;
begin
  result := inherited Top;
end;

procedure TDSBaseUIControl.setTop(value: integer);
begin
  inherited Top := value;
  RepositionSizer;
  if Assigned(FOnChange) then OnChange(self);
end;

function TDSBaseUIControl.getWidth: integer;
begin
  result := inherited Width;
end;

procedure TDSBaseUIControl.setWidth(value: integer);
begin
  inherited Width := value;
  RepositionSizer;
  if Assigned(FOnChange) then OnChange(self);
end;

function TDSBaseUIControl.getHeight: integer;
begin
  result := inherited Height;
end;

procedure TDSBaseUIControl.setHeight(value: integer);
begin
  inherited Height := value;
  RepositionSizer;
  if Assigned(FOnChange) then OnChange(self);
end;

procedure TDSBaseUIControl.setElementNAme(value: string);
var
  i                 : integer;
  MyParent          : TDSInterface;
  ctrl              : TDSBaseUIControl;
  PrevName          : string;
begin
  PrevName := FElementName;

  
  FElementName := value;



  if Assigned(FOnHeirarchyChange) then OnHeirarchyChange(self);
  if Assigned(FOnChange) then OnChange(self);
  invalidate;

  if Parent is TDSInterface then
  begin
    MYParent := TDSInterface(Parent);
    for i := 0 to MyParent.ControlCount - 1 do
    begin
      if MyParent.Controls[i] is TDSBaseUIControl then
      begin
        ctrl := TDSBaseUIControl(MyParent.Controls[i]);
        if comparetext(ctrl.ParentElement, PrevName) = 0 then
          ctrl.ParentElement := value;
      end;
    end;
  end;

end;

procedure TDSBaseUIControl.SetDesignerPattern(value: TBrushStyle);
begin

  FDesignerPattern := value;
  Brush.Style := value;
  invalidate;

end;

procedure TDSBaseUIControl.UpdateComponentTexture;
var
  fullpath          : string;
begin

  FTextureLoaded := false;
  if use_component_textures = false then
  begin
    if not FTextureBitmap.Empty then
    begin
      FreeAndNil(FTextureBitmap);
      FTextureBitmap := TBitmap.create;
    end;
  end;

  if Use_Component_Textures then
  begin

    FullPath := GetTextureFilename(texture);
    if FileExists(FullPath) then
    begin
      try
        FreeAndNil(FTextureBitmap);

        if UpperCase(ExtractFileExt(fullpath)) = '.RAW' then
          FTextureBitmap := TDSRawGraphic.Create
        else if Uppercase(ExtractFileExt(fullpath)) = '.PSD' then
          FTextureBitmap := TPSDGraphic.Create
        else
          raise Exception.Create('Unkown texture format');
        FTextureBitmap.LoadFromFile(FullPath);
        FTextureLoaded := true;
      except
        if assigned(FTextureBitmap) then FreeAndNil(FTextureBitmap);
        FTextureBitmap := TBitmap.Create;

      end;
    end
    else
    begin
      if (trim(uppercase(texture)) <> 'NONE') and (trim(texture) <> '') then
        UnknownTextureList.Add(texture);
    end;
  end;
  Invalidate;

end;

procedure TDSBaseUIControl.SetTexture(Value: string);
begin
  FTexture := Value;

  UpdateComponentTexture;

end;

procedure TDSBaseUIControl.SetUVCoords(Value: string);
var
  sl                : TSTringList;
begin
  FUVCoords := Value;
  FUVLeft := 0;
  FUVTop := 0;
  FUVRight := 1;
  FUVBottom := 1;

  if trim(value) <> '' then
  begin

    sl := nil;
    try
      sl := TStringlist.create;
      sl.commatext := value;
      if sl.count = 4 then
      begin
        FUVleft := StrToFloatDef(sl.strings[0], FUVLeft);
        FUVtop := StrToFloatDef(sl.strings[1], FUVtop);
        FUVright := StrToFloatDef(sl.strings[2], FUVright);
        FUVbottom := StrToFloatDef(sl.strings[3], FUVbottom);
      end;
    finally
      sl.free;
    end;
  end;

  invalidate;
end;

procedure TDSBaseUIControl.SetDesignerColor(value: TDSDesignerColor);
begin
  FDesignerColor := value;
  case Value of
    dcBlack: brush.color := clBlack;
    dcMaroon: brush.color := clMaroon;
    dcGreen: brush.color := clGreen;
    dcOlive: brush.color := clOlive;
    dcNavy: brush.color := clNavy;
    dcPurple: brush.color := clPurple;
    dcTeal: brush.color := clTeal;
    dcGray: brush.color := clGray;
    dcSilver: brush.color := clSilver;
    dcRed: brush.color := clRed;
    dcLime: brush.color := clLime;
    dcYellow: brush.color := clYellow;
    dcBlue: brush.color := clBlue;
    dcFuchsia: brush.color := clFuchsia;
    dcAqua: brush.color := clAqua;
    dcWhite: brush.color := clWhite;
  end;

end;

procedure TDSBaseUIControl.SetParentElement(value: string);
begin
  FParentElement := value;
  if assigned(FOnHeirarchyChange) then OnHeirarchyChange(self);
  if Assigned(FOnChange) then OnChange(self);
  invalidate;
end;

procedure TDSBaseUIControl.SetSelected(value: boolean);
begin
  FSelected := value;

  if value = true then
  begin
    if FSizer = nil then
    begin

      fsizer := TjanStickSizer.Create(self.Parent, TControl(self));
      fsizer.parent := self.Parent;
      //      ComponentList := TComponentList.Create(false);

      //      for i := ComponentIndex + 1 to Parent.ComponentCount -1 do
      //       if parent.Components[i] <> fsizer then ComponentList.Add(Parent.Components[i]);
      //
      //      for i := 0 to ComponentList.count -1 do
      //        TControl(ComponentList.Items[i]).BringToFront;

      //      FreeAndNil(ComponentList);
            //fsizer.BringToFront;
            //fsizer.SetFocus;
            //if Assigned(FOnSelected) then OnSelect(Self);
    end;

  end
  else
  begin
    if FSizer <> nil then freeAndNil(FSizer);
    invalidate;
  end;

end;

procedure TDSBaseUIControl.SetVisible(value: boolean);
begin
  inherited Visible := value;
  if Assigned(FOnHeirarchyChange) then OnHeirarchyChange(self);
  if Assigned(FOnChange) then OnChange(self);
end;

function TDSBaseUIControl.GetVisible: boolean;
begin
  result := inherited Visible;
end;

function TDSBaseUIControl.BuildParserComponent: TGasInterestItem;
var
  ParentParser      : TGasInterestItem;
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := nil;
  ParentParser := nil;
  try

    ParentParser := TGasInterestItem.create;
    ParentParser.ParseGas(stringreplace(FCode.text, '#name#', Name_, [rfReplaceAll, rfIgnoreCase]));

    //For some reason, we are one down
    ComponentParser := ParentParser.ChildBlocks.Items[0];
    ParentParser.ChildBlocks.OwnsObjects := false;
    ParentParser.ChildBlocks.Remove(ComponentParser);
    ParentParser.ChildBlocks.OwnsObjects := true;
    FreeAndNil(ParentParser);
    ComponentParser.Parent := nil;

    ComponentParser.Name := Name_;

    ComponentParser.SetProperty('rect', intToStr(Left) + ',' + intToStr(top) + ',' + intToStr(right) + ',' + intToStr(Bottom), false);
    ComponentParser.SetProperty('draw_order', draw_order, false);
    ComponentParser.SetProperty('group', group, false);
    ComponentParser.SetProperty('dock_group', dock_group, false);
    ComponentParser.SetProperty('wrap_mode', TextureModeToString(wrap_mode), false);
    ComponentParser.SetProperty('texture', texture, false);
    ComponentParser.SetProperty('uvcoords', uvcoords, false);
    ComponentParser.SetProperty('common_control', common_control, false);
    ComponentParser.SetProperty('alpha', alpha, false);
    ComponentParser.SetProperty('common_template', CommonTemplateToString(common_template), false);
    ComponentParser.SetProperty('pass_through', pass_through, false);
    ComponentParser.SetProperty('visible', visible_, false);
    ComponentParser.SetProperty('is_bottom_anchor', is_bottom_anchor, false);
    ComponentParser.SetProperty('is_top_anchor', is_top_anchor, false);
    ComponentParser.SetProperty('is_right_anchor', is_right_anchor, false);
    ComponentParser.SetProperty('index', index, false);
    ComponentParser.SetProperty('rollover_help', rollover_help, false);
    ComponentParser.SetProperty('designer_color', DesignerColorToString(designer_color), false);
    ComponentParser.SetProperty('designer_pattern', DesignerPatternToString(designer_pattern), false);

    ComponentParser.SetProperty('consumable', consumable, false);
    ComponentParser.SetProperty('type', type_, false);

    if is_Bottom_Anchor then
    begin
      if Parent is TDSInterface then
      begin
        bottom_anchor := TDSInterface(Parent).fintended_resolution_height - top;
      end;
      ComponentParser.SetProperty('bottom_anchor', bottom_anchor, true);
    end
    else
      ComponentParser.SetProperty('bottom_anchor', 0, false);

    if is_Right_Anchor then
    begin
      if Parent is TDSInterface then
      begin
        right_anchor := TDSInterface(Parent).fintended_resolution_Width - left;
      end;
      ComponentParser.SetProperty('right_anchor', right_anchor, true);
    end
    else
      ComponentParser.SetProperty('right_anchor', right_anchor, false);

    if is_Top_Anchor then
    begin
      if Parent is TDSInterface then
      begin
        top_anchor := top;
      end;
      ComponentParser.SetProperty('top_anchor', top_anchor, true);
    end
    else
      ComponentParser.SetProperty('top_anchor', 0, false);

    result := ComponentParser;
  except
    if assigned(ComponentParser) then ComponentParser.Free;
    raise;
  end;
end;

procedure TDSBaseUIControl.SetCode(Value: TStringlist);
begin
  fcode.Assign(Value);
  if Assigned(FOnChange) then OnChange(self);
end;

procedure TDSBaseUIControl.SetGroup(Value: string);
begin
  FGroup := value;

  if Assigned(FOnHeirarchyChange) then OnHeirarchyChange(self);
  if Assigned(FOnChange) then OnChange(self);
end;

procedure TDSBaseUIControl.CMFontChanged(var Message: TMessage);
begin
  invalidate;
end;

procedure TDSBaseUIControl.CMInvalidate(var Message: TMessage);
begin
  invalidate;
end;

procedure TDSBaseUIControl.CMMouseEnter(var Msg: TMessage);
begin
  // cursor:=crhandpoint;
end;

procedure TDSBaseUIControl.CMMouseLeave(var Msg: TMessage);
begin
  inherited cursor := crdefault;
end;

procedure TDSBaseUIControl.CMTextChanged(var Message: TMessage);
begin
  invalidate;
end;

procedure TDSBaseUIControl.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;

  if (button = mbleft) and (ptinrect(rect(0, 0, width, height), point(x, y))) then
  begin
    //      if not selected then
    //        Selected := true;
    Selected := not Selected;
    if Assigned(FOnSelected) then FOnSelected(self, button, shift, x, y);
    //    if ssctrl in shift then
    //    begin
    //      Selected := not Selected
    //    end
    //    else
  end
    //  else if (button=mbleft) and (ptinrect(rect(0,h3,20,2*h3),point(x,y))) then
    //  begin
    //    caption:=captiondialog(caption);
    //  end
    //  else if (button=mbleft) and (ptinrect(rect(0,2*h3,20,clientheight),point(x,y))) then
    //  begin
    //    with TColordialog.Create(self) do begin
    //      color:=FstickColor;
    //      if execute then StickColor:=color;
    //      free;
    //    end;
    //  end
  else if assigned(onMouseDown) then
    onMouseDown(self, button, shift, x, y);

end;

procedure TDSBaseUIControl.Paint;
var
  R                 : TRect;
  s                 : string;
  holdstyle         : TBrushStyle;

  Dest              : TRect;
  Source            : TRect;
  i : integer;
begin
  inherited;

  holdstyle := canvas.brush.style;
  canvas.brush.style := bsclear;

  if FTextureLoaded then
  begin

    Dest.Left := 0;
    Dest.Top := 0;
    Dest.Right := width;
    Dest.Bottom := height;

    Source.Left := round(FUVLeft * FTextureBitmap.Width);

    //ok.. for some reason the bitmaps are flipped
    Source.Bottom := FTextureBitmap.Height - round(FUVTop * FTextureBitmap.Height);
    Source.Top := FTextureBitmap.Height - round(FUVBottom * FTextureBitmap.Height);

    Source.Right := round(FUVRight * FTextureBitmap.Width);

    Canvas.BrushCopy(Dest, FTextureBitmap, Source, clWhite);
  end;

  R := Rect(15, 0, width, height);
  s := Name_;

  DrawText(canvas.handle, pchar(s), -1, R, DT_WORDBREAK);
  canvas.brush.style := holdstyle;
end;

//////////////////////////////////////////////////////
//      TDSWINDOW
/////////////////////////////////////////////////////

constructor TDSWindow.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  FTemplateName := 'window';

  PropertyXRef.Values['disable_color'] := '#disablecolor#';
  PropertyXRef.Values['leftright_button'] := '#leftrightbutton#';
  PropertyXRef.Values['center_x'] := '#centerx#';
  PropertyXref.Values['topmost'] := '#topmost#';
  PropertyXRef.Values['normalize_resize'] := '#normalizeresize#';
  PropertyXRef.Values['background_color'] := '#backgroundcolor#';
  PropertyXRef.Values['background_fill'] := '#backgroundfill#';
  PropertyXRef.Values['allow_user_press'] := '#allowuserpress#';
  PropertyXRef.Values['tag'] := '#tag#';
  PropertyXRef.Values['border_color'] := '#bordercolor#';
  PropertyXRef.Values['has_border'] := '#hasborder#';
  PropertyXRef.Values['help_text'] := '#helptext#';
  PropertyXRef.Values['help_tool_tip'] := '#helptooltip#';
  PropertyXRef.Values['help_text_box'] := '#helptextbox#';

end;

function TDSWindow.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('leftright_button', leftright_button, false);
  ComponentParser.SetProperty('center_x', center_x, false);
  ComponentParser.SetProperty('topmost', topmost, false);
  ComponentParser.SetProperty('normalize_resize', normalize_resize, false);
  ComponentParser.SetProperty('background_color', background_color, false);
  ComponentParser.SetProperty('background_fill', background_fill, false);
  ComponentParser.SetProperty('allow_user_press', allow_user_press, false);
  ComponentParser.SetProperty('tag', tag_, false);
  ComponentParser.SetProperty('border_color', border_color, false);
  ComponentParser.SetProperty('has_border', has_border, false);
  ComponentParser.SetProperty('help_text', help_text, false);
  ComponentParser.SetProperty('help_tool_tip', help_tool_tip, false);
  ComponentParser.SetProperty('help_text_box', help_text_box, false);
  result := ComponentParser;
end;

procedure TDSWindow.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSWindow;
begin
  inherited AssignTo(Dest);
  if Dest is TDSWindow then
  begin
    NewDest := TDSWindow(Dest);
    NewDest.LeftRight_Button := LeftRight_Button;
    NewDest.disable_color := disable_color;
    NewDest.Center_X := center_x;
    NewDest.topmost := topmost;
    newDest.Normalize_Resize := normalize_resize;
    NewDest.background_color := background_color;
    NewDest.background_fill := background_fill;
    NewDest.allow_user_press := allow_user_press;
    NewDest.Tag_ := tag_;
    NewDest.border_color := border_color;
    NewDest.has_border := has_border;
    NewDest.help_text := help_text;
    NewDest.help_tool_tip := help_tool_tip;
    NewDest.help_text_box := help_text_box;

  end;
end;

procedure TDSWindow.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  leftright_button := Parser.GetProperty('leftright_button', leftright_button);
  disable_color := Parser.GetProperty('disable_color', disable_color);
  center_x := Parser.GetProperty('center_x', center_x);
  topmost := Parser.GetProperty('topmost', topmost);
  normalize_resize := Parser.GetProperty('normalize_resize', normalize_resize);
  background_color := Parser.GetProperty('background_color', background_color);
  background_fill := Parser.GetProperty('background_fill', background_fill);
  allow_user_press := Parser.GetProperty('allow_user_press', allow_user_press);
  tag_ := Parser.GetProperty('tag', tag_);
  border_color := Parser.GetProperty('border_color', border_color);
  has_border := Parser.GetProperty('has_border', has_border);
  help_text := Parser.GetProperty('help_text', help_text);
  help_tool_tip := Parser.GetProperty('help_tool_tip', help_tool_tip);
  help_text_box := Parser.GetProperty('help_text_box', help_text_box);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSBUTTON
/////////////////////////////////////////////////////

constructor TDSButton.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'button';

  PropertyXRef.Values['repeat_rate'] := '#repeatrate#';
  PropertyXref.Values['repeater'] := '#repeater#';
  PropertyXref.Values['ignore_items'] := '#ignoreitems#';
  PropertyXRef.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['text'] := '#text#';

end;

destructor TDSButton.Destroy;
begin
  inherited destroy;
end;

function TDSButton.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('repeat_rate', repeat_rate, false);
  ComponentParser.SetProperty('repeater', repeater, false);
  ComponentParser.SetProperty('ignore_items', ignore_items, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('text', text, false);

  result := ComponentParser;
end;

procedure TDSButton.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSButton;
begin
  inherited AssignTo(Dest);
  if Dest is TDSButton then
  begin
    NewDest := TDSButton(Dest);
    NewDest.repeat_rate := repeat_rate;
    NewDest.repeater := repeater;
    NewDest.ignore_items := ignore_items;
    NewDest.font_type := font_type;
    NewDest.text := text;

  end;
end;

procedure TDSButton.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  repeater := Parser.GetProperty('repeater', repeater);
  repeat_rate := Parser.GetProperty('repeat_rate', repeat_rate);
  ignore_items := Parser.GetProperty('ignore_items', ignore_items);
  Font_Type := Parser.GetProperty('font_type', '');
  Text := Parser.GetProperty('text', '');

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSRadioButton
/////////////////////////////////////////////////////

constructor TDSRadioButton.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'radio_button';
  PropertyXRef.Values['radio_group'] := '#radiogroup#';
  PropertyXRef.Values['popup'] := '#popup#';
  PropertyXRef.Values['click_delay'] := '#clickdelay#';
  PropertyXRef.Values['send_uncheck_to_self'] := '#sendunchecktoself#';
  PropertyXRef.Values['ignore_items'] := '#ignoreitems#';

end;

procedure TDSRadioButton.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSRadioButton;
begin
  inherited AssignTo(Dest);
  if Dest is TDSRadioButton then
  begin
    NewDest := TDSRadioButton(Dest);
    NewDest.Radio_Group := Radio_Group;
    NewDest.popup := popup;
    NewDest.click_delay := click_delay;
    NewDest.send_uncheck_to_self := send_uncheck_to_self;
    NewDest.ignore_items := ignore_items;
  end;
end;

procedure TDSRadioButton.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  radio_group := Parser.GetProperty('radio_group', radio_group);
  popup := Parser.GetProperty('popup', popup);
  click_delay := Parser.GetProperty('click_delay', click_delay);
  send_uncheck_to_self := Parser.GetProperty('send_uncheck_to_self', send_uncheck_to_self);
  ignore_items := Parser.GetProperty('ignore_items', ignore_items);

  inherited LoadComponentFromParser(Parser);

end;

function TDSRadioButton.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('radio_group', radio_group, false);
  ComponentParser.SetProperty('popup', popup, false);
  ComponentParser.SetProperty('click_delay', click_delay, false);
  ComponentParser.SetProperty('send_uncheck_to_self', send_uncheck_to_self, false);
  ComponentParser.SetProperty('ignore_items', ignore_items, false);
  result := ComponentParser;
end;

//////////////////////////////////////////////////////
//      TDSCheckbox
/////////////////////////////////////////////////////

constructor TDSCheckbox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'checkbox';
  PropertyXRef.Values['popup'] := '#popup#';

end;

function TDSCheckbox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('popup', popup, false);
  result := ComponentParser;
end;

procedure TDSCheckbox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSCheckbox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSCheckbox then
  begin
    NewDest := TDSCheckbox(Dest);
    NewDest.Popup := popup;
  end;
end;

procedure TDSCheckBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  popup := Parser.GetProperty('popup', popup);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSListBox
/////////////////////////////////////////////////////

constructor TDSListBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'listbox';
  PropertyXRef.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['text_color'] := '#textcolor#';
  PropertyXRef.Values['text'] := '#text#';
  PropertyXRef.Values['rollover_select'] := '#rolloverselect#';
  PropertyXRef.Values['permanent_focus'] := '#permanentfocus#';
  PropertyXRef.Values['left_indent'] := '#leftindent#';
  PropertyXRef.Values['element_type'] := '#elementtype#';
  PropertyXRef.Values['font_color'] := '#fontcolor#';
  PropertyXRef.Values['selection_color'] := '#selectioncolor#';

end;

function TDSListBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('text_color', text_color, false);
  ComponentParser.SetProperty('text', text, false);
  ComponentParser.SetProperty('rollover_select', rollover_select, false);
  ComponentParser.SetProperty('permanent_focus', permanent_focus, false);
  ComponentParser.SetProperty('left_indent', left_indent, false);
  ComponentParser.SetProperty('element_type', LBElementTypeToString(element_type), false);
  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('selection_color', selection_color, false);

  result := ComponentParser;
end;

procedure TDSListBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSListBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSListBox then
  begin
    NewDest := TDSListBox(Dest);
    NewDest.font_type := font_type;
    NewDest.text_color := text_color;
    NewDest.Text := text;
    NewDest.rollover_select := rollover_select;
    NewDest.permanent_focus := permanent_focus;
    NewDest.left_indent := left_indent;
    NewDest.element_type := element_type;
    NewDest.Font_color := Font_color;
    NewDest.selection_color := selection_color;

  end;
end;

procedure TDSListBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  font_type := Parser.GetProperty('font_type', font_type);
  text_color := Parser.GetProperty('text_color', text_color);
  text := Parser.GetProperty('text', text);
  rollover_select := Parser.GetProperty('rollover_select', rollover_select);
  permanent_focus := Parser.GetProperty('permanent_focus', permanent_focus);
  left_indent := Parser.GetProperty('left_indent', left_indent);
  element_type := StringToLBElementTypeDef(Parser.GetProperty('element_type', ''), element_type);
  font_color := Parser.GetProperty('font_color', font_color);
  selection_color := Parser.GetProperty('selection_color', selection_color);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSDialogBox
/////////////////////////////////////////////////////

constructor TDSDialogBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'dialog_box';
end;

function TDSDialogBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  result := ComponentParser;
end;

procedure TDSDialogBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSDialogBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSDialogBox then
  begin
    NewDest := TDSDialogBox(Dest);

  end;
end;

procedure TDSDialogBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSTextBox
/////////////////////////////////////////////////////

constructor TDSTextBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'text_box';
  PropertyXRef.Values['font_color'] := '#fontcolor#';
  PropertyXRef.Values['font_size'] := '#fontsize#';
  PropertyXRef.Values['center_height'] := '#centerheight#';
  PropertyXRef.Values['fixed_location'] := '#fixedlocation#';
  PropertyXRef.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['has_slider'] := '#hasslider#';
  PropertyXRef.Values['justify'] := '#justify#';
  PropertyXRef.Values['max_height'] := '#maxheight#';
  PropertyXRef.Values['max_width'] := '#maxwidth#';
  PropertyXRef.Values['scroll_rate'] := '#scrollrate#';
  PropertyXRef.Values['stretch_x'] := '#stretchx#';

  PropertyXRef.Values['text'] := '#text#';
  PropertyXRef.Values['show_first_line'] := '#showfirstline#';
  PropertyXRef.Values['loc_font'] := '#locfont#';
  PropertyXRef.Values['text_color'] := '#textcolor#';
  PropertyXRef.Values['font_1024x768'] := '#font1024x768#';
  PropertyXRef.Values['border_padding'] := '#borderpadding#';
  PropertyXRef.Values['justify_y'] := '#justifyy#';
  PropertyXRef.Values['truncate'] := '#truncate#';

end;

function TDSTextBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('font_size', font_size, false);
  ComponentParser.SetProperty('center_height', center_height, false);
  ComponentParser.SetProperty('fixed_location', fixed_location, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('has_slider', has_slider, false);
  ComponentParser.SetProperty('justify', JustifyToString(justify), false);
  ComponentParser.SetProperty('max_height', max_height, false);
  ComponentParser.SetProperty('max_width', max_width, false);
  ComponentParser.SetProperty('scroll_rate', scroll_rate, false);
  ComponentParser.SetProperty('stretch_x', stretch_x, false);
  ComponentParser.SetProperty('text', text, false);
  ComponentParser.SetProperty('show_first_line', show_first_line, false);
  ComponentParser.SetProperty('loc_font', loc_font, false);
  ComponentParser.SetProperty('text_color', text_color, false);
  ComponentParser.SetProperty('font_1024x768', font_1024x768, false);
  ComponentParser.SetProperty('border_padding', border_padding, false);
  ComponentParser.SetProperty('justify_y', JustifyYToString(justify_y), false);
  ComponentParser.SetProperty('truncate', truncate, false);
  result := ComponentParser;
end;

procedure TDSTextBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSTextBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSTextBox then
  begin
    NewDest := TDSTextBox(Dest);
    NewDest.Center_Height := Center_Height;
    NewDest.Fixed_Location := Fixed_Location;
    NewDest.Font_color := Font_color;
    NEwDest.Font_Size := Font_Size;
    NewDest.Font_Type := Font_Type;
    NewDest.Has_Slider := Has_Slider;
    NewDest.Justify := Justify;
    NewDest.Max_Height := Max_Height;
    NewDest.Max_Width := Max_Width;
    NewDest.Scroll_Rate := Scroll_Rate;
    NewDest.Stretch_X := Stretch_X;
    NewDest.Text := Text;
    Newdest.show_first_line := show_first_line;
    NewDest.loc_font := loc_font;
    NewDest.text_color := text_color;
    NewDest.font_1024x768 := font_1024x768;
    NewDest.border_padding := border_padding;
    NewDest.justify_y := justify_y;
    NewDest.truncate := Truncate;
  end;
end;

procedure TDSTextBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  font_color := Parser.GetProperty('font_color', font_color);
  font_size := Parser.GetProperty('font_size', font_size);
  center_height := Parser.GetProperty('center_height', center_height);
  fixed_location := Parser.GetProperty('fixed_location', fixed_location);
  font_type := Parser.GetProperty('font_type', font_type);
  has_slider := Parser.GetProperty('has_slider', has_slider);
  justify := StringToJustifyDef(Parser.GetProperty('justify', ''), justify);
  max_height := Parser.GetProperty('max_height', max_height);
  max_width := Parser.GetProperty('max_width', max_width);
  scroll_rate := Parser.GetProperty('scroll_rate', scroll_rate);
  stretch_x := Parser.GetProperty('stretch_x', stretch_x);

  text := Parser.GetProperty('text', text);
  show_first_line := Parser.GetProperty('show_first_line', show_first_line);
  loc_font := Parser.GetProperty('loc_font', loc_font);
  text_color := Parser.GetProperty('text_color', text_color);
  font_1024x768 := Parser.GetProperty('font_1024x768', font_1024x768);
  border_padding := Parser.GetProperty('border_padding', border_padding);
  justify_y := StringToJustifyYDef(Parser.GetProperty('justify_y', ''), justify_y);
  truncate := Parser.GetProperty('truncate', truncate);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSText
/////////////////////////////////////////////////////

constructor TDSText.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'text';
  PropertyXRef.Values['font_color'] := '#fontcolor#';
  PropertyXRef.Values['font_size'] := '#fontsize#';
  PropertyXRef.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['justify'] := '#justify#';
  PropertyXRef.Values['text'] := '#text#';
  PropertyXRef.Values['truncate'] := '#truncate#';
  PropertyXRef.Values['normalize_resize'] := '#normalizeresize#';
  PropertyXRef.Values['auto_size'] := '#autosize#';
  PropertyXRef.Values['font_1024x768'] := '#font1024x768#';
  PropertyXRef.Values['text_color'] := '#textcolor#';
end;

function TDSText.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('font_size', font_size, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('justify', JustifyToString(justify), false);
  ComponentParser.SetProperty('text', text, false);
  ComponentParser.SetProperty('truncate', truncate, false);
  ComponentParser.SetProperty('normalize_resize', normalize_resize, false);
  ComponentParser.SetProperty('auto_size', auto_size, false);
  ComponentParser.SetProperty('font_1024x768', font_1024x768, false);
  ComponentParser.SetProperty('text_color', text_color, false);

  result := ComponentParser;
end;

procedure TDSText.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSText;
begin
  inherited AssignTo(Dest);
  if Dest is TDSText then
  begin
    NewDest := TDSText(Dest);

    NewDest.Font_color := Font_color;
    NEwDest.Font_Size := Font_Size;
    NewDest.Font_Type := Font_Type;
    NewDest.Justify := Justify;
    NewDest.Text := Text;
    NewDest.truncate := Truncate;
    newDest.Normalize_Resize := normalize_resize;
    NewDest.auto_size := auto_size;
    NewDest.font_1024x768 := font_1024x768;
    NewDest.text_color := text_color;

  end;
end;

procedure TDSText.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  font_color := Parser.GetProperty('font_color', font_color);
  font_size := Parser.GetProperty('font_size', font_size);
  font_type := Parser.GetProperty('font_type', font_type);
  justify := StringToJustifyDef(Parser.GetProperty('justify', ''), justify);
  text := Parser.GetProperty('text', text);
  truncate := Parser.GetProperty('truncate', truncate);
  normalize_resize := Parser.GetProperty('normalize_resize', normalize_resize);
  auto_size := Parser.GetProperty('auto_size', auto_size);
  font_1024x768 := Parser.GetProperty('font_1024x768', font_1024x768);
  text_color := Parser.GetProperty('text_color', text_color);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSItemSlot
/////////////////////////////////////////////////////

constructor TDSItemSlot.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'itemslot';
  accept_input := true;
  PropertyXRef.Values['activate_color'] := '#activatecolor#';
  PropertyXRef.Values['border_color'] := '#bordercolor#';
  PropertyXRef.Values['is_equip_slot'] := '#isequipslot#';
  PropertyXRef.Values['slot_type'] := '#slottype#';
  PropertyXRef.Values['accept_input'] := '#acceptinput#';
  PropertyXRef.Values['allow_input'] := '#allowinput#';

end;

function TDSItemSlot.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('activate_color', activate_color, false);
  ComponentParser.SetProperty('border_color', border_color, false);
  ComponentParser.SetProperty('is_equip_slot', is_equip_slot, false);
  ComponentParser.SetProperty('slot_type', SlotTypeToString(slot_type), false);
  ComponentParser.SetProperty('accept_input', accept_input, false);
  ComponentParser.SetProperty('allow_input', allow_input, false);

  result := ComponentParser;
end;

procedure TDSItemSlot.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSItemSlot;
begin
  inherited AssignTo(Dest);
  if Dest is TDSItemSlot then
  begin
    NewDest := TDSItemSlot(Dest);
    NewDest.activate_color := activate_color;
    NewDest.border_color := border_color;
    NewDest.is_equip_slot := is_equip_slot;
    NewDest.slot_type := slot_type;
    NewDest.accept_input := accept_input;
    NewDest.allow_input := allow_input;
  end;

end;

procedure TDSItemSlot.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  activate_color := Parser.GetProperty('activate_color', activate_color);
  border_color := Parser.GetProperty('border_color', border_color);
  is_equip_slot := Parser.GetProperty('is_equip_slot', is_equip_slot);
  slot_type := StringToSlotTypeDef(Parser.GetProperty('slot_type', ''), slot_type);
  accept_input := Parser.GetProperty('accept_input', accept_input);
  allow_input := Parser.GetProperty('allow_input', allow_input);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSStatusBar
/////////////////////////////////////////////////////

constructor TDSStatusBar.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'status_bar';
  PropertyXRef.Values['dynamic_edge'] := '#dynamicedge#';
  PropertyXRef.Values['draw_advance'] := '#drawadvance#';
  PropertyXRef.Values['advance_color'] := '#advancecolor#';
  PropertyXRef.Values['advance_duration'] := '#advanceduration#';
  PropertyXref.Values['topmost'] := '#topmost#';
  PropertyXref.Values['uv_map'] := '#uvmap#';
  PropertyXref.Values['draw_outline'] := '#drawoutline#';
  PropertyXref.Values['border_color'] := '#bordercolor#';

end;

function TDSStatusBar.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('dynamic_edge', EdgeToString(dynamic_edge), false);
  ComponentParser.SetProperty('draw_advance', draw_advance, false);
  ComponentParser.SetProperty('advance_color', advance_color, false);
  ComponentParser.SetProperty('advance_duration', advance_duration, false);
  ComponentParser.SetProperty('topmost', topmost, false);
  ComponentParser.SetProperty('uv_map', uv_map, false);
  ComponentParser.SetProperty('draw_outline', draw_outline, false);
  ComponentParser.SetProperty('border_color', border_color, false);

  result := ComponentParser;
end;

procedure TDSStatusBar.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSStatusBar;
begin
  inherited AssignTo(Dest);
  if Dest is TDSStatusBar then
  begin
    NewDest := TDSStatusBar(Dest);
    NewDest.Dynamic_Edge := Dynamic_Edge;
    NewDest.Draw_Advance := Draw_Advance;
    NewDest.Advance_Color := Advance_Color;
    NewDest.Advance_Duration := Advance_Duration;
    NewDest.topmost := topmost;
    NewDest.uv_map := uv_map;
    NewDest.draw_outline := draw_outline;
    NewDest.border_color := border_color;
  end;
end;

procedure TDSStatusBar.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  dynamic_edge := StringToEdgeDef(Parser.GetProperty('dynamic_edge', ''), dynamic_edge);
  draw_advance := Parser.GetProperty('draw_advance', draw_advance);
  advance_color := Parser.GetProperty('advance_color', advance_color);
  advance_duration := Parser.GetProperty('advance_duration', advance_duration);
  topmost := Parser.GetProperty('topmost', topmost);
  uv_map := Parser.GetProperty('uv_map', uv_map);
  draw_outline := Parser.GetProperty('draw_outline', draw_outline);
  border_color := Parser.GetProperty('border_color', border_color);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSDockBar
/////////////////////////////////////////////////////

constructor TDSDockBar.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'dockbar';
  PropertyXRef.Values['stretch_x'] := '#stretchx#';
  PropertyXRef.Values['stretch_y'] := '#stretchy#';
  PropertyXRef.Values['dockbar_type'] := '#dockbartype#';
  PropertyXRef.Values['drag_delay'] := '#dragdelay#';
  PropertyXRef.Values['drag_x'] := '#dragx#';
  PropertyXRef.Values['drag_y'] := '#dragy#';
  PropertyXRef.Values['draggable'] := '#draggable#';

end;

function TDSDockBar.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('stretch_x', stretch_x, false);
  ComponentParser.SetProperty('stretch_y', stretch_y, false);
  ComponentParser.SetProperty('dockbar_type', DockbarTypeToString(dockbar_type), false);
  ComponentParser.SetProperty('drag_delay', drag_delay, false);
  ComponentParser.SetProperty('drag_x', drag_x, false);
  ComponentParser.SetProperty('drag_y', drag_y, false);
  ComponentParser.SetProperty('draggable', draggable, false);

  result := ComponentParser;
end;

procedure TDSDockBar.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSDockbar;
begin
  inherited AssignTo(Dest);
  if Dest is TDSDockBar then
  begin
    NewDest := TDSDockBar(Dest);
    NewDest.Stretch_X := Stretch_X;
    NewDest.Stretch_Y := Stretch_Y;
    NewDest.Dockbar_Type := Dockbar_Type;
    NewDest.drag_delay := drag_delay;
    NewDest.drag_x := drag_x;
    NewDest.Drag_y := drag_y;
    NewDest.draggable := draggable;
  end;
end;

procedure TDSDockBar.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  stretch_x := Parser.GetProperty('stretch_x', stretch_x);
  stretch_y := Parser.GetProperty('stretch_y', stretch_y);
  dockbar_type := StringToDockbarTypeDef(Parser.GetProperty('dockbar_type', ''), dockbar_type);
  drag_delay := Parser.GetProperty('drag_delay', drag_delay);
  drag_x := Parser.GetProperty('drag_x', drag_x);
  drag_y := Parser.GetProperty('drag_y', drag_y);
  draggable := Parser.GetProperty('draggable', draggable);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSEditBox
/////////////////////////////////////////////////////

constructor TDSEditBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'edit_box';
  allow_ime := true;
  PropertyXref.Values['clear_select'] := '#clearselect#';
  PropertyXref.Values['font_color'] := '#fontcolor#';
  PropertyXref.Values['font_size'] := '#fontsize#';
  PropertyXref.Values['font_type'] := '#fonttype#';
  PropertyXref.Values['max_string_size'] := '#maxstringsize#';
  PropertyXref.Values['permanent_focus'] := '#permanentfocus#';
  PropertyXref.Values['excluded_chars'] := '#excludedchars#';
  PropertyXref.Values['allowed_chars'] := '#allowedchars#';
  PropertyXref.Values['max_pixels'] := '#maxpixels#';
  PropertyXref.Values['has_pixel_limit'] := '#haspixellimit#';
  PropertyXref.Values['allow_ime'] := '#allowime#';
  PropertyXref.Values['ascii_high_bound'] := '#asciihighbound#';
  PropertyXref.Values['ascii_low_bound'] := '#asciilowbound#';
  PropertyXref.Values['restrict_input'] := '#restrictinput#';
  PropertyXref.Values['text'] := '#text#';
  PropertyXref.Values['prompt_flash'] := '#promptflash#';
  PropertyXref.Values['tab_stop'] := '#tabstop#';
  PropertyXref.Values['hidden_text'] := '#hiddentext#';
end;

function TDSEditBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('clear_select', clear_select, false);
  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('font_size', font_size, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('max_string_size', max_string_size, false);
  ComponentParser.SetProperty('permanent_focus', permanent_focus, false);
  ComponentParser.SetProperty('excluded_chars', excluded_chars, false);
  ComponentParser.SetProperty('allowed_chars', allowed_chars, false);
  ComponentParser.SetProperty('max_pixels', max_pixels, false);
  ComponentParser.SetProperty('has_pixel_limit', has_pixel_limit, false);
  ComponentParser.SetProperty('allow_ime', allow_ime, false);
  ComponentParser.SetProperty('ascii_high_bound', ascii_high_bound, false);
  ComponentParser.SetProperty('ascii_low_bound', ascii_low_bound, false);
  ComponentParser.SetProperty('restrict_input', restrict_input, false);
  ComponentParser.SetProperty('text', text, false);
  ComponentParser.SetProperty('prompt_flash', prompt_flash, false);
  ComponentParser.SetProperty('tab_stop', tab_stop, false);
  ComponentParser.SetProperty('hidden_text', hidden_text, false);

  result := ComponentParser;
end;

procedure TDSEditBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSEditBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSEditBox then
  begin
    NewDest := TDSEditBox(Dest);
    NewDest.clear_select := clear_select;
    NewDest.font_color := font_color;
    NewDest.font_size := font_size;
    NewDest.font_type := font_type;
    NewDest.max_string_size := max_string_size;
    NewDest.permanent_focus := permanent_focus;
    NewDest.excluded_chars := excluded_chars;
    NewDest.allowed_chars := allowed_chars;
    NewDest.max_pixels := max_pixels;
    NewDest.has_pixel_limit := has_pixel_limit;
    NewDest.allow_ime := allow_ime;
    NewDest.ascii_high_bound := ascii_high_bound;
    NewDest.ascii_low_bound := ascii_low_bound;
    NewDest.restrict_input := restrict_input;
    NewDest.text := text;
    NewDest.prompt_flash := prompt_flash;
    NewDest.tab_stop := tab_stop;
    NewDest.hidden_text := hidden_text;
  end;
end;

procedure TDSEditBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  clear_select := Parser.GetProperty('clear_select', clear_select);
  font_color := Parser.GetProperty('font_color', font_color);
  font_size := Parser.GetProperty('font_size', font_size);
  font_type := Parser.GetProperty('font_type', font_type);
  max_string_size := Parser.GetProperty('max_string_size', max_string_size);
  permanent_focus := Parser.GetProperty('permanent_focus', permanent_focus);
  excluded_chars := Parser.GetProperty('excluded_chars', excluded_chars);
  allowed_chars := Parser.GetProperty('allowed_chars', allowed_chars);
  max_pixels := Parser.GetProperty('max_pixels', max_pixels);
  has_pixel_limit := Parser.GetProperty('has_pixel_limit', has_pixel_limit);
  allow_ime := Parser.GetProperty('allow_ime', allow_ime);
  ascii_high_bound := Parser.GetProperty('ascii_high_bound', ascii_high_bound);
  ascii_low_bound := Parser.GetProperty('ascii_low_bound', ascii_low_bound);
  restrict_input := Parser.GetProperty('restrict_input', restrict_input);
  text := Parser.GetProperty('text', text);
  prompt_flash := Parser.GetProperty('prompt_flash', prompt_flash);
  tab_stop := Parser.GetProperty('tab_stop', tab_stop);
  hidden_text := Parser.GetProperty('hidden_text', hidden_text);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSChatBox
/////////////////////////////////////////////////////

constructor TDSChatBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'chat_box';
  PropertyXref.Values['font_color'] := '#fontcolor#';
  PropertyXref.Values['font_size'] := '#fontsize#';
  PropertyXref.Values['font_type'] := '#fonttype#';
  PropertyXref.Values['has_slider'] := '#hasslider#';
  PropertyXref.Values['line_timeout'] := '#linetimeout#';
  PropertyXref.Values['max_string_size'] := '#maxstringsize#';
  PropertyXref.Values['scroll_down'] := '#scrolldown#';
  PropertyXref.Values['text'] := '#text#';
  PropertyXref.Values['delete_timeouts'] := '#deletetimeouts#';
  PropertyXref.Values['max_history'] := '#maxhistory#';
  PropertyXref.Values['retain_scroll_position'] := '#retainscrollposition#';

end;

function TDSChatBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;
  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('font_size', font_size, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('has_slider', has_slider, false);
  ComponentParser.SetProperty('line_timeout', line_timeout, false);
  ComponentParser.SetProperty('max_string_size', max_string_size, false);
  ComponentParser.SetProperty('scroll_down', scroll_down, false);
  ComponentParser.SetProperty('text', text, false);
  ComponentParser.SetProperty('delete_timeouts', delete_timeouts, false);
  ComponentParser.SetProperty('max_history', max_history, false);
  ComponentParser.SetProperty('retain_scroll_position', retain_scroll_position, false);
  result := ComponentParser;
end;

procedure TDSChatBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSChatBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSChatBox then
  begin
    NewDest := TDSChatBox(Dest);
    NewDest.font_color := font_color;
    NewDest.font_size := font_size;
    NewDest.font_type := font_type;
    NewDest.has_slider := has_slider;
    NewDest.line_timeout := line_timeout;
    NewDest.max_string_size := max_string_size;
    NewDest.scroll_down := scroll_down;
    NewDest.text := text;
    NewDest.delete_timeouts := delete_timeouts;
    NewDest.max_history := max_history;
    NewDest.retain_scroll_position := retain_scroll_position;

  end;
end;

procedure TDSChatBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  font_color := Parser.GetProperty('font_color', font_color);
  font_size := Parser.GetProperty('font_size', font_size);
  font_type := Parser.GetProperty('font_type', font_type);
  has_slider := Parser.GetProperty('has_slider', has_slider);
  line_timeout := Parser.GetProperty('line_timeout', line_timeout);
  max_string_size := Parser.GetProperty('max_string_size', max_string_size);
  scroll_down := Parser.GetProperty('scroll_down', scroll_down);
  text := Parser.GetProperty('text', text);
  delete_timeouts := Parser.GetProperty('delete_timeouts', delete_timeouts);
  max_history := Parser.GetProperty('max_history', max_history);
  retain_scroll_position := Parser.GetProperty('retain_scroll_position', retain_scroll_position);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSComboBox
/////////////////////////////////////////////////////

constructor TDSComboBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'combo_box';
  PropertyXRef.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['text_color'] := '#textcolor#';
  PropertyXRef.Values['has_background'] := '#hasbackground#';
  PropertyXRef.Values['auto_size'] := '#autosize#';
  PropertyXRef.Values['max_visible_elements'] := '#maxvisibleelements#';
  PropertyXref.Values['show_text'] := '#showtext#';
  PropertyXRef.Values['truncate'] := '#truncate#';
end;

function TDSComboBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('text_color', text_color, false);
  ComponentParser.SetProperty('has_background', has_background, false);
  ComponentParser.SetProperty('auto_size', auto_size, false);
  ComponentParser.SetProperty('max_visible_elements', max_visible_elements, false);
  ComponentParser.SetProperty('show_text', show_text, false);
  ComponentParser.SetProperty('truncate', truncate, false);

  result := ComponentParser;
end;

procedure TDSComboBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSComboBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSComboBox then
  begin
    NewDest := TDSComboBox(Dest);
    NewDest.font_type := font_type;
    NewDest.text_color := text_color;
    NewDest.has_background := has_background;
    NewDest.auto_size := auto_size;
    NewDest.max_visible_elements := max_visible_elements;
    NewDest.show_text := show_text;
    NewDest.truncate := Truncate;
  end;
end;

procedure TDSComboBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  font_type := Parser.GetProperty('font_type', font_type);
  text_color := Parser.GetProperty('text_color', text_color);
  has_background := Parser.GetProperty('has_background', has_background);
  auto_size := Parser.GetProperty('auto_size', auto_size);
  max_visible_elements := Parser.GetProperty('max_visible_elements', max_visible_elements);
  show_text := Parser.GetProperty('show_text', show_text);
  truncate := Parser.GetProperty('truncate', truncate);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSGridBox
/////////////////////////////////////////////////////

constructor TDSGridBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'gridbox';
  PropertyXRef.Values['box_height'] := '#boxheight#';
  PropertyXRef.Values['box_width'] := '#boxwidth#';
  PropertyXRef.Values['columns'] := '#columns#';
  PropertyXRef.Values['grid_type'] := '#gridtype#';
  PropertyXRef.Values['rows'] := '#rows#';
  PropertyXRef.Values['autoplace_only'] := '#autoplaceonly#';
  PropertyXRef.Values['consumes_items'] := '#consumesitems#';

end;

function TDSGridBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('box_height', box_height, false);
  ComponentParser.SetProperty('box_width', box_width, false);
  ComponentParser.SetProperty('columns', columns, false);
  ComponentParser.SetProperty('grid_type', GridTypeToString(grid_type), false);
  ComponentParser.SetProperty('rows', rows, false);
  ComponentParser.SetProperty('autoplace_only', autoplace_only, false);
  ComponentParser.SetProperty('consumes_items', consumes_items, false);

  result := ComponentParser;
end;

procedure TDSGridBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSGridBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSGridBox then
  begin
    NewDest := TDSGridBox(Dest);
    Newdest.box_height := box_height;
    NewDest.box_width := box_width;
    NewDest.columns := columns;
    NewDest.rows := rows;
    NewDest.grid_type := grid_type;
    NewDest.autoplace_only := autoplace_only;
    NewDest.consumes_items := consumes_items;
  end;
end;

procedure TDSGridBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  box_height := Parser.GetProperty('box_height', box_height);
  box_width := Parser.GetProperty('box_width', box_width);
  columns := Parser.GetProperty('columns', columns);
  grid_type := StringToGridTypeDef(Parser.GetProperty('grid_type', ''), grid_type);
  rows := Parser.GetProperty('rows', rows);
  autoplace_only := Parser.GetProperty('autoplace_only', autoplace_only);
  consumes_items := Parser.GetProperty('consumes_items', consumes_items);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSSlider
/////////////////////////////////////////////////////

constructor TDSSlider.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'slider';
  repeater_buttons := true;
  PropertyXRef.Values['slide_axis'] := '#slideaxis#';
  PropertyXRef.Values['min_value'] := '#minvalue#';
  PropertyXRef.Values['max_value'] := '#maxvalue#';
  PropertyXRef.Values['step_value'] := '#stepvalue#';
  PropertyXRef.Values['dynamic_button'] := '#dynamicbutton#';
  PropertyXRef.Values['has_scroll_buttons'] := '#hasscrollbuttons#';
  PropertyXRef.Values['enabled'] := '#enabled#';
  PropertyXRef.Values['show_popup_value'] := '#showpopupvalue#';
  PropertyXRef.Values['popup_value_font'] := '#popupvaluefont#';
  PropertyXRef.Values['default_value'] := '#defaultvalue#';
  PropertyXRef.Values['repeater_buttons'] := '#repeaterbuttons#';
end;

function TDSSlider.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('slide_axis', SlideAxisToString(slide_axis), false);
  ComponentParser.SetProperty('min_value', min_value, false);
  ComponentParser.SetProperty('max_value', max_value, false);
  ComponentParser.SetProperty('step_value', step_value, false);
  ComponentParser.SetProperty('dynamic_button', dynamic_button, false);
  ComponentParser.SetProperty('has_scroll_buttons', has_scroll_buttons, false);
  ComponentParser.SetProperty('enabled', enabled, false);
  ComponentParser.SetProperty('show_popup_value', show_popup_value, false);
  ComponentParser.SetProperty('popup_value_font', popup_value_font, false);
  ComponentParser.SetProperty('default_value', default_value, false);
  ComponentParser.SetProperty('repeater_buttons', repeater_buttons, false);

  result := ComponentParser;
end;

procedure TDSSlider.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSSlider;
begin
  inherited AssignTo(Dest);
  if Dest is TDSSlider then
  begin
    NewDest := TDSSlider(Dest);
    NewDest.slide_axis := slide_axis;
    NewDest.min_value := min_value;
    NewDest.max_value := max_value;
    NewDest.step_value := step_value;
    NewDest.dynamic_button := dynamic_button;
    NewDest.has_scroll_buttons := has_scroll_buttons;
    NEwDest.enabled_ := enabled_;
    NewDest.show_popup_value := show_popup_value;
    NewDest.popup_value_font := popup_value_font;
    NewDest.default_value := default_value;
    NewDest.repeater_buttons := repeater_buttons;
  end;
end;

procedure TDSSlider.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  slide_axis := StringToSlideAxisDef(Parser.GetProperty('slide_axis', ''), slide_axis);
  min_value := Parser.GetProperty('min_value', min_value);
  max_value := Parser.GetProperty('max_value', max_value);
  step_value := Parser.GetProperty('step_value', step_value);
  dynamic_button := Parser.GetProperty('dynamic_button', dynamic_button);
  has_scroll_buttons := Parser.GetProperty('has_scroll_buttons', has_scroll_buttons);
  enabled := Parser.GetProperty('enabled', enabled);
  show_popup_value := Parser.GetProperty('show_popup_value', show_popup_value);
  popup_value_font := Parser.GetProperty('popup_value_font', popup_value_font);
  default_value := Parser.GetProperty('default_value', default_value);
  repeater_buttons := Parser.GetProperty('repeater_buttons', repeater_buttons);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSTab
/////////////////////////////////////////////////////

constructor TDSTab.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'tab';
  PropertyXRef.Values['radio_group'] := '#radiogroup#';
  PropertyXRef.Values['column'] := '#column#';
  PropertyXRef.Values['row'] := '#row#';
  PropertyXRef.Values['disable_color'] := '#disablecolor#';
  PropertyXRef.Values['enabled'] := '#enabled#';
end;

function TDSTab.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('radio_group', radio_group, false);
  ComponentParser.SetProperty('column', column, false);
  ComponentParser.SetProperty('row', row, false);
  ComponentParser.SetProperty('disable_color', disable_color, false);
  ComponentParser.SetProperty('enabled', enabled, false);

  result := ComponentParser;
end;

procedure TDSTab.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSTab;
begin
  inherited AssignTo(Dest);
  if Dest is TDSTab then
  begin
    NewDest := TDSTab(Dest);
    NewDest.Radio_Group := Radio_Group;
    NewDest.column := column;
    NewDest.row := row;
    NewDest.disable_color := disable_color;
    NewDest.enabled_ := enabled_;

  end;
end;

procedure TDSTab.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  radio_group := Parser.GetProperty('radio_group', radio_group);
  column := Parser.GetProperty('column', column);
  row := Parser.GetProperty('row', row);
  disable_color := Parser.GetProperty('disable_color', disable_color);
  enabled := Parser.GetProperty('enabled', enabled);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSInfoSlot
/////////////////////////////////////////////////////

constructor TDSInfoSlot.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'infoslot';
end;

function TDSInfoSlot.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  result := ComponentParser;
end;

procedure TDSInfoSlot.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSInfoSlot;
begin
  inherited AssignTo(Dest);
  if Dest is TDSInfoSlot then
  begin
    NewDest := TDSInfoSlot(Dest);
  end;
end;

procedure TDSInfoSlot.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSListener
/////////////////////////////////////////////////////

constructor TDSListener.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'listener';
end;

function TDSListener.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  result := ComponentParser;
end;

procedure TDSListener.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSListener;
begin
  inherited AssignTo(Dest);
  if Dest is TDSListener then
  begin
    NewDest := TDSListener(Dest);
  end;
end;

procedure TDSListener.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSPopupMenu
/////////////////////////////////////////////////////

constructor TDSPopupmenu.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'popupmenu';
  PropertyXref.Values['element_height'] := '#elementheight#';
  PropertyXref.Values['auto_size'] := '#autosize#';
  PropertyXref.Values['font_type'] := '#fonttype#';
  PropertyXref.Values['alignment'] := '#alignment#';

end;

function TDSPopupmenu.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('element_height', element_height, false);
  ComponentParser.SetProperty('auto_size', auto_size, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('alignment', PopAlignmentToString(alignment), false);

  result := ComponentParser;
end;

procedure TDSPopupmenu.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSPopupmenu;
begin
  inherited AssignTo(Dest);
  if Dest is TDSPopupmenu then
  begin
    NewDest := TDSPopupmenu(Dest);
    NewDest.element_height := element_height;
    NewDest.auto_size := auto_size;
    NewDest.font_type := font_type;
    NewDest.alignment := alignment;

  end;
end;

procedure TDSPopupmenu.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  element_height := Parser.GetProperty('element_height', element_height);
  auto_size := Parser.GetProperty('auto_size', auto_size);
  font_type := Parser.GetProperty('font_type', font_type);
  alignment := StringToPopAlignmentDef(Parser.GetProperty('alignment', ''), alignment);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSListReport
/////////////////////////////////////////////////////

constructor TDSListReport.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'listreport';
  PropertyXref.Values['element_type'] := '#elementtype#';
  PropertyXref.Values['font_color'] := '#fontcolor#';
  PropertyXref.Values['font_size'] := '#fontsize#';
  PropertyXref.Values['font_type'] := '#fonttype#';
  PropertyXRef.Values['selection_color'] := '#selectioncolor#';
  PropertyXRef.Values['custom_column_sort'] := '#customcolumnsort#';
  PropertyXRef.Values['dual_column_sort'] := '#dualcolumnsort#';

end;

function TDSListReport.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  ComponentParser.SetProperty('element_type', LBElementTypeToString(element_type), false);
  ComponentParser.SetProperty('font_color', font_color, false);
  ComponentParser.SetProperty('font_size', font_size, false);
  ComponentParser.SetProperty('font_type', font_type, false);
  ComponentParser.SetProperty('selection_color', selection_color, false);
  ComponentParser.SetProperty('custom_column_sort', custom_column_sort, false);
  ComponentParser.SetProperty('dual_column_sort', dual_column_sort, false);

  result := ComponentParser;
end;

procedure TDSListReport.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSListReport;
begin
  inherited AssignTo(Dest);
  if Dest is TDSListReport then
  begin
    NewDest := TDSListReport(Dest);
    NewDest.element_type := element_type;
    NewDest.font_color := font_color;
    NewDest.font_size := font_size;
    NewDest.font_type := font_type;
    NewDest.selection_color := selection_color;
    NewDest.custom_column_sort := custom_column_sort;
    NewDest.dual_column_sort := dual_column_sort;
  end;
end;

procedure TDSListReport.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  element_type := StringToLBElementTypeDef(Parser.GetProperty('element_type', ''), element_type);
  font_color := Parser.GetProperty('font_color', font_color);
  font_size := Parser.GetProperty('font_size', font_size);
  font_type := Parser.GetProperty('font_type', font_type);
  selection_color := Parser.GetProperty('selection_color', selection_color);
  custom_column_sort := Parser.GetProperty('custom_column_sort', custom_column_sort);
  dual_column_sort := Parser.GetProperty('dual_column_sort', dual_column_sort);

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSRolloverTexture
/////////////////////////////////////////////////////

constructor TDSRolloverTexture.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'rollover_texture';
end;

function TDSRolloverTexture.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  result := ComponentParser;
end;

procedure TDSRolloverTexture.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSRolloverTexture;
begin
  inherited AssignTo(Dest);
  if Dest is TDSRolloverTexture then
  begin
    NewDest := TDSRolloverTexture(Dest);
  end;
end;

procedure TDSRolloverTexture.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSSelectionTexture
/////////////////////////////////////////////////////

constructor TDSSelectionTexture.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'selection_texture';
  //  HiddenProperties := '
end;

function TDSSelectionTexture.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  result := ComponentParser;
end;

procedure TDSSelectionTexture.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSSelectionTexture;
begin
  inherited AssignTo(Dest);
  if Dest is TDSSelectionTexture then
  begin
    NewDest := TDSSelectionTexture(Dest);
  end;
end;

procedure TDSSelectionTexture.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

//////////////////////////////////////////////////////
//      TDSSelectionBox
/////////////////////////////////////////////////////

constructor TDSSelectionBox.Create(AOwner: TComponent);
begin
  inherited Create(Aowner);
  TemplateName := 'selection_box';
end;

function TDSSelectionBox.BuildParserComponent: TGasInterestItem;
var
  ComponentParser   : TGasInterestItem;
begin
  ComponentParser := inherited BuildParserComponent;

  result := ComponentParser;
end;

procedure TDSSelectionBox.AssignTo(Dest: TPersistent);
var
  NewDest           : TDSSelectionBox;
begin
  inherited AssignTo(Dest);
  if Dest is TDSSelectionBox then
  begin
    NewDest := TDSSelectionBox(Dest);
  end;
end;

procedure TDSSelectionBox.LoadComponentFromParser(Parser: TGasInterestItem);
begin

  inherited LoadComponentFromParser(Parser);

end;

procedure Register;
begin
  RegisterComponents('Dungeon Siege GUI', [TDSInterface]);
  //    [TDSInterface, TDSWindow, TDSButton, TDSRadioButton,
  //    TDSCheckbox, TDSListbox, TDSDialogBox, TDSTextBox, TDSText,
  //      TDSItemSlot, TDSStatusBar, TDSDockBar, TDSEditBox, TDSComboBox,
  //      TDSGridbox, TDSSlider, TDSTab, TDSInfoSlot,TDSListener, TDSPopupmenu, TDSListReport]);
end;

initialization
  TexturePathXref := TStringlist.create;
  UnknownTextureList := TStringlist.create;
  ParseWarningsList := TStringlist.create;

finalization
  FreeAndNil(TexturePathXref);
  FreeAndNil(UnknownTextureList);
  FreeAndNil(ParseWarningsList);

end.

