unit janSticker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, stdctrls,Forms, Dialogs;

const
  sc_DragMove: Longint = $F012;


type

  TjanStickSizer = class (TCustomControl)
  private
    FControl: TControl;
    FRectList: array [1..8] of TRect;
    FPosList: array [1..8] of Integer;
  protected

  public
    GripColor : TColor;
    constructor Create (AOwner: TComponent;
      AControl: TControl);
    procedure CreateParams (var Params: TCreateParams);
      override;
    procedure CreateHandle; override;
    procedure WmNcHitTest (var Msg: TWmNcHitTest);
      message wm_NcHitTest;
    procedure WmSize (var Msg: TWmSize);
      message wm_Size;
    procedure WmLButtonDown (var Msg: TWmLButtonDown);
      message wm_LButtonDown;
    procedure WmMove (var Msg: TWmMove);
      message wm_Move;
    procedure Paint; override;
    procedure SizerControlExit (Sender: TObject);

  end;


  TjanSticker = class(TGraphicControl)
  private
    FStickColor: TColor;
    procedure SetStickColor(const Value: TColor);
    function captionDialog(s: string): string;
    { Private declarations }
  protected
    { Protected declarations }
    procedure CMMouseLeave (var Msg:TMessage); message CM_MouseLeave;
    procedure CMMouseEnter (var Msg:TMessage); message CM_MouseEnter;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
  public
    { Public declarations }
    constructor create(AOwner:TComponent); override;
    procedure   Paint; override;
  published
    { Published declarations }
    property StickColor:TColor read FStickColor write SetStickColor;
    property Align;
    property Caption;
    property Font;
    property popupmenu;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('janBasic', [TjanSticker]);
end;

{ TjanStickSizer }

// TjanStickSizer methods

constructor TjanStickSizer.Create (
  AOwner: TComponent; AControl: TControl);
var
  R: TRect;
begin
  inherited Create (AOwner);
  GripColor := clSilver;
  FControl := AControl;
  // install the new handler
  OnExit := SizerControlExit;
  // set the size and position
  R := FControl.BoundsRect;
  InflateRect (R, 2, 2);
  BoundsRect := R;
  // set the parent
  Parent := FControl.Parent;
  // create the list of positions
  FPosList [1] := htTopLeft;
  FPosList [2] := htTop;
  FPosList [3] := htTopRight;
  FPosList [4] := htRight;
  FPosList [5] := htBottomRight;
  FPosList [6] := htBottom;
  FPosList [7] := htBottomLeft;
  FPosList [8] := htLeft;
end;

procedure TjanStickSizer.CreateHandle;
begin
  inherited CreateHandle;
  SetFocus;
end;

procedure TjanStickSizer.CreateParams (var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.ExStyle := Params.ExStyle +
    ws_ex_Transparent;
end;

procedure TjanStickSizer.Paint;
var
  I: Integer;
begin
  Canvas.Brush.Color := GripColor;
  for I := 1 to  8 do
    Canvas.Rectangle (FRectList [I].Left, FRectList [I].Top,
      FRectList [I].Right, FRectList [I].Bottom);
end;

procedure TjanStickSizer.WmNcHitTest(var Msg: TWmNcHitTest);
var
  Pt: TPoint;
  I: Integer;
begin
  Pt := Point (Msg.XPos, Msg.YPos);
  Pt := ScreenToClient (Pt);
  Msg.Result := 0;
  for I := 1 to  8 do
    if PtInRect (FRectList [I], Pt) then
      Msg.Result := FPosList [I];
  // if the return value was not set
  if Msg.Result = 0 then
    inherited;
end;

procedure TjanStickSizer.WmSize (var Msg: TWmSize);
var
  R: TRect;
begin
  R := BoundsRect;
  InflateRect (R, -2, -2);
  FControl.BoundsRect := R;
  // setup data structures
  FRectList [1] := Rect (0, 0, 5, 5);
  FRectList [2] := Rect (Width div 2 - 3, 0,
    Width div 2 + 2, 5);
  FRectList [3] := Rect (Width - 5, 0, Width, 5);
  FRectList [4] := Rect (Width - 5, Height div 2 - 3,
   Width, Height div 2 + 2);
  FRectList [5] := Rect (Width - 5, Height - 5,
   Width, Height);
  FRectList [6] := Rect (Width div 2 - 3, Height - 5,
    Width div 2 + 2, Height);
  FRectList [7] := Rect (0, Height - 5, 5, Height);
  FRectList [8] := Rect (0, Height div 2 - 3,
   5, Height div 2 + 2);
end;

procedure TjanStickSizer.SizerControlExit (Sender: TObject);
begin
  //Free;

end;

procedure TjanStickSizer.WmLButtonDown (var Msg: TWmLButtonDown);
begin
  Perform (wm_SysCommand, sc_DragMove, 0);
end;

procedure TjanStickSizer.WmMove (var Msg: TWmMove);
var
  R: TRect;
begin
  R := BoundsRect;
  InflateRect (R, -2, -2);
  FControl.Invalidate; // repaint entire surface
  FControl.BoundsRect := R;
end;




{ TjanSticker }


procedure TjanSticker.CMFontChanged(var Message: TMessage);
begin
  invalidate;
end;

procedure TjanSticker.CMMouseEnter(var Msg: TMessage);
begin
 // cursor:=crhandpoint;
end;

procedure TjanSticker.CMMouseLeave(var Msg: TMessage);
begin
  cursor:=crdefault;
end;

procedure TjanSticker.CMTextChanged(var Message: TMessage);
begin
  invalidate;
end;

constructor TjanSticker.create(AOwner: TComponent);
begin
  inherited;
  width:=65;
  height:=65;
  FStickColor:=clyellow;
end;

function TjanSticker.captionDialog(s:string):string;
var frm:TForm; mem:Tmemo;
begin
  frm:=TForm.Create(self);
  frm.width:=350;
  frm.height:=200;
  frm.BorderStyle:=bsdialog;
  frm.caption:='Edit sticker';
  mem:=Tmemo.create(frm);
  with mem do begin
    align:=alclient;
    font.size:=10;
    scrollbars:=ssvertical;
    text:=s;
    parent:=frm;
  end;
  frm.position:=podesktopcenter;
  frm.ShowModal;
  result:=mem.text;
  frm.free;
end;

procedure TjanSticker.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var h3:integer;
begin
  inherited;
  h3:=height div 3;
  if (button=mbleft) and (ptinrect(rect(0,0,20,h3),point(x,y))) then
  begin
    with TjanStickSizer.Create(self.Parent,TControl(self)) do parent:=self.parent;
  end
  else if (button=mbleft) and (ptinrect(rect(0,h3,20,2*h3),point(x,y))) then
  begin
    caption:=captiondialog(caption);
  end
  else if (button=mbleft) and (ptinrect(rect(0,2*h3,20,clientheight),point(x,y))) then
  begin
    with TColordialog.Create(self) do begin
      color:=FstickColor;
      if execute then StickColor:=color;
      free;
    end;
  end
  else if assigned(onMouseDown) then
    onMouseDown(self,button,shift,x,y);
end;

procedure TjanSticker.Paint;
var R:TRect;
    s:string;
    h3:integer;
    i:integer;
begin
  inherited;
  h3:=height div 3;
  canvas.brush.color:=StickColor;
  canvas.fillrect(Rect(15,0,width,height));
  // draw grips
  canvas.brush.color:=clSilver;
  canvas.fillrect(Rect(0,0,15,height));
  // size grip
  for i:=1 to 4 do
  begin
    canvas.pen.color:=clwhite;
    canvas.MoveTo(i*3,3);
    canvas.lineto(i*3,h3-2);
    canvas.pen.color:=clbtnshadow;
    canvas.MoveTo(i*3+1,3);
    canvas.lineto(i*3+1,h3-2);
  end;
  // edit grip
  for i:=1 to 4 do
  begin
    canvas.pen.color:=clwhite;
    canvas.MoveTo(i*3,h3+2);
    canvas.lineto(i*3,2*h3-2);
    canvas.pen.color:=clnavy;
    canvas.MoveTo(i*3+1,h3+2);
    canvas.lineto(i*3+1,2*h3-2);
  end;
  // color grip
  for i:=1 to 4 do
  begin
    canvas.pen.color:=clwhite;
    canvas.MoveTo(i*3,2*h3+2);
    canvas.lineto(i*3,height-3);
    canvas.pen.color:=clmaroon;
    canvas.MoveTo(i*3+1,2*h3+2);
    canvas.lineto(i*3+1,height-3);
  end;
  R:=Rect(15,0,width,height);
  s:=caption;
  canvas.brush.style:=bsclear;
  DrawText(canvas.handle,pchar(s),-1,R,DT_WORDBREAK);
end;



procedure TjanSticker.SetStickColor(const Value: TColor);
begin
  FStickColor := Value;
  invalidate;
end;


end.
