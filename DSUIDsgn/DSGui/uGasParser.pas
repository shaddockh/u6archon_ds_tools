(*

 Copyright (C) 2003  T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uGasParser.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * A parser for GAS files
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.7 $              $Date: 2004-01-12 15:25:37 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit uGasParser;

interface
uses sysutils, classes, contnrs;
const
  EXCLUDE_CHILDREN_COMPONENT_TYPES = ' SELECTION_TEXTURE ROLLOVER_TEXTURE SELECTION_BOX SLIDER_BUTTON SLIDER ';
type

  TGasComponentList = class;
  TGasComponent = class(Tobject)
  private
    FGas: string;
  protected
    procedure SetGas(value: string);
  public
    SubComponents: TGasComponentList;
    CommentFreeGas: string;
    Template: string;
      Name: string;
    parent: string;
    property Gas: string read FGas write SetGas;
    function GetPropertyValue(prop: string): string;
    function DoesPropertyExist(prop: string): boolean;
    function IsExcluded: boolean;
    procedure UpdatePropertyToVariable(prop: string; value: string);
    constructor create;
    destructor destroy; override;
    procedure ReInsertExcludedChildren;
  end;
  TGasComponentList = class(TObjectList)
  public
    ShellGas: string;
    InterfaceName: string;
    ChildComponentList: boolean;

    procedure ParseGas(Gas: string);
  end;

  //****************************************** NEW PARSER ***************************//
  TGasTokenTypes = (gtUnknown, gtComment, gtWhitespace, gtComponentBlock, gtTemplateBlock, gtName, gtValue, gtSymbol, gtNameValuePair, gtEOL, gtEmbeddedCode, gtSubComponent);
  TGasIndentationStyles = (gisStandard, gisMajorHostility);
  TGasComponentTypes = (gctComponentBlock, gctTemplateBlock);
  TGasInterestItem = class;
  PGasToken = ^TGasToken;
  TGasToken = record
    data: string;
    tokentype: TGasTokenTypes;
    position: integer;
    line: integer;
    FlaggedForDeletion: boolean;
    ChildBlockPointer: TGasInterestItem; //if this is a component/template this points to it
  end;

  TGasTokenList = class(TList)
  protected
    function GetToken(Index: integer): PGasToken;
  public
    OwnsItems: boolean;
    constructor Create; overload;
    constructor Create(OwnsItems: boolean); overload;
    procedure TokenizeGas(Gas: string);
    procedure Delete(Index: integer);
    procedure Clear; override;
    destructor Destroy; override;
    function AsString: string;
    property Tokens[index: integer]: PGasToken read GetToken;

  end;

  TGasBlockList = class(TObjectList)
  protected
    procedure SetItem(Index: integer; Value: TGasInterestItem);
    function GetItem(Index: integer): TGasInterestItem;
  public
    function GetBlockByName(Name: string): TGasInterestItem;
    property Items[Index: integer]: TGasInterestItem read GetItem write SetItem;
  end;

  TGasInterestItem = class(Tobject)
  protected
    NamePointer: PGasToken;
    procedure ParseBlockHeader(Gas: TStringStream);
    procedure ParseBlockDetail(Gas: TStringStream);
    function GetName: string;
    procedure SetName(Value: string);
    function GetNewPropertyInsertPoint: integer;
  public
    ComponentType: TGasComponentTypes;
    ComponentTemplate: string;
    Parent: TGasInterestItem;
    TokenList: TGasTokenList;
    ChildBlocks: TGasBlockList;
    NameValueList: TStringList;
    constructor create; overload;
    constructor create(Owner: TGasInterestItem); overload;
    destructor Destroy; override;
    procedure ParseGas(Gas: string); overload;
    procedure ParseGas(Gas: TStringStream); overload;

    function BuildGas(
      ExpandSubComponents: boolean = true; ExpandComponentDepth: integer = -1; ListOfComponentsToExpand: string = ''; LimitComponentExpansion: boolean = false;
      ExpandSubTemplates: boolean = true; ExpandTemplateDepth: integer = -1; ListOfTemplatesToExpand: string = ''; LimitTemplateExpansion: boolean = false;
      IncludeComments: boolean = true): string;

    function QueryValue(Key: string; default: string): string;
    function QueryValueToken(Key: string): PGasToken;
    function Depth: integer;
    procedure SetTemplateValue(Key: string; value: string; AddIfDoesntExist: boolean);

    function GetProperty(Key: string; default: string): string; overload;
    function GetProperty(Key: string; default: integer): integer; overload;
    function GetProperty(Key: string; default: double): double; overload;
    function GetProperty(Key: string; default: boolean): boolean; overload;

    procedure SetProperty(Key: string; value: string; AddIfDoesntExist: boolean = true); overload;
    procedure SetProperty(Key: string; value: integer; AddIfDoesntExist: boolean = true); overload;
    procedure SetProperty(Key: string; value: double; AddIfDoesntExist: boolean = true); overload;
    procedure SetProperty(Key: string; value: boolean; AddIfDoesntExist: boolean = true); overload;

    procedure InsertChildBlock(Child: TGasInterestItem);

    procedure Reindent(Style: TGasIndentationStyles);
    property Name: string read GetName write SetName;
  end;

function RemoveBlock(Text, StartToken, EndToken: string; leaveEndToken: boolean = false; InstancesToRemove: integer = -1): string;
function ExtractBlock(Text, StartToken, EndToken: string; InstancesToExtract: integer = -1): string;
function BoolToString(b: boolean): string;
function StringToBoolDef(s: string; default: boolean): boolean;
implementation

function BoolToString(b: boolean): string;
begin
  if b then
    result := 'True'
  else
    result := 'False';
end;

function StringToBoolDef(s: string; default: boolean): boolean;
begin
  if comparetext(trim(s), 'True') = 0 then
    result := true
  else if comparetext(trim(s), 'False') = 0 then
    result := false
  else
    result := default;
end;

function PeekNextChar(Stream: TStringStream; count: integer): string;
var
  curpos            : int64;
begin
  curpos := Stream.Position;
  result := Stream.ReadString(count);
  Stream.Position := curpos;
end;

function GetNextToken(Stream: TStringStream): TGasToken;
var
  curchar           : string;
begin

  result.data := '';

  curchar := PeekNextChar(Stream, 1);

  //compress the white space
  if pos(curchar, #10#9#32) > 0 then
  begin
    result.data := curchar;
    result.tokentype := gtWhitespace;
    Stream.ReadString(1);               //increment the pointer
    while (pos(PeekNextChar(Stream, 1), #10#9#32) > 0) and (Stream.Position < Stream.size) do
      result.data := result.data + Stream.ReadString(1);
    exit;
  end;

  if pos(curchar, #13) > 0 then
  begin
    if PeekNextChar(Stream, 2) = #13#10 then
    begin
      result.data := Stream.ReadString(2);
      result.tokentype := gtEOL;
      exit;
    end;
    result.data := Stream.ReadString(1);
    result.tokentype := gtWhitespace;
    exit;
  end;

  //grab out quoted strings
  if (curchar = '"') then
  begin
    result.data := Stream.ReadString(1);
    result.tokentype := gtName;
    curchar := Stream.ReadString(1);
    result.data := result.data + curchar;
    while curchar <> '"' do
    begin
      curchar := Stream.ReadString(1);
      result.data := result.data + curchar;
    end;
    exit;
  end;

  if (curchar = '/') then
  begin
    //line comment
    if PeekNextChar(Stream, 2) = '//' then
    begin
      result.data := Stream.ReadString(2);
      while (PeekNextChar(Stream, 1) <> #13) and (Stream.Position < Stream.size) do
        result.data := result.data + Stream.ReadString(1);

      result.tokentype := gtComment;
      exit;
    end;

    //TODO add bracked counters
    //multi line comment
    if PeekNextChar(Stream, 2) = '/*' then
    begin
      result.data := Stream.ReadString(2);
      while (PeekNextChar(Stream, 2) <> '*/') and (Stream.Position < Stream.Size - 1) do
        result.data := result.data + Stream.ReadString(1);
      if PeekNextChar(Stream, 2) = '*/' then result.data := result.data + Stream.ReadString(2);
      result.tokentype := gtComment;
      exit;
    end;

    result.data := result.data + Stream.ReadString(1);
    result.tokentype := gtSymbol;
    exit;
  end;

  //TODO add bracket counters
  if pos(curchar, '[') > 0 then
  begin
    if PeekNextChar(Stream, 2) = '[[' then
    begin

      result.data := Stream.ReadString(2);

      while (PeekNextChar(Stream, 2) <> ']]') and (Stream.Position < Stream.Size - 1) do
      begin
        result.data := result.data + Stream.ReadString(1);

      end;
      if PeekNextChar(Stream, 2) = ']]' then result.data := result.data + Stream.ReadString(2);
      result.tokentype := gtEmbeddedCode;
      exit;
    end;

    result.data := result.data + Stream.ReadString(1);
    result.tokentype := gtSymbol;
    exit;
  end;

  if pos(curchar, '\/#$+=*[];,:{}()&|%^"') > 0 then
  begin
    result.data := Stream.ReadString(1);
    result.tokentype := gtSymbol;
    exit;
  end;

  if pos(lowercase(curchar), 'abcdefghijklmnopqrstuvwxyz0123456789_.-') > 0 then
  begin
    while pos(lowercase(curchar), 'abcdefghijklmnopqrstuvwxyz0123456789_.-') > 0 do
    begin
      result.data := result.data + Stream.ReadString(1);
      curchar := PeekNextChar(Stream, 1);
    end;

    result.tokenType := gtName;
    exit;
  end;

  result.data := stream.readString(1);
  result.tokentype := gtUnknown;
end;

function PeekNextToken(Stream: TStringStream): TGasToken;
var
  strpos            : integer;
begin
  strpos := Stream.Position;
  result := GetNextToken(Stream);
  Stream.Position := strpos;
end;

procedure PeekNextTokenPtr(Stream: TStringStream; Token: PGasToken);
var
  strpos            : integer;
begin
  strpos := Stream.Position;
  Token^ := GetNextToken(Stream);
  Stream.Position := strpos;
end;

procedure GetNextTokenPtr(Stream: TStringStream; Token: PGasToken);
begin
  Token^ := GetNextToken(Stream);
end;

//TODO:Make this more solid

function GetLastTokenPos(token: string; block: string): integer;
var
  Strm              : TStringStream;
  res               : TGasToken;
begin
  strm := TStringStream.Create(block);
  result := -1;
  while strm.Position <= strm.Size do
  begin
    res := GetNextToken(strm);
    res.data := res.data;
    if res.data = token then result := strm.position;
    if strm.position = strm.size then exit;
  end;

end;

function CreateNewTokenPtr(TokenType: TGasTokenTypes; TokenValue: string): PGasToken;
var
  token             : PGasToken;
begin
  new(token);
  token.tokentype := TokenType;
  token.data := TokenValue;
  result := token;
end;

{************************************
 * TGasTokenList
 ************************************}

constructor TGasTokenList.Create();
begin
  inherited Create;
  OwnsItems := true;
end;

constructor TGasTokenList.Create(OwnsItems: boolean);
begin
  inherited Create;
  self.OwnsItems := OwnsItems;
end;

function TGasTokenList.GetToken(Index: integer): PGasToken;
begin
  result := Items[Index];
end;

procedure TGasTokenList.Delete(Index: integer);
var
  Token             : PGasToken;
begin
  if OwnsItems then
  begin
    Token := items[Index];
    Dispose(Token);
  end;
  inherited Delete(Index);
end;

procedure TGasTokenList.Clear;
var
  i                 : integer;
  Token             : PGasToken;
begin
  if OwnsItems then
  begin
    for i := 0 to count - 1 do
    begin
      Token := Items[i];
      Dispose(Token);
    end;
  end;
  inherited Clear;
end;

function CountChar(Text: string; Char: char; var LastPos: integer): integer;
var
  i                 : integer;
begin
  result := 0;
  for i := 1 to length(text) do
    if text[i] = char then
    begin
      result := result + 1;
      LastPos := i;
    end;
end;

procedure TGasTokenList.TokenizeGas(Gas: string);
var
  GasStream         : TStringStream;
  Token             : PGasToken;
  curline           : integer;
  StartOfLinePos    : integer;
  LastPos           : integer;
  curpos            : integer;
begin
  GasStream := TStringStream.Create(Gas);
  curline := 1;
  StartOfLinePos := 1;
  while true do
  begin
    try
      New(Token);
      curpos := (GasStream.Position - StartOfLinePos) + 1;
      GetNextTokenPtr(GasStream, Token);
      Token^.position := curpos;
      Token^.line := curline;
      case Token^.tokentype of
        gtEOL:
          begin
            curline := curline + 1;
            StartOfLinePos := GasStream.position + 1;
          end;
        gtComment:
          begin
            curline := curline + CountChar(Token^.data, #10, LastPos);
            StartOfLinePos := GasStream.Position - (Length(Token^.Data) - LastPos);
          end;
      end;
      Add(Pointer(Token));
      if GasStream.position = GasStream.size then break;
    except
      Dispose(Token);
      Clear;
      raise;
    end;
  end;
end;

function TGasTokenList.AsString: string;
var
  i                 : integer;
  s                 : string;
begin
  s := '';
  for i := 0 to count - 1 do
    s := s + Tokens[i].data;
  result := s;
end;

destructor TGasTokenList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TGasBlockList.SetItem(Index: integer; Value: TGasInterestItem);
begin
  inherited Items[Index] := Value;
end;

function TGasBlockList.GetItem(Index: integer): TGasInterestItem;
begin
  result := TGasInterestItem(inherited Items[index]);
end;

function TGasBlockList.GetBlockByName(Name: string): TGasInterestItem;
var
  i                 : integer;
  CurBlock          : TGasInterestItem;
begin
  result := nil;
  for i := 0 to count - 1 do
  begin
    CurBlock := TGasInterestItem(Items[i]);
    if CompareText(CurBlock.Name, Name) = 0 then
    begin
      result := CurBlock;
      break;
    end;
  end;
end;
{******** TGasInterestItem ***************}
//interest item should contain
//  block name
//  block template
//  list of item name/value pairs
//  sub blocks

constructor TGasInterestItem.create;
begin
  inherited Create;
  TokenList := TGasTokenList.create;
  ChildBlocks := TGasBlockList.create;
  NameValueList := TStringList.create;
  Parent := nil;
end;

constructor TGasInterestItem.create(Owner: TGasInterestItem);
begin
  inherited Create;
  TokenList := TGasTokenList.create;
  ChildBlocks := TGasBlockList.create;
  NameValueList := TStringList.create;
  Parent := Owner;

end;

destructor TGasInterestItem.Destroy;
begin
  TokenList.free;
  ChildBlocks.free;
  NameValueList.free;
  inherited destroy;
end;

function TGasInterestItem.GetName: string;
begin
  if NamePointer <> nil then
    result := NamePointer.data;
end;

procedure TGasInterestItem.SetName(Value: string);
var
  prevname          : string;
  i                 : integer;
begin
  if NamePointer <> nil then
  begin
    prevname := NamePointer.data;
    NamePointer.data := value;

    if Parent <> nil then
    begin
      for i := 0 to Parent.TokenList.Count - 1 do
        case ComponentType of
          gctComponentBlock:
            if (Parent.TokenList.Tokens[i].tokentype = gtComponentBlock)
              and (compareText(Parent.Tokenlist.Tokens[i].data, prevname) = 0) then
            begin
              parent.tokenlist.tokens[i].data := value;
              break;
            end;
          gctTemplateBlock:
            if (Parent.TokenList.Tokens[i].tokentype = gtTemplateBlock)
              and (compareText(Parent.Tokenlist.Tokens[i].data, prevname) = 0) then
            begin
              parent.tokenlist.tokens[i].data := value;
              break;
            end;
        end;
    end;
  end;
end;

function TGasInterestItem.GetNewPropertyInsertPoint: integer;
var
  i                 : integer;
  x                 : integer;
  z                 : integer;
  newindex          : integer;
begin
  newindex := 0;
  for i := 0 to Tokenlist.count - 1 do
  begin
    if (tokenlist.Tokens[i].tokentype = gtsymbol) and (tokenlist.tokens[i].data = '{') then
    begin
      newindex := i;
      break;
    end;
  end;

  result := newindex;
  for i := newindex to TokenList.Count - 1 do
  begin
    if tokenlist.tokens[i].tokentype <> gtValue then continue;
    if (Tokenlist.Tokens[i].tokentype = gtValue) and (pos('[[', tokenlist.tokens[i].data) = 0) then
    begin
      for x := i to tokenlist.count - 1 do //scan till we reach the next semicolon
      begin
        if (tokenlist.tokens[x].tokentype = gtSymbol) and (tokenlist.tokens[x].Data = ';') then
        begin
          result := x;
          for z := x to tokenlist.count - 1 do //scan till we reach eol or the next name
          begin
            if (tokenlist.tokens[z].tokentype = gtComment) or (tokenlist.tokens[z].tokentype = gtWhiteSpace) then
              result := z;
            break;
          end;
          break;
        end;
      end;
    end;
  end;
end;

procedure TGasInterestItem.ParseGas(Gas: string);
var
  strm              : TStringStream;
begin
  strm := nil;
  try
    strm := TStringStream.Create(gas);
    ParseGas(strm);
  finally
    if assigned(strm) then freeAndNil(strm);
  end;
end;

procedure TGasInterestItem.ParseGas(Gas: TStringStream);
var
  PeekToken         : TGasToken;
  Token             : PGasToken;
  ChildBlock        : TGasInterestItem;
begin

  while true do
  begin
    //Get past the initial
    PeekToken := PeekNextToken(Gas);
    if (PeekToken.tokentype = gtSymbol) and (PeekToken.data = '[') then
    begin
      if Parent = nil then
      begin
        ChildBlock := TGasInterestItem.create(self);
        ChildBlocks.Add(ChildBlock);
        ChildBlock.ParseGas(Gas);
        New(Token);
        if ChildBlock.ComponentType = gctComponentBlock then
          Token.tokentype := gtComponentBlock
        else
          Token.tokentype := gtTemplateBlock;

        Token.data := ChildBlock.Name;
        TokenList.Add(Token);
      end
      else
      begin
        //we need to parse this ourselves

        ParseBlockHeader(Gas);
        //New(Token);
        //if GasType = '' then
        //  token.tokentype := gtComponentBlock
        //else
        //  token.tokentype := gtTemplateBlock;
        //Token.data := name;
        //Parent.TokenList.Add(Token);

        ParseBlockDetail(Gas);
        break;
      end;
    end
    else
    begin
      //this is just fluff
      New(Token);
      GetNextTokenPtr(Gas, Token);
      TokenList.Add(Token);
    end;
    if Gas.Position = Gas.Size then break;
  end;

end;

procedure TGasInterestItem.ParseBlockHeader(Gas: TStringStream);
var
  Token             : PGasToken;
  FilteredTokenList : TGasTokenList;
begin
  FilteredTokenList := nil;
  try
    FilteredTokenList := TGasTokenList.create(false);
    while true do
    begin
      New(Token);
      GetNextTokenPtr(Gas, Token);
      TokenList.Add(Token);
      case Token.tokentype of
        gtName, gtValue, gtSymbol: FilteredTokenList.Add(Token);
      end;
      if (Token.tokentype = gtSymbol) and (Token.data = ']') then
      begin
        break;
      end;
      if Gas.Position = gas.Size then raise Exception.create('Could not parse component header.  Line: ' + intToStr(Token.line));
    end;

    //parse out the name
    if FilteredTokenList.Count = 3 then //[name]
    begin
      NamePointer := FilteredTokenList.Tokens[1];
      ComponentType := gctComponentBlock;
      ComponentTemplate := '';
    end
    else if FilteredTokenList.Count = 9 then //[t:type,n:name]
    begin
      NamePointer := FilteredTokenList.Tokens[7];
      ComponentType := gctTemplateBlock;
      ComponentTemplate := FilteredTokenList.Tokens[3].Data;
    end
    else if FilteredTokenList.Count = 11 then //[dev,t:type,n:name]
    begin
      NamePointer := FilteredTokenList.Tokens[9];
      ComponentType := gctTemplateBlock;
      ComponentTemplate := FilteredTokenList.Tokens[5].Data;
    end
    else if (FilteredTokenList.Count = 4) and
      (FilteredTokenList.Tokens[2].data = '*') then //[name*]
    begin
      NamePointer := FilteredTokenList.Tokens[1];
      ComponentType := gctComponentBlock;
      ComponentTemplate := '';
    end
    else
      raise Exception.create('Invalid Component Header: ' + TokenList.AsString);

  finally
    if assigned(filteredTokenList) then freeandnil(FilteredTokenList);
  end;
end;

procedure TGasInterestItem.ParseBlockDetail(Gas: TStringStream);
var
  PeekToken         : TGasToken;
  Token             : PGasToken;
  ChildBlock        : TGasInterestItem;
  i                 : integer;
  SubTokenList      : TGasTokenList;

  FirstValue        : PGasToken;
  FirstValueAssigned: boolean;
  CurrentName       : string;
begin

  SubTokenList := nil;
  try
    SubTokenList := TGasTokenList.Create(false);
    while true do
    begin
      PeekToken := PeekNextToken(Gas);
      if (PeekToken.tokentype = gtSymbol) and (PeekToken.data = '[') then
      begin
        ChildBlock := TGasInterestItem.create(self);
        ChildBlocks.Add(ChildBlock);
        ChildBlock.ParseGas(Gas);
        New(Token);
        if ChildBlock.ComponentType = gctComponentBlock then
          Token.tokentype := gtComponentBlock
        else
          Token.tokentype := gtTemplateBlock;
        Token.data := ChildBlock.Name;
        Token.FlaggedForDeletion := false;
        TokenList.Add(Token);
      end
      else if (PeekToken.tokentype = gtName) then
      begin
        New(Token);
        GetNextTokenPtr(Gas, Token);
        TokenList.Add(Token);
        CurrentName := Token.data;
      end
      else if (PeekToken.tokentype = gtSymbol) and (PeekToken.data = '=') then
      begin
        New(Token);
        GetNextTokenPtr(Gas, Token);
        TokenList.Add(Token);
        FirstValueAssigned := false;
        while True do
        begin
          PeekToken := PeekNextToken(Gas);
          if (PeekToken.TokenType = gtSymbol) and (Peektoken.data = ';') then break;
          New(Token);
          GetNextTokenPtr(Gas, Token);
          TokenList.Add(Token);
          if token.tokentype = gtComment then continue;

          if FirstValueAssigned = false then
          begin
            if token.tokentype = gtEOL then continue;
            if token.tokentype = gtWhitespace then continue;
            FirstValue := Token;
            FirstValueAssigned := true;
            FirstValue.tokentype := gtValue;
          end
          else
          begin
            FirstValue.data := FirstValue.data + token.data;
            token.FlaggedForDeletion := true;
          end;
        end;
        if FirstValueAssigned = false then
        begin
          new(Token);
          Token.tokentype := gtValue;
          Token.data := '';
          Token.FlaggedForDeletion := false;
          TokenList.Add(Token);
          FirstValue := Token;
        end;
        FirstValue.data := trim(FirstValue.data);
        NameValueList.AddObject(CurrentName + '=' + FirstValue.data, TObject(FirstValue));
      end
      else
      begin
        //this is detail
        New(Token);
        GetNextTokenPtr(Gas, Token);
        TokenList.Add(Token);
        if (Token.tokentype = gtSymbol) and (Token.data = '}') then break;
      end;
      if Gas.Position = Gas.Size then raise Exception.create('Could not parse component  Line: ' + TokenList.AsString);
    end;

    //Now run through our list and combine our values
    for i := Tokenlist.Count - 1 downto 0 do
      if TokenList.Tokens[i].FlaggedForDeletion then TokenList.Delete(i);

  finally
    if assigned(SubTokenList) then FreeAndNil(SubTokenList);
  end;

end;

{* \brief
 *  This function will return a formatted gas as text.
 *
 *  \param ExpandSubComponents Expand out subcomponents
 *  \param ExpandComponentDepth
 *  \param ListOfComponentsToExpand Only expand the following templates..leave blank to expand all
 *  \param ExpandSubTemplates  Expand out subTemplates
 *  \param ExpandTemplateDepth
 *  \param ExpandDepth         Depth to recurse for expansion
 *  \param ListOfTemplatesToExpand Only expand the following templates..leave blank to expand all
 *  \param IncludeComments     Return comments or replace with spaces
 *}

function TGasInterestItem.BuildGas(
  ExpandSubComponents: boolean = true; ExpandComponentDepth: integer = -1; ListOfComponentsToExpand: string = ''; LimitComponentExpansion: boolean = false;
  ExpandSubTemplates: boolean = true; ExpandTemplateDepth: integer = -1; ListOfTemplatesToExpand: string = ''; LimitTemplateExpansion: boolean = false;
  IncludeComments: boolean = true): string;
var
  i, x              : integer;
  s                 : string;
  CurrentToken      : PGasToken;
  ChildBlock        : TGasInterestItem;
  NewComponentDepth : integer;
  NewTemplateDepth  : integer;
  slComponentExpandList: TStringList;
  slTemplateExpandList: TStringList;
begin
  slComponentExpandList := nil;
  slTemplateExpandList := nil;
  try
    if LimitComponentExpansion then
    begin
      slComponentExpandList := TStringlist.create;
      slComponentExpandList.CommaText := ListOfComponentsToExpand;
    end;

    if LimitTemplateExpansion then
    begin
      slTemplateExpandList := TStringlist.create;
      slTemplateExpandList.CommaText := ListOfTemplatesToExpand;
    end;

    s := '';
    for i := 0 to TokenList.Count - 1 do
    begin
      CurrentToken := TokenList.Tokens[i];
      case CurrentToken.tokentype of
        gtComponentBlock:
          begin
            if (ExpandSubComponents = true) and (ExpandComponentDepth <> 0) then
            begin
              NewComponentDepth := ExpandComponentDepth - 1;
              if NewComponentDepth < -1 then NewComponentDepth := -1;
              ChildBlock := ChildBlocks.GetBlockByName(CurrentToken.data);

              if LimitComponentExpansion then
                if slComponentExpandList.IndexOf(ChildBlock.GetName) = -1 then continue;

              if ChildBlock <> nil then
                s := s + ChildBlock.BuildGas(ExpandSubComponents, NewComponentDepth, ListOfComponentsToExpand, LimitComponentExpansion,
                  ExpandSubTemplates, ExpandTemplateDepth, ListOfTemplatesToExpand, LimitTemplateExpansion,
                  IncludeComments);
            end;
          end;
        gtTemplateBlock:
          begin
            if (ExpandSubTemplates = true) and (ExpandTemplateDepth <> 0) then
            begin
              NewTemplateDepth := ExpandTemplateDepth - 1;
              if NewTemplateDepth < -1 then NewTemplateDepth := -1;
              ChildBlock := ChildBlocks.GetBlockByName(CurrentToken.data);

              if LimitTemplateExpansion then
                if slTemplateExpandList.IndexOf(ChildBlock.ComponentTemplate) = -1 then continue;

              if ChildBlock <> nil then
                s := s + ChildBlock.BuildGas(ExpandSubComponents, ExpandComponentDepth, ListOfComponentsToExpand, LimitComponentExpansion,
                  ExpandSubTemplates, NewTemplateDepth, ListOfTemplatesToExpand, LimitTemplateExpansion,
                  IncludeComments);
            end;
          end;

        gtComment:
          begin
            if IncludeComments then
              s := s + CurrentToken.data
            else
            begin
              for x := 1 to length(CurrentToken.data) do
              begin
                if CurrentToken.data[x] = #10 then
                  s := s + #10
                else if currentToken.data[x] = #13 then
                  s := s + #13
                else
                  s := s + #32;
              end;
            end;
          end;

      else
        s := s + CurrentToken.data;

      end;
    end;

    result := s;
  finally
    if assigned(slComponentExpandList) then freeAndNil(slComponentExpandList);
    if assigned(slTemplateExpandList) then freeAndNil(slTemplateExpandList);
  end;

end;

function TGasInterestItem.QueryValue(Key: string; default: string): string;
var
  componentname     : string;
  subkey            : string;
  i                 : integer;
  SubComponent      : TGasInterestItem;
begin

  i := pos(':', key);
  if i > 0 then
  begin

    componentname := copy(key, 1, i - 1);
    subkey := key;
    delete(subkey, 1, i);
    SubComponent := ChildBlocks.GetBlockByName(componentName);
    if SubComponent = nil then
      result := default
    else
      result := SubComponent.QueryValue(subkey, default);

  end
  else
  begin
    if NameValueList.IndexOfName(key) = -1 then
      result := default
    else
      result := NameValueList.Values[key];
  end;

end;

function TGasInterestItem.QueryValueToken(Key: string): PGasToken;
var
  componentname     : string;
  subkey            : string;
  i                 : integer;
  SubComponent      : TGasInterestItem;
begin

  i := pos(':', key);
  if i > 0 then
  begin

    componentname := copy(key, 1, i - 1);
    subkey := key;
    delete(subkey, 1, i);
    SubComponent := ChildBlocks.GetBlockByName(componentName);
    if SubComponent = nil then
      result := nil
    else
      result := SubComponent.QueryValueToken(subkey);

  end
  else
  begin
    if NameValueList.IndexOfName(key) = -1 then
      result := nil
    else
      result := Pointer(NameValueList.Objects[NameValueList.IndexOfName(key)]);
  end;

end;

function TGasInterestItem.Depth: integer;
var
  NextUp            : TGasInterestItem;
begin
  NextUp := Parent;
  result := 0;
  while nextup <> nil do
  begin
    result := result + 1;
    nextup := nextup.Parent;
  end;
end;

procedure TGasInterestItem.SetTemplateValue(Key: string; value: string; AddIfDoesntExist: boolean);
var
  val               : PGasToken;
  i, x              : integer;
  token             : PGasToken;
  componentname     : string;
  subkey            : string;
  subcomponent      : TGasInterestItem;
begin

  i := pos(':', key);
  if i > 0 then
  begin

    componentname := copy(key, 1, i - 1);
    subkey := key;
    delete(subkey, 1, i);
    SubComponent := ChildBlocks.GetBlockByName(componentName);
    if SubComponent = nil then
      raise exception.create('SubComponent: ' + componentName + ' does not exist.')
    else
      SubComponent.SetTemplateValue(subkey, value, AddIfDoesntExist);
  end
  else
  begin
    val := QueryValueToken(key);
    if val = nil then
    begin
      if AddIfDoesntExist then
      begin
        x := GetNewPropertyInsertPoint;
        if x <> -1 then
        begin
          x := x + 1;
          tokenlist.Insert(x, CreateNewTokenPtr(gtEOL, #13#10));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtWhiteSpace, StringOfChar(#9, depth)));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtName, Key));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtWhiteSpace, #32));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtSymbol, '='));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtWhiteSpace, #32));
          x := x + 1;

          token := CreateNewTokenPtr(gtValue, value);
          NameValueList.AddObject(Key + '=' + Value, TObject(token));
          tokenlist.Insert(x, token);
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtSymbol, ';'));
          x := x + 1;

          tokenlist.Insert(x, CreateNewTokenPtr(gtEOL, #13#10));

        end;

      end;
    end
    else
    begin
      val.data := value;
      NameValueList.Values[key] := value;
    end;
  end;
end;

procedure TGasInterestItem.InsertChildBlock(Child: TGasInterestItem);
var
  i                 : integer;
  x                 : integer;
begin
  if ChildBlocks.IndexOf(Child) = -1 then
  begin
    ChildBlocks.Add(Child);
    Child.Parent := self;
    for i := TokenList.count - 1 downto 0 do
    begin
      if (tokenlist.Tokens[i].tokentype = gtSymbol) and (tokenlist.tokens[i].data = '}') then
      begin
        x := i;

        tokenlist.Insert(x, CreateNewTokenPtr(gtEOL, #13#10));
        x := x + 1;

        tokenlist.Insert(x, CreateNewTokenPtr(gtWhiteSpace, StringOfChar(#9, depth)));
        x := x + 1;

        if Child.ComponentType = gctComponentBlock then
          tokenlist.Insert(x, CreateNewTokenPtr(gtComponentBlock, Child.Name))
        else
          tokenlist.Insert(x, CreateNewTokenPtr(gtTemplateBlock, Child.Name));

        tokenlist.Insert(x, CreateNewTokenPtr(gtEOL, #13#10));
        x := x + 1;

        tokenlist.Insert(x, CreateNewTokenPtr(gtWhiteSpace, StringOfChar(#9, depth - 1)));
        break;
      end;
    end;
  end;
end;

function TGasInterestItem.GetProperty(Key: string; default: string): string;
begin
  result := QueryValue(key, default);
end;

function TGasInterestItem.GetProperty(Key: string; default: integer): integer;
begin
  try
    result := StrToInt(QueryValue(Key, intToStr(default)));
  except
    result := default;
  end;
end;

function TGasInterestItem.GetProperty(Key: string; default: double): double;
begin
  try
    result := StrToFloat(QueryValue(Key, floatToStr(default)));
  except
    result := default;
  end;
end;

function TGasInterestItem.GetProperty(Key: string; default: boolean): boolean;
begin
  try
    result := StringToBoolDef(QueryValue(Key, BoolToString(default)), default);
  except
    result := default;
  end;
end;

procedure TGasInterestItem.SetProperty(Key: string; value: string; AddIfDoesntExist: boolean = true);
begin
  SetTemplateValue(Key, Value, AddIfDoesntExist);
end;

procedure TGasInterestItem.SetProperty(Key: string; value: integer; AddIfDoesntExist: boolean = true);
begin
  SetTemplateValue(Key, IntToStr(Value), AddIfDoesntExist);
end;

procedure TGasInterestItem.SetProperty(Key: string; value: double; AddIfDoesntExist: boolean = true);
begin
  SetTemplateValue(Key, FloatToStr(Value), AddIfDoesntExist);
end;

procedure TGasInterestItem.SetProperty(Key: string; value: boolean; AddIfDoesntExist: boolean = true);
begin
  SetTemplateValue(Key, BoolToString(Value), AddIfDoesntExist);
end;

procedure TGasInterestItem.Reindent(Style: TGasIndentationStyles);
begin
  //Not Implemented Yet
end;

//////////////////////////////////////////////////////////

function RemoveBlock(Text, StartToken, EndToken: string; leaveEndToken: boolean = false; InstancesToRemove: integer = -1): string;
var
  pos1, pos2        : integer;
  pagehold          : string;
  mirrorText        : string;
  Count             : integer;
begin
  StartToken := uppercase(StartToken);
  EndToken := uppercase(EndToken);
  PageHold := '';

  mirrorText := uppercase(Text);
  pos1 := pos(StartToken, MirrorText);
  Count := 0;
  while (pos1 > 0) and ((Count < InstancesToRemove) or (InstancesToRemove = -1)) do
  begin
    pageHold := copy(text, 1, pos1 - 1);
    delete(text, 1, pos1 + length(starttoken) - 1);
    delete(mirrortext, 1, pos1 + length(starttoken) - 1);

    if LeaveEndToken then
      pos2 := pos(endToken, mirrortext) - 1
    else
      pos2 := pos(endToken, mirrortext) + length(endtoken) - 1;
    delete(text, 1, pos2);
    delete(mirrorText, 1, pos2);
    text := pagehold + text;
    MirrorText := pagehold + MirrorText;
    pos1 := pos(StartToken, MirrorText);
    Count := Count + 1;
  end;
  result := text;
end;

function TrimWhitespace(text: string): string;
var
  i                 : integer;
  tempstr           : string;
begin
  tempstr := trim(text);
  if tempstr = '' then
  begin
    result := tempstr;

    exit;
  end;

  //start trimming left
  for i := 1 to length(tempstr) do
  begin
    if pos(tempstr[i], #9#10#13#32) > 0 then
    begin
      tempstr[i] := #32;
      continue;
    end;
    break;
  end;

  for i := length(tempstr) downto 1 do
  begin
    if pos(tempstr[i], #9#10#13#32) > 0 then
    begin
      tempstr[i] := #32;
      continue;
    end;
    break;
  end;

  result := trim(tempstr);
end;

function ExtractGasNameValue(Text: string; Name: string; ReturnCharPrecedingName: boolean = false): string;
var
  temp              : string;
  i                 : integer;
  Newtext           : string;
  done              : boolean;
  precedingChar     : char;
begin

  //this needs to be modified
  //basically, needs to be more robust in seeking out the name/property

  //maybe it should iterate through and actually do some real parsing.
  Name := trimwhitespace(name);
  result := '';
  i := pos(uppercase(name), uppercase(text));
  if i = 0 then exit;

  NewText := Text;
  done := false;

  while not done do
  begin
    if (i > 1) then
      if (pos(Newtext[i - 1], #13#10#9#32';')) = 0 then
      begin
        delete(NewText, 1, i + length(name));
        i := pos(uppercase(name), uppercase(NewText));
        if i = 0 then exit;
        continue;
      end;

    PrecedingChar := NewText[i - 1];
    delete(NewText, 1, i - 1);
    if pos('=', NewText) = 0 then exit;
    temp := copy(Newtext, 1, pos('=', NewText) - 1);
    temp := Trimwhitespace(temp);
    if uppercase(temp) <> uppercase(name) then
    begin
      delete(NewText, 1, pos(uppercase(name), uppercase(NewText)) + length(name));
      i := pos(uppercase(name), uppercase(NewText));
      if i = 0 then exit;
      continue;
    end;

    temp := copy(NewText, 1, pos(';', NewText));
    result := temp;
    if ReturnCharPrecedingName then result := precedingChar + result;
    done := true;
  end;

  //    temp := ExtractBlock(text, Name, '=', 1);
   //     if pos(';', temp) > 0 then exit;
    //  if temp <> '' then
     //   temp := ExtractBlock(text, temp, ';', 1);

      //result := temp;

end;

function ExtractBlock(Text, StartToken, EndToken: string; InstancesToExtract: integer = -1): string;
var
  pos1, pos2        : integer;
  pagehold          : string;
  mirrorText        : string;
  Count             : integer;
begin
  StartToken := uppercase(StartToken);
  EndToken := uppercase(EndToken);
  PageHold := '';
  mirrorText := uppercase(Text);
  pos1 := pos(StartToken, MirrorText);
  Count := 0;
  result := '';
  while (pos1 > 0) and ((Count < InstancesToExtract) or (InstancesToExtract = -1)) do
  begin

    delete(text, 1, pos1 - 1);
    delete(mirrortext, 1, pos1 - 1);
    result := result + copy(text, 1, length(StartToken));
    delete(text, 1, length(startToken));
    delete(mirrortext, 1, length(starttoken));
    pos2 := pos(endToken, mirrortext) + length(endtoken) - 1;
    result := result + copy(text, 1, pos2);
    delete(text, 1, pos2);
    delete(mirrortext, 1, pos2);
    pos1 := pos(StartToken, MirrorText);
    Count := Count + 1;
  end;
end;

function ExtractBlock2(Text, StartToken, EndToken: string; InstancesToExtract: integer = -1): string;
var
  pos1, pos2        : integer;
  pagehold          : string;
  mirrorText        : string;
  Count             : integer;
begin
  StartToken := uppercase(StartToken);
  EndToken := uppercase(EndToken);
  PageHold := '';
  mirrorText := uppercase(Text);
  pos1 := pos(StartToken, MirrorText);
  Count := 0;
  result := '';
  while (pos1 > 0) and ((Count < InstancesToExtract) or (InstancesToExtract = -1)) do
  begin

    delete(text, 1, pos1 - 1);
    delete(mirrortext, 1, pos1 - 1);
    result := result + copy(text, 1, length(StartToken));
    delete(text, 1, length(startToken));
    delete(mirrortext, 1, length(starttoken));
    pos2 := pos(endToken, mirrortext) + length(endtoken) - 1;
    result := result + copy(text, 1, pos2);
    delete(text, 1, pos2);
    delete(mirrortext, 1, pos2);
    pos1 := pos(StartToken, MirrorText);
    Count := Count + 1;
  end;
end;

/////////////////////////////////////////////////////////////////

procedure TGasComponentList.ParseGas(Gas: string);
var
  done              : boolean;
  curchar           : char;
  curblock          : string;
  pos               : integer;
  gaslength         : integer;

  ExternalStuff     : string;
  InComponent       : boolean;
  bracecount        : integer;
  InMultilineComment: boolean;
  InLineComment     : boolean;
  GasComponent      : TGasComponent;
  CleanBlock        : string;           //no comments
  firstcomponent    : boolean;
  i                 : integer;
  tempstring        : string;
begin

  done := false;
  InComponent := false;
  FirstComponent := true;
  pos := 1;
  gaslength := length(gas);
  InMultiLineComment := false;
  InLineComment := false;
  bracecount := 0;
  while not done do
  begin
    curchar := gas[pos];
    if not InComponent then
    begin
      if (curchar = #13) and (inLineComment) then
      begin
        InLineComment := false;
        ExternalStuff := ExternalStuff + curchar;

      end
      else if (curchar = '/') and (gaslength - pos > 2) then
      begin
        if gas[pos + 1] = '/' then
          inLineComment := true
        else if gas[pos + 1] = '*' then
          InMultilineComment := true
        else if (InMultilineComment = true) and (gas[pos - 1] = '*') then
          InMultilinecomment := false;
        ExternalStuff := ExternalStuff + curchar;
      end
      else if (InMultilinecomment = true) or (inLineComment = true) then
      begin
        //do nothing
        ExternalStuff := ExternalStuff + curchar;

      end
      else if (curchar = '[') and (gaslength - pos > 3) then
      begin
        //see if we are starting a template
        if ((gas[pos + 1] = 't') or (gas[pos + 1] = 'T')) and (gas[pos + 2] = ':') then
        begin
          InComponent := true;
          curblock := '[';
          CleanBlock := '[';
          bracecount := 0;

          if FirstComponent then
          begin
            ExternalStuff := ExternalStuff + #13#10 + '#interfacecomponents#' + #13#10;
            FirstComponent := false;
          end;
        end
        else
          ExternalStuff := ExternalStuff + curchar;
      end

      else
        ExternalStuff := ExternalStuff + curchar;
    end
    else
    begin
      CurBlock := CurBlock + curchar;

      if (curchar = #13) and (inLineComment) then
      begin
        InLineComment := false;
      end
      else if (curchar = '/') and (gaslength - pos > 2) then
      begin
        if gas[pos + 1] = '/' then
        begin
          inLineComment := true;
        end
        else if gas[pos + 1] = '*' then
        begin
          InMultilineComment := true;
        end
        else if (InMultilineComment = true) and (gas[pos - 1] = '*') then
        begin
          InMultilinecomment := false;
        end
      end
      else if (InMultilinecomment = true) or (inLineComment = true) then
      begin
        //do nothing
      end
      else if (curchar = '}') then
      begin
        bracecount := bracecount - 1;
        if bracecount = 0 then
        begin
          InComponent := false;
          GasComponent := TGasComponent.Create;
          if (system.Pos('#childcomponents#', LowerCase(cleanblock)) = 0) and (ChildComponentList = false) then
          begin
            CurBlock[length(curblock)] := ' ';
            CurBlock := CurBlock + #13#10 + '#childcomponents#' + #13#10 + '}';
            CleanBlock := CleanBlock + #13#10 + '#childcomponents#' + #13#10;
          end;
          GasComponent.CommentFreeGas := CleanBlock + '}';
          GasComponent.Gas := CurBlock;

          Add(GasComponent);

          //Now run through the list of subcomponents...
          for i := GasComponent.subcomponents.Count - 1 downto 0 do
          begin
            if (TGasComponent(GasComponent.SubComponents[i]).IsExcluded = false) and (ChildComponentList = false) then
            begin
              Add(GasComponent.SubComponents.Items[i]);
              GasComponent.SubComponents.Delete(i);
            end
          end;

        end;
      end
      else if (curchar = '{') then
      begin
        bracecount := bracecount + 1;
      end;

      if (inLinecomment = true) or (InMultilineComment = true) then
      begin
        if CurChar > #32 then
          CleanBlock := CleanBlock + ' '
        else
          CleanBlock := CleanBlock + curchar;
      end
      else
        CleanBlock := CleanBlock + curchar;

    end;
    Pos := Pos + 1;
    if Pos > gaslength then done := true;
  end;

  //Here, we need to find the last valid } char and determine if
  // there is a #childcomponents# block. if not, add it.
  if system.pos('#interfacecomponents#', lowercase(ExternalStuff)) = 0 then
  begin
    i := GetLastTokenPos('}', ExternalStuff);
    if i <> -1 then
    begin
      tempstring := copy(ExternalStuff, 1, i - 1);
      System.Delete(ExternalStuff, 1, i - 1);
      ExternalStuff := tempstring + #13#10 + '#interfacecomponents#' + #13#10 + ExternalStuff;
    end;

    //
  end;
  ShellGas := ExternalStuff;

end;

///////////////////////////////////////////////////////////////////////////////

procedure TGasComponent.SetGas(value: string);
var
  block             : string;
  block2            : string;
  embedded          : string;
  i                 : integer;
  subgas            : TGasComponent;
  sl, sl2           : TStringlist;
  PrevIsSpace       : boolean;

begin
  FGas := Value;
  //  CommentFreeGas := RemoveBlock(CommentFreeGas, '/*', '*/');
  //  CommentFreeGas := RemoveBlock(Value, '//', #13#10, true);
  PrevIsSpace := false;
  sl := TStringlist.create;
  sl2 := TStringlist.create;
  sl.Text := value;
  for i := 0 to sl.Count - 1 do
  begin
    if (PrevIsSpace = true) and (trim(sl.Strings[i]) = '') then
    begin
      // don't add
    end
    else
    begin
      sl2.add(trimRight(sl.strings[i]));

    end;
    if trim(sl.strings[i]) = '' then
      prevIsSpace := true
    else
      prevIsSpace := false;
  end;
  FGas := sl2.Text;
  FreeAndNil(sl);
  FreeAndNil(sl2);

  block := ExtractBlock(CommentFreeGas, '[t:', ']', 1);

  block2 := ExtractBlock(Block, 't:', ',', 1);
  Template := StringReplace(Block2, 't:', '', [rfIgnoreCase]);
  Template := StringReplace(Template, ',', '', []);

  block2 := ExtractBlock(Block, 'n:', ']', 1);
  Name := StringReplace(Block2, 'n:', '', [rfIgnoreCase]);
  Name := StringReplace(Name, ']', '', []);

  embedded := RemoveBlock(fgas, '[', ']', false, 1);
  SubComponents.ParseGas(embedded);
  for i := subcomponents.Count - 1 downto 0 do
  begin
    subgas := TGasComponent(Subcomponents.Items[i]);
    subgas.parent := Name;

    if Subgas.IsExcluded then
    begin
      fgas := Stringreplace(fgas, subgas.Gas, '#t:' + subgas.Template + ',n:' + subgas.name + '#', [rfignorecase]);
    end
    else
    begin
      if pos('#childcomponents#', LowerCase(fgas)) = 0 then
      begin
        fgas := Stringreplace(fgas, subgas.Gas, '#childcomponents#', [rfignorecase]);

      end
      else
        fgas := Stringreplace(fgas, subgas.Gas, '', [rfignorecase]);
    end;
  end;

end;

function TGasComponent.DoesPropertyExist(prop: string): boolean;

var
  block             : string;
begin
  result := false;

  block := ExtractGasNameValue(CommentFreeGas, prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, ';' + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #10 + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #9 + prop);
  if trimwhitespace(block) = '' then
    exit;

  //  block := ExtractBlock(CommentFreeGas, ' ' + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, ';' + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, #10 + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, #9 + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    exit;
  if pos('=', block) = 0 then exit;
  result := true;
end;

function TGasComponent.GetPropertyValue(prop: string): string;
var
  block             : string;
begin

  // doesn't work right for:
  //  #9#9#9text="test";
  //  [b]type=text;

  result := '';
  block := ExtractGasNameValue(CommentFreeGas, prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, ';' + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #10 + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #9 + prop);
  //  if trimwhitespace(block) = '' then
  //    exit;
  block := trimwhitespace(block);
  if block = '' then exit;

  {  block := ExtractBlock(CommentFreeGas, ' ' + prop, ';', 1);
    if trim(block) = '' then
      block := ExtractBlock(CommentFreeGas, ';' + prop, ';', 1);
    if trim(block) = '' then
      block := ExtractBlock(CommentFreeGas, #10 + prop, ';', 1);
    if trim(block) = '' then
    begin
      block := ExtractBlock(CommentFreeGas, #9 + prop, ';', 1);
      if block <> '' then
        Delete(block, 1, 1);              //remove the #9
    end;
    if trim(block) = '' then
      exit;}
//  block := trimwhitespace(block);
  if pos('=', block) = 0 then exit;
  block := RemoveBlock(block, prop, '=');

  Block := stringReplace(block, ';', '', [rfReplaceall]);

  result := trimwhitespace(block);

  //hack
  if length(result) > 1 then
  begin
    if (copy(result, 1, 1) = '#') and (copy(result, length(result), 1) = '#') then result := '';

  end;

end;

procedure TGasComponent.UpdatePropertyToVariable(prop: string; value: string);
var
  block             : string;
  oldblock          : string;
begin
  //try bProp, ;Prop, #10Prop

  block := ExtractGasNameValue(CommentFreeGas, prop, true);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, ';' + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #10 + prop);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractGasNameValue(CommentFreeGas, #9 + prop);
  if trimwhitespace(block) = '' then
    exit;

  //  block := ExtractBlock(CommentFreeGas, ' ' + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, ';' + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, #10 + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    block := ExtractBlock(CommentFreeGas, #9 + prop, ';', 1);
  //  if trimwhitespace(block) = '' then
  //    exit;
  oldblock := block;

  if pos('=', block) = 0 then exit;

  block := RemoveBlock(block, '=', ';');
  block := block + '= ' + value + ';';

  FGas := StringReplace(FGas, oldblock, block, [rfignoreCase]);
end;

constructor TGasComponent.create;
begin
  inherited Create;
  SubComponents := TGasComponentList.create(false);
  SubComponents.ChildComponentList := true;
end;

destructor TGasComponent.destroy;
var
  i                 : integer;
begin
  for i := subcomponents.count - 1 downto 0 do
    SubComponents.Items[i].Free;
  SubComponents.Free;

  inherited destroy;
end;

function TGasComponent.IsExcluded: boolean;
begin
  if pos(' ' + uppercase(Template) + ' ', EXCLUDE_CHILDREN_COMPONENT_TYPES) > 0 then
    result := true
  else
    result := false;
end;

procedure TGasComponent.ReInsertExcludedChildren;
var
  I                 : integer;
  subgas            : TGasComponent;
begin
  for i := subcomponents.count - 1 downto 0 do
  begin
    subgas := TGasComponent(subcomponents.Items[i]);
    Subcomponents.Remove(subgas);
    fgas := Stringreplace(fgas, '#t:' + subgas.Template + ',n:' + subgas.name + '#', subgas.Gas, [rfignorecase]);
    subgas.free;
  end;
end;
end.

