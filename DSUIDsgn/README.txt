Ultima VI Project
User Interface Designer
Presented by Archon (www.u6archon.com)


PURPOSE:
---------------------------------------------------------
The U6P Interface Designer has been created to make it easy to visually layout your custom Dungeon Siege user interfaces. With the ability to move components around and immediately see a representation of how it will look, it's ideally suited for not only modifying existing interfaces but for creating interfaces from scratch.



REQUIREMENTS:
---------------------------------------------------------
Dungeon Siege Toolkit (DSTK) (www.dungeonsiege.com)
Dungeon Siege (www.dungeonsiege.com)
Windows 9x/2000/XP (tested on 2000 and XP)



INSTALLATION:
---------------------------------------------------------
To install, run the setup program.  A shortcut should be installed in the start menu and the following directory structure created:

UIDesigner
|
|-----templates
|-----texturecache

RUNNING:
---------------------------------------------------------
To launch the UI Designer, double+click U6PID.exe in the root directory or launch from the Dungeon Siege Toolkit folder on the start menu.


HOW TO USE:
---------------------------------------------------------
Tutorials are available at http://www.u6archon.com


SUPPORT:
---------------------------------------------------------
A forum for UIDesigner questions has been set up at http://dynamic6.gamespy.com/~archon/phpBB2


CREDITS:
---------------------------------------------------------

Thanks go out to the following:

  Ultima 6 Archon Team (www.u6archon.com)
  Bjorn XX
  Dan
  Major Hostility
  non`
  Tiberius Moongazer
  Wind Walker
  Wizardo55
  Xaa
  xmen90s
  

Dungeon Siege Community:

  Gas Powered Games (www.dungeonsiege.com)
  Unkle Ernsie and all the folks on Siegworks.org (www.siegeworks.org)
  GameEditing.net (site no longer active)
  Planet Dungeon Siege (www.planetdungeonsiege.com)
  Siege Network (www.siegenetwork.com)
  Dungeon Siege Heaven (ds.heavengames.com)

Delphi Resources:

  Borland International (www.borland.com)
  JVCL (Project Jedi) Component Team (jvcl.sourceforge.net)
  Jan Verhoeven for the janSticker component (www.jansfreeware.com)
  Synedit Component Team (synedit.sourceforge.net)
  Mike Lischke for the GraphicEx library (www.delphi-gems.com)


And finally... 

  My Wife for putting up with this quirky obsession of mine that is called Dungeon Siege. =P




DISCLAIMER
---------------------------------------------------------
Dungeon Siege is a registered trademark of Gas Powered Games, Inc. and Microsoft, Inc.
Ultima is a registered trademark of Origin Systems and EA
