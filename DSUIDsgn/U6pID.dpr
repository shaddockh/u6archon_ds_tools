(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*)
program U6pID;

uses
  Forms,
  frmSpellbook in 'frmSpellbook.pas' {frmUIDesigner},
  uUIHelper in 'uUIHelper.pas',
  uFrmAbout in 'uFrmAbout.pas' {AboutBox},
  uFrmOptions in 'uFrmOptions.pas' {frmOptions},
  uFrmSplash in 'uFrmSplash.pas' {frmSplash},
  uFrmTextureBrowser in 'uFrmTextureBrowser.pas' {frmTextureBrowser},
  uFrmUVCoordWizard in 'uFrmUVCoordWizard.pas' {frmUVCoordWizard};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Dungeon Siege UI Designer';
  Application.CreateForm(TfrmUIDesigner, frmUIDesigner);

//  if ParamCount > 0 then
//  begin
//     frmUIDesigner.LoadGasFile(ParamStr(1));
//  end
//  else
//     Application.CreateForm(TfrmSplash, frmSplash);
  Application.Run;
end.
