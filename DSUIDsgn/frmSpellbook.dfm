object frmUIDesigner: TfrmUIDesigner
  Left = 108
  Top = 36
  Width = 867
  Height = 710
  Caption = 'U6P Interface Designer - By Frilly Wumpus (Shaddock)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnMouseWheel = FormMouseWheel
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 185
    Top = 29
    Width = 4
    Height = 614
    Cursor = crHSplit
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 643
    Width = 859
    Height = 21
    Panels = <>
    SimplePanel = False
  end
  object ToolBar2: TToolBar
    Left = 0
    Top = 0
    Width = 859
    Height = 29
    Caption = 'ToolBar2'
    Flat = True
    Images = ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object btnNew: TToolButton
      Left = 0
      Top = 0
      Action = actNewInterface
    end
    object ToolButton1: TToolButton
      Left = 23
      Top = 0
      Action = actOpenGas
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton4: TToolButton
      Left = 46
      Top = 0
      Action = actSaveGas
    end
    object ToolButton18: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton18'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton19: TToolButton
      Left = 77
      Top = 0
      Action = actUserTool1
    end
    object ToolButton20: TToolButton
      Left = 100
      Top = 0
      Action = actUserTool2
    end
    object ToolButton21: TToolButton
      Left = 123
      Top = 0
      Action = actUserTool3
    end
    object ToolButton22: TToolButton
      Left = 146
      Top = 0
      Action = actUserTool4
    end
    object ToolButton23: TToolButton
      Left = 169
      Top = 0
      Action = actUserTool5
    end
    object ToolButton45: TToolButton
      Left = 192
      Top = 0
      Width = 8
      Caption = 'ToolButton45'
      ImageIndex = 12
      Style = tbsSeparator
    end
    object ToolButton44: TToolButton
      Left = 200
      Top = 0
      Action = actTextured
      Style = tbsCheck
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 29
    Width = 185
    Height = 614
    Align = alLeft
    Caption = 'Panel3'
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 1
      Top = 462
      Width = 183
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object Label6: TLabel
      Left = 1
      Top = 1
      Width = 183
      Height = 16
      Align = alTop
      Caption = '    Properties'
      Color = clAppWorkSpace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold, fsItalic]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
    end
    object JvInspector1: TJvInspector
      Left = 1
      Top = 17
      Width = 183
      Height = 445
      Align = alClient
      BandWidth = 250
      Divider = 146
      ItemHeight = 16
      Painter = JvInspectorDotNETPainter1
      AfterItemCreate = JvInspector1AfterItemCreate
      OnDataValueChanged = JvInspector1DataValueChanged
    end
    object Panel2: TPanel
      Left = 1
      Top = 465
      Width = 183
      Height = 148
      Align = alBottom
      TabOrder = 1
      object Label5: TLabel
        Left = 1
        Top = 1
        Width = 181
        Height = 16
        Align = alTop
        Caption = '    Element Heirarchy'
        Color = clAppWorkSpace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindow
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object tvElementHeirarchy: TTreeView
        Left = 1
        Top = 17
        Width = 181
        Height = 130
        Align = alClient
        HideSelection = False
        Images = ImageList1
        Indent = 19
        MultiSelect = True
        PopupMenu = mnuComponentPopup
        ReadOnly = True
        RowSelect = True
        ShowRoot = False
        SortType = stText
        TabOrder = 0
        OnChange = tvElementHeirarchyChange
      end
    end
  end
  object Panel1: TPanel
    Left = 189
    Top = 29
    Width = 670
    Height = 614
    Align = alClient
    TabOrder = 3
    object Splitter2: TSplitter
      Left = 1
      Top = 417
      Width = 668
      Height = 5
      Cursor = crVSplit
      Align = alTop
    end
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 668
      Height = 16
      Align = alTop
      Alignment = taRightJustify
      Caption = 'Form Designer    '
      Color = clAppWorkSpace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold, fsItalic]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
    end
    object PageControl4: TPageControl
      Left = 1
      Top = 422
      Width = 668
      Height = 191
      ActivePage = TabSheet3
      Align = alClient
      TabIndex = 0
      TabOrder = 0
      TabPosition = tpBottom
      object TabSheet3: TTabSheet
        Caption = 'Code'
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 660
          Height = 16
          Align = alTop
          Alignment = taRightJustify
          Caption = 'Element Code    '
          Color = clAppWorkSpace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          ParentColor = False
          ParentFont = False
          Layout = tlCenter
        end
        object rtfElementCode2: TJvHLEditor
          Left = 120
          Top = 24
          Width = 660
          Height = 72
          Cursor = crIBeam
          GutterWidth = 0
          RightMarginColor = clSilver
          Completion.ItemHeight = 13
          Completion.Interval = 800
          Completion.ListBoxStyle = lbStandard
          Completion.CaretChar = '|'
          Completion.CRLF = '/n'
          Completion.Separator = '='
          TabStops = '3 5'
          BracketHighlighting.StringEscape = #39#39
          SelForeColor = clHighlightText
          SelBackColor = clHighlight
          OnChange = rtfElementCode2Change
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabStop = True
          UseDockManager = False
          Highlighter = hlCBuilder
          Colors.Comment.Style = [fsItalic]
          Colors.Comment.ForeColor = clTeal
          Colors.Comment.BackColor = clWindow
          Colors.Number.ForeColor = clNavy
          Colors.Number.BackColor = clWindow
          Colors.Strings.ForeColor = clPurple
          Colors.Strings.BackColor = clWindow
          Colors.Symbol.ForeColor = clBlue
          Colors.Symbol.BackColor = clWindow
          Colors.Reserved.Style = [fsBold]
          Colors.Reserved.ForeColor = clWindowText
          Colors.Reserved.BackColor = clWindow
          Colors.Identifier.ForeColor = clWindowText
          Colors.Identifier.BackColor = clWindow
          Colors.Preproc.ForeColor = clNavy
          Colors.Preproc.BackColor = clWindow
          Colors.FunctionCall.ForeColor = clWindowText
          Colors.FunctionCall.BackColor = clWindow
          Colors.Declaration.ForeColor = clWindowText
          Colors.Declaration.BackColor = clWindow
          Colors.Statement.Style = [fsBold]
          Colors.Statement.ForeColor = clWindowText
          Colors.Statement.BackColor = clWindow
          Colors.PlainText.ForeColor = clWindowText
          Colors.PlainText.BackColor = clWindow
        end
        object rtfElementCode: TSynEdit
          Left = 0
          Top = 16
          Width = 660
          Height = 149
          Cursor = crIBeam
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          OnKeyUp = rtfElementCodeKeyUp
          Gutter.Font.Charset = DEFAULT_CHARSET
          Gutter.Font.Color = clWindowText
          Gutter.Font.Height = -11
          Gutter.Font.Name = 'Terminal'
          Gutter.Font.Style = []
          Highlighter = SynCppSyn1
          Keystrokes = <
            item
              Command = ecUp
              ShortCut = 38
            end
            item
              Command = ecSelUp
              ShortCut = 8230
            end
            item
              Command = ecScrollUp
              ShortCut = 16422
            end
            item
              Command = ecDown
              ShortCut = 40
            end
            item
              Command = ecSelDown
              ShortCut = 8232
            end
            item
              Command = ecScrollDown
              ShortCut = 16424
            end
            item
              Command = ecLeft
              ShortCut = 37
            end
            item
              Command = ecSelLeft
              ShortCut = 8229
            end
            item
              Command = ecWordLeft
              ShortCut = 16421
            end
            item
              Command = ecSelWordLeft
              ShortCut = 24613
            end
            item
              Command = ecRight
              ShortCut = 39
            end
            item
              Command = ecSelRight
              ShortCut = 8231
            end
            item
              Command = ecWordRight
              ShortCut = 16423
            end
            item
              Command = ecSelWordRight
              ShortCut = 24615
            end
            item
              Command = ecPageDown
              ShortCut = 34
            end
            item
              Command = ecSelPageDown
              ShortCut = 8226
            end
            item
              Command = ecPageBottom
              ShortCut = 16418
            end
            item
              Command = ecSelPageBottom
              ShortCut = 24610
            end
            item
              Command = ecPageUp
              ShortCut = 33
            end
            item
              Command = ecSelPageUp
              ShortCut = 8225
            end
            item
              Command = ecPageTop
              ShortCut = 16417
            end
            item
              Command = ecSelPageTop
              ShortCut = 24609
            end
            item
              Command = ecLineStart
              ShortCut = 36
            end
            item
              Command = ecSelLineStart
              ShortCut = 8228
            end
            item
              Command = ecEditorTop
              ShortCut = 16420
            end
            item
              Command = ecSelEditorTop
              ShortCut = 24612
            end
            item
              Command = ecLineEnd
              ShortCut = 35
            end
            item
              Command = ecSelLineEnd
              ShortCut = 8227
            end
            item
              Command = ecEditorBottom
              ShortCut = 16419
            end
            item
              Command = ecSelEditorBottom
              ShortCut = 24611
            end
            item
              Command = ecToggleMode
              ShortCut = 45
            end
            item
              Command = ecCopy
              ShortCut = 16429
            end
            item
              Command = ecCut
              ShortCut = 8238
            end
            item
              Command = ecPaste
              ShortCut = 8237
            end
            item
              Command = ecDeleteChar
              ShortCut = 46
            end
            item
              Command = ecDeleteLastChar
              ShortCut = 8
            end
            item
              Command = ecDeleteLastChar
              ShortCut = 8200
            end
            item
              Command = ecDeleteLastWord
              ShortCut = 16392
            end
            item
              Command = ecUndo
              ShortCut = 32776
            end
            item
              Command = ecRedo
              ShortCut = 40968
            end
            item
              Command = ecLineBreak
              ShortCut = 13
            end
            item
              Command = ecLineBreak
              ShortCut = 8205
            end
            item
              Command = ecTab
              ShortCut = 9
            end
            item
              Command = ecShiftTab
              ShortCut = 8201
            end
            item
              Command = ecContextHelp
              ShortCut = 16496
            end
            item
              Command = ecSelectAll
              ShortCut = 16449
            end
            item
              Command = ecCopy
              ShortCut = 16451
            end
            item
              Command = ecPaste
              ShortCut = 16470
            end
            item
              Command = ecCut
              ShortCut = 16472
            end
            item
              Command = ecBlockIndent
              ShortCut = 24649
            end
            item
              Command = ecBlockUnindent
              ShortCut = 24661
            end
            item
              Command = ecLineBreak
              ShortCut = 16461
            end
            item
              Command = ecInsertLine
              ShortCut = 16462
            end
            item
              Command = ecDeleteWord
              ShortCut = 16468
            end
            item
              Command = ecDeleteLine
              ShortCut = 16473
            end
            item
              Command = ecDeleteEOL
              ShortCut = 24665
            end
            item
              Command = ecUndo
              ShortCut = 16474
            end
            item
              Command = ecRedo
              ShortCut = 24666
            end
            item
              Command = ecGotoMarker0
              ShortCut = 16432
            end
            item
              Command = ecGotoMarker1
              ShortCut = 16433
            end
            item
              Command = ecGotoMarker2
              ShortCut = 16434
            end
            item
              Command = ecGotoMarker3
              ShortCut = 16435
            end
            item
              Command = ecGotoMarker4
              ShortCut = 16436
            end
            item
              Command = ecGotoMarker5
              ShortCut = 16437
            end
            item
              Command = ecGotoMarker6
              ShortCut = 16438
            end
            item
              Command = ecGotoMarker7
              ShortCut = 16439
            end
            item
              Command = ecGotoMarker8
              ShortCut = 16440
            end
            item
              Command = ecGotoMarker9
              ShortCut = 16441
            end
            item
              Command = ecSetMarker0
              ShortCut = 24624
            end
            item
              Command = ecSetMarker1
              ShortCut = 24625
            end
            item
              Command = ecSetMarker2
              ShortCut = 24626
            end
            item
              Command = ecSetMarker3
              ShortCut = 24627
            end
            item
              Command = ecSetMarker4
              ShortCut = 24628
            end
            item
              Command = ecSetMarker5
              ShortCut = 24629
            end
            item
              Command = ecSetMarker6
              ShortCut = 24630
            end
            item
              Command = ecSetMarker7
              ShortCut = 24631
            end
            item
              Command = ecSetMarker8
              ShortCut = 24632
            end
            item
              Command = ecSetMarker9
              ShortCut = 24633
            end
            item
              Command = ecNormalSelect
              ShortCut = 24654
            end
            item
              Command = ecColumnSelect
              ShortCut = 24643
            end
            item
              Command = ecLineSelect
              ShortCut = 24652
            end
            item
              Command = ecMatchBracket
              ShortCut = 24642
            end>
          TabWidth = 4
          WantTabs = True
          OnChange = rtfElementCode2Change
        end
      end
      object tabGeneratedCode: TTabSheet
        Caption = 'Preview Generated Code'
        ImageIndex = 2
        OnShow = tabGeneratedCodeShow
        object Label3: TLabel
          Left = 545
          Top = 0
          Width = 115
          Height = 16
          Align = alTop
          Alignment = taRightJustify
          Caption = 'Generated Code    '
          Color = clAppWorkSpace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          ParentColor = False
          ParentFont = False
          Layout = tlCenter
        end
        object rtf_code: TSynEdit
          Left = 0
          Top = 16
          Width = 660
          Height = 149
          Cursor = crIBeam
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Gutter.Font.Charset = DEFAULT_CHARSET
          Gutter.Font.Color = clWindowText
          Gutter.Font.Height = -11
          Gutter.Font.Name = 'Terminal'
          Gutter.Font.Style = []
          Highlighter = SynCppSyn1
          Keystrokes = <
            item
              Command = ecUp
              ShortCut = 38
            end
            item
              Command = ecSelUp
              ShortCut = 8230
            end
            item
              Command = ecScrollUp
              ShortCut = 16422
            end
            item
              Command = ecDown
              ShortCut = 40
            end
            item
              Command = ecSelDown
              ShortCut = 8232
            end
            item
              Command = ecScrollDown
              ShortCut = 16424
            end
            item
              Command = ecLeft
              ShortCut = 37
            end
            item
              Command = ecSelLeft
              ShortCut = 8229
            end
            item
              Command = ecWordLeft
              ShortCut = 16421
            end
            item
              Command = ecSelWordLeft
              ShortCut = 24613
            end
            item
              Command = ecRight
              ShortCut = 39
            end
            item
              Command = ecSelRight
              ShortCut = 8231
            end
            item
              Command = ecWordRight
              ShortCut = 16423
            end
            item
              Command = ecSelWordRight
              ShortCut = 24615
            end
            item
              Command = ecPageDown
              ShortCut = 34
            end
            item
              Command = ecSelPageDown
              ShortCut = 8226
            end
            item
              Command = ecPageBottom
              ShortCut = 16418
            end
            item
              Command = ecSelPageBottom
              ShortCut = 24610
            end
            item
              Command = ecPageUp
              ShortCut = 33
            end
            item
              Command = ecSelPageUp
              ShortCut = 8225
            end
            item
              Command = ecPageTop
              ShortCut = 16417
            end
            item
              Command = ecSelPageTop
              ShortCut = 24609
            end
            item
              Command = ecLineStart
              ShortCut = 36
            end
            item
              Command = ecSelLineStart
              ShortCut = 8228
            end
            item
              Command = ecEditorTop
              ShortCut = 16420
            end
            item
              Command = ecSelEditorTop
              ShortCut = 24612
            end
            item
              Command = ecLineEnd
              ShortCut = 35
            end
            item
              Command = ecSelLineEnd
              ShortCut = 8227
            end
            item
              Command = ecEditorBottom
              ShortCut = 16419
            end
            item
              Command = ecSelEditorBottom
              ShortCut = 24611
            end
            item
              Command = ecToggleMode
              ShortCut = 45
            end
            item
              Command = ecCopy
              ShortCut = 16429
            end
            item
              Command = ecCut
              ShortCut = 8238
            end
            item
              Command = ecPaste
              ShortCut = 8237
            end
            item
              Command = ecDeleteChar
              ShortCut = 46
            end
            item
              Command = ecDeleteLastChar
              ShortCut = 8
            end
            item
              Command = ecDeleteLastChar
              ShortCut = 8200
            end
            item
              Command = ecDeleteLastWord
              ShortCut = 16392
            end
            item
              Command = ecUndo
              ShortCut = 32776
            end
            item
              Command = ecRedo
              ShortCut = 40968
            end
            item
              Command = ecLineBreak
              ShortCut = 13
            end
            item
              Command = ecLineBreak
              ShortCut = 8205
            end
            item
              Command = ecTab
              ShortCut = 9
            end
            item
              Command = ecShiftTab
              ShortCut = 8201
            end
            item
              Command = ecContextHelp
              ShortCut = 16496
            end
            item
              Command = ecSelectAll
              ShortCut = 16449
            end
            item
              Command = ecCopy
              ShortCut = 16451
            end
            item
              Command = ecPaste
              ShortCut = 16470
            end
            item
              Command = ecCut
              ShortCut = 16472
            end
            item
              Command = ecBlockIndent
              ShortCut = 24649
            end
            item
              Command = ecBlockUnindent
              ShortCut = 24661
            end
            item
              Command = ecLineBreak
              ShortCut = 16461
            end
            item
              Command = ecInsertLine
              ShortCut = 16462
            end
            item
              Command = ecDeleteWord
              ShortCut = 16468
            end
            item
              Command = ecDeleteLine
              ShortCut = 16473
            end
            item
              Command = ecDeleteEOL
              ShortCut = 24665
            end
            item
              Command = ecUndo
              ShortCut = 16474
            end
            item
              Command = ecRedo
              ShortCut = 24666
            end
            item
              Command = ecGotoMarker0
              ShortCut = 16432
            end
            item
              Command = ecGotoMarker1
              ShortCut = 16433
            end
            item
              Command = ecGotoMarker2
              ShortCut = 16434
            end
            item
              Command = ecGotoMarker3
              ShortCut = 16435
            end
            item
              Command = ecGotoMarker4
              ShortCut = 16436
            end
            item
              Command = ecGotoMarker5
              ShortCut = 16437
            end
            item
              Command = ecGotoMarker6
              ShortCut = 16438
            end
            item
              Command = ecGotoMarker7
              ShortCut = 16439
            end
            item
              Command = ecGotoMarker8
              ShortCut = 16440
            end
            item
              Command = ecGotoMarker9
              ShortCut = 16441
            end
            item
              Command = ecSetMarker0
              ShortCut = 24624
            end
            item
              Command = ecSetMarker1
              ShortCut = 24625
            end
            item
              Command = ecSetMarker2
              ShortCut = 24626
            end
            item
              Command = ecSetMarker3
              ShortCut = 24627
            end
            item
              Command = ecSetMarker4
              ShortCut = 24628
            end
            item
              Command = ecSetMarker5
              ShortCut = 24629
            end
            item
              Command = ecSetMarker6
              ShortCut = 24630
            end
            item
              Command = ecSetMarker7
              ShortCut = 24631
            end
            item
              Command = ecSetMarker8
              ShortCut = 24632
            end
            item
              Command = ecSetMarker9
              ShortCut = 24633
            end
            item
              Command = ecNormalSelect
              ShortCut = 24654
            end
            item
              Command = ecColumnSelect
              ShortCut = 24643
            end
            item
              Command = ecLineSelect
              ShortCut = 24652
            end
            item
              Command = ecMatchBracket
              ShortCut = 24642
            end>
          ReadOnly = True
          TabWidth = 4
          WantTabs = True
        end
      end
      object tabTextureBrowser: TTabSheet
        Caption = 'Texture Browser'
        ImageIndex = 2
        OnShow = tabTextureBrowserShow
        object Splitter4: TSplitter
          Left = 169
          Top = 16
          Width = 3
          Height = 136
          Cursor = crHSplit
        end
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 660
          Height = 16
          Align = alTop
          Alignment = taRightJustify
          Caption = 'Texture Browser    '
          Color = clAppWorkSpace
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindow
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          ParentColor = False
          ParentFont = False
          Layout = tlCenter
        end
        object JvThumbView1: TJvThumbView
          Left = 172
          Top = 16
          Width = 488
          Height = 136
          HorzScrollBar.Smooth = True
          HorzScrollBar.Tracking = True
          VertScrollBar.Increment = 50
          VertScrollBar.Tracking = True
          Align = alClient
          BevelEdges = []
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Color = clWindow
          ParentColor = False
          PopupMenu = mnuAssignTexture
          TabOrder = 0
          TabStop = True
          OnContextPopup = JvThumbView1ContextPopup
          OnMouseWheel = JvThumbView1MouseWheel
          AlignView = vtCenter
          AutoScrolling = False
          ThumbGap = 1
          AutoHandleKeyb = True
          MinMemory = False
          MaxWidth = 172
          MaxHeight = 172
          Size = 100
          ScrollMode = smVertical
          Sorted = True
          OnStartScanning = JvThumbView1StartScanning
          OnStopScanning = JvThumbView1StopScanning
          OnScanProgress = JvThumbView1ScanProgress
          AsButtons = False
          Filter = 'Photoshop Files (*.psd)|*.psd|Raw Files (*.raw)|*.raw'
          ThumbColor = clNone
          ShowShadow = False
          ShadowColor = clBlack
        end
        object prgTextures: TProgressBar
          Left = 0
          Top = 152
          Width = 660
          Height = 13
          Align = alBottom
          Min = 0
          Max = 100
          TabOrder = 1
          Visible = False
        end
        object Panel4: TPanel
          Left = 0
          Top = 16
          Width = 169
          Height = 136
          Align = alLeft
          Caption = 'Panel4'
          TabOrder = 2
          object JvDirectoryListBox1: TJvDirectoryListBox
            Left = 1
            Top = 1
            Width = 167
            Height = 134
            Align = alClient
            Directory = 'C:\'
            ItemHeight = 17
            ScrollBars = ssBoth
            TabOrder = 0
            OnChange = JvDirectoryListBox1Change
          end
        end
      end
    end
    object ToolBar1: TToolBar
      Left = 1
      Top = 17
      Width = 668
      Height = 28
      ButtonHeight = 28
      Caption = 'ToolBar1'
      Color = clBtnFace
      Flat = True
      Images = ImageList1
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object btn_Delete: TToolButton
        Left = 0
        Top = 0
        Action = actDeleteComponent
      end
      object btn_copy: TToolButton
        Left = 23
        Top = 0
        Action = actCopyComponents
      end
      object ToolButton17: TToolButton
        Left = 46
        Top = 0
        Action = actRefresh
      end
      object ToolButton2: TToolButton
        Left = 69
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 1
        Style = tbsSeparator
      end
      object btn_Window: TToolButton
        Left = 77
        Top = 0
        Action = actInsertButton
      end
      object btn_Button: TToolButton
        Left = 100
        Top = 0
        Action = actInsertCheckbox
      end
      object ToolButton9: TToolButton
        Left = 123
        Top = 0
        Action = actInsertRadioButton
      end
      object btn_radiobutton: TToolButton
        Left = 146
        Top = 0
        Action = actinsertCombobox
      end
      object ToolButton5: TToolButton
        Left = 169
        Top = 0
        Action = actInsertListbox
      end
      object ToolButton12: TToolButton
        Left = 192
        Top = 0
        Action = actInsertTab
      end
      object ToolButton13: TToolButton
        Left = 215
        Top = 0
        Action = actInsertText
      end
      object ToolButton14: TToolButton
        Left = 238
        Top = 0
        Action = actInsertTextBox
      end
      object btn_textbox: TToolButton
        Left = 261
        Top = 0
        Action = actInsertEditbox
      end
      object ToolButton43: TToolButton
        Left = 284
        Top = 0
        Action = actInsertChatbox
      end
      object ToolButton15: TToolButton
        Left = 307
        Top = 0
        Action = actInsertWindow
      end
      object ToolButton16: TToolButton
        Left = 330
        Top = 0
        Width = 8
        Caption = 'ToolButton16'
        ImageIndex = 19
        Style = tbsSeparator
      end
      object btn_checkbox: TToolButton
        Left = 338
        Top = 0
        Action = actInsertDialogBox
      end
      object btn_listbox: TToolButton
        Left = 361
        Top = 0
        Action = actInsertDockBar
      end
      object btn_text: TToolButton
        Left = 384
        Top = 0
        Action = actInsertGridbox
      end
      object btn_dialogbox: TToolButton
        Left = 407
        Top = 0
        Action = actInsertInfoSlot
      end
      object ToolButton3: TToolButton
        Left = 430
        Top = 0
        Action = actInsertItemSlot
      end
      object ToolButton6: TToolButton
        Left = 453
        Top = 0
        Action = actInsertListener
      end
      object ToolButton7: TToolButton
        Left = 476
        Top = 0
        Action = actInsertListReport
      end
      object ToolButton8: TToolButton
        Left = 499
        Top = 0
        Action = actInsertPopupMenu
      end
      object ToolButton10: TToolButton
        Left = 522
        Top = 0
        Action = actInsertSlider
      end
      object ToolButton11: TToolButton
        Left = 545
        Top = 0
        Action = actInsertStatusBar
      end
    end
    object ScrollBox1: TScrollBox
      Left = 1
      Top = 73
      Width = 668
      Height = 344
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alTop
      TabOrder = 2
      object scrollbox_Interface: TDSInterface
        Left = 0
        Top = 0
        Width = 640
        Height = 480
        Color = clBtnHighlight
        PopupMenu = mnuComponentPopup
        TabOrder = 0
        OnClick = scrollbox_InterfaceClick
        OnEnter = scrollbox_InterfaceEnter
        OnExit = scrollbox_InterfaceExit
        isinterface = True
        intended_resolution_height = 480
        intended_resolution_width = 640
        disable_camera = False
        modal = False
        passive_modal = False
        interface_name = 'Untitled'
        designer_ref_img_x = 0
        designer_ref_img_y = 0
        OnHeirarchyChange = Element_HeirarchyChange
      end
    end
    object ToolBar3: TToolBar
      Left = 1
      Top = 45
      Width = 668
      Height = 28
      ButtonHeight = 25
      Caption = 'ToolBar1'
      Color = clBtnFace
      Flat = True
      Images = ImageList1
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      object ToolButton24: TToolButton
        Left = 0
        Top = 0
        Action = actAlignLeft
      end
      object ToolButton25: TToolButton
        Left = 23
        Top = 0
        Action = actAlignTop
      end
      object ToolButton26: TToolButton
        Left = 46
        Top = 0
        Action = actAlignRight
      end
      object ToolButton27: TToolButton
        Left = 69
        Top = 0
        Action = actAlignBottom
      end
      object ToolButton28: TToolButton
        Left = 92
        Top = 0
        Width = 8
        Caption = 'ToolButton28'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object ToolButton29: TToolButton
        Left = 100
        Top = 0
        Action = actBringToFront
      end
      object ToolButton30: TToolButton
        Left = 123
        Top = 0
        Action = actSendToBack
      end
      object ToolButton31: TToolButton
        Left = 146
        Top = 0
        Width = 8
        Caption = 'ToolButton31'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object ToolButton32: TToolButton
        Left = 154
        Top = 0
        Action = actMakeSameHeight
      end
      object ToolButton33: TToolButton
        Left = 177
        Top = 0
        Action = actMakeSameWidth
      end
      object ToolButton34: TToolButton
        Left = 200
        Top = 0
        Action = actMakeSameSize
      end
      object ToolButton35: TToolButton
        Left = 223
        Top = 0
        Width = 8
        Caption = 'ToolButton35'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object ToolButton36: TToolButton
        Left = 231
        Top = 0
        Action = actStackHorizontally
      end
      object ToolButton37: TToolButton
        Left = 254
        Top = 0
        Action = actStackVertically
      end
      object ToolButton38: TToolButton
        Left = 277
        Top = 0
        Width = 8
        Caption = 'ToolButton38'
        ImageIndex = 0
        Style = tbsSeparator
      end
      object ToolButton39: TToolButton
        Left = 285
        Top = 0
        Action = actShiftLeft
      end
      object ToolButton40: TToolButton
        Left = 308
        Top = 0
        Action = actShiftTop
      end
      object ToolButton41: TToolButton
        Left = 331
        Top = 0
        Action = actShiftRight
      end
      object ToolButton42: TToolButton
        Left = 354
        Top = 0
        Action = actShiftDown
      end
      object ToolButton47: TToolButton
        Left = 377
        Top = 0
        Width = 8
        Caption = 'ToolButton47'
        ImageIndex = 27
        Style = tbsSeparator
      end
      object ToolButton46: TToolButton
        Left = 385
        Top = 0
        Action = actUVCoordHelper
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = ImageList1
    Left = 96
    Top = 16
    object File1: TMenuItem
      Caption = '&File'
      object New1: TMenuItem
        Action = actNewInterface
      end
      object Open1: TMenuItem
        Action = actOpenGas
      end
      object Save1: TMenuItem
        Action = actSaveGas
      end
      object SaveAs1: TMenuItem
        Action = actSaveAs
      end
      object ReloadCurrentFile1: TMenuItem
        Action = actReload
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
    end
    object mnuEdit: TMenuItem
      Caption = '&Edit'
      object Copy1: TMenuItem
        Action = actCopyComponents
      end
      object Delete1: TMenuItem
        Action = actDeleteComponent
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object SendToBack1: TMenuItem
        Action = actSendToBack
      end
      object BringToFront1: TMenuItem
        Action = actBringToFront
      end
    end
    object Components1: TMenuItem
      Caption = '&Components'
      object Insertbutton1: TMenuItem
        Action = actInsertButton
      end
      object NewRadioButton1: TMenuItem
        Action = actInsertRadioButton
      end
      object NewChatbox1: TMenuItem
        Action = actInsertChatbox
      end
      object NewCheckbox1: TMenuItem
        Action = actInsertCheckbox
      end
      object NewCombobox1: TMenuItem
        Action = actinsertCombobox
      end
      object NewDialogBox1: TMenuItem
        Action = actInsertDialogBox
      end
      object NewDockbar1: TMenuItem
        Action = actInsertDockBar
      end
      object NewEditBox1: TMenuItem
        Action = actInsertEditbox
      end
      object NewGridbox1: TMenuItem
        Action = actInsertGridbox
      end
      object NewInfoslot1: TMenuItem
        Action = actInsertInfoSlot
      end
      object NewItemSlot1: TMenuItem
        Action = actInsertItemSlot
      end
      object NewListener1: TMenuItem
        Action = actInsertListener
      end
      object NewListbox1: TMenuItem
        Action = actInsertListbox
      end
      object NewListReport1: TMenuItem
        Action = actInsertListReport
      end
      object NewPopupMenu1: TMenuItem
        Action = actInsertPopupMenu
      end
      object NewStatusbar1: TMenuItem
        Action = actInsertStatusBar
      end
      object NewText1: TMenuItem
        Action = actInsertText
      end
      object NewTextBox1: TMenuItem
        Action = actInsertTextBox
      end
      object NewSlider1: TMenuItem
        Action = actInsertSlider
      end
      object NewTab1: TMenuItem
        Action = actInsertTab
      end
      object Insertwindow1: TMenuItem
        Action = actInsertWindow
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Recalculatedraworder1: TMenuItem
        Caption = 'Recalculate draw_order'
        OnClick = Recalculatedraworder1Click
      end
    end
    object ools1: TMenuItem
      Caption = '&Tools'
      object mnuLaunchDS: TMenuItem
        Caption = 'Launch Dungeon Siege'
        Enabled = False
        OnClick = mnuLaunchDSClick
      end
      object Refresh1: TMenuItem
        Action = actRefresh
      end
      object SetBackgroundReferenceImage1: TMenuItem
        Caption = 'Set Background Reference Image...'
        OnClick = SetBackgroundReferenceImage1Click
      end
      object RenderComponents1: TMenuItem
        Action = actTextured
      end
      object UVCoordinatesHelper1: TMenuItem
        Action = actUVCoordHelper
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object UserTool11: TMenuItem
        Action = actUserTool1
      end
      object UserTool21: TMenuItem
        Action = actUserTool2
      end
      object UserTool31: TMenuItem
        Action = actUserTool3
      end
      object UserTool41: TMenuItem
        Action = actUserTool4
        Caption = 'User Tool 4'
      end
      object UseTool51: TMenuItem
        Action = actUserTool5
        Hint = 'User Tool 5'
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object mnuOptions: TMenuItem
        Caption = '&Settings...'
        OnClick = mnuOptionsClick
      end
      object DumpInterfaceWithTokens1: TMenuItem
        Caption = 'Dump untokenized properties (Debug Tool)'
        OnClick = DumpInterfaceWithTokens1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About...'
        OnClick = About1Click
      end
      object ipoftheDay1: TMenuItem
        Caption = 'Tip of the Day...'
        OnClick = ipoftheDay1Click
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object utorials1: TMenuItem
        Caption = 'Tutorials...'
        ImageIndex = 45
        OnClick = utorials1Click
      end
      object DiscussionBoards1: TMenuItem
        Caption = 'Discussion Boards...'
        ImageIndex = 45
        OnClick = DiscussionBoards1Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'gas'
    Filter = 'UI Interface (*.gas)|*.gas'
    Left = 232
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'gas'
    Filter = 'UI Interface (*.gas)|*.gas'
    Title = 'Export Interface to GAS'
    Left = 256
  end
  object JvInspectorDotNETPainter1: TJvInspectorDotNETPainter
    DrawNameEndEllipsis = False
    Left = 288
  end
  object ImageList1: TImageList
    Left = 280
    Top = 528
    Bitmap = {
      494C01012E003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D0000000010020000000000000D0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF000000FF000000FF000000FF000000FF00000000FF0000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF000000FF000000FF000000FF00000000FF0000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000FF00000000FF000000FF000000FF0000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF00000000FF000000FF000000FF000000FF000000FF
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF00000000FF000000FF000000FF000000FF000000FF
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD000000000000000000BDBDBD0000000000BDBDBD0000000000BDBDBD000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF00000000FF000000FF000000FF0000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD000000000000000000BDBDBD0000000000BDBDBD0000000000BDBDBD000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FF00000000FF0000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BDBDBD000000000000000000BDBDBD00000000000000000000000000BDBD
      BD0000000000000000000000000000000000FF000000FF000000FF000000FF00
      000000FF0000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF00000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF00000000FF
      000000FF0000FF000000FF000000FF00000000FF0000FF000000FF000000FF00
      0000FF00000000FF000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF0000FF000000FF000000FF00
      0000FF00000000FF000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF0000FF000000FF000000FF00
      0000FF000000FF00000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF0000FF000000FF000000FF00
      000000FF000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000FF000000FF0000FF000000FF00000000FF000000FF0000FF000000FF00
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF000000FF0000FF000000FF00000000FF000000FF0000FF000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00D6D6D600D6D6D6000000
      0000D6D6D600BDBDBD0000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00D6D6D600D6D6D600D6D6
      D60000000000BDBDBD0000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE0000000000D6D6D600D6D6D6000000
      0000D6D6D600BDBDBD0000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00D6D6D600D6D6D600D6D6
      D60000000000BDBDBD0000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00F7F7
      F70000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00000000000000
      0000F7F7F700D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF0000000000EFEFEF000000
      0000F7F7F700D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF000000000000000000000000000000
      0000F7F7F700D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF0000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF0000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF0000000000EFEFEF000000
      0000F7F7F700D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF0000000000EFEFEF00EFEFEF00EFEF
      EF00F7F7F700D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF0000000000EFEFEF00EFEFEF00EFEF
      EF0000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF0000000000EFEFEF00EFEFEF00EFEF
      EF0000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00000000000000
      0000EFEFEF00D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF0000000000EFEFEF00EFEF
      EF00EFEFEF00D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF0000000000000000000000
      0000EFEFEF00D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF0000000000000000000000
      0000EFEFEF00D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF000000
      0000EFEFEF00D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF0000000000000000000000
      000000000000D6D6D60000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A42290000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A42290000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A42290000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A42290000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B630000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B630000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B630000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00D6D6D600D6D6D6000000
      0000D6D6D600BDBDBD00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF000000
      0000F7F7F700D6D6D600000000000000000000000000FFFFFF0084848400FFFF
      FF00FFFFFF00FFFFFF000000000084848400848484008484840000000000FFFF
      FF00FFFFFF00FFFFFF0084848400FFFFFF000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF0000000000EFEFEF000000
      0000F7F7F700D6D6D60000000000000000000000000084848400848484008484
      840084848400FFFFFF000000000084848400848484008484840000000000FFFF
      FF00848484008484840084848400848484000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00000000000000
      0000EFEFEF00D6D6D600000000000000000000000000FFFFFF0084848400FFFF
      FF00FFFFFF00FFFFFF000000000084848400848484008484840000000000FFFF
      FF00FFFFFF00FFFFFF0084848400FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF000000
      0000EFEFEF00D6D6D600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A42290000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B630000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0084848400848484000000000000000000FFFFFF00848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484008484
      840084848400848484000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000000000000848484000000000000000000FFFFFF0000000000000000000000
      00008484840000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF008484840084848400848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000000000000848484000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008484840000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000FFFFFF008484
      84008484840084848400848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000FFFFFF000000
      00000000000000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000FFFFFF000000
      00000000000000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF0000000000848484000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840000000000000000000000000000000000FFFFFF000000
      00000000000000000000848484000000000000000000FFFFFF00000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000848484000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000848484000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00848484000000
      0000FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484008484
      84008484840084848400848484008484840000000000FFFFFF00848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000008484
      8400000000000000000000000000000000000000000084848400848484008484
      8400000000008484840084848400848484000000000000000000000000000000
      0000848484008484840084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      84000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0000000000848484000000000000000000000000000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0000000000848484000000000000000000000000000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0000000000848484000000000000000000000000000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0000000000848484000000000000000000000000000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000008484
      840000000000FFFFFF0000000000848484000000000000000000000000000000
      0000FFFFFF000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484008484
      84000000000000000000000000000000000000000000FFFFFF00FFFFFF008484
      840000000000FFFFFF00FFFFFF00848484000000000000000000000000000000
      0000FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E7009C9C9C009C9C9C009C9C9C009C9C
      9C00C6C6C600DEDEDE00DEDEDE009C9C9C000000000000000000000000000000
      000000000000000000000000000000000000ADB5B500A5ADAD00949C9C00737B
      8400737B84006B7373006B737300525A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A5A5A5009C9C9C00DEDEDE00E7E7E700DEDEDE00CECE
      CE009C9C9C00BDBDBD00DEDEDE009C9C9C000000000000000000C6C6C600BDBD
      BD00BDBDBD00BDBDBD00BDBDBD009494940008080800ADADB500737B8400ADB5
      B500E7E7E700D6D6D6008C9494005A6363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E7009C9C9C00DEDEDE00E7E7E700E7E7E700E7E7E700F7F7
      F700E7E7E7009C9C9C00D6D6D6009C9C9C000000000000000000FFFFFF00E7E7
      E700E7E7E700E7E7E700000000000000000042424A00949C9C00848C8C003131
      3100101010008C8C8C00DEDEDE005A6363000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00C6C6C600E7E7E7009C9C9C0021212100212121007B7B
      7B00E7E7E700CECECE009C9C9C009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100E7E7E700000000008C8C8C00ADB5B500949C9C00525A5A001821
      21000808080010181800E7E7E700737B84000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C00C6C6C600E7E7E7004A4A4A0029292900101010002121
      2100E7E7E700DEDEDE009C9C9C009C9C9C000000000000000000FFFFFF006B4A
      310073523900E7E7E700000000000000000042424A00949C9C00525A63003942
      42002121290031313100949C9C00636B6B000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00C6C6C600E7E7E7007B7B7B006B6B6B00292929002121
      2100E7E7E700E7E7E7009C9C9C009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF00EFEFEF00BDBDBD0008080800949C9C00949C9C00525A
      5A00525A6300848C8C00737B84006B7373000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00BDBDBD00DEDEDE00DEDE
      DE00DEDEDE009C9C9C00C6C6C600DEDEDE00CECECE00636363005A5A5A00ADAD
      AD00E7E7E700E7E7E7009C9C9C009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF005252520000000000C6C6CE00ADB5B500949C9C00949C
      9C00949C9C00848C94008C949400737B84000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE00F7F7F700E7E7
      E70052525200949494009C9C9C00C6C6C600E7E7E700E7E7E700E7E7E700E7E7
      E700DEDEDE009C9C9C00EFEFEF009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF00EFEFEF00080808008C8C8C0073737300080808004242
      4A00949C9C0042424A000000000084848C000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE00D6D6D6005A5A
      5A000000000039393900CECECE009C9C9C00CECECE00C6C6C600C6C6C600C6C6
      C6009C9C9C00BDBDBD00FFFFFF009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF005A5A5A004A4A4A000808080000000000424242000000
      0000B5BDBD000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000FFFF
      FF00840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE007B7B7B002929
      2900A5A5A50031313100BDBDBD00F7F7F7009C9C9C009C9C9C009C9C9C009C9C
      9C00E7E7E700FFFFFF00FFFFFF009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE00E7E7E700CECE
      CE00EFEFEF00848484005A5A5A00E7E7E700DEDEDE009C9C9C00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100EFEFEF005A5A5A005A5A5A005A5A5A005A5A5A00EFEFEF00EFEF
      EF00C6C6C6000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE00F7F7F700F7F7
      F700F7F7F700F7F7F7008484840084848400DEDEDE009C9C9C00F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F7009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100F7F7F700F7F7F700F7F7F700F7F7F700E7E7E700848484004A4A
      4A00101010000000000000000000000000000000000000000000FFFFFF000000
      000000000000FFFFFF0000000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00CECECE00F7F7F700F7F7
      F700F7F7F700F7F7F700E7E7E700B5B5B500DEDEDE009C9C9C00F7F7F7000000
      00000000000000000000F7F7F7009C9C9C000000000000000000FFFFFF006B4A
      31006B4A3100F7F7F7005A5A5A005A5A5A00F7F7F700ADADAD00737373005A5A
      5A00424242000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00BDBDBD00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00BDBDBD009C9C9C00F7F7F700F7F7
      F70000000000F7F7F700F7F7F7009C9C9C000000000000000000FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700C6C6C600FFFFFF00ADAD
      AD00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E700C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000737373006B6B6B006B6B
      6B006B6B6B00ADADAD006B6B6B006B6B6B006B6B6B006B6B6B00ADADAD006B6B
      6B006B6B6B006B6B6B0063636300000000000000000000000000F7F7F700EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F7007B7B7B00000000000000000000000000000000006B6B6B00FFFF
      FF00FFFFFF00EFEFEF00ADADAD00000000000000000000000000EFFFFF00C6CE
      D600C6CED6008C949400C6CECE00C6CECE0094949C00C6CECE00C6CECE008C94
      9400BDC6CE00B5BDBD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700B5B5B5000000000000000000B5B5B500F7F7F700F7F7
      F700FFFFFF00EFEFEF00ADADAD00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00B5BDBD00FFFFFF00FFFFFF00B5BDC600EFFFFF00EFFFFF00ADB5
      B500EFFFFF00C6CECE0000000000000000000000000000000000D6D6D600CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00D6D6D600D6D6D600D6D6
      D600D6D6D600BDBDBD000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD00000000000000000000000000FFFFFF00C6CE
      D600C6CED60094949C00C6CED600C6CED6004A52C600424AAD00424AAD00424A
      AD00C6CED600A5ADAD0000000000000000000000000000000000F7F7F700E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00F7F7
      F700F7F7F700D6D6D600000000000000000000000000C6C6C600BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00ADADAD000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00ADB5B500FFFFFF00F7FFFF004A52C60042BDFF0042BDFF00424A
      AD00EFFFFF00C6CECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00F7F7F700D6D6D6000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD00000000000000000000000000FFFFFF00C6CE
      D600C6CED60094949C00C6CED600C6CED6004A5AF7004A52C6004A52C6004A52
      C600C6CED600A5ADAD0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00D6D6D6000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00B5BDBD00FFFFFF00FFFFFF00B5BDC600EFFFFF00EFFFFF00ADB5
      B500EFFFFF00C6CECE0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00D6D6D6000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00F7F7
      F700F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD00000000000000000000000000F7F7F700C6CE
      CE00C6CECE008C949400C6CECE00C6CECE00949C9C00C6CECE00C6CECE008C94
      9400C6CECE00A5ADAD0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF00ADAD
      AD00F7F7F700F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700F7F7
      F700B5B5B500EFEFEF00ADADAD00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00EFEFEF00FFFFFF00FFFFFF00E7E7E700FFFFFF00FFFFFF00D6D6
      D600FFFFFF00EFFFFF0000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00CECECE00000000000000000000000000C6C6C600BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00ADADAD000000000000000000ADADAD00FFFFFF000000
      0000ADADAD00F7F7F700F7F7F7000000000000000000F7F7F700F7F7F700B5B5
      B50000000000EFEFEF00ADADAD00000000000000000018181800C6C6C600A5A5
      A500A5A5A5008C8C8C007B7B7B0063636300636363004A4A4A004A4A4A004A4A
      4A00313131003131310000000000000000000000000000000000F7F7F700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700EFEFEF00EFEF
      EF00EFEFEF00CECECE000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B000000000000000000ADADAD00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDB5A5006B4A
      31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A31006B4A
      31006B4A31005A4229000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7DED6009C8C
      73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C73009C8C
      73009C8C73008C7B63000000000000000000000000007B7B7B00737373007373
      730073737300BDBDBD0073737300737373007373730073737300BDBDBD007373
      730073737300737373006B6B6B0000000000000000000000000000000000ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B00C6C6C6007B7B7B007B7B7B007B7B7B007B7B7B00C6C6C6007B7B
      7B007B7B7B007B7B7B0073737300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADADAD00000000002929
      2900212121000000000000000000000000000000000000000000000000001818
      1800181818000000000000000000B5B5B5000000000000000000000000000000
      000000000000000000000000000000FFFF007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000FFFF000000000000000000000000009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00000000006B635A007363
      4A00A5A5A500C6C6C6009494940094949400848484006B6B6B007B7B7B004A39
      29005A4A39008C84840039311800000000000000000000000000000000000000
      000000000000000000000000000000FFFF007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000FFFF000000000000000000000000009C9C9C00DEDE
      DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF009C9C9C000000000000000000B5B5B500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B500000000000000000000000000847B6B00B59C8400AD9C
      8400CECECE00E7E7E700423121008C7B5A00C6C6C600C6C6C600B5B5B5003929
      100084735A00B5AD9C00735A4A00000000000000000000000000000000000000
      000000000000000000000000000000FFFF007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000FFFF000000000000000000000000009C9C9C00E7E7
      E700F7F7F700F7F7F700F7F7F7009494940042424200DEDEDE00F7F7F700F7F7
      F700F7F7F700FFFFFF009C9C9C0000000000000000000000000073737300DEEF
      E700BDD6D6009CBDBD009CBDBD009CBDBD009CBDBD009CBDBD009CBDBD00849C
      9C005A736B0000000000000000000000000000000000EFE7CE00BDA58400AD9C
      8400C6C6C600CECECE005242290084735A00C6C6C600C6C6C600BDBDBD003121
      100084735A00B5AD9C00735A4A00000000000000000000000000000000000000
      000000000000000000000000000000FFFF007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000FFFF000000000000000000000000009C9C9C00E7E7
      E700F7F7F700DEDEDE00A5A5A50010101000000000008C8C8C00F7F7F700F7F7
      F700F7F7F700FFFFFF009C9C9C000000000000000000000000008C9C9400E7EF
      EF00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6
      CE008CA5A50000000000B5B5B5000000000000000000EFE7CE00BDAD8C00B5A5
      8400C6C6C600C6C6C600422910005A4A3100D6D6D600C6C6C600BDBDBD003929
      1000846B5200B5A59400735A4A00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B007B7B
      7B0000000000000000007B7B7B000000000000000000000000009C9C9C00E7E7
      E700F7F7F7006363630010101000000000000000000042424200F7F7F700F7F7
      F700F7F7F700FFFFFF009C9C9C000000000000000000000000007B8484008C94
      9400D6E7E700ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6
      CE00ADD6CE006B848400000000000000000000000000EFE7CE00BDAD8C00BDAD
      8C00BDB5AD00C6C6C600C6C6C600CECECE00EFEFEF00D6D6D6009C9C9C005242
      2900ADADA500AD9C8400735A4A000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00007B7B7B007B7B7B0000FFFF000000000000000000000000009C9C9C00E7E7
      E7007B7B7B000000000000000000A5A5A5004242420018181800BDBDBD00F7F7
      F700F7F7F700FFFFFF009C9C9C000000000000000000000000009CA59C001821
      2100E7F7EF00BDDED600ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6
      CE00ADD6CE0084A59C0000000000B5B5B50000000000EFE7CE00BDAD8C00BDAD
      8C00BDA58C00AD9C7B00AD9C8400AD9C8400AD9C8400AD9C8400AD9C7B00AD94
      7B00AD9C8400AD9C8400735A4A000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00007B7B7B0000FFFF0000FFFF000000000000000000000000009C9C9C00E7E7
      E700F7F7F7006B6B6B0094949400F7F7F700CECECE000000000073737300F7F7
      F700F7F7F700FFFFFF009C9C9C00000000000000000000000000BDC6C6005263
      5A00848C8C00C6E7DE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6CE00ADD6
      CE00ADD6CE008CADA5006B8C84000000000000000000EFE7CE00BDAD8C00CEBD
      AD00DED6CE00DECEC600D6CEBD00D6CEBD00D6CEBD00D6CEBD00D6CEBD00D6CE
      BD00C6B5A500AD9C8400735A4A000000000000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF0000000000000000000000
      00000000FF0000000000000000007B7B7B0000000000000000009C9C9C00E7E7
      E700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F7007373730000000000B5B5
      B500F7F7F700FFFFFF009C9C9C00000000000000000000000000BDC6C6007384
      840018212100EFF7F700C6E7DE00BDDED600ADD6CE00ADD6CE00ADD6CE00ADD6
      CE00ADD6CE00A5CEC600849C9C000000000000000000EFE7CE00BDAD8C00EFEF
      EF00FFFFFF00FFFFFF00F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00AD9C8400735A4A000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000009C9C9C00E7E7
      E700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700737373004242
      4200C6C6C600FFFFFF009C9C9C00000000000000000000000000F7F7F700738C
      840052636300394A4A00394A4A00314239003142420031424200314242003142
      390031393900394A420010181800B5B5B50000000000EFE7CE00BDAD8C00D6D6
      D600D6D6D600B5B5B5006B6B6B00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF00000000000000FF000000
      FF000000FF000000FF000000FF000000000000000000000000009C9C9C00E7E7
      E700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F7008484
      840042424200FFFFFF009C9C9C00000000000000000000000000DEEFEF007B8C
      8C007B948C007B9494007B8C8C00849C9C0084949400849C9C0084949400A5B5
      B50094A5A50000000000000000000000000000000000EFE7CE00BDAD8C00D6D6
      D600BDBDBD0052525200738C940039738C0031738400296B8400316B8400316B
      84008CADB500ADBDC600ADBDBD000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000000000009C9C9C00E7E7
      E700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700DEDEDE00FFFFFF009C9C9C00000000000000000000000000F7F7F7007B94
      8C007B9494007B949400849C94005A736B000000000000000000000000000000
      000000000000B5B5B500000000000000000000000000EFE7CE00BDAD8C006363
      630031313100E7E7E700ADC6D60063A5C600529CBD004294BD004A94B50084AD
      C6009CC6CE0029293100BDCED6004A52520000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000009C9C9C00E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700DEDEDE009C9C9C000000000000000000B5B5B50000000000DEE7
      E700CED6D600CED6D6009CA5A50000000000B5B5B50000000000000000000000
      00000000000000000000000000000000000000000000EFE7CE00BDAD8C00C6C6
      C600BDBDBD0073737300DEDEDE007BB5CE005AA5BD0052ADCE006BBDD60063B5
      CE00ADC6D600C6D6D600B5C6C6000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00000000000000000000000000B5B5B5000000
      0000000000000000000000000000B5B5B5000000000000000000000000000000
      00000000000000000000000000000000000000000000F7E7DE00BDAD8C00A5A5
      A500BDBDBD009C9C9C005A5A5A00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000000000BDBD
      BD00FFFFFF0000000000FFFFFF000000000000000000000000007B7B7B000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFF700EFDECE00C6C6
      C600EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00F7F7F700EFD6BD00EFD6BD000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5B5B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00000000000000000000000000ADADAD009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00ADADAD0000000000DEDEDE00ADADAD00ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00ADADAD00ADADAD00DEDEDE00000000009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00ADADAD00000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700FFFFFF00ADAD
      AD00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00D6D6D600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00D6D6D600ADADAD009C9C9C00E7E7E700FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE007373730073737300737373007373730073737300F7F7F700F7F7F700ADAD
      AD00FFFFFF0000000000FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F7007373
      73007B7B7B007B7B7B00FFFFFF00FFFFFF00FFFFFF00BDBDBD00FFFFFF000000
      00000000000000000000FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700ADAD
      AD00BDBDBD00BDBDBD00BDBDBD009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00F7F7F700F7F7
      F700F7F7F7000000000000000000000000000000000000000000000000000000
      0000F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F700F7F7
      F700F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE00737373007373730073737300737373007373730073737300F7F7F700ADAD
      AD00DEDEDE00DEDEDE00DEDEDE009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000000000
      00000000000000000000DED6C600AD947300AD947300AD947300947B63000000
      0000F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F7007373
      7300737373007373730073737300FFFFFF00FFFFFF00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE009C9C9C0000000000000000009C9C9C00DEDE
      DE00EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700ADAD
      AD00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000004263
      E7001842DE001842DE006B6B6B00C6B59C00AD947300AD94730000000000ADAD
      AD00F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700FFFFFF00DEDEDE00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD009C9C9C0000000000000000009C9C9C00DEDE
      DE007373730073737300737373007373730073737300F7F7F700F7F7F700ADAD
      AD00BDBDBD00BDBDBD00BDBDBD009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000004263
      E7001842DE001842DE0008217B00DED6C600AD947300947B630042424200F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F7007373
      73007373730073737300F7F7F700F7F7F700FFFFFF00BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE00EFEFEF00EFEFEF00EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700ADAD
      AD00FFFFFF0000000000FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000004263
      E7001842DE001842DE001842DE0084848400BDA58C0000000000292929009494
      9400F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700FFFFFF00BDBDBD00FFFFFF00DEDE
      DE00FFFFFF00DEDEDE00FFFFFF009C9C9C0000000000000000009C9C9C00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00ADAD
      AD00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000004263
      E7001842DE001842DE001842DE0000000000D6CEC6000000000063A5BD002142
      4A0094949400F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F7007373
      73007373730073737300F7F7F700F7F7F700FFFFFF00BDBDBD00F7F7F700F7F7
      F700F7F7F700FFFFFF00FFFFFF009C9C9C00ADADAD009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF0000000000637B
      EF004263E7004263E7004263E70000000000000000005A94A50084D6EF007BCE
      E70021212100F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700FFFFFF00D6D6D600BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD009C9C9C009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000ADADAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD009C9C9C00FFFFFF00000000000000
      00000000000000000000000000000000000084D6EF0084D6EF0084D6EF0084D6
      EF0000000000F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F7007373
      730073737300F7F7F700F7F7F700F7F7F700FFFFFF00BDBDBD00F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F7009C9C9C009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBD
      BD00FFFFFF0000000000FFFFFF009C9C9C0000000000ADADAD00000000000000
      00000000000000000000B5B5B5000000000000000000DEDEDE0000000000DEDE
      DE00000000000000000000000000ADADAD009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0029292900BDE7EF00A5E7F70084D6EF0084D6EF007BCE
      E70021212100F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700FFFFFF00BDBDBD00F7F7F7000000
      00000000000000000000F7F7F7009C9C9C009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBD
      BD00FFFFFF00FFFFFF00FFFFFF009C9C9C0000000000E7E7E700ADADAD00CECE
      CE0000000000B5B5B50000000000B5B5B50000000000ADADAD0000000000ADAD
      AD0000000000ADADAD00ADADAD00DEDEDE009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF009494940021424A00BDE7EF00BDEFF700ADDEEF002142
      4A0094949400F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700EFEFEF007373
      7300737373007373730073737300F7F7F700FFFFFF00BDBDBD00F7F7F700F7F7
      F70000000000F7F7F700F7F7F7009C9C9C009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBD
      BD00FFFFFF0000000000FFFFFF009C9C9C000000000000000000000000000000
      00000000000000000000B5B5B500000000000000000000000000DEDEDE000000
      0000000000000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00949494002929290000000000292929009494
      9400F7F7F700F7F7F700FFFFFF009C9C9C009C9C9C00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700FFFFFF00BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00D6D6D600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00D6D6D6009C9C9C00000000009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C0000000000ADADAD009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00ADADAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADADAD009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001842DE001842DE001842
      DE001842DE001842DE001842DE001842DE007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B00000000007B7B7B0000000000000000000000
      FF000000FF000000FF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001842DE00EFF7FF00FFFF
      FF001842DE00F7F7FF00DEDEFF001842DE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00ADADAD009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00ADADAD009C9C9C000000000000000000000000000000
      0000FFFFFF00EFEFEF00F7F7F700F7F7F700F7F7F7001842DE00F7F7FF00FFFF
      FF00E7EFFF00FFFFFF00FFFFFF001842DE000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00DEDEDE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDEDE00ADADAD000000000000000000000000000000
      0000FFFFFF00A5A5A500A5A5A500A5A5A500F7F7F7001842DE001842DE00EFF7
      FF00FFFFFF00EFEFFF001842DE001842DE000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700A5A5A5009C9C9C00BDBDBD00E7E7E7000000
      0000000000000000000000000000000000009C9C9C00FFFFFF00F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF00EFEFEF00EFEFEF00EFEFEF00F7F7F7001842DE00FFFFFF00FFFF
      FF00EFEFFF00FFFFFF00F7F7FF001842DE000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600000000000000000000000000000000009C9C9C00FFFFFF00FFFFFF00F7F7
      F700ADADAD0029292900ADADAD00F7F7F70029292900F7F7F70029292900F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF009C9C9C009C9C9C00A5A5A500EFEFEF001842DE00EFF7FF00FFFF
      FF001842DE00F7F7FF00DEDEFF001842DE000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000E7E7E700FFFFFF00FFFFFF005252520031313100ADADAD00FFFFFF00ADAD
      AD00E7E7E7000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF0029292900F7F7F70029292900F7F7F70029292900F7F7F70029292900F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00738CE7001842DE001842
      DE001842DE001842DE001842DE001842DE000000000000000000000000000000
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000A5A5A500FFFFFF005252520031313100292929000808080052525200F7F7
      F700A5A5A5000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF0029292900FFFFFF0029292900FFFFFF002929290029292900B5B5B500F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF009C9C9C009C9C9C009C9C9C00EFEFEF00A5A5A500A5A5A500A5A5
      A500F7F7F7000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00009C9C9C00F7F7F700313131004A4A4A00313131000000000031313100FFFF
      FF009C9C9C000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF0029292900FFFFFF0029292900FFFFFF0029292900D6D6D60029292900F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF00E7E7E700E7E7E700EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00F7F7F700000000000000000000000000000000000000000000000000FFFF
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000BDBDBD00E7E7E700ADADAD007B7B7B008C8C8C0039393900ADADAD00DEDE
      DE00BDBDBD000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00B5B5B50029292900B5B5B500FFFFFF0029292900FFFFFF0029292900F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF009C9C9C009C9C9C009C9C9C00EFEFEF009C9C9C009C9C9C00A5A5
      A500EFEFEF000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000FFFF00FFFFFF0000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E7005252520031313100ADADAD00FFFFFF00ADAD
      AD00E7E7E7000000000000000000000000009C9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7
      F700F7F7F700F7F7F700FFFFFF009C9C9C000000000000000000000000000000
      0000FFFFFF00DEDEDE00E7E7E700E7E7E700E7E7E700EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF00000000000000FFFF00FFFFFF0000FFFF00FFFFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600ADADAD00D6D6D600E7E7E700D6D6D600ADADAD000000
      000000000000000000000000000000000000A5A5A500D6D6D600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEDEDE00A5A5A5000000000000000000000000000000
      000094737B00845A6300845A6300845A6300845A6300845A6300845A6300845A
      6300845A63000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700A5A5A5009C9C9C00BDBDBD00E7E7E7000000
      0000000000000000000000000000000000009C9C9C00ADADAD009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00A5A5A5009C9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000BDBDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00C6C6
      C60000000000C6C6C600FFFFFF00C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008400000084000000840000008400000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000C6C6C6000000
      000000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      0000FF00000084000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000C6C6C6000000
      000000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      000084000000FF0000008400000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000000000
      000000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      0000FF00000084000000FF00000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000008484000084840000848400008484000084840000848400008484000084
      840000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      000084000000FF0000008400000000000000FFFFFF00FFFF0000FFFFFF00FFFF
      000084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFFFF00FFFFFF0000000000FFFFFF000000
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000000084
      840000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      0000FF00000084000000FF00000000000000FFFF0000FFFFFF00FFFF0000FFFF
      FF0084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      000000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      000084000000FF0000008400000000000000FFFFFF00FFFF0000FFFFFF00FFFF
      000084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      000000848400000000000000000000000000FF00FF00FF00FF00FF00FF008400
      0000FF00000084000000FF00000000000000FFFF0000FFFFFF00FFFF0000FFFF
      FF0084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000084840000000000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000
      0000C6C6C600000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00000000000000000000000000000000000000000000000000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000BDBDBD00BDBDBD00FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000BDBDBD00BDBDBD00000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00000000000084000000840000008400000084000000000000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00000000000000000000000000000000000000000000000000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFC3F00000000FFFFE00F00000000
      FFFFC007000000005555800300000000FFFF8001000000004005000100000000
      C0070000000000004005000000000000C0070000000000004005000000000000
      FFFF0001000000005555000100000000FFFF800100000000FFFFC00300000000
      FFFFE00700000000FFFFF01F00000000FFFFFFFFFFFFFFFFFF07FF8FFFEFFF8F
      FFBFFF77FFEFFF77FFDFFFF7FF07FFF7C003C003C003C003C003C003C003C003
      C003C003C003C003C003C003C003C003C003C003C003C003C003C003C003C003
      C003C003C003C003C003C003C003C003C003C003C003C003C003C003C003C003
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000FFFFFFFFFFFF8000FF00FF00FFEF
      8000FF3EFF3EFFEF8000FF3EFF3EFFEF8000E03EE006C0038000E03EE006C003
      8000E03EE006C0038000E000E000C003800000070007C00380003E073007C003
      80003E073007C00380003E073007C00380003E073007C00380003EFF3EFFC003
      800000FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF73800083F081FFFF738000EFF0
      81F7FF73BFFCC7F4B9E3FF73BFFCABF4B9D5FF738000EFF4B9F78173800FEFF4
      B9F78173800FEE04B9F7B973BFCFEE04B9FFB903BFCFEEC4B9C0B9FF800FEEC4
      B9C0B9DFFFFFEEC4B9DCB9DF6FF6EEC4B9DCB9DF5FFAAAC481DCB9570000C6C4
      FFDC818F5FFAEE00FFDCFFDF6FF683FFFE07FFFFFF8080FFFE078070F78080FF
      FEE78070EFBFBCFFEE07A274C0BFBCF7EFFFA274EFBFBCFBEFFFA274F7808081
      EFFFA274FFFFFFFBAA07A274FFFFFFF7C6078070FFFFFFFFEEE7FFFFFFFFFFFF
      FE07FF7F8007E001FE07FEFF8007E001FE07FC07BFE7EFF9FEE7FEFFBFE7EFF9
      FE07FF7FBFE7EFF9FFFFFFFF8007E001FFFFFEFFFFFFFC7FFFFFFEFFFFFFFC7F
      FFFFFC7FFFFFFC7FFF3FFC7FFFFFFC7FFC3FF83FFCFFFC7FF03FF83FFC3FFC7F
      C000F01FFC0FE00F0000F01F0003E00FC000E00F0000F01FF03FE00F0003F01F
      FC3FFC7FFC0FF83FFF3FFC7FFC3FF83FFFFFFC7FFCFFFC7FFFFFFC7FFFFFFC7F
      FFFFFC7FFFFFFEFFFFFFFC7FFFFFFEFFFE00FF00FFFFFFFFFC00C000FFFFFFFF
      F800C200FE00EFFDF800C200FE00C7FFF800C300FE00C3FB0000C0008000E3F7
      0000C0008000F1E70000C0028000F8CF0000C0078000FC1F0000C01F8001FE3F
      0000C0078003FC1F0000C0078007F8CF001CC007807FE1E70008C00F80FFC3F3
      0000C01F81FFC7FDFF80FFFFFFFFFFFF0000FFFFFFFFFFFF0000FFFFFFFF8001
      0000FFFFFFFF80010000C003FFFF80010000C003C00380010000C003C0038001
      0000C003C00380010000C003C00380010000C003C00380010000C003C0038001
      0000C003C003800100008003C00380010000FFFFC00380010000FFFFC0038001
      0000FFFFFFFF80010000FFFFFFFFFFFFFFFFFFFF8000FC00C001FFFF0000FC00
      C00180030000FC00C00180030000FC00C081800100000000C181800100000000
      C601800000000000C041800000000000C021800000000023C001800000000001
      C001800300000000C001800300000023C001807F00000063C001C0FF000000C3
      FFFFFFFF00000107FFFFFFFF000003FF8001C000800080010000C000BFFE0000
      0000C004BFFE00000000C000BFFE07F00000C000BFFE3C100000C000BFFE2020
      0000C000BFFE20000000C004BFFE20400000C000BFFE214000000000BFFE2180
      00000000BADE3F0800000004B88E0000000000008888000000000004F88F0040
      00000000FAAF000080010000FFFF8001FFFFFFFFFFFFFF800162FFFFFFFFFF80
      FFE3FFFF0000F000FE63FFFF0000F000FC03FC1F0000F000F803F80F0000F000
      F003F0070000F000F003F0070000F007E003F0470000F007C003F0070000F007
      8003F0070000F0070003F81F0000F0070003FC1F0000FFFF01E3FFFFFFFFFFFF
      83F7FFFFFFFFFFFFC7F7FFFFFFFFFFFFFFFFFF7EFFC8FFFFC001BFFFFFEB0000
      8031F003038000008031E003008000008031E003000000008001E00300000000
      8001E0030000000080012003000000008FF1E002000000008FF1E00300000000
      8FF1E003000000008FF1E003000000008FF1E003000000008FF5FFFF00000000
      8001BF7D0000FFFFFFFF7F7E0000FFFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 296
    Top = 24
    object Action1: TAction
      Caption = 'Action1'
    end
    object actInsertCheckbox: TAction
      Category = 'Components'
      Caption = 'New Checkbox'
      Hint = 'checkbox'
      ImageIndex = 12
      OnExecute = actInsertComponent
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Quits the application'
      ImageIndex = 2
    end
    object FileRun1: TFileRun
      Category = 'File'
      Browse = False
      BrowseDlg.Title = 'Run'
      Caption = '&Run...'
      Hint = 'Run|Runs an application'
      Operation = 'open'
      ShowCmd = scShowNormal
    end
    object actInsertWindow: TAction
      Category = 'Components'
      Caption = 'New Window'
      Hint = 'window'
      ImageIndex = 18
      OnExecute = actInsertComponent
    end
    object actUserTool2: TAction
      Caption = 'User Tool 2'
      Hint = 'User Tool 2'
      ImageIndex = 40
      ShortCut = 16434
      OnExecute = actUserTool2Execute
    end
    object actInsertButton: TAction
      Category = 'Components'
      Caption = 'New  Button'
      Hint = 'button'
      ImageIndex = 6
      OnExecute = actInsertComponent
    end
    object actInsertRadioButton: TAction
      Category = 'Components'
      Caption = 'New RadioButton'
      Hint = 'radio_button'
      ImageIndex = 5
      OnExecute = actInsertComponent
    end
    object actSendToBack: TAction
      Category = 'Components'
      Caption = 'Send To Back'
      Hint = 'Send To Back'
      ImageIndex = 37
      OnExecute = actSendToBackExecute
    end
    object actInsertListbox: TAction
      Category = 'Components'
      Caption = 'New Listbox'
      Hint = 'listbox'
      ImageIndex = 8
      OnExecute = actInsertComponent
    end
    object actInsertDialogBox: TAction
      Category = 'Components'
      Caption = 'New DialogBox'
      Hint = 'dialog_box'
      ImageIndex = 3
      OnExecute = actInsertComponent
    end
    object actInsertTextBox: TAction
      Category = 'Components'
      Caption = 'New TextBox'
      Hint = 'text_box'
      ImageIndex = 16
      OnExecute = actInsertComponent
    end
    object actInsertText: TAction
      Category = 'Components'
      Caption = 'New Text'
      Hint = 'text'
      ImageIndex = 16
      OnExecute = actInsertComponent
    end
    object actInsertItemSlot: TAction
      Category = 'Components'
      Caption = 'New ItemSlot'
      Hint = 'itemslot'
      ImageIndex = 17
      OnExecute = actInsertComponent
    end
    object actInsertStatusBar: TAction
      Category = 'Components'
      Caption = 'New Statusbar'
      Hint = 'status_bar'
      ImageIndex = 21
      OnExecute = actInsertComponent
    end
    object Action2: TAction
      Category = 'Components'
      Caption = 'New Edit Box'
      Hint = 'edit_box'
      ImageIndex = 16
      OnExecute = actInsertComponent
    end
    object actInsertChatbox: TAction
      Category = 'Components'
      Caption = 'New Chatbox'
      Hint = 'chat_box'
      ImageIndex = 16
      OnExecute = actInsertComponent
    end
    object actInsertDockBar: TAction
      Category = 'Components'
      Caption = 'New Dockbar'
      Hint = 'dockbar'
      ImageIndex = 10
      OnExecute = actInsertComponent
    end
    object actBringToFront: TAction
      Category = 'Components'
      Caption = 'Bring To Front'
      Hint = 'Bring To Front'
      ImageIndex = 38
      OnExecute = actBringToFrontExecute
    end
    object actHideComponents: TAction
      Category = 'Components'
      Caption = 'Hide Component(s)'
      ShortCut = 16456
      OnExecute = actHideComponentsExecute
    end
    object actShowComponents: TAction
      Category = 'Components'
      Caption = 'Show Component(s)'
      OnExecute = actShowComponentsExecute
    end
    object actOpenGas: TAction
      Caption = 'Open'
      ImageIndex = 13
      ShortCut = 16463
      OnExecute = actOpenGasExecute
    end
    object actSaveGas: TAction
      Caption = 'Save'
      ImageIndex = 0
      ShortCut = 16467
      OnExecute = actSaveGasExecute
    end
    object actSaveAs: TAction
      Caption = 'Save As...'
      ImageIndex = 14
      OnExecute = actSaveAsExecute
    end
    object actInsertEditbox: TAction
      Category = 'Components'
      Caption = 'New Edit Box'
      Hint = 'edit_box'
      ImageIndex = 16
      OnExecute = actInsertComponent
    end
    object actinsertCombobox: TAction
      Category = 'Components'
      Caption = 'New Combobox'
      Hint = 'combo_box'
      ImageIndex = 9
      OnExecute = actInsertComponent
    end
    object actInsertGridbox: TAction
      Category = 'Components'
      Caption = 'New Gridbox'
      Hint = 'gridbox'
      ImageIndex = 19
      OnExecute = actInsertComponent
    end
    object actInsertSlider: TAction
      Category = 'Components'
      Caption = 'New Slider'
      Hint = 'slider'
      ImageIndex = 20
      OnExecute = actInsertComponent
    end
    object actInsertTab: TAction
      Category = 'Components'
      Caption = 'New Tab'
      Hint = 'tab'
      ImageIndex = 20
      OnExecute = actInsertComponent
    end
    object actInsertInfoSlot: TAction
      Category = 'Components'
      Caption = 'New Infoslot'
      Hint = 'infoslot'
      ImageIndex = 20
      OnExecute = actInsertComponent
    end
    object actInsertListener: TAction
      Category = 'Components'
      Caption = 'New Listener'
      Hint = 'listener'
      ImageIndex = 20
      OnExecute = actInsertComponent
    end
    object actInsertPopupMenu: TAction
      Category = 'Components'
      Caption = 'New Popup Menu'
      Hint = 'popupmenu'
      ImageIndex = 9
      OnExecute = actInsertComponent
    end
    object actInsertListReport: TAction
      Category = 'Components'
      Caption = 'New ListReport'
      Hint = 'listreport'
      ImageIndex = 3
      OnExecute = actInsertComponent
    end
    object actRefresh: TAction
      Caption = 'Reparse Interface'
      Hint = 'Reparse Interface'
      ImageIndex = 20
      OnExecute = actRefreshExecute
    end
    object actUserTool1: TAction
      Caption = 'User Tool 1'
      Hint = 'User Tool 1'
      ImageIndex = 39
      ShortCut = 16433
      OnExecute = actUserTool1Execute
    end
    object actUserTool3: TAction
      Caption = 'User Tool 3'
      Hint = 'User Tool 3'
      ImageIndex = 41
      ShortCut = 16435
      OnExecute = actUserTool3Execute
    end
    object actAlignLeft: TAction
      Caption = 'Align left edges'
      Hint = 'Align left edges'
      ImageIndex = 30
      OnExecute = actAlignLeftExecute
    end
    object actAlignRight: TAction
      Caption = 'Align right edges'
      Hint = 'Align right edges'
      ImageIndex = 31
      OnExecute = actAlignRightExecute
    end
    object actAlignTop: TAction
      Caption = 'Align top edges'
      Hint = 'Align top edges'
      ImageIndex = 33
      OnExecute = actAlignTopExecute
    end
    object actAlignBottom: TAction
      Caption = 'Align bottom edges'
      Hint = 'Align bottom edges'
      ImageIndex = 32
      OnExecute = actAlignBottomExecute
    end
    object actMakeSameWidth: TAction
      Caption = 'Make same width'
      Hint = 'Make same width'
      ImageIndex = 34
      OnExecute = actMakeSameWidthExecute
    end
    object actMakeSameHeight: TAction
      Caption = 'Make same height'
      Hint = 'Make same height'
      ImageIndex = 35
      OnExecute = actMakeSameHeightExecute
    end
    object actMakeSameSize: TAction
      Caption = 'Make same size'
      Hint = 'Make same size'
      ImageIndex = 36
      OnExecute = actMakeSameSizeExecute
    end
    object actStackVertically: TAction
      Caption = 'Stack Vertically'
      Hint = 'Stack Vertically'
      ImageIndex = 28
      OnExecute = actStackVerticallyExecute
    end
    object actStackHorizontally: TAction
      Caption = 'Stack Horizontally'
      Hint = 'Stack Horizontally'
      ImageIndex = 29
      OnExecute = actStackHorizontallyExecute
    end
    object actUserTool4: TAction
      Caption = 'Use Tool 4'
      Hint = 'Use Tool 4'
      ImageIndex = 42
      ShortCut = 16436
      OnExecute = actUserTool4Execute
    end
    object actUserTool5: TAction
      Caption = 'Use Tool 5'
      ImageIndex = 43
      ShortCut = 16437
      OnExecute = actUserTool5Execute
    end
    object actReload: TAction
      Caption = 'Reload Current File'
      Hint = 'Reload Current File'
      ShortCut = 16466
      OnExecute = actReloadExecute
    end
    object actShiftLeft: TAction
      Caption = 'Shift left'
      Hint = 'Shift selected elements left'
      ImageIndex = 24
      OnExecute = actShiftLeftExecute
    end
    object actShiftRight: TAction
      Caption = 'Shift Right'
      Hint = 'Shift selected elements right'
      ImageIndex = 26
      OnExecute = actShiftRightExecute
    end
    object actShiftTop: TAction
      Caption = 'Shift Up'
      Hint = 'Shift selected elements up'
      ImageIndex = 27
      OnExecute = actShiftTopExecute
    end
    object actShiftDown: TAction
      Caption = 'Shift Down'
      Hint = 'Shift selected elements down'
      ImageIndex = 25
      OnExecute = actShiftDownExecute
    end
    object actNewInterface: TAction
      Category = 'File'
      Caption = 'New Interface'
      Hint = 'Create New Interface'
      ImageIndex = 1
      ShortCut = 16462
      OnExecute = actNewInterfaceExecute
    end
    object actTextured: TAction
      Category = 'Components'
      Caption = 'Render Components with Texture'
      Hint = 'Toggle texture rendering'
      ImageIndex = 11
      ShortCut = 16468
      OnExecute = actTexturedExecute
    end
    object actBrowseForTexture: TAction
      Caption = 'Assign Texture...'
      Hint = 'Assign Texture To Selected Components...'
      ImageIndex = 44
      ShortCut = 16450
    end
    object actUVCoordHelper: TAction
      Caption = 'UV Coordinates Helper...'
      Hint = 'UV Coordinates Helper...'
      ImageIndex = 44
      OnExecute = actUVCoordHelperExecute
    end
    object actCopyComponents: TAction
      Caption = 'Copy Component(s)'
      Hint = 'Clone|Make a copy of selected component(s)'
      ImageIndex = 22
      ShortCut = 16429
      OnExecute = actCopyComponentsExecute
    end
    object actDeleteComponent: TAction
      Caption = 'Delete component(s)'
      Hint = 'Delete|Delete selected component(s)'
      ImageIndex = 23
      ShortCut = 16430
      OnExecute = actDeleteComponentExecute
    end
  end
  object JvTipOfDay1: TJvTipOfDay
    AppStorage = JvAppIniFileStorage1
    ButtonNext.Caption = '&Next Tip'
    ButtonNext.Flat = False
    ButtonNext.HotTrack = False
    ButtonNext.HotTrackFont.Charset = DEFAULT_CHARSET
    ButtonNext.HotTrackFont.Color = clWindowText
    ButtonNext.HotTrackFont.Height = -11
    ButtonNext.HotTrackFont.Name = 'MS Sans Serif'
    ButtonNext.HotTrackFont.Style = []
    ButtonNext.HotTrackFontOptions = []
    ButtonNext.ShowHint = False
    ButtonClose.Caption = '&Close'
    ButtonClose.Flat = False
    ButtonClose.HotTrack = False
    ButtonClose.HotTrackFont.Charset = DEFAULT_CHARSET
    ButtonClose.HotTrackFont.Color = clWindowText
    ButtonClose.HotTrackFont.Height = -11
    ButtonClose.HotTrackFont.Name = 'MS Sans Serif'
    ButtonClose.HotTrackFont.Style = []
    ButtonClose.HotTrackFontOptions = []
    ButtonClose.ShowHint = False
    CheckBoxText = '&Show Tips on StartUp'
    HeaderText = 'Did you know...'
    OnAfterExecute = JvTipOfDay1AfterExecute
    Options = []
    Style = tsStandard
    Tips.Strings = (
      
        'This is an early BETA version.  Not everything is fully function' +
        'al.  Please let me know of any suggestions/bugs/etc!'
      
        'To view/modify the properties of the interface, select the root ' +
        'node in the element heirarchy window or click the background of ' +
        'the Form Designer.'
      
        'You can select multiple components by holding down the CTRL key ' +
        'and selecting multiple items'
      
        'Shifting items defaults to 1 pixel, hold down the CTRL key to sh' +
        'ift in 5 pixel increments'
      
        'Hold down the SHIFT key to size items instead of shift items whe' +
        'n using the shifting buttons'
      
        'Assign Dungeon Seige with the parameters to launch your map to y' +
        'our available external tools, It comes in VERY handy when you ne' +
        'ed to quickly relaunch DS after editing'
      
        'You can quickly launch your user tools by pressing CTRL+(Tool Nu' +
        'mber)'
      
        'You can now use the arrow keys on your keyboard to move compenen' +
        'ts around in the designer.  Use Shift in combination with the ar' +
        'row keys to size the components instead of moving them.'
      
        'The UV Coordinate helper will allow you to visually define your ' +
        'UV Coords for your textures.'
      
        'If you extract objects.dsres to a folder and point your bits pat' +
        'h under Settings... to /art/bitmaps/gui you will be able to use ' +
        'textured components in the designer.  The Dungeon Siege Raw file' +
        's will be read in and mapped to your compoenent.'
      
        'The textured component reader will read both *.RAW files as well' +
        ' as *.PSD files, so if you create new textures in PSD files, you' +
        ' will be able to use them in the designer.'
      'More to come later....')
    Title = 'Tips and Tricks'
    Left = 496
  end
  object mnuComponentPopup: TPopupMenu
    Left = 144
    Top = 504
    object BringToFront2: TMenuItem
      Action = actBringToFront
    end
    object SendToBack2: TMenuItem
      Action = actSendToBack
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object HideComponents1: TMenuItem
      Action = actHideComponents
    end
    object ShowComponents1: TMenuItem
      Action = actShowComponents
    end
    object mnuAlign: TMenuItem
      Caption = 'Align'
      object AlignLeftEdges1: TMenuItem
        Action = actAlignLeft
      end
      object AlignRightEdges1: TMenuItem
        Action = actAlignRight
      end
      object AlignTopEdges1: TMenuItem
        Action = actAlignTop
      end
      object AlignBottomEdges1: TMenuItem
        Action = actAlignBottom
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object Makesamesize1: TMenuItem
        Action = actMakeSameSize
      end
      object Makesameheight1: TMenuItem
        Action = actMakeSameHeight
      end
      object Makesamewidth1: TMenuItem
        Action = actMakeSameWidth
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object StackHorizontally1: TMenuItem
        Action = actStackHorizontally
      end
      object StackVertically1: TMenuItem
        Action = actStackVertically
      end
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object SelectAllInGroup1: TMenuItem
      Caption = 'Select All In Group'
      ShortCut = 16455
      OnClick = SelectAllInGroup1Click
    end
    object CopyComponents1: TMenuItem
      Action = actCopyComponents
    end
    object Deletecomponents1: TMenuItem
      Action = actDeleteComponent
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object UVCoordHelper1: TMenuItem
      Action = actUVCoordHelper
    end
  end
  object JvMRUManager1: TJvMRUManager
    Duplicates = dupIgnore
    IniStorage = JvFormStorage1
    RecentMenu = File1
    OnClick = JvMRUManager1Click
    Left = 536
  end
  object SynCppSyn1: TSynCppSyn
    DefaultFilter = 'C++ Files (*.c,*.cpp,*.h,*.hpp)|*.c;*.cpp;*.h;*.hpp'
    CommentAttri.Foreground = clTeal
    KeyAttri.Foreground = clNavy
    NumberAttri.Foreground = clMaroon
    HexAttri.Foreground = clRed
    StringAttri.Foreground = clGreen
    Left = 200
    Top = 432
  end
  object JvDragDrop1: TJvDragDrop
    DropTarget = Owner
    OnDrop = JvDragDrop1Drop
    Left = 336
  end
  object openDlgBGImage: TOpenDialog
    DefaultExt = 'gas'
    Filter = 'Bitmap Images (*.bmp)|*.bmp'
    Left = 384
  end
  object mnuAssignTexture: TPopupMenu
    Left = 312
    Top = 544
    object mnuAssignTextureToComp: TMenuItem
      Caption = 'Assign Texture to Selected Components'
      Enabled = False
      OnClick = mnuAssignTextureToCompClick
    end
  end
  object JvAppIniFileStorage1: TJvAppIniFileStorage
    StorageOptions.BooleanStringTrueValues = 'TRUE, YES, Y'
    StorageOptions.BooleanStringFalseValues = 'FALSE, NO, N'
    FileName = 'U6pID.ini'
    DefaultSection = 'MRU'
    SubStorages = <>
    Left = 568
    Top = 8
  end
  object JvFormStorage1: TJvFormStorage
    AppStorage = JvAppIniFileStorage1
    AppStoragePath = 'frmUIDesigner\'
    StoredValues = <>
    Left = 600
    Top = 8
  end
end
