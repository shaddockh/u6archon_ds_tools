(*

 Copyright (C) 2003  T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: frmSpellbook.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Main interface for the UI Designer
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.23 $              $Date: 2006-05-06 14:41:02 $
 *----------------------------------------------------------------------------
 *
 * 2004-02-25 Shaddock (archon)
 *  - Added mousewheel support to the texture browser and the main interface
 */
*)
unit frmSpellbook;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uUIHelper, DSGuiComponents, ComCtrls,
  janSticker, Menus, JvComponent,
  JvInspector, JvEditor, JvHLEditor, JvComponentPanel, StdActns, ActnList,
  ImgList, ToolWin, ExtActns, JvBaseDlg, JvTipOfDay, inifiles, uGasParser,
  JvMRUList, registry, shellapi, contnrs, Buttons, SynEdit,
  SynEditHighlighter, SynHighlighterJava, SynHighlighterCpp, JvDragDrop, uRawRapiReader, GraphicEx,
  ShellCtrls, JvBaseThumbnail, JvThumbviews, JvListBox, JvDriveCtrls,
  JvCombobox, JvMRUManager, JvComponentBase, JvExStdCtrls, JvExForms,
  JvEditorCommon, JvExControls, JvAppStorage, JvAppIniStorage,
  JvFormPlacement;

type
  TfrmUIDesigner = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    New1: TMenuItem;
    StatusBar1: TStatusBar;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    JvInspectorDotNETPainter1: TJvInspectorDotNETPainter;
    ToolBar2: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    Action1: TAction;
    FileExit1: TFileExit;
    Splitter1: TSplitter;
    Panel3: TPanel;
    JvInspector1: TJvInspector;
    Panel1: TPanel;
    Splitter2: TSplitter;
    Label1: TLabel;
    PageControl4: TPageControl;
    TabSheet3: TTabSheet;
    Label4: TLabel;
    rtfElementCode2: TJvHLEditor;
    tabGeneratedCode: TTabSheet;
    Label3: TLabel;
    ToolBar1: TToolBar;
    btn_Delete: TToolButton;
    ToolButton2: TToolButton;
    btn_Window: TToolButton;
    btn_Button: TToolButton;
    btn_radiobutton: TToolButton;
    btn_listbox: TToolButton;
    Splitter3: TSplitter;
    Label6: TLabel;
    btn_checkbox: TToolButton;
    btn_dialogbox: TToolButton;
    btn_textbox: TToolButton;
    btn_text: TToolButton;
    ools1: TMenuItem;
    mnuLaunchDS: TMenuItem;
    btn_copy: TToolButton;
    FileRun1: TFileRun;
    ToolButton4: TToolButton;
    Components1: TMenuItem;
    actInsertWindow: TAction;
    Insertwindow1: TMenuItem;
    actInsertButton: TAction;
    Insertbutton1: TMenuItem;
    actInsertRadioButton: TAction;
    NewRadioButton1: TMenuItem;
    actInsertCheckbox: TAction;
    actInsertListbox: TAction;
    actInsertDialogBox: TAction;
    actInsertTextBox: TAction;
    actInsertText: TAction;
    actInsertItemSlot: TAction;
    actInsertStatusBar: TAction;
    actInsertDockBar: TAction;
    NewCheckbox1: TMenuItem;
    NewDialogBox1: TMenuItem;
    NewDockbar1: TMenuItem;
    NewItemSlot1: TMenuItem;
    NewListbox1: TMenuItem;
    NewStatusbar1: TMenuItem;
    NewText1: TMenuItem;
    NewTextBox1: TMenuItem;
    JvTipOfDay1: TJvTipOfDay;
    Panel2: TPanel;
    tvElementHeirarchy: TTreeView;
    Label5: TLabel;
    ScrollBox1: TScrollBox;
    scrollbox_Interface: TDSInterface;
    mnuComponentPopup: TPopupMenu;
    actBringToFront: TAction;
    actSendToBack: TAction;
    mnuEdit: TMenuItem;
    BringToFront1: TMenuItem;
    SendToBack1: TMenuItem;
    BringToFront2: TMenuItem;
    SendToBack2: TMenuItem;
    actHideComponents: TAction;
    actShowComponents: TAction;
    N2: TMenuItem;
    HideComponents1: TMenuItem;
    ShowComponents1: TMenuItem;
    actOpenGas: TAction;
    actSaveGas: TAction;
    actSaveAs: TAction;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    N3: TMenuItem;
    actInsertEditbox: TAction;
    actinsertCombobox: TAction;
    actInsertGridbox: TAction;
    actInsertSlider: TAction;
    actInsertTab: TAction;
    actInsertInfoSlot: TAction;
    actInsertListener: TAction;
    actInsertPopupMenu: TAction;
    actInsertListReport: TAction;
    NewEditBox1: TMenuItem;
    NewCombobox1: TMenuItem;
    NewGridbox1: TMenuItem;
    NewSlider1: TMenuItem;
    NewTab1: TMenuItem;
    NewInfoslot1: TMenuItem;
    NewListener1: TMenuItem;
    NewPopupMenu1: TMenuItem;
    NewListReport1: TMenuItem;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    Help1: TMenuItem;
    About1: TMenuItem;
    actRefresh: TAction;
    Copy1: TMenuItem;
    Delete1: TMenuItem;
    N4: TMenuItem;
    Refresh1: TMenuItem;
    ToolButton17: TToolButton;
    N5: TMenuItem;
    actUserTool1: TAction;
    actUserTool2: TAction;
    actUserTool3: TAction;
    UserTool11: TMenuItem;
    UserTool21: TMenuItem;
    UserTool31: TMenuItem;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    mnuAlign: TMenuItem;
    AlignLeftEdges1: TMenuItem;
    AlignRightEdges1: TMenuItem;
    actAlignLeft: TAction;
    actAlignRight: TAction;
    actAlignTop: TAction;
    actAlignBottom: TAction;
    AlignTopEdges1: TMenuItem;
    AlignBottomEdges1: TMenuItem;
    N6: TMenuItem;
    actMakeSameWidth: TAction;
    actMakeSameHeight: TAction;
    actMakeSameSize: TAction;
    Makesamesize1: TMenuItem;
    Makesameheight1: TMenuItem;
    Makesamewidth1: TMenuItem;
    actStackVertically: TAction;
    actStackHorizontally: TAction;
    N7: TMenuItem;
    StackHorizontally1: TMenuItem;
    StackVertically1: TMenuItem;
    N8: TMenuItem;
    mnuOptions: TMenuItem;
    actUserTool4: TAction;
    actUserTool5: TAction;
    ToolButton22: TToolButton;
    ToolButton23: TToolButton;
    UserTool41: TMenuItem;
    UseTool51: TMenuItem;
    actReload: TAction;
    ReloadCurrentFile1: TMenuItem;
    N9: TMenuItem;
    Recalculatedraworder1: TMenuItem;
    ToolBar3: TToolBar;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    ToolButton27: TToolButton;
    ToolButton28: TToolButton;
    ToolButton29: TToolButton;
    ToolButton30: TToolButton;
    ToolButton31: TToolButton;
    ToolButton32: TToolButton;
    ToolButton33: TToolButton;
    ToolButton34: TToolButton;
    ToolButton35: TToolButton;
    ToolButton36: TToolButton;
    ToolButton37: TToolButton;
    actShiftLeft: TAction;
    actShiftRight: TAction;
    actShiftTop: TAction;
    actShiftDown: TAction;
    ToolButton38: TToolButton;
    ToolButton39: TToolButton;
    ToolButton40: TToolButton;
    ToolButton41: TToolButton;
    ToolButton42: TToolButton;
    JvMRUManager1: TJvMRUManager;
    N10: TMenuItem;
    SelectAllInGroup1: TMenuItem;
    rtfElementCode: TSynEdit;
    rtf_code: TSynEdit;
    SynCppSyn1: TSynCppSyn;
    ipoftheDay1: TMenuItem;
    JvDragDrop1: TJvDragDrop;
    actNewInterface: TAction;
    btnNew: TToolButton;
    DumpInterfaceWithTokens1: TMenuItem;
    Action2: TAction;
    actInsertChatbox: TAction;
    NewChatbox1: TMenuItem;
    ToolButton43: TToolButton;
    SetBackgroundReferenceImage1: TMenuItem;
    openDlgBGImage: TOpenDialog;
    ToolButton44: TToolButton;
    actTextured: TAction;
    ToolButton45: TToolButton;
    RenderComponents1: TMenuItem;
    actBrowseForTexture: TAction;
    ToolButton46: TToolButton;
    ToolButton47: TToolButton;
    tabTextureBrowser: TTabSheet;
    JvThumbView1: TJvThumbView;
    Splitter4: TSplitter;
    prgTextures: TProgressBar;
    mnuAssignTexture: TPopupMenu;
    mnuAssignTextureToComp: TMenuItem;
    Panel4: TPanel;
    JvDirectoryListBox1: TJvDirectoryListBox;
    actUVCoordHelper: TAction;
    UVCoordHelper1: TMenuItem;
    UVCoordinatesHelper1: TMenuItem;
    utorials1: TMenuItem;
    DiscussionBoards1: TMenuItem;
    N11: TMenuItem;
    actCopyComponents: TAction;
    actDeleteComponent: TAction;
    N12: TMenuItem;
    CopyComponents1: TMenuItem;
    Deletecomponents1: TMenuItem;
    Label2: TLabel;
    JvAppIniFileStorage1: TJvAppIniFileStorage;
    JvFormStorage1: TJvFormStorage;
    procedure btn_generate_codeClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Element_Selected(Sender: TDSBaseUIControl; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Element_HeirarchyChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure scrollbox_InterfaceClick(Sender: TObject);
    procedure rtfElementCode2Change(Sender: TObject);
    procedure tabGeneratedCodeShow(Sender: TObject);
    procedure tvElementHeirarchyChange(Sender: TObject; Node: TTreeNode);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actInsertComponent(Sender: TObject);
    procedure actBringToFrontExecute(Sender: TObject);
    procedure actSendToBackExecute(Sender: TObject);
    procedure actHideComponentsExecute(Sender: TObject);
    procedure actShowComponentsExecute(Sender: TObject);
    procedure actOpenGasExecute(Sender: TObject);
    procedure actSaveGasExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure JvMRUManager1Click(Sender: TObject; const RecentName,
      Caption: string; UserData: Integer);
    procedure About1Click(Sender: TObject);
    procedure actCopyComponentsExecute(Sender: TObject);
    procedure actDeleteComponentExecute(Sender: TObject);
    procedure actRefreshExecute(Sender: TObject);
    procedure mnuLaunchDSClick(Sender: TObject);
    procedure actUserTool1Execute(Sender: TObject);
    procedure actUserTool2Execute(Sender: TObject);
    procedure actUserTool3Execute(Sender: TObject);
    procedure actAlignLeftExecute(Sender: TObject);
    procedure actAlignRightExecute(Sender: TObject);
    procedure actAlignTopExecute(Sender: TObject);
    procedure actAlignBottomExecute(Sender: TObject);
    procedure actMakeSameWidthExecute(Sender: TObject);
    procedure actMakeSameHeightExecute(Sender: TObject);
    procedure actMakeSameSizeExecute(Sender: TObject);
    procedure actStackVerticallyExecute(Sender: TObject);
    procedure actStackHorizontallyExecute(Sender: TObject);
    procedure mnuOptionsClick(Sender: TObject);
    procedure actUserTool4Execute(Sender: TObject);
    procedure actUserTool5Execute(Sender: TObject);
    procedure actReloadExecute(Sender: TObject);
    procedure Recalculatedraworder1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actShiftLeftExecute(Sender: TObject);
    procedure actShiftRightExecute(Sender: TObject);
    procedure actShiftTopExecute(Sender: TObject);
    procedure actShiftDownExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvInspector1DataValueChanged(Sender: TObject;
      const Data: TJvCustomInspectorData);
    procedure SelectAllInGroup1Click(Sender: TObject);
    procedure ipoftheDay1Click(Sender: TObject);
    procedure JvTipOfDay1AfterExecute(Sender: TObject);
    procedure actNewInterfaceExecute(Sender: TObject);
    procedure DumpInterfaceWithTokens1Click(Sender: TObject);
    procedure rtfElementCodeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetBackgroundReferenceImage1Click(Sender: TObject);
    procedure scrollbox_InterfaceEnter(Sender: TObject);
    procedure scrollbox_InterfaceExit(Sender: TObject);
    procedure actTexturedExecute(Sender: TObject);
    procedure JvInspector1AfterItemCreate(Sender: TObject;
      const Item: TJvCustomInspectorItem);
    procedure JvThumbView1StartScanning(Sender: TObject; Max: Integer);
    procedure JvThumbView1StopScanning(Sender: TObject);
    procedure JvThumbView1ScanProgress(Sender: TObject; Position: Integer;
      var Break: Boolean);
    procedure JvThumbView1ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure tabTextureBrowserShow(Sender: TObject);
    procedure JvDirectoryListBox1Change(Sender: TObject);
    procedure mnuAssignTextureToCompClick(Sender: TObject);
    procedure JvThumbView1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure actUVCoordHelperExecute(Sender: TObject);
    procedure utorials1Click(Sender: TObject);
    procedure DiscussionBoards1Click(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure JvDragDrop1Drop(Sender: TObject; Pos: TPoint;
      Value: TStrings);
  private
    { Private declarations }
    FCurrentElement: TDSBaseUIControl;
    FCurrentFilename: string;
    FDirty: boolean;
    ctrlPressed: boolean;
    shiftpressed: boolean;
    ElementSelectionList: TObjectList;
    BackupOnSave: boolean;
    InDesigner: boolean;

    procedure SetDirty(Value: boolean);
    procedure SetCurrentFilename(Value: string);
    procedure SetComponentVisibility(Node: TTreeNode; isVisible: boolean);
    procedure SelectGroup(Root: TTreeNode);

    procedure ApplicationMessage(var Msg: TMsg; var Handled: Boolean);
  public
    { Public declarations }

    procedure SetCurrentElement(Value: TDSBaseUIControl);
    procedure UpdateElementHeirarchy;
    procedure ImportGas(Filename: string);
    procedure RebuildFromGas(Gas: string);
    procedure ExportGas(Filename: string);
    function GetTemplateText(ControlType: string): string;
    procedure AddNewControl(Control: TDSBaseUIControl; UpdateHeirarchy: boolean = true);
    property CurrentElement: TDSBaseUIControl read FCurrentElement write SetCurrentElement;
    property CurrentFilename: string read FCurrentFilename write SetCurrentFilename;
    property Dirty: boolean read FDirty write SetDirty;
    procedure LaunchUserTool(toolnum: integer);
    procedure AddToSelectionList(Element: TDSBaseUIControl);
    procedure ClearSelectionList;
    function GetNodeByData(Treeview: TTreeView; Data: Pointer): TTreeNode;
    procedure UpdateEnabledFlags;
    procedure Element_Changed(Sender: TObject);
    procedure ElementBoundsChanged(Sender: TObject);
    function GenerateNewControlName(TemplateName: string): string;
    procedure UpdateEditWindowWithChanges(NewText: TStringList);
    procedure LoadGasFile(filename: string);
  end;

var
  frmUIDesigner: TfrmUIDesigner;
  DUNGEON_SIEGE_PATH: string;

const
  HiddenProperties = ' ALIGN ANCHORS CONSTRAINTS CURSOR DRAGCURSOR DRAGKIND ' +
    ' DRAGMODE ENABLED HELPCONTEXT HELPKEYWORD HELPTYPE ' +
    ' PARENTSHOWHINT SHOWHINT TAG AUTOSCROLL AUTOSIZE ' +
    ' BEVELEDGES BEVELINNER BEVELKIND BEVELOUTER BEVELWIDTH BIDIMODE ' +
    ' BORDERSTYLE COLOR CTL3D DOCKSITE FONT HINT HORZSCROLLBAR ' +
    ' NAME PARENTBIDIMODE PARENTCOLOR PARENTCTL3D PARENTFONT ' +
    ' POPUPMENU SKRIT_BLOCK TABORDER TABSTOP VERTSCROLLBAR CODE ' +
    ' BRUSH PEN BOTTOMANCHOR RIGHTANCHOR CAPTION ALIGNMENT BORDERWIDTH FULLREPAINT ' +
    ' LOCKED USEDOCKMANAGER ';

  HiddenProperties_Interface = HiddenProperties + ' TOP LEFT WIDTH HEIGHT VISIBLE ';
const
  IDX_FOLDER = 13;
  IDX_DIALOGBOX = 3;
  IDX_BUTTON = 6;
  IDX_RADIOBUTTON = 5;
  IDX_CHECKBOX = 12;
  IDX_LISTBOX = 8;
  IDX_COMBOBOX = 9;
  IDX_TEXTBOX = 16;
  IDX_TEXT = 16;
  IDX_GRIDBOX = 19;
  IDX_WINDOW = 18;
  IDX_STATUSBAR = 21;
  IDX_ITEMSLOT = 17;

  IDX_GROUP = 20;

  TITLE = 'U6P:ID - ';

  VERSION_STRING = 'Version 0.2.8 Beta';
  TUTORIALS_URL = 'http://www.planetdungeonsiege.com/archon/resources/u6pid.asp';
  DISCUSSION_BOARD_URL = 'http://www.projectbritannia.com/forum';

implementation
uses uFrmAbout, uFrmOptions, uFrmSplash, uFrmUVCoordWizard;
{$R *.dfm}

function GetElementImageIndex(ctrl: TDSBaseUIControl): integer;
begin

  if ctrl is TDSButton then
    result := IDX_BUTTON
  else if ctrl is TDSRadioButton then
    result := IDX_RADIOBUTTON
  else if ctrl is TDSCheckbox then
    result := IDX_CHECKBOX
  else if ctrl is TDSListBox then
    result := IDX_LISTBOX
  else if ctrl is TDSComboBox then
    result := IDX_COMBOBOX
  else if ctrl is TDSTextBox then
    result := IDX_TEXTBOX
  else if ctrl is TDSText then
    result := IDX_TEXT
  else if ctrl is TDSDialogBox then
    result := IDX_DIALOGBOX
  else if ctrl is TDSStatusBar then
    result := IDX_STATUSBAR
  else if ctrl is TDSItemSlot then
    result := IDX_ITEMSLOT
  else if ctrl is TDSWindow then
    result := IDX_WINDOW
  else if ctrl is TDSGridbox then
    result := IDX_GRIDBOX

  else
    result := IDX_WINDOW;
end;

function TFrmUIDesigner.GenerateNewControlName(TemplateName: string): string;
var
  cnt: integer;
  i: integer;
  ctrl: TDSBaseUIControl;
  NameList: TStringlist;
  NewName: string;
begin
  NameList := nil;
  try
    NameList := TStringlist.create;
    for i := 0 to scrollbox_interface.ControlCount - 1 do
    begin
      if scrollbox_interface.Controls[i] is TDSBaseUIControl then
      begin
        ctrl := TDSBaseUIControl(scrollbox_interface.Controls[i]);
        if comparetext(ctrl.templatename, templateName) = 0 then
          NameList.Add(ctrl.Name_);
      end;
    end;
    cnt := 0;
    NewName := TemplateName + intToStr(cnt);
    while NameList.IndexOf(NewName) <> -1 do
    begin
      cnt := cnt + 1;
      NewName := TemplateName + intToStr(cnt);
    end;
    result := NewName;
  finally
    freeAndNil(NameList);
  end;

end;

procedure TFrmUIDesigner.SetCurrentElement(Value: TDSBaseUIControl);
var
  InspCat: TJvInspectorCustomCategoryItem;

  i: integer;

begin

  if jvInspector1.Root.Count = 1 then
    for i := 0 to jvInspector1.Root.items[0].Count - 1 do
    begin
      if jvInspector1.Root.items[0].Items[i].Editing then jvInspector1.Root.items[0].Items[i].DoneEdit(true);
    end;

  jvInspector1.Root.Clear;
  if FCurrentElement <> nil then
  begin
    FCurrentElement.Pen.Style := psSolid;
    FCurrentElement.Pen.Color := clBlack;
    //FCurrentElement.Selected := false;
  end;
  FCurrentElement := Value;
  if Value = nil then
  begin
    ClearSelectionList;
    rtfElementCode.lines.Text := '';
    // tsCode.Caption := 'Code []';
    InspCat := TJvInspectorCustomCategoryItem.Create(jvInspector1.Root, nil);
    Inspcat.Flags := [iifExpanded, iifVisible];
    InspCat.DisplayName := scrollbox_Interface.interface_name + ' : interface';
    TJvInspectorPropData.New(InspCat, scrollbox_interface);
    Inspcat.Expanded := true;
    for i := inspcat.Count - 1 downto 0 do
    begin
      if pos(' ' + uppercase(inspcat.Items[i].DisplayName) + ' ', HiddenProperties_interface) > 0 then
        inspcat.items[i].Hidden := true
      else if comparetext(inspcat.items[i].DisplayName, 'isinterface') = 0 then
        inspcat.items[i].DisplayName := 'interface'
    end;
    inspcat.Sort;
    rtfElementCode.lines.Text := scrollbox_Interface.code;
    if tvElementHeirarchy.items[0].selected = false then
      tvElementHeirarchy.Items[0].selected := true;
  end
  else
  begin
    //Value.BringToFront;
    InspCat := TJvInspectorCustomCategoryItem.Create(jvInspector1.Root, nil);
    Inspcat.Flags := [iifExpanded, iifVisible];
    InspCat.DisplayName := Value.name_ + ': ' + Value.templatename;

    TJvInspectorPropData.New(InspCat, VAlue);

    //Inspcat.Flags := Inspcat.Flags - [iifReadOnly];
    Inspcat.Expanded := true;

    for i := inspcat.Count - 1 downto 0 do
    begin
      if pos(' ' + uppercase(inspcat.Items[i].DisplayName) + ' ', HiddenProperties) > 0 then inspcat.items[i].Hidden := true;
      if pos(' ' + uppercase(inspcat.Items[i].DisplayName) + ' ', CurrentElement.HiddenProperties) > 0 then
        inspcat.items[i].Hidden := true
      else if comparetext(inspcat.items[i].DisplayName, 'visible_') = 0 then
        inspcat.items[i].DisplayName := 'visible'
      else if comparetext(inspcat.items[i].DisplayName, 'enabled_') = 0 then
        inspcat.items[i].DisplayName := 'enabled'
      else if comparetext(inspcat.items[i].DisplayName, 'tag_') = 0 then
        inspcat.items[i].DisplayName := 'tag'
      else if comparetext(inspcat.items[i].DisplayName, 'visible') = 0 then
        inspcat.items[i].DisplayName := 'VisibleInDesigner'
      else if comparetext(inspcat.items[i].DisplayName, 'name_') = 0 then
        inspcat.items[i].DisplayName := 'name'

    end;
    inspcat.Sort;
    rtfElementCode.lines.Text := CurrentElement.Code.text;
    //rtfElementCode.SetLeftTop(0, 0); TSH-10
    //    for i := 0 to tvElementHeirarchy.Items.Count - 1 do
    //      if tvElementHeirarchy.Items[i].Data = value then
    //        if tvElementHeirarchy.items[i].selected = false then
    //        begin
    //          tvElementHeirarchy.Items[i].Selected := true;
    //          tvElementHeirarchy.Items[i].Cut := not CurrentElement.Visible;
    //        end;
    FCurrentElement.Pen.Style := psDash;
    FCurrentElement.Pen.Color := clRed;
    //    if FCurrentElement.Selected = false then
    //      FCurrentElement.Selected := true;
        // tsCode.Caption := 'Code [' +Value.elementname + ': ' + Value.templatename +' ]';
  end;
end;

procedure TFrmUIDesigner.SetDirty(Value: boolean);
begin
  FDirty := value;
  setcurrentfilename(CurrentFilename);
  actsaveGas.Enabled := dirty;
end;

procedure TFrmUIDesigner.SetCurrentFilename(Value: string);
begin
  FCurrentFilename := Value;
  if dirty then
    caption := TITLE + '[*' + ExtractFileName(currentfilename) + ']'
  else
    caption := TITLE + '[' + ExtractFileName(currentfilename) + ']';
end;

procedure TFrmUIDesigner.RebuildFromGas(Gas: string);
var
  sl: TSTringlist;
  i: integer;
begin

  ParseWarningsList.Clear;
  CurrentElement := nil;
  scrollbox_interface.ImportFromGas(Gas);
  rtfElementCode.Lines.text := scrollbox_interface.code;
  //    rtfEmbeddedSkrit.lines.Text := scrollbox_interface.skrit_block.Text;

  //sort the list based on draw_order
  sl := TStringlist.create;
  for i := 0 to scrollbox_interface.controlcount - 1 do
  begin
    if scrollbox_interface.Controls[i] is TDSBaseUIControl then
      sl.AddObject(Format('%9.9d', [TDSBaseUIControl(scrollbox_interface.Controls[i]).draw_order]), scrollbox_interface.controls[i]);
  end;

  sl.Sort;
  for i := 0 to sl.Count - 1 do
    TControl(sl.Objects[i]).BringToFront;

  sl.free;
  for i := 0 to scrollbox_Interface.ControlCount - 1 do
  begin
    if scrollbox_interface.Controls[i] is TDSBaseUIControl then
    begin
      TDSBaseUIControl(scrollbox_interface.Controls[i]).OnSelect := Element_Selected;
      TDSBaseUIControl(scrollbox_interface.Controls[i]).OnHeirarchyChange := Element_HeirarchyChange;
      TDSBaseUIControl(scrollbox_interface.Controls[i]).OnChange := Element_Changed;
      TDSBaseUIControl(scrollbox_interface.Controls[i]).OnBoundsChange := ElementBoundsChanged;

    end;
  end;

  UpdateElementHeirarchy;
  ClearSelectionList;
  if ParseWarningsList.count <> 0 then
    MessageDlg('The following parsing errors occured:' + #13#10 + ParseWarningsList.text, mtError, [mbOK], 0);

end;

procedure TFrmUIDesigner.ImportGas(Filename: string);
var
  sl: TSTringlist;
begin
  CurrentElement := nil;
  sl := nil;
  try
    sl := TStringlist.create;
    sl.LoadFromFile(Filename);

    REbuildFromGas(sl.text);

  finally
    if assigned(sl) then freeAndNil(sl);
  end;
  //
end;

procedure TFrmUIDesigner.ExportGas(Filename: string);
var
  sl: TSTringlist;

begin
  sl := nil;
  try
    sl := TStringlist.create;
    sl.text := scrollbox_interface.GenerateInterface;

    //create a backup
    if BackupOnSave then
      if FileExists(Filename) then
      begin
        CopyFile(pchar(filename), pchar(ChangeFileExt(filename, '.bak')), false);
      end;

    sl.SaveToFile(Filename);
  finally
    if assigned(sl) then freeAndNil(sl);
  end;
  //
end;

procedure TFrmUIDesigner.UpdateElementHeirarchy;
var
  i: integer;
  Parentnode: TTreeNode;

  Node: TTreeNode;
  Ctrl: TDSBaseUIControl;
  sl: TStringlist;
  Groups: TStringlist;
begin
  tvElementHeirarchy.Items.BeginUpdate;
  sl := nil;
  Groups := nil;
  try
    tvElementHeirarchy.Items.Clear;
    sl := TStringlist.Create;
    Groups := TStringlist.create;
    ParentNode := tvElementHeirarchy.Items.Add(nil, scrollbox_Interface.interface_name + ' : Interface');
    ParentNode.ImageIndex := IDX_FOLDER;
    ParentNode.SelectedIndex := ParentNode.ImageIndex;

    //Get a list of the groups and create the group nodes
    for i := 0 to scrollbox_interface.controlcount - 1 do
    begin
      if scrollbox_interface.controls[i] is TDSBaseUIControl then
      begin
        ctrl := TDSBaseUIControl(scrollbox_interface.Controls[i]);
        if trim(ctrl.group) <> '' then
        begin
          if groups.indexof(trim(ctrl.group)) = -1 then
          begin
            node := tvElementHeirarchy.Items.AddChild(ParentNode, 'Group: ' + ctrl.group);
            node.ImageIndex := IDX_GROUP;
            node.SelectedIndex := IDX_GROUP;
            groups.AddObject(trim(ctrl.group), node);
          end;
        end;
      end;
    end;

    //first run through and add the top level elements
    for i := 0 to scrollbox_Interface.ControlCount - 1 do
    begin
      if scrollbox_interface.controls[i] is TDSBaseUIControl then
      begin

        ctrl := TDSBaseUIControl(scrollbox_interface.Controls[i]);

        if ctrl.parentElement = '' then
        begin
          if trim(ctrl.Group) = '' then
            node := tvElementHeirarchy.Items.AddChildObject(ParentNode, ctrl.Name_ + ' : ' + ctrl.TemplateName, pointer(ctrl))
          else
            node := tvElementHeirarchy.Items.AddChildObject(TTreeNode(groups.Objects[groups.IndexOf(trim(ctrl.Group))]), ctrl.Name_ + ' : ' + ctrl.TemplateName, pointer(ctrl));
          sl.AddObject(ctrl.Name_, node);
          node.ImageIndex := GetElementImageIndex(ctrl);

          node.SelectedIndex := node.ImageIndex;
          node.Cut := not ctrl.visible;
        end;
      end;
    end;
    ParentNode.Expand(true);
    //now run through and add the sub level elements
    for i := 0 to scrollbox_Interface.ControlCount - 1 do
    begin
      if scrollbox_interface.controls[i] is TDSBaseUIControl then
      begin

        ctrl := TDSBaseUIControl(scrollbox_interface.Controls[i]);

        if ctrl.parentElement <> '' then
        begin
          if sl.IndexOf(ctrl.ParentElement) = -1 then
            //we have an orphan
          else
          begin
            ParentNode := TTreeNode(sl.objects[sl.IndexOf(Ctrl.ParentElement)]);
          end;
          node := tvElementHeirarchy.Items.AddChildObject(ParentNode, ctrl.Name_ + ' : ' + ctrl.TemplateName, pointer(ctrl));
          node.ImageIndex := GetElementImageIndex(ctrl);
          node.SelectedIndex := node.ImageIndex;
          ParentNode.Expand(true);
        end;
      end;
    end;
  finally
    tvElementHeirarchy.items.endupdate;
    if assigned(sl) then freeAndNil(sl);
    if assigned(groups) then freeAndNil(groups);
  end;
end;

function TfrmUIDesigner.GetTemplateText(ControlType: string): string;
var
  sl: TStringlist;
  fn: string;
begin
  sl := nil;
  fn := ExtractFilePath(Application.ExeName) + '\Templates\' + ControlType + '.template';
  if FileExists(fn) then
  begin
    try
      sl := TStringlist.create;
      sl.LoadFromFile(fn);
      result := sl.text;
    finally
      if assigned(sl) then sl.free;
    end;
  end;
end;

procedure TFrmUIDesigner.AddNewControl(Control: TDSBaseUIControl; UpdateHeirarchy: boolean = true);
var
  s: string;
begin
  Control.Name_ := GenerateNewControlName(Control.TemplateName);
  Control.Onselect := Element_Selected;
  Control.OnHeirarchyChange := Element_HeirarchyChange;
  Control.OnChange := Element_Changed;
  Control.OnBoundsChange := ElementBoundsChanged;

  if trim(control.code.text) = '' then
  begin
    s := GetTemplateText(Control.templatename);
    if trim(s) <> '' then
      control.Code.text := s;
  end;

  scrollbox_interface.InsertControl(control);
  if updateheirarchy then
    UpdateElementHeirarchy;
  dirty := true;

end;

procedure TfrmUIDesigner.btn_generate_codeClick(Sender: TObject);
begin
  rtf_code.lines.text := scrollbox_interface.GenerateInterface;

end;

procedure TfrmUIDesigner.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmUIDesigner.Element_Changed(Sender: TObject);
begin
  dirty := true;

end;

procedure TfrmUIDesigner.ElementBoundsChanged(Sender: TObject);
begin
  dirty := true;
  if (CurrentElement <> nil) then
  begin
    if Sender is TDSBaseUIControl then
      TDSBaseUIControl(Sender).InsertPropertyTemplate('rect'); //update rect

    if ElementSelectionList.Count = 1 then
      UpdateEditWindowWithChanges(CurrentElement.Code);
  end;
end;

procedure TfrmUIDesigner.Element_Selected(Sender: TDSBaseUIControl; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  //  if GetAsyncKeyState(VK_CONTROL) = 0 then
  //  begin
  if ssctrl in Shift then
  begin
    AddToSelectionList(Sender);
    Sender.Sizer.GripColor := clgray;
  end
  else
  begin

    ClearSelectionList;
    CurrentElement := Sender;

    AddToSelectionList(CurrentElement);
  end;
  //  end
  //  else if currentElement = nil then
  //    CurrentElement := TDSBaseUIControl(Sender);

  //Dirty := true;
end;

procedure TfrmUIDesigner.Element_HeirarchyChange(Sender: TObject);
begin
  //CurrentElement := TDSBaseUIControl(Sender);
  UpdateElementHeirarchy;
  Dirty := true;
end;

procedure TfrmUIDesigner.LoadGasFile(filename: string);
begin

  CurrentElement := nil;
  try
    actNewInterface.Execute;
    ImportGas(filename);
    CurrentFilename := filename;
    JvMRUManager1.Add(CurrentFilename, 0);
    JvMRUManager1.UpdateRecentMenu;
    dirty := false;
  except
    on E: Exception do
    begin
      MessageDlg('Error loading interface:' + E.message, mtError, [mbOK], 0);
    end;
  end;


end;

procedure TfrmUIDesigner.FormCreate(Sender: TObject);
var
  s: string;
  ini: TIniFile;
  reg: TRegistry;
  splash: TfrmSplash;
begin
  Application.OnMessage := ApplicationMessage;
  FileFormatList.RegisterFileFormat('raw', 'Raw', 'GPG Raw', [ftRaster], true, true, TDSRawGraphic);

  if (ParamCount = 0) then
  begin
          Splash := TFrmSplash.Create(self);
          Splash.Show;
  end;
  rtfElementCode.Lines.text := scrollbox_interface.code;
  ElementSelectionList := TObjectList.create(false);
  s := GetTemplateText('interface');
  if trim(s) <> '' then
    scrollbox_Interface.Code := s;
  UpdateElementHeirarchy;

  ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
  //jvmrumanager1.LoadFromIni(ini, 'MRU');
  jvmrumanager1.Load;

  jvMRUManager1.RemoveInvalid;

  BackupOnSave := ini.ReadBool('Options', 'createbackup', true);

  actUserTool1.Caption := ini.ReadString('USERTOOL1', 'Name', 'User Tool 1');
  actUserTool2.Caption := ini.ReadString('USERTOOL2', 'Name', 'User Tool 2');
  actUserTool3.Caption := ini.ReadString('USERTOOL3', 'Name', 'User Tool 3');
  actUserTool4.Caption := ini.ReadString('USERTOOL4', 'Name', 'User Tool 4');
  actUserTool5.Caption := ini.ReadString('USERTOOL5', 'Name', 'User Tool 5');

  if ini.ReadString('USERTOOL1', 'ExeName', '') = '' then
    actUserTool1.Enabled := false
  else
    actUserTool1.Enabled := true;

  if ini.ReadString('USERTOOL2', 'ExeName', '') = '' then
    actUserTool2.Enabled := false
  else
    actUserTool2.Enabled := true;

  if ini.ReadString('USERTOOL3', 'ExeName', '') = '' then
    actUserTool3.Enabled := false
  else
    actUserTool3.Enabled := true;

  if ini.ReadString('USERTOOL4', 'ExeName', '') = '' then
    actUserTool4.Enabled := false
  else
    actUserTool4.Enabled := true;

  if ini.ReadString('USERTOOL5', 'ExeName', '') = '' then
    actUserTool5.Enabled := false
  else
    actUserTool5.Enabled := true;

  actUserTool1.Hint := actUserTool1.caption;
  actUserTool2.Hint := actUserTool2.caption;
  actUserTool3.Hint := actUserTool3.caption;
  actUserTool4.Hint := actUserTool4.caption;
  actUserTool5.Hint := actUserTool5.caption;
  use_component_textures := false;
  Texture_Cache_Root := ini.ReadString('Options', 'Texture Cache Dir', ExtractFilePath(Application.ExeName) + '\texturecache');
  try
    jvDirectoryListBox1.Directory := Texture_cache_root;
  except
    ;
  end;
  //  ShellTreeView1.Path  := Texture_Cache_Root;

  ini.free;

  currentElement := nil;

  reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\SOFTWARE\Microsoft\Microsoft Games\DungeonSiege\1.0', True) then
    begin

      DUNGEON_SIEGE_PATH := Reg.ReadString('EXE PATH') + '\DungeonSiege.exe';
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
    inherited;
  end;
  if fileExists(DUNGEON_SIEGE_PATH) then
    mnuLaunchDS.enabled := true
  else
    mnuLaunchDS.enabled := false;

  if ParamCount > 0 then
  begin
     Show;
     LoadGasFile(ParamStr(1));
  end;


end;

procedure TfrmUIDesigner.scrollbox_InterfaceClick(Sender: TObject);
begin
  scrollbox_interface.Setfocus;
  CurrentElement := nil;
end;

procedure TfrmUIDesigner.rtfElementCode2Change(Sender: TObject);
begin

  if CurrentElement <> nil then
  begin
    CurrentElement.Code.text := rtfElementCode.text;
    dirty := true;
  end
  else
  begin
    scrollbox_Interface.code := rtfElementCode.text;
    dirty := true;
  end;
end;

procedure TfrmUIDesigner.tabGeneratedCodeShow(Sender: TObject);
begin
  try
    rtf_code.BeginUpdate;
    rtf_code.Text := '';
    rtf_code.text := scrollbox_interface.GenerateInterface;
  finally
    rtf_code.EndUpdate;
  end;
end;

procedure TfrmUIDesigner.tvElementHeirarchyChange(Sender: TObject;
  Node: TTreeNode);
var
  i: integer;
begin
  if (tvElementHeirarchy.SelectionCount = 1) then
  begin
    for i := 0 to ElementSelectionList.Count - 1 do
    begin
      TDSBaseUIControl(ElementSelectionList.Items[i]).Selected := false;
    end;
    ElementSelectionList.Clear;

    if (Node = tvElementHeirarchy.Items[0]) then // root
      CurrentElement := nil
    else if Node.Data = nil then
    begin
      if pos('GROUP:', uppercase(node.Text)) > 0 then
      begin
        //
      end
      else
      begin
        CurrentElement := nil;
      end
    end
    else
    begin
      CurrentElement := TDSBaseUIControl(Node.Data);
      CurrentElement.Selected := Node.Selected;

      if Node.Selected then
      begin
        if ElementSelectionList.IndexOf(TDSBaseUIControl(Node.Data)) = -1 then
          ElementSelectionList.Add(TDSBaseUIControl(Node.Data));
      end
      else
      begin
        ElementSelectionList.Remove(TDSBaseUIControl(Node.Data));
        TDSBaseUIControl(Node.Data).Selected := false;
      end;

      //AddToSelectionList(CurrentElement);
      //CurrentElement.BringToFront;
    end;
    if (Node <> tvElementHeirarchy.Selected) and (Node.Selected = false) then
    begin
      CurrentElement := TDSBaseUIControl(tvElementHeirarchy.Selected.Data);
      CurrentElement.Selected := true;
      ElementSelectionList.Add(TDSBaseUIControl(tvElementHeirarchy.Selected.Data));
    end;
    UpdateEnabledflags;
  end
  else
  begin

    if Node <> nil then
      if Node.Data <> nil then
      begin
        TDSBaseUIControl(Node.Data).Selected := Node.Selected;

        if Node.Selected then
        begin
          if ElementSelectionList.IndexOf(TDSBaseUIControl(Node.Data)) = -1 then
            ElementSelectionList.Add(TDSBaseUIControl(Node.Data));
        end
        else
        begin
          ElementSelectionList.Remove(TDSBaseUIControl(Node.Data));
          TDSBaseUIControl(Node.Data).Selected := false;
        end;

        //AddToSelectionList(TDSBaseUIControl(Node.Data));
      end;
    UpdateEnabledflags;
  end;

end;

procedure TfrmUIDesigner.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  rslt: integer;
  ini: TIniFile;
begin
  if dirty then
  begin
    rslt := MessageDlg('Save Changes?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if rslt = mrCancel then canclose := false;
    if rslt = mrYes then actSaveGas.Execute;
  end;
  ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
  //jvmrumanager1.SaveToIni(ini, 'MRU');
  jvmrumanager1.Save;
  ini.free;
end;

procedure TfrmUIDesigner.actInsertComponent(Sender: TObject);
var
  ctrl: TDSBaseUIControl;
  RootParser: TGasInterestItem;
  ParentParser: TGasInterestItem;
  Parser: TGasInterestItem;
  i: integer;
  x: integer;
  templateText: string;
  ParentCtrl: TDSBaseUIControl;
begin
  if Sender is TAction then
  begin

    // This needs to be modified to be able to handle those instances where our
    // template has more than one control
    RootParser := nil;
    try

      RootParser := TGasInterestItem.create;
      //We're going to add a dummy interface around this template
      //text so that the component depth function works correctly
      //
      //We don't want to nest more than two components deep from the top
      //of the interface and the parser is assuming that the interface is
      //the root of the parse tree.  For templates, this is not the case.
      templatetext := '[dummy]{' + GetTemplateText(TAction(Sender).hint) + '}';

      RootParser.ParseGas(TemplateText);
      if RootParser.ChildBlocks.count = 0 then raise exception.create('Template is invalid: ' + TAction(Sender).hint);

      ParentParser := RootParser.ChildBlocks.Items[0];
      ClearSelectionList;

      for i := 0 to ParentParser.ChildBlocks.Count - 1 do
      begin

        if ParentParser.ChildBlocks.Items[i].ComponentType = gctComponentBlock then continue;
        if not ShouldComponentizeTemplate(ParentParser.ChildBlocks.Items[i].ComponentTemplate) then continue;
        ParentCtrl := CreateUIControlFromTemplateName(ParentParser.ChildBlocks.Items[i].ComponentTemplate, scrollbox_interface);

        AddNewControl(Parentctrl, false);
        ParentParser.ChildBlocks.Items[i].Name := ParentCtrl.Name_;
        ParentCtrl.LoadComponentFromParser(ParentParser.ChildBlocks.Items[i]);
        if i = 0 then CurrentElement := Parentctrl;
        Parentctrl.Selected := true;
        AddToSelectionList(Parentctrl);

        Parser := ParentParser.ChildBlocks.Items[i];

        for x := 0 to Parser.ChildBlocks.Count - 1 do
        begin
          if Parser.ChildBlocks.Items[x].ComponentType = gctComponentBlock then continue;
          if not ShouldComponentizeTemplate(Parser.ChildBlocks.Items[x].ComponentTemplate) then continue;
          Ctrl := CreateUIControlFromTemplateName(Parser.ChildBlocks.Items[x].ComponentTemplate, scrollbox_interface);
          AddNewControl(ctrl, false);
          Parser.ChildBlocks.Items[x].Name := ctrl.Name_;
          Ctrl.LoadComponentFromParser(Parser.ChildBlocks.Items[x]);

          ctrl.ParentElement := ParentCtrl.Name_;

          ctrl.Selected := true;
          AddToSelectionList(ctrl);
        end;
      end;
      UpdateElementHeirarchy;
      //Force a refresh if this component has more than one child
//      if Parser.count > 1 then
//        actRefresh.Execute;
    finally
      freeAndNil(RootParser);
    end;

  end;

end;

procedure TfrmUIDesigner.actBringToFrontExecute(Sender: TObject);
var
  i: integer;
  currentz: integer;
  ctrl: TDSBaseUIControl;
begin

  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    ctrl.BringToFront;
  end;
  currentz := 0;
  for i := 0 to scrollbox_Interface.Controlcount - 1 do
  begin
    if scrollbox_interface.controls[i] is TDSBaseUIControl then
    begin
      TDSBaseUIControl(scrollbox_interface.controls[i]).draw_order := currentz;
      TDSBaseUIControl(scrollbox_interface.controls[i]).InsertPropertyTemplate('draw_order', intToStr(currentz)); //update rect
      currentz := currentz + 1;
    end;
  end;

  if (CurrentElement <> nil) and (ElementSelectionList.Count = 1) then
    UpdateEditWindowWithChanges(CurrentElement.Code);

  dirty := true;

end;

procedure TfrmUIDesigner.actSendToBackExecute(Sender: TObject);
var
  i: integer;
  currentz: integer;
  ctrl: TDSBaseUIControl;
begin

  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    ctrl.SendToBack;
  end;
  currentz := 0;
  for i := 0 to scrollbox_Interface.Controlcount - 1 do
  begin
    if scrollbox_interface.controls[i] is TDSBaseUIControl then
    begin
      TDSBaseUIControl(scrollbox_interface.controls[i]).draw_order := currentz;
      TDSBaseUIControl(scrollbox_interface.controls[i]).InsertPropertyTemplate('draw_order', intToStr(currentz)); //update rect
      currentz := currentz + 1;
    end;
  end;

  if (CurrentElement <> nil) and (ElementSelectionList.Count = 1) then
    UpdateEditWindowWithChanges(CurrentElement.Code);

  dirty := true;
end;

procedure TfrmUIDesigner.SetComponentVisibility(Node: TTreeNode; isVisible: boolean);
var
  i: integer;
  OldHeirarchy: TNotifyEvent;
begin

  if Node <> nil then
  begin
    if Node.Data <> nil then
    begin
      if TObject(Node.Data) is TDSBaseUIControl then
      begin
        OldHeirarchy := TDSBaseUIControl(Node.Data).OnHeirarchyChange;
        TDSBaseUIControl(Node.Data).OnHeirarchyChange := nil;
        TDSBaseUIControl(Node.Data).Visible := isVisible;
        TDSBaseUIControl(Node.Data).OnHeirarchyChange := OldHeirarchy;
        node.Cut := not isVisible;
      end;
    end;
    if Node.HasChildren then
    begin
      for i := 0 to Node.Count - 1 do
        SetComponentVisibility(Node.Item[i], isVisible);
    end;
  end;

end;

procedure TfrmUIDesigner.actHideComponentsExecute(Sender: TObject);
var
  i: integer;
begin

  for i := 0 to tvElementHeirarchy.SelectionCount - 1 do
    SetComponentVisibility(tvElementHeirarchy.Selections[i], false);

  //  UpdateElementHeirarchy;
end;

procedure TfrmUIDesigner.actShowComponentsExecute(Sender: TObject);
var
  i: integer;
begin

  for i := 0 to tvElementHeirarchy.SelectionCount - 1 do
    SetComponentVisibility(tvElementHeirarchy.Selections[i], true);
  //  UpdateElementHeirarchy;
end;

procedure TfrmUIDesigner.actOpenGasExecute(Sender: TObject);
var
  rslt: integer;
begin
  if dirty then
  begin
    rslt := MessageDlg('Save Changes?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if rslt = mrCancel then exit;
    if rslt = mrYes then actSaveGas.Execute;
  end;

  if OpenDialog1.Execute then
  begin
    CurrentElement := nil;
    try
      dirty := false;
      actNewInterface.Execute;
      ImportGas(OpenDialog1.FileName);
      CurrentFilename := OpenDialog1.Filename;
      JvMRUManager1.Add(CurrentFilename, 0);
      JvMRUManager1.UpdateRecentMenu;
      dirty := false;
    except
      on E: Exception do
      begin
        MessageDlg('Error loading interface:' + E.message, mtError, [mbOK], 0);
      end;
    end;

  end;

end;

procedure TfrmUIDesigner.actSaveGasExecute(Sender: TObject);
begin
  if not fileexists(CurrentFilename) then
    actSaveAs.execute
  else
  begin
    ExportGas(CurrentFilename);

    dirty := false;
  end;

end;

procedure TfrmUIDesigner.actSaveAsExecute(Sender: TObject);
begin
  if not fileexists(CurrentFilename) then
    SaveDialog1.FileName := scrollbox_interface.interface_name
  else
    savedialog1.filename := currentfilename;

  if SaveDialog1.Execute then
  begin
    ExportGas(SaveDialog1.FileName);

    CurrentFilename := SaveDialog1.filename;
    JvMRUManager1.Add(CurrentFilename, 0);
    JvMRUManager1.UpdateRecentMenu;
    Dirty := false;
  end;

end;

procedure TfrmUIDesigner.JvMRUManager1Click(Sender: TObject;
  const RecentName, Caption: string; UserData: Integer);
var
  rslt: integer;
begin
  if dirty then
  begin
    rslt := MessageDlg('Save Changes?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if rslt = mrCancel then exit;
    if rslt = mrYes then actSaveGas.Execute;
  end;
  CurrentElement := nil;
  try
    dirty := false;
    actNewInterface.Execute;
    ImportGas(RecentName);
    Currentfilename := recentname;
    OpenDialog1.filename := RecentName;
    dirty := false;
  except
    on E: Exception do
    begin
      MessageDlg('Error loading interface:' + E.message, mtError, [mbOK], 0);
    end;
  end;

end;

procedure TfrmUIDesigner.About1Click(Sender: TObject);
begin
  with TAboutBox.Create(self) do
  begin
    ShowModal;
    free;
  end;
end;

procedure TfrmUIDesigner.actCopyComponentsExecute(Sender: TObject);
var
  TempCopy: TDSBaseUIControl;
  i: integer;

  lst: TObjectList;

  NameMapping: TStringlist;
begin
  lst := nil;
  NameMapping := nil;
  try
    lst := TObjectList.create(false);
    NameMapping := TStringlist.create;
    for i := 0 to ElementSelectionList.count - 1 do
    begin
      TempCopy := CloneUIControl(TDSBaseUIControl(ElementSelectionList.Items[i]));
      AddNewControl(TempCopy, false);
      TDSBaseUIControl(ElementSelectionList.Items[i]).AssignTo(TempCopy);
      lst.Add(tempcopy);

      NameMapping.Add(TDSBaseUIControl(ElementSelectionList.Items[i]).Name_ + '=' + tempcopy.Name_);

    end;

    ClearSelectionList;
    for i := 0 to lst.count - 1 do
    begin
      if i = 0 then CurrentElement := TDSBaseUIControl(lst.Items[i]);
      TDSBaseUIControl(lst.Items[i]).BringToFront;

      //if we are copying parents with children, make sure that the new child is under
      //the new parent instead of the old parent.
      if NameMapping.IndexOfName(TDSBaseUIControl(lst.Items[i]).ParentElement) <> -1 then
        TDSBaseUIControl(lst.Items[i]).ParentElement := NameMapping.Values[TDSBaseUIControl(lst.Items[i]).ParentElement];

      TDSBaseUIControl(lst.Items[i]).Selected := true;
      AddToSelectionList(TDSBaseUIControl(lst.Items[i]));
    end;

    UpdateElementHeirarchy;
    //    UpdateElementHeirarchy;
  finally
    if assigned(lst) then FreeAndNil(lst);
    if assigned(NameMapping) then freeAndNil(NameMapping);

  end;
end;

procedure TfrmUIDesigner.actDeleteComponentExecute(Sender: TObject);
var
  TempCopy: TDSBaseUIControl;
  i: integer;
  NewList: TObjectList;
begin
  NewList := nil;
  try
    NewList := TObjectList.Create(false);
    for i := 0 to ElementSelectionList.Count - 1 do
      NewList.Add(ElementSelectionList.Items[i]);

    CurrentElement := nil;

    for i := 0 to NewList.count - 1 do
    begin

      TempCopy := TDSBaseUIControl(NewList.Items[i]);
      Tempcopy.Selected := false;
      scrollbox_Interface.RemoveComponent(TempCopy);
      TempCopy.Free;

    end;

    UpdateElementHeirarchy;
  finally
    if assigned(newList) then FreeAndNil(NewList);
  end;

end;

procedure TfrmUIDesigner.actRefreshExecute(Sender: TObject);
var
  s: string;
  //For sanity, we really should parse this twice.. first to see if we can then
  //again for real
begin
  //this will rebuild the display
  s := scrollbox_interface.GenerateInterface;
  RebuildFromGas(s);
end;

procedure TfrmUIDesigner.mnuLaunchDSClick(Sender: TObject);
begin
  ShellExecute(handle, 'open', pchar(DUNGEON_SIEGE_PATH), '', '', 0);
end;

procedure TFrmUIDesigner.LaunchUserTool(toolnum: integer);
var
  exe: string;
  parms: string;
  ini: TIniFile;
begin
  ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
  exe := ini.ReadString('USERTOOL' + intToStr(toolnum), 'EXEName', '');
  Parms := ini.ReadString('USERTOOL' + intToStr(toolnum), 'Parms', '');
  if ini.ReadBool('USERTOOL' + intToStr(toolnum), 'SaveFirst', false) then
    actSaveGas.Execute;
  ini.free;

  parms := Stringreplace(parms, '%f', currentfilename, [rfreplaceall, rfignorecase]);
  ShellExecute(handle, 'open', pchar(exe), pchar(parms), '', 0);

end;

procedure TfrmUIDesigner.actUserTool1Execute(Sender: TObject);
begin
  LaunchUserTool(1);
end;

procedure TfrmUIDesigner.actUserTool2Execute(Sender: TObject);
begin
  LaunchUserTool(2);
end;

procedure TfrmUIDesigner.actUserTool3Execute(Sender: TObject);
begin
  LaunchUserTool(3);
end;

procedure TfrmUIDesigner.actAlignLeftExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.Left := CurrentElement.Left;

  end;
end;

procedure TfrmUIDesigner.actAlignRightExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.Left := CurrentElement.Right - ctrl.width;

  end;
end;

procedure TfrmUIDesigner.actAlignTopExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.top := CurrentElement.top;

  end;

end;

procedure TfrmUIDesigner.actAlignBottomExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.top := CurrentElement.Bottom - ctrl.height;
  end;
end;

procedure TfrmUIDesigner.actMakeSameWidthExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.width := CurrentElement.width;
  end;

end;

procedure TfrmUIDesigner.actMakeSameHeightExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.height := CurrentElement.height;
  end;
end;

procedure TfrmUIDesigner.actMakeSameSizeExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.width := CurrentElement.width;
    ctrl.height := CurrentElement.height;
  end;

end;

procedure TfrmUIDesigner.actStackVerticallyExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
  Newbottom: integer;
begin
  NewBottom := Currentelement.Bottom;
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.Top := newbottom + 1;
    newbottom := ctrl.bottom;
    ctrl.left := CurrentElement.left;
  end;

end;

procedure TfrmUIDesigner.actStackHorizontallyExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
  Newright: integer;
begin
  NewRight := Currentelement.Right;
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrl = CurrentElement then continue;
    ctrl.left := newright + 1;
    newright := ctrl.right;
    ctrl.top := CurrentElement.top;
  end;

end;

procedure TfrmUIDesigner.mnuOptionsClick(Sender: TObject);
var
  options: TfrmOptions;
  ini: TMemIniFile;
begin
  ini := TMemIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
  Options := TfrmOptions.Create(self);
  Options.Ini := ini;
  Options.ckBackupOnSave.Checked := BackupOnSave;

  Options.edTextureSource.text := Texture_Cache_root;
  Options.lstTools.Items.Strings[0] := ini.ReadString('USERTOOL1', 'Name', 'User Tool 1');
  Options.lstTools.Items.Strings[1] := ini.ReadString('USERTOOL2', 'Name', 'User Tool 2');
  Options.lstTools.Items.Strings[2] := ini.ReadString('USERTOOL3', 'Name', 'User Tool 3');
  Options.lstTools.Items.Strings[3] := ini.ReadString('USERTOOL4', 'Name', 'User Tool 4');
  Options.lstTools.Items.Strings[4] := ini.ReadString('USERTOOL5', 'Name', 'User Tool 5');

  if Options.ShowModal = mrOk then
  begin
    actUserTool1.Caption := ini.ReadString('USERTOOL1', 'Name', 'User Tool 1');
    actUserTool2.Caption := ini.ReadString('USERTOOL2', 'Name', 'User Tool 2');
    actUserTool3.Caption := ini.ReadString('USERTOOL3', 'Name', 'User Tool 3');
    actUserTool4.Caption := ini.ReadString('USERTOOL4', 'Name', 'User Tool 4');
    actUserTool5.Caption := ini.ReadString('USERTOOL5', 'Name', 'User Tool 5');

    if ini.ReadString('USERTOOL1', 'ExeName', '') = '' then
      actUserTool1.Enabled := false
    else
      actUserTool1.Enabled := true;

    if ini.ReadString('USERTOOL2', 'ExeName', '') = '' then
      actUserTool2.Enabled := false
    else
      actUserTool2.Enabled := true;

    if ini.ReadString('USERTOOL3', 'ExeName', '') = '' then
      actUserTool3.Enabled := false
    else
      actUserTool3.Enabled := true;

    if ini.ReadString('USERTOOL4', 'ExeName', '') = '' then
      actUserTool4.Enabled := false
    else
      actUserTool4.Enabled := true;

    if ini.ReadString('USERTOOL5', 'ExeName', '') = '' then
      actUserTool5.Enabled := false
    else
      actUserTool5.Enabled := true;

    actUserTool1.Hint := actUserTool1.caption;
    actUserTool2.Hint := actUserTool2.caption;
    actUserTool3.Hint := actUserTool3.caption;
    actUserTool4.Hint := actUserTool4.caption;
    actUserTool5.Hint := actUserTool5.caption;

    BackupOnSave := Options.ckBackupOnSave.checked;
    Texture_Cache_Root := Options.edTextureSource.text;

    try
      jvDirectoryListBox1.Directory := Texture_cache_root;
    except
      ;
    end;
    ini.WriteString('Options', 'Texture Cache Dir', Texture_Cache_Root);
    ini.WriteBool('Options', 'createbackup', BackupOnSave);
    ini.UpdateFile;

    actRefresh.Execute;

  end;
  Options.free;
  ini.free;
end;

procedure TfrmUIDesigner.actUserTool4Execute(Sender: TObject);
begin
  LaunchUserTool(4);
end;

procedure TfrmUIDesigner.actUserTool5Execute(Sender: TObject);
begin
  LaunchUserTool(5);
end;

procedure TfrmUIDesigner.actReloadExecute(Sender: TObject);
var
  sl: TSTringlist;
  rslt: integer;
begin
  if not fileExists(CurrentFilename) then
  begin
    MessageDlg('File has not been saved.  Cannot reload.', mtError, [mbOK], 0);
    exit;

  end;

  if dirty then
  begin
    rslt := MessageDlg('Reload file and lose changes?', mtConfirmation, [mbYes, mbNo], 0);
    if rslt = mrNo then exit;
  end;
  CurrentElement := nil;
  sl := nil;
  try
    sl := TStringlist.create;
    sl.LoadFromFile(CurrentFilename);
    REbuildFromGas(sl.text);

  finally
    if assigned(sl) then freeAndNil(sl);
  end;
end;

procedure TfrmUIDesigner.Recalculatedraworder1Click(Sender: TObject);
var
  i: integer;
  res: string;
  basez: integer;
  currentz: integer;
begin
  basez := 0;
  for i := 0 to scrollbox_Interface.Controlcount - 1 do
  begin
    if scrollbox_interface.controls[i] is TDSBaseUIControl then
    begin
      basez := TDSBaseUIControl(scrollbox_interface.controls[i]).draw_order;
      break;
    end;
  end;

  res := IntToStr(basez);
  if InputQuery('Rebuild component draw_order indexes', 'draw_order value of bottom element', res) then
  begin
    basez := StrToIntDef(res, basez);
    currentz := basez;
    for i := 0 to scrollbox_Interface.Controlcount - 1 do
    begin
      if scrollbox_interface.controls[i] is TDSBaseUIControl then
      begin
        TDSBaseUIControl(scrollbox_interface.controls[i]).draw_order := currentz;
        currentz := currentz + 1;
      end;
    end;
  end;
end;

procedure TfrmUIDesigner.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Assigned(ElementSelectionList) then freeAndNil(ElementSelectionList);
end;

procedure TfrmUIDesigner.AddToSelectionList(Element: TDSBaseUIControl);
var
  HoldEvent: TTvChangedEvent;
  Node: TTreeNode;
  NodeSelList: TList;
  i: integer;
begin
  if Element.Selected then
  begin
    if ElementSelectionList.IndexOf(Element) = -1 then
      ElementSelectionList.Add(Element);
  end
  else
    ElementSelectionList.Remove(Element);

  HoldEvent := tvElementHeirarchy.OnChange;
  tvElementHeirarchy.OnChange := nil;
  tvElementHeirarchy.Items.beginUpdate;
  NodeSelList := nil;
  try
    NodeSelList := TList.create;
    for i := 0 to ElementSelectionList.Count - 1 do
    begin
      Node := GetNodeByData(tvElementHeirarchy, ElementSelectionList.Items[i]);
      if node <> nil then NodeSelList.Add(Node);
    end;
    tvElementHeirarchy.Select(NodeSelList);
  finally
    if assigned(NodeSelList) then freeAndNil(NodeSelList);
  end;
  //  if node <> nil then
  //  begin
   //   node.Selected := Element.Selected;
   // end;
  tvElementHeirarchy.Items.EndUpdate;
  tvElementHeirarchy.OnChange := holdEvent;
  UpdateEnabledflags;
end;

procedure TfrmUIDesigner.ClearSelectionList;
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    TDSBaseUIControl(ElementSelectionList.Items[i]).Selected := false;
  end;
  ElementSelectionList.Clear;
  tvElementHeirarchy.Items.BeginUpdate;
  for i := 0 to tvElementHeirarchy.Items.Count - 1 do
    if tvElementHeirarchy.Items[i].Data <> nil then
    begin
      ctrl := TDSBaseUIControl(tvElementHeirarchy.Items[i].Data);
      tvElementHeirarchy.Items[i].Selected := ctrl.Selected;
      tvElementHeirarchy.Items[i].Cut := not ctrl.Visible;
    end;
  tvElementHeirarchy.Items.EndUpdate;
  UpdateEnabledflags;
end;

function TfrmUIDesigner.GetNodeByData(Treeview: TTreeView; Data: Pointer): TTreeNode;
var
  i: integer;
begin
  result := nil;
  for i := 0 to TreeView.Items.Count - 1 do
    if TreeView.Items[i].Data = Data then
    begin
      result := TreeView.Items[i];
      break;
    end;

end;

procedure TfrmUIDesigner.actShiftLeftExecute(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
  inc: integer;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrlPressed then
      inc := 5
    else
      inc := 1;
    if ShiftPressed then
      ctrl.Width := ctrl.Width - inc
    else
      ctrl.Left := ctrl.Left - inc;
  end;
end;

procedure TfrmUIDesigner.actShiftRightExecute(Sender: TObject);
var
  i: integer;
  incr: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrlPressed then
      incr := 5
    else
      incr := 1;
    if ShiftPressed then
      ctrl.Width := ctrl.Width + incr
    else
      ctrl.Left := ctrl.Left + incr;
  end;

end;

procedure TfrmUIDesigner.actShiftTopExecute(Sender: TObject);
var
  i: integer;
  incr: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrlPressed then
      incr := 5
    else
      incr := 1;
    if ShiftPressed then
      ctrl.Height := ctrl.Height - incr
    else
      ctrl.Top := ctrl.Top - incr;

  end;

end;

procedure TfrmUIDesigner.actShiftDownExecute(Sender: TObject);
var
  i: integer;
  incr: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);
    if ctrlPressed then
      incr := 5
    else
      incr := 1;
    if ShiftPressed then
      ctrl.Height := ctrl.Height + incr
    else
      ctrl.Top := ctrl.Top + incr;
  end;

end;

procedure TFrmUIDesigner.UpdateEnabledFlags;
begin
  if ElementSelectionList.Count = 0 then
  begin
    actBringToFront.enabled := false;
    actSendToBack.enabled := false;
    actAlignLeft.Enabled := false;
    actAlignRight.enabled := false;
    actAlignTop.enabled := false;
    actAlignBottom.enabled := false;
    actMakeSameWidth.enabled := false;
    actMakeSameHeight.enabled := false;
    actMakeSameSize.enabled := false;
    actStackVertically.enabled := false;
    actStackHorizontally.enabled := false;
    actShiftLeft.enabled := false;
    actShiftRight.enabled := false;
    actShiftTop.enabled := false;
    actShiftDown.enabled := false;
    actDeleteComponent.Enabled := false;
    actCopycomponents.Enabled := false;
    actBrowseForTexture.enabled := false;
    actUVCoordHelper.enabled := false;

    rtfElementCode.ReadOnly := false;
    rtfElementCode.Font.Color := clWindowText;
    rtfElementCode.Color := clWindow;
    jvInspector1.ReadOnly := false;
    jvInspector1.Painter.BackgroundColor := clWindow;
    jvInspector1.Painter.NameColor := clWindowText;
    jvInspector1.Painter.ValueColor := clWindowText;

  end
  else if ElementSelectionList.Count = 1 then
  begin
    actBringToFront.enabled := true;
    actSendToBack.enabled := true;
    actAlignLeft.Enabled := false;
    actAlignRight.enabled := false;
    actAlignTop.enabled := false;
    actAlignBottom.enabled := false;
    actMakeSameWidth.enabled := false;
    actMakeSameHeight.enabled := false;
    actMakeSameSize.enabled := false;
    actStackVertically.enabled := false;
    actStackHorizontally.enabled := false;
    actShiftLeft.enabled := true;
    actShiftRight.enabled := true;
    actShiftTop.enabled := true;
    actShiftDown.enabled := true;
    actDeleteComponent.Enabled := true;
    actCopycomponents.Enabled := true;
    actBrowseForTexture.enabled := true;
    actUVCoordHelper.enabled := true;

    rtfElementCode.ReadOnly := false;
    rtfElementCode.Font.Color := clWindowText;
    rtfElementCode.Color := clWindow;
    jvInspector1.ReadOnly := false;
    jvInspector1.Painter.BackgroundColor := clWindow;
    jvInspector1.Painter.NameColor := clWindowText;
    jvInspector1.Painter.ValueColor := clWindowText;

  end
  else
  begin
    actBringToFront.enabled := true;
    actSendToBack.enabled := true;
    actAlignLeft.Enabled := true;
    actAlignRight.enabled := true;
    actAlignTop.enabled := true;
    actAlignBottom.enabled := true;
    actMakeSameWidth.enabled := true;
    actMakeSameHeight.enabled := true;
    actMakeSameSize.enabled := true;
    actStackVertically.enabled := true;
    actStackHorizontally.enabled := true;
    actShiftLeft.enabled := true;
    actShiftRight.enabled := true;
    actShiftTop.enabled := true;
    actShiftDown.enabled := true;
    actDeleteComponent.Enabled := true;
    actCopycomponents.Enabled := true;
    actBrowseForTexture.enabled := true;

    rtfElementCode.ReadOnly := true;
    rtfElementCode.Font.Color := clGrayText;
    rtfElementCode.Color := clBtnHighlight;
    jvInspector1.ReadOnly := true;
    jvInspector1.Painter.BackgroundColor := clBtnHighlight;
    jvInspector1.Painter.NameColor := clGrayText;
    jvInspector1.Painter.ValueColor := clGrayText;

  end;
end;

procedure TfrmUIDesigner.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ssCtrl in Shift then CtrlPressed := true;
  if ssShift in Shift then ShiftPressed := true;
end;

procedure TfrmUIDesigner.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if not (ssCtrl in Shift) then CtrlPressed := false;
  if not (ssShift in Shift) then ShiftPressed := false;
end;

procedure TfrmUIDesigner.JvInspector1DataValueChanged(Sender: TObject;
  const Data: TJvCustomInspectorData);
var
  sl: TStringlist;
begin

  Dirty := true;
  if CurrentElement <> nil then
  begin
    if Data.ItemCount = 1 then
    begin

      CurrentElement.InsertPropertyTemplate(Data.Items[0].DisplayName, Data.Items[0].DisplayValue);

    end
    else
      CurrentElement.InsertPropertyTemplate(Data.Name);
    UpdateEditWindowWithChanges(CurrentElement.Code);
    rtfElementCode.Lines.Text := CurrentElement.Code.text;
    //rtfElementCode.SetLeftTop(0, 0); TSH-10

  end
  else
  begin

    if Data.ItemCount = 1 then
    begin

      scrollbox_Interface.InsertPropertyTemplate(Data.Items[0].DisplayName, Data.Items[0].DisplayValue);
    end
    else
      scrollbox_Interface.InsertPropertyTemplate(Data.Name);

    sl := nil;
    try
      sl := TStringlist.create;
      sl.Text := scrollbox_interface.code;
      UpdateEditWindowWithChanges(sl);
    finally
      if assigned(sl) then freeAndNil(sl);
    end;

  end;

  if uppercase(Data.Name) = 'INTERFACE_NAME' then
    jvInspector1.root.Items[0].DisplayName := scrollbox_Interface.interface_name + ' : interface'
  else if (uppercase(Data.Name) = 'ELEMENTNAME') and (CurrentElement <> nil) then
    jvInspector1.Root.Items[0].DisplayName := CurrentElement.name_ + ': ' + CurrentElement.templatename;

end;

procedure TfrmUIDesigner.SelectAllInGroup1Click(Sender: TObject);
begin
  //  Node := tvElementHeirarchy.Selected;
  SelectGroup(tvElementHeirarchy.Selected);
  //  tvElementHeirarchy.Select(Node, []);
end;

procedure TFrmUIDesigner.SelectGroup(Root: TTreeNode);
var
  i: integer;
  grouptoselect: string;
begin
  if Root <> nil then
  begin
    if Root.Data <> nil then
    begin
      grouptoselect := TDSBaseUIControl(Root.Data).group;
      if Root.Parent <> nil then
      begin
        for i := 0 to Root.Parent.Count - 1 do
        begin
          if Root.Parent.item[i].Data <> nil then
          begin
            if comparetext(TDSBaseUIControl(root.parent.item[i].data).group, grouptoselect) = 0 then
            begin
              TDSBaseUIControl(root.parent.item[i].data).Selected := true;
              AddToSelectionList(TDSBaseUIControl(root.parent.item[i].data));
              TDSBaseUIControl(root.parent.item[i].data).Sizer.GripColor := clgray;
            end;
          end;
        end;
      end;
    end;

    if Root.HasChildren then
    begin
      for i := 0 to Root.Count - 1 do
        SelectGroup(Root.Item[i]);
    end;
  end;
end;

procedure TfrmUIDesigner.ipoftheDay1Click(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := nil;
  try
    ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
    if ini.ReadBool('Options', 'TipOfDay', true) then
      jvTipOfDay1.Options := jvTipOfDay1.Options + [toShowOnStartup]
    else
      jvTipOfDay1.Options := jvTipOfDay1.Options - [toShowOnStartup]

  finally
    if assigned(ini) then freeAndNil(ini);
  end;
  jvTipOfDay1.Execute;
end;

procedure TfrmUIDesigner.JvTipOfDay1AfterExecute(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := nil;
  try
    ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
    ini.WriteBool('Options', 'TipOfDay', toShowOnStartup in jvTipOfDay1.Options);
  finally
    if assigned(ini) then freeAndNil(ini);

  end;

end;


procedure TfrmUIDesigner.actNewInterfaceExecute(Sender: TObject);
var

  s: string;
  rslt: integer;
begin
  if dirty then
  begin
    rslt := MessageDlg('Save Changes?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
    if rslt = mrCancel then exit;
    if rslt = mrYes then actSaveGas.Execute;
  end;
  scrollbox_Interface.SetFocus;
  CurrentElement := nil;
  scrollbox_interface.Clear;
  CurrentFilename := 'Untitled';
  s := GetTemplateText('interface');
  if trim(s) <> '' then
    scrollbox_Interface.Code := s;

  UpdateElementHeirarchy;
  CurrentElement := nil;
end;

procedure TfrmUIDesigner.DumpInterfaceWithTokens1Click(Sender: TObject);
var
  i: integer;
  sl: TStringlist;

begin
  sl := TStringlist.create;
  for i := 0 to scrollbox_Interface.ComponentCount - 1 do
  begin
    if scrollbox_interface.Controls[i] is TDSBaseUIControl then
    begin

      sl.text := sl.text + StringReplace(TDSBaseUIControl(scrollbox_interface.controls[i]).Code.text, '#name#', TDSBaseUIControl(scrollbox_interface.controls[i]).Name_, [rfignorecase]);

    end;
  end;

  for i := sl.Count - 1 downto 0 do
  begin
    if pos('[', uppercase(sl.strings[i])) > 0 then continue;
    //    if pos('[MESSAGES]',uppercase(sl.strings[i])) > 0 then continue;
    if Pos('=', sl.strings[i]) = 0 then
    begin
      sl.Delete(i);
      continue;
    end
    else if pos('#', sl.strings[i]) <> 0 then
    begin
      sl.delete(i);
      continue;
    end;
  end;

  rtfElementCode.Text := sl.text;
  sl.Free;
end;

procedure TfrmUIDesigner.rtfElementCodeKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //this needs to be done because for some reason the change event does not get the text
  //post delete, just pre-delete, so we need to grab it again after the key is released.
  case key of
    46, 8: rtfElementCode.OnChange(rtfElementCode); //bksp or del
  end;

end;

procedure TfrmUIDesigner.SetBackgroundReferenceImage1Click(
  Sender: TObject);
begin
  if openDlgBGImage.Execute then
  begin
    Dirty := true;
    scrollbox_interface.designer_ref_img := openDlgBGImage.Filename;
    scrollbox_Interface.InsertPropertyTemplate('designer_ref_img');

    if CurrentElement = nil then CurrentElement := nil;

  end;
end;

procedure TfrmUIDesigner.scrollbox_InterfaceEnter(Sender: TObject);
begin
  InDesigner := true;
end;

procedure TfrmUIDesigner.scrollbox_InterfaceExit(Sender: TObject);
begin
  InDesigner := false;
end;

procedure TfrmUIDesigner.ApplicationMessage(var Msg: TMsg;
  var Handled: Boolean);
begin
  if InDesigner then
  begin
    case Msg.Message of
      WM_KEYDOWN:
        case Msg.wParam of
          VK_LEFT: if actShiftLeft.Enabled then actShiftLeft.Execute;
          VK_RIGHT: if actShiftRight.Enabled then actShiftRight.Execute;
          VK_UP: if actShiftTop.Enabled then actShiftTop.Execute;
          VK_DOWN: if actShiftDown.Enabled then actShiftDown.execute;
          VK_DELETE: if actDeleteComponent.enabled then
            begin
              if ElementSelectionList.Count > 1 then
              begin
                if MessageDlg('Are you sure you wish to remove these components?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
                  actDeleteComponent.Execute;
              end
              else
              begin
                if MessageDlg('Are you sure you wish to remove this component?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
                  actDeleteComponent.Execute;
              end;
            end;

        end;
    end;
  end;
end;

procedure TfrmUIDesigner.actTexturedExecute(Sender: TObject);
begin
  actTextured.Checked := not actTextured.Checked;
  scrollbox_Interface.SetRenderComponentTextures(actTextured.Checked);
  if UnknownTextureList.Count > 0 then
    raise Exception.create('The following Textures could not be located: ' + #13#10 + UnknownTextureList.text);

end;

procedure TfrmUIDesigner.JvInspector1AfterItemCreate(Sender: TObject;
  const Item: TJvCustomInspectorItem);
begin
  //  if comparetext(item.DisplayName, 'texture') = 0 then
  //  begin
  //    item.flags := item.flags + [iifEditButton];
  //  end;

end;

procedure TfrmUIDesigner.JvThumbView1StartScanning(Sender: TObject;
  Max: Integer);
begin

  prgTextures.Position := 0;
  prgTextures.Max := max;
  prgTextures.Visible := true;
end;

procedure TfrmUIDesigner.JvThumbView1StopScanning(Sender: TObject);
begin
  prgTextures.Visible := false;
end;

procedure TfrmUIDesigner.JvThumbView1ScanProgress(Sender: TObject;
  Position: Integer; var Break: Boolean);
begin
  prgTextures.position := position;
end;

procedure TfrmUIDesigner.JvThumbView1ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  if ElementSelectionList.count < 1 then
    mnuAssignTextureToComp.enabled := false
  else
  begin
    if JvThumbView1.Selected = -1 then
      mnuAssignTextureToComp.Enabled := false
    else
      mnuAssignTextureToComp.enabled := true;
  end;

end;

procedure TfrmUIDesigner.tabTextureBrowserShow(Sender: TObject);
begin
  if jvThumbView1.Directory <> JvDirectoryListBox1.Directory then
    jvThumbView1.Directory := JvDirectoryListBox1.Directory;

end;

procedure TfrmUIDesigner.JvDirectoryListBox1Change(Sender: TObject);
begin
  if tabTextureBrowser.Showing then
    if jvThumbView1.Directory <> JvDirectoryListBox1.Directory then
      jvThumbView1.Directory := JvDirectoryListBox1.Directory;

end;

procedure TfrmUIDesigner.mnuAssignTextureToCompClick(Sender: TObject);
var
  i: integer;
  ctrl: TDSBaseUIControl;
begin
  for i := 0 to ElementSelectionList.Count - 1 do
  begin
    ctrl := TDSBaseUIControl(ElementSelectionList.Items[i]);

    ctrl.texture := ChangeFileExt(ExtractFilename(JvThumbView1.SelectedFile), '');
    ctrl.InsertPropertyTemplate('texture', ctrl.texture);
  end;

  if (CurrentElement <> nil) and (ElementSelectionList.Count = 1) then
    UpdateEditWindowWithChanges(CurrentElement.Code);

  Dirty := true;

  actRefresh.execute;

end;

procedure TfrmUIDesigner.JvThumbView1MouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  //jvThumbView1.ScrollBy(0,WheelDelta);
end;

procedure TfrmUIDesigner.actUVCoordHelperExecute(Sender: TObject);
var

  UVCoordHelper: TfrmUVCoordWizard;
  fn: string;
begin
  UVCoordHelper := nil;
  try

    fn := GetTextureFilename(currentElement.texture);
    if fn = '' then
      raise Exception.create('Texture could not be located: ' + currentelement.texture);

    UVCoordHelper := TFrmUVCoordWizard.create(self);
    UVCoordHelper.imgTexture.Picture.LoadFromFile(fn);
    UVCoordHelper.UVCoords := CurrentElement.uvcoords;

    if UVCoordHelper.ShowModal = mrOk then
    begin
      CurrentElement.uvcoords := UVCoordHelper.uvcoords;
      CurrentElement.InsertPropertyTemplate('uvcoords', CurrentElement.uvcoords);
      if CurrentElement <> nil then
        UpdateEditWindowWithChanges(CurrentElement.Code);
    end;
  finally
    if assigned(UVCoordHelper) then freeAndNil(UVCoordHelper);
  end;
end;

procedure TfrmUIDesigner.utorials1Click(Sender: TObject);
begin
  ShellExecute(0, nil, TUTORIALS_URL, nil, nil, SW_NORMAL);
end;

procedure TfrmUIDesigner.DiscussionBoards1Click(Sender: TObject);
begin
  ShellExecute(0, nil, DISCUSSION_BOARD_URL, nil, nil, SW_NORMAL);
end;

procedure TfrmUIDesigner.UpdateEditWindowWithChanges(NewText: TStringList);
var
  i: integer;
  editlines: integer;

begin
  rtfElementCode.BeginUpdate;
  try
    editlines := rtfElementCode.Lines.Count;
    for i := 0 to NewText.Count - 1 do
    begin
      if i < editlines then
      begin
        if rtfElementCode.Lines.Strings[i] <> NewText.Strings[i] then
          rtfElementCode.Lines.strings[i] := NewText.Strings[i];
      end
      else
        rtfElementCode.Lines.Add(Newtext.strings[i]);
    end;
    if editlines > Newtext.Count then
    begin
      for i := editlines - 1 downto Newtext.count do
        rtfElementcode.Lines.Delete(i);
    end;
  finally
    rtfElementCode.EndUpdate;
  end;

end;

procedure TfrmUIDesigner.FormMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
var
  msg: Cardinal;
  code: Cardinal;
  i, n: Integer;
  l: hwnd;

  TargetControl: TScrollbox;
begin

  //See if we can capture the mouse wheel messages and pass them on to the
  //appropriate scrollbox.  (TScrollbox does not capture mousewheel messages correctly)

  TargetControl := nil;
  if ChildWindowFromPoint(jvThumbView1.Handle, jvThumbView1.ScreenToClient(Mouse.CursorPos)) <> 0 then
    TargetControl := jvThumbView1
  else if ChildWindowFromPoint(Scrollbox1.Handle, Scrollbox1.ScreenToClient(Mouse.CursorPos)) <> 0 then
    TargetControl := Scrollbox1
  else
    TargetControl := nil;

  if TargetControl <> nil then
  begin
    Handled := true;
    if ssShift in Shift then
      msg := WM_HSCROLL
    else
      msg := WM_VSCROLL;

    if WheelDelta > 0 then
      code := SB_LINEUP
    else
      code := SB_LINEDOWN;

    n := Mouse.WheelScrollLines;
    for i := 1 to n do
      TargetControl.Perform(msg, code, 0);
    TargetControl.Perform(msg, SB_ENDSCROLL, 0);
  end;

end;

procedure TfrmUIDesigner.JvDragDrop1Drop(Sender: TObject; Pos: TPoint;
  Value: TStrings);

var
  s: string;
  rslt: integer;
begin

  if Value.Count > 0 then
  begin
    s := value.Strings[0];

    if dirty then
    begin
      rslt := MessageDlg('Save Changes?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
      if rslt = mrCancel then exit;
      if rslt = mrYes then actSaveGas.Execute;
    end;
    dirty := false;
    CurrentElement := nil;
    try
      actNewInterface.Execute;
      ImportGas(s);
      CurrentFilename := s;
      JvMRUManager1.Add(CurrentFilename, 0);
      JvMRUManager1.UpdateRecentMenu;
      dirty := false;
    except
      on E: Exception do
      begin
        dirty := false;
        actNewInterface.execute;
        MessageDlg('Error loading interface:' + E.message, mtError, [mbOK], 0);
      end;

    end;

  end;

end;

end.

