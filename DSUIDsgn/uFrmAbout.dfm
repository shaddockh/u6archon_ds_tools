object AboutBox: TAboutBox
  Left = 376
  Top = 324
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 237
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 369
    Height = 185
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object ScrollBox1: TScrollBox
      Left = 16
      Top = 16
      Width = 337
      Height = 161
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Color = clWhite
      ParentColor = False
      TabOrder = 0
      object JvLinkLabel1: TJvLinkLabel
        Left = 1
        Top = 0
        Width = 370
        Height = 520
        Caption = 
          '<b>Ultima VI Project</b><br>'#13#10'User Interface Designer<br>'#13#10'Versi' +
          'on <dynamic> <br>'#13#10'<p>'#13#10'<b><u>Thanks go out to the following:</u' +
          '></b>'#13#10' <p>'#13#10'  Ultima 6 Archon Team (<link>www.u6archon.com</lin' +
          'k>) <br>'#13#10'  Bjorn XX<br>'#13#10'  Dan<br>'#13#10'  Major Hostility<br>'#13#10'  no' +
          'n`<br>'#13#10'  Tiberius Moongazer<br>'#13#10'  Wind Walker<br>'#13#10'  Wizardo55' +
          '<br>'#13#10'  Xaa<br>'#13#10'  xmen90s<br>'#13#10'<br> '#13#10'<b><u>Dungeon Siege Commu' +
          'nity:</u></b>'#13#10'<p>'#13#10'  Gas Powered Games (<link>www.dungeonsiege.' +
          'com</link>) <br>'#13#10'  Unkle Ernsie and all the folks on Siegworks.' +
          'org (<link>www.siegeworks.org</link>)<br>'#13#10'  GameEditing.net (si' +
          'te no longer active)<br>'#13#10'  Planet Dungeon Siege (<link>www.plan' +
          'etdungeonsiege.com</link>)<br>'#13#10'  Siege Network (<link>www.siege' +
          'network.com</link>)<br>'#13#10'  Dungeon Siege Heaven (<link>ds.heaven' +
          'games.com</link>)<br>'#13#10'<br>'#13#10'<b><u>Delphi Resources:</b></u>'#13#10'<p' +
          '>'#13#10'  Borland International (<link>www.borland.com</link>)<br>'#13#10' ' +
          ' JVCL (Project Jedi) Component Team (<link>jvcl.sourceforge.net<' +
          '/link>)<br>'#13#10'  Jan Verhoeven for the janSticker component (<link' +
          '>www.jansfreeware.com</link>) <br>'#13#10'  Synedit Component Team (<l' +
          'ink>synedit.sourceforge.net</link>)<br>'#13#10'  Mike Lischke for the ' +
          'GraphicEx library (<link>www.delphi-gems.com</link>)<br>'#13#10'<br>'#13#10 +
          #13#10'<b><u>And finally... </b></u>'#13#10'<p>'#13#10'  My Wife for putting up w' +
          'ith this quirky obsession of mine that is called Dungeon Siege. ' +
          ':)<br>'#13#10'</p>'#13#10
        Text.Strings = (
          
            '<b>Ultima VI Project</b><br>'#13#10'User Interface Designer<br>'#13#10'Versi' +
            'on <dynamic> <br>'#13#10'<p>'#13#10'<b><u>Thanks go out to the following:</u' +
            '></b>'#13#10' <p>'#13#10'  Ultima 6 Archon Team (<link>www.u6archon.com</lin' +
            'k>) <br>'#13#10'  Bjorn XX<br>'#13#10'  Dan<br>'#13#10'  Major Hostility<br>'#13#10'  no' +
            'n`<br>'#13#10'  Tiberius Moongazer<br>'#13#10'  Wind Walker<br>'#13#10'  Wizardo55' +
            '<br>'#13#10'  Xaa<br>'#13#10'  xmen90s<br>'#13#10'<br> '#13#10'<b><u>Dungeon Siege Commu' +
            'nity:</u></b>'#13#10'<p>'#13#10'  Gas Powered Games (<link>www.dungeonsiege.' +
            'com</link>) <br>'#13#10'  Unkle Ernsie and all the folks on Siegworks.' +
            'org (<link>www.siegeworks.org</link>)<br>'#13#10'  GameEditing.net (si' +
            'te no longer active)<br>'#13#10'  Planet Dungeon Siege (<link>www.plan' +
            'etdungeonsiege.com</link>)<br>'#13#10'  Siege Network (<link>www.siege' +
            'network.com</link>)<br>'#13#10'  Dungeon Siege Heaven (<link>ds.heaven' +
            'games.com</link>)<br>'#13#10'<br>'#13#10'<b><u>Delphi Resources:</b></u>'#13#10'<p' +
            '>'#13#10'  Borland International (<link>www.borland.com</link>)<br>'#13#10' ' +
            ' JVCL (Project Jedi) Component Team (<link>jvcl.sourceforge.net<' +
            '/link>)<br>'#13#10'  Jan Verhoeven for the janSticker component (<link' +
            '>www.jansfreeware.com</link>) <br>'#13#10'  Synedit Component Team (<l' +
            'ink>synedit.sourceforge.net</link>)<br>'#13#10'  Mike Lischke for the ' +
            'GraphicEx library (<link>www.delphi-gems.com</link>)<br>'#13#10'<br>'#13#10 +
            #13#10'<b><u>And finally... </b></u>'#13#10'<p>'#13#10'  My Wife for putting up w' +
            'ith this quirky obsession of mine that is called Dungeon Siege. ' +
            ':)<br>'#13#10'</p>'#13#10)
        Transparent = False
        LinkColor = clBlue
        LinkColorClicked = clRed
        LinkColorHot = clPurple
        LinkStyle = [fsUnderline]
        HotLinks = False
        AutoHeight = True
        MarginWidth = 0
        MarginHeight = 0
        OnDynamicTagInit = JvLinkLabel1DynamicTagInit
        OnLinkClick = JvLinkLabel1LinkClick
      end
    end
  end
  object OKButton: TButton
    Left = 151
    Top = 204
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
end
