(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmAbout.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * About box
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.6 $              $Date: 2004-01-08 19:12:55 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit uFrmAbout;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, JvLinkLabel,shellapi;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    OKButton: TButton;
    ScrollBox1: TScrollBox;
    JvLinkLabel1: TJvLinkLabel;
    procedure FormCreate(Sender: TObject);
    procedure JvLinkLabel1DynamicTagInit(Sender: TObject;
      out Source: String; Number: Integer);
    procedure JvLinkLabel1LinkClick(Sender: TObject; LinkNumber: Integer;
      LinkText: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

{$R *.dfm}
uses frmSpellbook;
procedure TAboutBox.FormCreate(Sender: TObject);
begin

Application.ProcessMessages;
jvLinkLabel1.UpdateDynamicTag(0,VERSION_STRING);

end;

procedure TAboutBox.JvLinkLabel1DynamicTagInit(Sender: TObject;
  out Source: String; Number: Integer);
begin
//Source := VERSION_STRING;
end;

procedure TAboutBox.JvLinkLabel1LinkClick(Sender: TObject;
  LinkNumber: Integer; LinkText: String);
begin
ShellExecute(0, nil, pchar('http://' + LinkText), nil, nil, SW_NORMAL);
end;

end.
 
