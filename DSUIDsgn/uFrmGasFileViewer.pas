unit uFrmGasFileViewer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, uGasParser, ImgList, SynEditHighlighter,
  SynHighlighterCpp, SynEdit, ExtCtrls, ComCtrls, StdCtrls, ActnList,
  ToolWin;

type
  TfrmGasViewer = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    OpenGasFile1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SynCppSyn2: TSynCppSyn;
    Panel1: TPanel;
    lvTokenizeGas: TListView;
    Splitter1: TSplitter;
    SynEdit1: TSynEdit;
    Panel2: TPanel;
    edQuery: TEdit;
    Button1: TButton;
    lvlQueryResults: TLabel;
    Button2: TButton;
    edNewValue: TEdit;
    ImageList2: TImageList;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    actOpenGas: TAction;
    TreeView1: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure lvTokenizeGasSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure actOpenGasExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    GasTree: TGasInterestItem;
    procedure ParseGas(gas: string);
    procedure FillList(interest: TGasInterestItem; Indent: integer);

  end;

var
  frmGasViewer      : TfrmGasViewer;

implementation

{$R *.dfm}

procedure TFrmGasViewer.ParseGas(gas: string);
begin
  try
    lvTokenizeGas.Items.BeginUpdate;
    lvTokenizeGas.Items.Clear;
    FreeandNil(GasTree);
    GasTree := TGasInterestItem.create;
    try
      GasTree.ParseGas(Gas);
    except
      ;
    end;
    FillList(GasTree, 0);

  finally
    lvTokenizeGas.Items.EndUpdate;
  end;

end;

procedure TfrmGasViewer.FillList(interest: TGasInterestItem; Indent: integer);
var
  i                 : integer;
  li                : TListItem;
  Token             : PGasToken;
begin
  li := lvTokenizeGas.Items.Add;
  li.caption := interest.Name;
  li.subitems.add(interest.ComponentTemplate);
  li.Indent := indent;
  li.imageindex := 13;
  li.data := pointer(interest);

  for i := 0 to Interest.NameValueList.Count - 1 do
  begin
    li := lvTokenizeGas.Items.Add;
    li.Caption := '[' + Interest.NameValueList.Names[i] + ']';
    Token := Pointer(Interest.NameValueList.Objects[i]);
    li.SubItems.Add(Token.data);
    li.indent := indent + 1;
    li.imageindex := 16;
  end;
  for i := 0 to interest.ChildBlocks.count - 1 do
    FillList(TGasInterestItem(interest.ChildBlocks.Items[i]), indent + 1);

end;

procedure TfrmGasViewer.FormCreate(Sender: TObject);
begin
  GasTree := TGasInterestItem.create;
end;

procedure TfrmGasViewer.lvTokenizeGasSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
var
  interestItem      : TGAsInterestItem;
begin
  if Item.data <> nil then
  begin
    interestItem := TGasInterestItem(item.data);
    synedit1.Text := interestitem.BuildGas();
  end;


end;

procedure TfrmGasViewer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  FreeAndNil(GasTree);
end;

procedure TfrmGasViewer.Button1Click(Sender: TObject);
begin
  lvlQueryResults.Caption := GasTree.QueryValue(edQuery.Text, '#NOT FOUND#');
end;

procedure TfrmGasViewer.Button2Click(Sender: TObject);
begin
  GasTree.SetProperty(edQuery.Text, edNewValue.text);
end;

procedure TfrmGasViewer.actOpenGasExecute(Sender: TObject);
var
  sl                : TStringlist;
begin
  if OpenDialog1.Execute then
  begin
    sl := nil;
    try
      sl := TStringlist.create;
      sl.loadFromFile(OpenDialog1.FileName);
      ParseGas(sl.Text);
    finally
      if assigned(sl) then freeAndNil(sl);
    end;
  end;

end;

end.

