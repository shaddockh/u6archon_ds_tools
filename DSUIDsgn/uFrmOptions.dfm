object frmOptions: TfrmOptions
  Left = 290
  Top = 468
  BorderStyle = bsDialog
  Caption = 'Program Options'
  ClientHeight = 429
  ClientWidth = 318
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 318
    Height = 385
    ActivePage = TabSheet1
    Align = alTop
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Settings'
      object Label4: TLabel
        Left = 8
        Top = 0
        Width = 66
        Height = 13
        Caption = 'User Tools:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 8
        Top = 269
        Width = 232
        Height = 13
        Caption = 'Bits directory - location of UI textures (*.raw;*.psd)'
      end
      object btnBrowseTextureDir: TSpeedButton
        Left = 272
        Top = 284
        Width = 19
        Height = 19
        Caption = '...'
        OnClick = btnBrowseTextureDirClick
      end
      object lstTools: TListBox
        Left = 16
        Top = 16
        Width = 265
        Height = 73
        ItemHeight = 13
        Items.Strings = (
          'Tool 1'
          'Tool 2'
          'Tool 3'
          'Tool 4'
          'Tool 5')
        TabOrder = 0
        OnClick = lstToolsClick
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 96
        Width = 289
        Height = 137
        Caption = 'Tool Details'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Tool Name:'
        end
        object Label2: TLabel
          Left = 8
          Top = 37
          Width = 56
          Height = 13
          Caption = 'Executable:'
        end
        object Label3: TLabel
          Left = 8
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Parameters'
        end
        object SpeedButton1: TSpeedButton
          Left = 264
          Top = 52
          Width = 19
          Height = 19
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object ckToolSaveFirst: TCheckBox
          Left = 8
          Top = 112
          Width = 129
          Height = 17
          Caption = 'Save before running?'
          TabOrder = 0
          OnClick = ckToolSaveFirstClick
        end
        object edToolname: TEdit
          Left = 72
          Top = 13
          Width = 209
          Height = 21
          TabOrder = 1
          OnChange = edToolnameChange
        end
        object edToolExecutable: TEdit
          Left = 16
          Top = 51
          Width = 249
          Height = 21
          TabOrder = 2
          OnChange = edToolExecutableChange
        end
        object edToolParms: TEdit
          Left = 16
          Top = 88
          Width = 265
          Height = 21
          TabOrder = 3
          OnChange = edToolParmsChange
        end
      end
      object ckBackupOnSave: TCheckBox
        Left = 8
        Top = 248
        Width = 257
        Height = 17
        Caption = 'Create a backup of interface on save (.BAK)'
        TabOrder = 2
      end
      object edTextureSource: TEdit
        Left = 16
        Top = 283
        Width = 255
        Height = 21
        TabOrder = 3
        OnChange = edToolExecutableChange
      end
    end
  end
  object btnOK: TButton
    Left = 176
    Top = 394
    Width = 65
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 248
    Top = 394
    Width = 65
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Executables|*.exe'
    Title = 'Browse for Program'
    Left = 248
    Top = 272
  end
  object JvSelectDirectory1: TJvSelectDirectory
    ClassicDialog = False
    Title = 'Location of Texture Bitmaps (*.bmp)'
    Left = 280
    Top = 272
  end
end
