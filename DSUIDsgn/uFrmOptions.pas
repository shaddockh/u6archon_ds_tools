(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmOptions.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Form containing the program options
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.10 $              $Date: 2004-01-12 15:25:37 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit uFrmOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, inifiles, ColorGrd, JvBaseDlg,
  JvSelectDirectory;

type
  TfrmOptions = class(TForm)
    PageControl1: TPageControl;
    btnOK: TButton;
    btnCancel: TButton;
    TabSheet1: TTabSheet;
    lstTools: TListBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    ckToolSaveFirst: TCheckBox;
    edToolname: TEdit;
    edToolExecutable: TEdit;
    edToolParms: TEdit;
    ckBackupOnSave: TCheckBox;
    Label4: TLabel;
    OpenDialog1: TOpenDialog;
    Label5: TLabel;
    edTextureSource: TEdit;
    btnBrowseTextureDir: TSpeedButton;
    JvSelectDirectory1: TJvSelectDirectory;
    procedure btnOKClick(Sender: TObject);
    procedure lstToolsClick(Sender: TObject);
    procedure edToolnameChange(Sender: TObject);
    procedure edToolExecutableChange(Sender: TObject);
    procedure edToolParmsChange(Sender: TObject);
    procedure ckToolSaveFirstClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnBrowseTextureDirClick(Sender: TObject);
  private
    { Private declarations }
    FIniFile: TMemIniFile;
  public
    { Public declarations }
    property Ini: TMemIniFile read Finifile write Finifile;
  end;

var
  frmOptions        : TfrmOptions;

implementation

{$R *.dfm}

procedure TfrmOptions.btnOKClick(Sender: TObject);
begin
  ini.WriteBool('Options', 'createbackup', ckBackupOnSave.checked);

  Ini.UpdateFile;
end;

procedure TfrmOptions.lstToolsClick(Sender: TObject);
var
  curtool           : string;
begin
  curtool := 'usertool' + intToStr(lstTools.ItemIndex + 1);

  edToolname.text := ini.ReadString(curtool, 'Name', curtool);
  edToolExecutable.text := ini.ReadString(curTool, 'ExeName', '');
  edToolParms.Text := ini.ReadString(curTool, 'Parms', '');
  ckToolSaveFirst.checked := ini.ReadBool(curtool, 'SaveFirst', false);

end;

procedure TfrmOptions.edToolnameChange(Sender: TObject);
var
  curtool           : string;
begin
  curtool := 'usertool' + intToStr(lstTools.ItemIndex + 1);
  //  lstTools.Items[lstTools.ItemIndex] := edToolname.text;
  ini.WriteString(curtool, 'Name', edToolname.text);

  lstTools.Items[lstTools.ItemIndex] := edToolName.Text;
end;

procedure TfrmOptions.edToolExecutableChange(Sender: TObject);
var
  curtool           : string;
begin
  curtool := 'usertool' + intToStr(lstTools.ItemIndex + 1);
  ini.WriteString(curtool, 'ExeName', edToolExecutable.text);

end;

procedure TfrmOptions.edToolParmsChange(Sender: TObject);
var
  curtool           : string;
begin
  curtool := 'usertool' + intToStr(lstTools.ItemIndex + 1);
  ini.WriteString(curtool, 'Parms', edToolParms.text);
end;

procedure TfrmOptions.ckToolSaveFirstClick(Sender: TObject);
var
  curtool           : string;
begin
  curtool := 'usertool' + intToStr(lstTools.ItemIndex + 1);
  ini.WriteBool(curtool, 'SaveFirst', ckToolSaveFirst.checked);

end;

procedure TfrmOptions.SpeedButton1Click(Sender: TObject);
begin
  if fileexists(edToolExecutable.text) then
    OpenDialog1.filename := edToolExecutable.Text
  else
    OpenDialog1.filename := application.exename;
  if OpenDialog1.Execute then
    edToolExecutable.text := opendialog1.FileName;
end;

{procedure TfrmOptions.FormCreate(Sender: TObject);
var
  ini               : TIniFile;
  i                 : integer;
  li                : TListItem;

  procedure AddComponent(componentname: string) ;
  begin
    li := lvColors.Items.Add;
    li.Caption := componentName;
    li.SubItems.Add(ini.ReadString('Component-' + ComponentName, 'Color', 'clBlack'));
    li.SubItems.Add(ini.ReadString('Component-' + ComponentName, 'Brush', 'brSolid'));
  end;

begin
  ini := nil;
  try
    ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
    AddComponent('window');
    AddComponent('button');
    AddComponent('checkbox');
    AddComponent('dialog_box');
    AddComponent('dockbar');
    AddComponent('itemslot');
    AddComponent('listbox');
    AddComponent('radio_button');
    AddComponent('status_bar');
    AddComponent('text');
    AddComponent('text_box');
    AddComponent('edit_box');
    AddComponent('combo_box');
    AddComponent('gridbox');
    AddComponent('slider');
    AddComponent('tab');
    AddComponent('infoslot');
    AddComponent('listener');
    AddComponent('popupmenu');
    AddComponent('listreport');
    AddComponent('rollover_texture');
    AddComponent('selection_texture');
    AddComponent('selection_box');

  finally
    if assigned(ini) then freeAndNil(ini);
  end;
end;}

procedure TfrmOptions.btnBrowseTextureDirClick(Sender: TObject);
begin
//jvSelectDirectory1.InitialDir := edTextureSource.Text;

if JvSelectDirectory1.execute then
begin
  edTextureSource.Text := jvSelectDirectory1.Directory;
end;
end;

end.

