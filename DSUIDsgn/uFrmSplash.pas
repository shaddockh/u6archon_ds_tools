(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmSplash.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Splash Screen
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.6 $              $Date: 2006-05-06 14:41:02 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit uFrmSplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls,inifiles,JvTipOfDay;

type
  TfrmSplash = class(TForm)
    Timer1: TTimer;
    Timer2: TTimer;
    Panel1: TPanel;
    Image1: TImage;
    Version: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation
          uses frmSpellbook;
{$R *.dfm}

procedure TfrmSplash.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ini               : TIniFile;

begin
Action := caFree;
Hide;
  ini := nil;
  try
    ini := TIniFile.create(ChangeFileExt(Application.ExeName, '.ini'));
    if ini.ReadBool('Options','TipOfDay',true) then
    begin
      frmUIDesigner.JvTipOfDay1.Options := frmUIDesigner.JvTipOfDay1.Options + [toShowOnStartup];
     //THIS DOESN'T WORK!
     // frmUIDesigner.jvTipOfDay1.Execute;
    end;
  finally
    if assigned(ini) then freeAndNil(ini);

  end;

end;

procedure TfrmSplash.Timer1Timer(Sender: TObject);
begin

Timer1.enabled := false;
AlphaBlend := true;
Timer2.Enabled := true;
end;

procedure TfrmSplash.Timer2Timer(Sender: TObject);
begin
AlphaBlendValue := AlphaBlendValue - 10;
if AlphaBlendValue < 20 then
begin
  Timer2.Enabled := false;
  close;
end;
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  version.Caption := VERSION_STRING;

end;

end.
