object frmTextureBrowser: TfrmTextureBrowser
  Left = 272
  Top = 274
  Width = 497
  Height = 403
  Caption = 'Texture Browser'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 256
    Top = 0
    Width = 9
    Height = 357
    Cursor = crHSplit
    Align = alRight
    Beveled = True
    MinSize = 60
  end
  object JvThumbView1: TJvThumbView
    Left = 0
    Top = 0
    Width = 256
    Height = 357
    HorzScrollBar.Tracking = True
    VertScrollBar.Tracking = True
    Align = alClient
    Color = clWindow
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnClick = JvThumbView1Click
    OnDblClick = JvThumbView1DblClick
    OnMouseDown = JvThumbView1MouseDown
    AlignView = vtNormal
    AutoScrolling = True
    ThumbGap = 4
    AutoHandleKeyb = True
    MinMemory = True
    MaxWidth = 128
    MaxHeight = 128
    Size = 100
    ScrollMode = smBoth
    Sorted = True
    OnStopScanning = JvThumbView1StopScanning
    AsButtons = False
    TitlePlacement = tpDown
    Filter = 'Photoshop Files (*.psd)|*.psd|Raw Files (*.raw)|*.raw'
    ThumbColor = clNone
    ShowShadow = False
    ShadowColor = clBlack
  end
  object Panel1: TPanel
    Left = 265
    Top = 0
    Width = 224
    Height = 357
    Align = alRight
    TabOrder = 1
    DesignSize = (
      224
      357)
    object Label1: TLabel
      Left = 1
      Top = 209
      Width = 222
      Height = 16
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Color = clAppWorkSpace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object btnAssign: TButton
      Left = 63
      Top = 321
      Width = 73
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Assign'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 144
      Top = 321
      Width = 72
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 222
      Height = 208
      Align = alTop
      BevelInner = bvLowered
      BorderStyle = bsSingle
      TabOrder = 2
      object JvThumbImage1: TJvThumbImage
        Left = 2
        Top = 2
        Width = 214
        Height = 200
        Align = alClient
        Center = True
        Proportional = True
        IgnoreMouse = False
        Angle = AT0
        Zoom = 0
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 357
    Width = 489
    Height = 19
    Panels = <>
    SimplePanel = False
  end
end
