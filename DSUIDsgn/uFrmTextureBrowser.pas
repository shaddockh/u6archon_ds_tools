(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmTextureBrowser.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Texture browser used to select a texture to apply to a particular component
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.3 $              $Date: 2004-01-08 19:12:55 $
 *----------------------------------------------------------------------------
 *
 */
*)

unit uFrmTextureBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvBaseThumbnail, JvThumbviews, ExtCtrls, JvThumbImage, StdCtrls,
  ComCtrls;

type
  TfrmTextureBrowser = class(TForm)
    JvThumbView1: TJvThumbView;
    Splitter1: TSplitter;
    Panel1: TPanel;
    btnAssign: TButton;
    Button2: TButton;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    Label1: TLabel;
    JvThumbImage1: TJvThumbImage;
    procedure JvThumbView1DblClick(Sender: TObject);
    procedure JvThumbView1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure JvThumbView1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvThumbView1StopScanning(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTextureBrowser : TfrmTextureBrowser;

implementation

{$R *.dfm}

procedure TfrmTextureBrowser.JvThumbView1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmTextureBrowser.JvThumbView1Click(Sender: TObject);
begin
  //if JvThumbView1.Selected > -1 then
  //begin
  //jvThumbImage1.Picture.Assign(jvThumbView1.ThumbList.Thumbnail[jvThumbView1.selected].Photo.Picture);
  //label1.Caption := ChangeFileExt(ExtractFileName(jvThumbView1.SelectedFile),'');
  //end;
end;

procedure TfrmTextureBrowser.FormShow(Sender: TObject);
begin
  if JvThumbView1.Selected > -1 then
  begin
    jvThumbImage1.Picture.Assign(jvThumbView1.ThumbList.Thumbnail[jvThumbView1.selected].Photo.Picture);
    label1.Caption := ChangeFileExt(ExtractFileName(jvThumbView1.SelectedFile), '');
    btnAssign.Enabled := true;
  end;

end;

procedure TfrmTextureBrowser.JvThumbView1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if JvThumbView1.Selected > -1 then
  begin
    jvThumbImage1.Picture.Assign(jvThumbView1.ThumbList.Thumbnail[jvThumbView1.selected].Photo.Picture);
    label1.Caption := ChangeFileExt(ExtractFileName(jvThumbView1.SelectedFile), '');
    btnAssign.Enabled := true;
  end
  else
  begin
    label1.caption := '';
    jvThumbImage1.Picture.Bitmap.FreeImage;
    btnAssign.Enabled := false;
  end;

end;

procedure TfrmTextureBrowser.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TfrmTextureBrowser.JvThumbView1StopScanning(Sender: TObject);
begin
jvThumbView1.ThumbList.EndUpdate;
end;

end.

