object frmUVCoordWizard: TfrmUVCoordWizard
  Left = 207
  Top = 399
  BorderStyle = bsDialog
  Caption = 'UV Coordinates Helper'
  ClientHeight = 413
  ClientWidth = 361
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTexture: TPanel
    Left = 8
    Top = 8
    Width = 257
    Height = 257
    TabOrder = 0
    object imgTexture: TImage
      Left = 1
      Top = 1
      Width = 255
      Height = 255
      Align = alClient
    end
  end
  object Button1: TButton
    Left = 280
    Top = 16
    Width = 73
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 280
    Top = 48
    Width = 73
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 272
    Width = 337
    Height = 137
    Caption = 'UV Coordinates'
    TabOrder = 3
    object JvThumbImage1: TJvThumbImage
      Left = 184
      Top = 16
      Width = 137
      Height = 113
      Center = True
      Proportional = True
      Stretch = True
      IgnoreMouse = False
      Angle = AT0
      Zoom = 0
    end
    object Label2: TLabel
      Left = 20
      Top = 18
      Width = 44
      Height = 13
      Caption = 'U1 (Left):'
    end
    object Label3: TLabel
      Left = 100
      Top = 18
      Width = 44
      Height = 13
      Caption = 'V1 (Top):'
    end
    object Label4: TLabel
      Left = 20
      Top = 59
      Width = 54
      Height = 13
      Caption = 'U2 (Width):'
    end
    object Label5: TLabel
      Left = 100
      Top = 59
      Width = 56
      Height = 13
      Caption = 'V2 (Height):'
    end
    object edU1: TEdit
      Left = 19
      Top = 33
      Width = 38
      Height = 21
      Color = clBtnHighlight
      ReadOnly = True
      TabOrder = 0
    end
    object udU1: TUpDown
      Left = 57
      Top = 34
      Width = 24
      Height = 19
      Min = 0
      Max = 255
      Orientation = udHorizontal
      Position = 0
      TabOrder = 1
      Wrap = False
      OnClick = udU1Click
    end
    object edV1: TEdit
      Left = 99
      Top = 34
      Width = 38
      Height = 22
      Color = clBtnHighlight
      ReadOnly = True
      TabOrder = 2
    end
    object udV1: TUpDown
      Left = 141
      Top = 32
      Width = 15
      Height = 24
      Min = 0
      Max = 255
      Position = 0
      TabOrder = 3
      Wrap = False
      OnClick = udV1Click
    end
    object edU2: TEdit
      Left = 19
      Top = 76
      Width = 38
      Height = 21
      Color = clBtnHighlight
      ReadOnly = True
      TabOrder = 4
    end
    object udU2: TUpDown
      Left = 58
      Top = 75
      Width = 25
      Height = 22
      Min = 0
      Max = 255
      Orientation = udHorizontal
      Position = 20
      TabOrder = 5
      Wrap = False
      OnClick = udU2Click
    end
    object edV2: TEdit
      Left = 99
      Top = 74
      Width = 38
      Height = 23
      Color = clBtnHighlight
      ReadOnly = True
      TabOrder = 6
    end
    object udV2: TUpDown
      Left = 142
      Top = 73
      Width = 15
      Height = 24
      Min = 0
      Max = 255
      Position = 20
      TabOrder = 7
      Wrap = False
      OnClick = udV2Click
    end
    object edUVCoords: TEdit
      Left = 16
      Top = 104
      Width = 145
      Height = 21
      Color = clBtnHighlight
      ReadOnly = True
      TabOrder = 8
    end
  end
end
