unit uFrmUVCoordWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DSGuiComponents, JvBaseThumbnail,
  JvThumbImage, ComCtrls;

type
  TfrmUVCoordWizard = class(TForm)
    pnlTexture: TPanel;
    imgTexture: TImage;
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    JvThumbImage1: TJvThumbImage;
    edU1: TEdit;
    udU1: TUpDown;
    edV1: TEdit;
    udV1: TUpDown;
    edU2: TEdit;
    udU2: TUpDown;
    edV2: TEdit;
    udV2: TUpDown;
    edUVCoords: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure udU1Click(Sender: TObject; Button: TUDBtnType);
    procedure udV1Click(Sender: TObject; Button: TUDBtnType);
    procedure udU2Click(Sender: TObject; Button: TUDBtnType);
    procedure udV2Click(Sender: TObject; Button: TUDBtnType);
    procedure SetUVCoords(value: string);
    function GetUVCoords: string;
  private
    { Private declarations }
    procedure Element_Changed(Sender: TObject);
  public
    { Public declarations }
    Indicator: TDSWindow;
    property UVCoords: string read getUVCoords write setUVCoords;
  end;

var
  frmUVCoordWizard  : TfrmUVCoordWizard;

implementation

{$R *.dfm}

procedure TfrmUVCoordWizard.FormCreate(Sender: TObject);
begin
  Indicator := TDSWindow.Create(pnlTexture);
  Indicator.Brush.Style := bsClear;
  Indicator.Pen.Color := clWhite;
  Indicator.Pen.Mode := pmXor;
  Indicator.Parent := pnlTexture;
  Indicator.OnChange := Element_Changed;
  Indicator.Selected := true;
end;

procedure TfrmUVCoordWizard.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if assigned(indicator) then freeAndNil(Indicator);
end;

procedure TfrmUVCoordWizard.Element_Changed(Sender: TObject);
var
  Dest              : TRect;
  Source            : TRect;
  bmp               : TBitmap;
  h : integer;
  w : integer;
begin
  h := imgTexture.picture.Bitmap.Height;
  w := imgTexture.picture.Bitmap.Width;
  edU1.text := floatToStr(indicator.Left / w);
  edV1.text := floatToStr(1 - indicator.bottom / h);
  edU2.text := floatToStr(indicator.right / w);
  edV2.text := floatToStr(1 - indicator.top / h);




  bmp := TBitmap.create;
  bmp.PixelFormat := pf24bit;
  bmp.Width := indicator.Width;
  bmp.height := indicator.Height;

  Dest.Left := 0;
  Dest.Top := 0;
  Dest.Right := indicator.width;
  Dest.Bottom := indicator.height;

  Source.Left := indicator.Left;

  //ok.. for some reason the bitmaps are flipped
  Source.Bottom := indicator.Bottom;
  Source.Top := indicator.top;

  Source.Right := indicator.Right;
  bmp.Canvas.CopyRect(dest, imgTexture.Canvas, source);
  //imgTexture.Canvas.CopyRect(dest,bmp.canvas,source);
  jvThumbImage1.Picture.Bitmap.Assign(bmp);
  bmp.Free;

  udU1.position := indicator.left;
  udv1.position := indicator.top;
  udU2.position := indicator.right;
  udV2.position := indicator.bottom;

  edUVCoords.Text := edU1.Text + ',' + edV1.text + ',' + edU2.text + ',' + edV2.Text;

end;

procedure TfrmUVCoordWizard.udU1Click(Sender: TObject; Button: TUDBtnType);
begin
  Indicator.Left := udU1.Position;
end;

procedure TfrmUVCoordWizard.udV1Click(Sender: TObject; Button: TUDBtnType);
begin
  Indicator.Top := udV1.Position;
end;

procedure TfrmUVCoordWizard.udU2Click(Sender: TObject; Button: TUDBtnType);
begin
  Indicator.right := udU2.Position;
end;

procedure TfrmUVCoordWizard.udV2Click(Sender: TObject; Button: TUDBtnType);
begin
  Indicator.bottom := udv2.Position;

end;

procedure TfrmUVCoordWizard.SetUVCoords(value: string);

var
  sl                : TSTringList;
  FUVCoords         : string;
  FUVleft           : double;
  FUVTop            : double;
  FUVRight          : double;
  FUVBottom         : double;

  h                 : integer;
  w                 : integer;

begin
  FUVCoords := Value;
  FUVLeft := 0;
  FUVTop := 0;
  FUVRight := 1;
  FUVBottom := 1;
  h := imgTexture.Picture.Bitmap.height;
  w := imgTexture.Picture.Bitmap.width;

  if trim(value) <> '' then
  begin

    sl := nil;
    try
      sl := TStringlist.create;
      sl.commatext := value;
      if sl.count = 4 then
      begin
        FUVleft := StrToFloatDef(sl.strings[0], FUVLeft);
        FUVtop := StrToFloatDef(sl.strings[1], FUVtop);
        FUVright := StrToFloatDef(sl.strings[2], FUVright);
        FUVbottom := StrToFloatDef(sl.strings[3], FUVbottom);
      end;
    finally
      sl.free;
    end;
  end;

  Indicator.Left := round(FUVLeft * w);
  Indicator.Right := round(FUVRight * w);
  Indicator.Top := h - round(FUVBottom * h);
  Indicator.Bottom := h - round(FUVTop * h);

end;

function TfrmUVCoordWizard.GetUVCoords: string;
begin

  result := edUVCoords.Text;

end;

end.

