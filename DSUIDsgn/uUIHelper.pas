(*

	Copyright (C) 2003  T. Shaddock Heath

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uUIHelper.pas,v $
 * Author(s):  Shaddock (archon)
 *
 * @copyright (C) 2003 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Misc. utility functions used by the UI designer (OBSOLETE)
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.4 $              $Date: 2004-01-08 19:12:55 $
 *----------------------------------------------------------------------------
 *
 */

*)

unit uUIHelper;

interface
uses sysutils, classes, controls, StdCtrls, ExtCtrls;

implementation


end.

