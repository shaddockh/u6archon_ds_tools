

SET ORIG_SOURCE_PATH=..\..\bits
SET PROCESSED_SOURCE_PATH=U6P_SRC
SET CHM_FILENAME=U6PCodeDocumentation.chm
SET CUSTOM_DOX_INCLUDE_MASK=U6P_*.dox
SET DOXYGEN_CONFIG_FILE=doxyfile.u6p

REM ================================================================
REM GRAB THE PATH THIS BAT FILE WAS RUN FROM.. THIS WILL BE OUR CWD
SET BINPATH=%~d0%~p0

MkDir %PROCESSED_SOURCE_PATH%

Copy %BINPATH%\base_DS.dox %PROCESSED_SOURCE_PATH%
Copy %BINPATH%\%CUSTOM_DOX_INCLUDE_MASK% %PROCESSED_SOURCE_PATH%



REM First Process the Templates
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\templates\*.gas /B /S') do (
	call :PREPROCESS_GAS %%B
)


REM Next Process the Components
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Components
)

REM Next Process the Global Includes
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\Global\Skrits\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B _Includes
)


REM Next Process UI
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\ui\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B UI
)


REM Next Process AI
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\ai\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B AI
)

REM Next Process AI\Jobs
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\ai\jobs\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Jobs
)

REM Next Process AI\Jobs\Common
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\ai\jobs\common\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Common_Jobs
)



REM -----------------------------------------------------------------------------------------
REM NOW GO IN AND FINE TUNE
FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\u6archon\spellcasting\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Spellcasting_Components
)

FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\u6archon\Moongates\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Moongate_Components
)

FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\u6archon\Misc\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Misc_Components
)

FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\u6archon\Boats\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Boat_Components
)

FOR /f %%B in ('dir %ORIG_SOURCE_PATH%\world\contentdb\components\u6archon\Classes\*.skrit /B /S') do (
	call :PREPROCESS_SKRIT_NS %%B Class_Components
)

SET PATH=%BINPATH%;%PATH%

doxygen %DOXYGEN_CONFIG_FILE%
copy docs\%CHM_FILENAME% .\
rmdir docs /S /Q


GOTO :EOF


:PREPROCESS_GAS
%BINPATH%\DSToDOxygen.exe -input=%1 -output=%PROCESSED_SOURCE_PATH%\%~n1%~x1 -format=GAS -inheritcategory=Y

GOTO :EOF

:PREPROCESS_SKRIT
%BINPATH%\DSToDOxygen.exe -input=%1 -output=%PROCESSED_SOURCE_PATH%\%~n1%~x1 -format=SKRIT

GOTO :EOF

:PREPROCESS_SKRIT_NS
%BINPATH%\DSToDOxygen.exe -input=%1 -output=%PROCESSED_SOURCE_PATH%\%~n1%~x1 -format=SKRIT -baseclass=%2

GOTO :EOF



:EOF
