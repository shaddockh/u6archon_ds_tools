unit uDSToDOxygen;

interface
uses sysutils, uGasParser,
  Classes;

procedure ProcessGas;
procedure ProcessSkrit;
procedure Process;

type
  TParms = record
    AutoDoc: boolean;
    Format: string;
    Input: string;
    Output: string;
    BaseClass : string;
    InheritCategory : boolean;
    is_file : boolean;
  end;
var

  CommandLineList   : TStringlist;
  Parms             : TParms;

implementation

function YNToBool(YN: string): boolean;
begin
  if uppercase(trim(YN)) = 'Y' then
    result := true
  else
    result := false;
end;

function GetCommandLineParm(parm: string; default: string): string;
begin

  result := CommandLineList.Values[parm];
  if result = '' then result := default;

end;

procedure ProcessGas();
var
  GasTree           : TGasInterestItem;
  sl                : TStringlist;
  i,x                : integer;
  interest          : TGasInterestItem;
  Token             : PGasToken;
  tempstr           : PChar;
  category           : PChar;
  newCategory : string;
begin
  { TODO -oUser -cConsole Main : Insert code here }

  sl := nil;
  GasTree := nil;
  try
    sl := TStringlist.create;
    GasTree := TGasInterestItem.create;
    sl.LoadFromFile(Parms.Input);
    GasTree.ParseGas(sl.text);

    for i := 0 to GasTree.ChildBlocks.Count - 1 do
    begin
      sl.clear;
      interest := GasTree.ChildBlocks.Items[i];

      new(Token);
      Token.tokentype := gtComment;
      if Parms.AutoDoc then
      begin

        sl.Add('/**');
        tempstr := pchar(interest.GetProperty('doc', ''));

        sl.add(' *  ' + AnsiExtractQuotedStr(Tempstr, '"'));
        sl.add(' *  ');
        tempstr := pchar(interest.GetProperty('extra_doc', ''));
        sl.add(' *  ' + AnsiExtractQuotedStr(Tempstr, '"'));

        sl.add(' */');
      end;

      category := pchar( interest.GetProperty('category_name',''));
      newcategory := AnsiExtractQuotedStr(category, '"');

      if interest.GetProperty('specializes', '') = '' then
      begin
        if trim(newcategory) = '' then newcategory := Parms.baseclass;
        if Parms.InheritCategory then
           sl.Add('class ' + interest.name + ' : ' + newcategory)
        else
         //sl.add('class ' + interest.Name + ' : ' + interest.componentTemplate);
         sl.add('class ' + interest.Name + ' : ' + Parms.baseclass);

      end
      else
      begin
        if trim(newcategory) = '' then newcategory := interest.GetProperty('specializes', '');
        if Parms.InheritCategory then
           sl.Add('class ' + interest.name + ' : ' + newcategory)
         else
           sl.add('class ' + interest.Name + ' : ' + interest.GetProperty('specializes', ''));
      end;
      sl.add('{');
      sl.add('public:');
      for x := 0 to interest.ChildBlocks.Count -1 do
      begin
        if interest.ChildBlocks.Items[x].ComponentType = gctComponentBlock then
        begin
          sl.add('/** components::' + interest.ChildBlocks.Items[x].Name + ' Component */');
          sl.Add(interest.ChildBlocks.Items[x].Name + ' ' + interest.ChildBlocks.Items[x].Name + ';');
        end;
      end;
      sl.add('/**');
      sl.add('  * @code');
      Token.data := sl.Text;
      interest.TokenList.Insert(0, Token);


      new(token);
      token.tokentype := gtComment;
      token.data := '* @endcode' + #13#10 + '*/';
      interest.TokenList.Add(token);

      new(Token);
      token.tokentype := gtSymbol;
      token.data := '}';
      interest.TokenList.Add(token);

      new(Token);
      token.tokentype := gtSymbol;
      token.data := ';';
      interest.TokenList.Add(token);

    end;
    sl.text := GasTree.BuildGas();

    sl.SaveToFile(Parms.Output);

  finally
    if assigned(Sl) then FreeAndNil(sl);
    if assigned(GasTree) then freeAndNil(GasTree);
  end;

end;
function isdOxygenComment(comment : string):boolean;
begin
  comment := trim(comment);
  if length(comment) < 3 then
  begin
    result := false;
    exit;
  end;

  if copy(comment,1,3) <> '/**' then
  begin
    result := false;
    exit;
  end;

  result := true;

end;

procedure ProcessSkrit();
var
  GasTree           : TGasInterestItem;
  sl                : TStringlist;
  i                 : integer;
  interest          : TGasInterestItem;
  Token             : PGasToken;
  tempstr           : PChar;
begin

  sl := nil;
  GasTree := nil;
  try
    sl := TStringlist.create;

    GasTree := TGasInterestItem.create;
    sl.LoadFromFile(Parms.Input);

    //preprocess it a bit
    sl.text := stringreplace(sl.text, '#only (game)', '', [rfReplaceAll, rfignorecase]);
    sl.text := stringreplace(sl.text, '[[', '', [rfReplaceAll, rfignorecase]);
    sl.text := stringreplace(sl.text, ']]', '', [rfReplaceAll, rfignorecase]);

    GasTree.ParseGas(sl.text);

    i := 0;

    //This is difficult
    //we need to look for the @class directive
    //

    for i := 0 to GasTree.TokenList.Count - 1 do
    begin
      case GasTree.TokenList.Tokens[i].tokenType of
        gtWhitespace,gtEOL : continue;
        gtComment : if isdOxygenComment(GasTree.TokenList.Tokens[i].data) then break else continue;
        else break;
      end;
//      if (GasTree.TokenList.Tokens[i].tokentype <> gtWhitespace) and (GasTree.TokenList.Tokens[i].tokentype <> gtComment) and (GasTree.TokenList.Tokens[i].tokentype <> gtEol) then
//        break;
    end;

    i := i + 1;
    sl.clear;

    new(Token);
    Token.tokentype := gtComment;

    if Parms.is_File = false then
    begin
    sl.Add('class components::' + ChangeFileExt(ExtractFileName(Parms.Input), '') + ' : ' + Parms.BaseClass);
    sl.add('{');
    sl.add('public:');
    Token.data := sl.text;
    GasTree.TokenList.Insert(i, Token);

    new(Token);
    token.tokentype := gtSymbol;
    token.data := '}';
    GasTree.TokenList.Add(token);

    new(Token);
    token.tokentype := gtSymbol;
    token.data := ';';
    GasTree.TokenList.Add(token);
    end;

    sl.text := GasTree.BuildGas();

    sl.SaveToFile(Parms.Output);

  finally
    if assigned(Sl) then FreeAndNil(sl);
    if assigned(GasTree) then freeAndNil(GasTree);
  end;

end;

procedure Process;
var
  i                 : integer;
begin
  //Params:  <Input> <Output> <-Format=GAS|SKRIT> [-AutoDoc]
  CommandLineList := nil;
  try
    CommandLineList := TStringlist.create;
    for i := 1 to ParamCount do
    begin
      if pos('=',paramstr(i)) = 0 then
        CommandLineList.Add(Paramstr(i) + '=Y')
      else
        CommandLineList.add(Paramstr(i));
    end;

    Parms.AutoDoc := YNToBool(GetCommandLineParm('-Autodoc', 'N'));
    Parms.input := GetCommandLineParm('-Input', '');
    Parms.output := GetCommandLineParm('-Output', '');
    Parms.Format := uppercase(GetCommandLineParm('-Format', 'GAS'));

    if Parms.Format = 'GAS' then
       Parms.BaseClass := GetCommandLineParm('-BaseClass','Templates')
    else
       Parms.BaseClass := GetCommandLineParm('-BaseClass','Components');
    Parms.InheritCategory :=   YNToBool(GetCommandLineParm('-InheritCategory', 'N'));
    Parms.is_file := YNToBool(GetCommandLineParm('-is_file','N'));

    if Parms.Format = 'SKRIT' then
      ProcessSkrit
    else
      ProcessGas;
  finally
    if assigned(CommandLineList) then FreeAndNil(CommandLineList);
  end;
end;
end.

