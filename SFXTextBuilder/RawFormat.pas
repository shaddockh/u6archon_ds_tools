unit RawFormat;

interface

uses Classes, SysUtils, Graphics;

type
  TRawGraphic = class(TBitmap)
  public
    procedure SaveToFile(const Filename: string); override;
    procedure SaveToStream(Stream: TStream); override;
  end;

  FOURCC = Array[1..4] of char;

  TRawHeader = packed record
    mHeaderMagic  : FOURCC;// special magic number
    mFormat       : FOURCC; // format of bits
    mFlags	  : Word;  // any special flags (for future expansion)
    mSurfaceCount : Word;  // total surfaces stored (for mip maps), always >= 1
    mWidth	  : Word;  // width of surface 0
    mHeight	  : Word;  // height of surface 0
  end;
  PRawHeader = ^TRawHeader;

const
  HEADER_MAGIC: FOURCC = 'ipaR'; // $69706152

  FORMAT_UNKNOWN   = $00000000;
  FORMAT_ARGB_8888: FOURCC = '8888';
  FLAG_NONE        = $0000;

implementation

{ TRawGraphic }

procedure TRawGraphic.SaveToFile(const Filename: string);
var
  f: TFileStream;
begin
  f := TfileStream.Create(FileName, fmCreate);
  SaveToStream(f);
  f.Free;
end;

procedure TRawGraphic.SaveToStream(Stream: TStream);
var
  Hdr: TRawHeader;
begin
  with Hdr do
  begin
    mHeaderMagic := HEADER_MAGIC;
    mFormat := FORMAT_ARGB_8888;
    mFlags := FLAG_NONE;
    mSurfaceCount := 1;
    mWidth := Width;
    mHeight := Height;
  end;
  Stream.Write(Hdr, SizeOf(TRawHeader));
  Stream.Write(ScanLine[Height -1]^, Height * Width * 4);
end;

end.
