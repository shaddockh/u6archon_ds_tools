program SFXTextBuilder;

uses
  Forms,
  uFrmSFXTextBuilder in 'uFrmSFXTextBuilder.pas' {frmFontBuilder},
  RawFormat in 'RawFormat.pas',
  uFrmBatchRender in 'uFrmBatchRender.pas' {frmBatchRender};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfrmFontBuilder, frmFontBuilder);
  Application.CreateForm(TfrmBatchRender, frmBatchRender);
  Application.Run;
end.
