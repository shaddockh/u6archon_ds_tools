object frmBatchRender: TfrmBatchRender
  Left = 236
  Top = 280
  Width = 419
  Height = 445
  Caption = 'Batch Render'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    411
    411)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 6
    Width = 226
    Height = 13
    Caption = 'List of SFX-Text Items to Render (One per Line):'
  end
  object Label2: TLabel
    Left = 8
    Top = 288
    Width = 261
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'File Naming Convention (%s will be replaced with word):'
  end
  object Label3: TLabel
    Left = 24
    Top = 346
    Width = 258
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '(% Name expansion will be applied to the filename part)'
  end
  object Button1: TButton
    Left = 331
    Top = 378
    Width = 73
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object Button2: TButton
    Left = 250
    Top = 378
    Width = 73
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Ok'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object memoBatchRender: TMemo
    Left = 8
    Top = 24
    Width = 396
    Height = 257
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
  end
  object edFileNameConvention: TEdit
    Left = 8
    Top = 302
    Width = 393
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 3
    Text = 'b_pb_sfxt_%s.raw'
  end
  object ckNameValuePairs: TCheckBox
    Left = 8
    Top = 328
    Width = 393
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'List is formatted as:  FILENAME=<Text> '
    TabOrder = 4
  end
end
