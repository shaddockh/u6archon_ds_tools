unit uFrmBatchRender;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmBatchRender = class(TForm)
    Button1: TButton;
    Button2: TButton;
    memoBatchRender: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    edFileNameConvention: TEdit;
    ckNameValuePairs: TCheckBox;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBatchRender: TfrmBatchRender;

implementation

{$R *.dfm}

end.
