object frmFontBuilder: TfrmFontBuilder
  Left = 258
  Top = 146
  BorderStyle = bsDialog
  Caption = 'SFX-Text Texture Generator'
  ClientHeight = 339
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 288
    Top = 24
    Width = 256
    Height = 256
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 24
    Height = 13
    Caption = 'Font:'
  end
  object Label2: TLabel
    Left = 224
    Top = 8
    Width = 27
    Height = 13
    Caption = 'Pitch:'
  end
  object Label7: TLabel
    Left = 8
    Top = 53
    Width = 74
    Height = 13
    Caption = 'Text to Render:'
  end
  object Label8: TLabel
    Left = 288
    Top = 8
    Width = 38
    Height = 13
    Caption = 'Render:'
  end
  object Label9: TLabel
    Left = 8
    Top = 289
    Width = 62
    Height = 13
    Caption = 'Texture Size:'
  end
  object Label10: TLabel
    Left = 0
    Top = 314
    Width = 555
    Height = 25
    Align = alBottom
    AutoSize = False
    Caption = 
      'PB Standard is: 256x256  Single Word - 20pt Tahoma AA.  Multi Wo' +
      'rd - 18pt Tahoma AA'
    Color = clInfoBk
    ParentColor = False
    Layout = tlCenter
    Visible = False
    WordWrap = True
  end
  object Label11: TLabel
    Left = 8
    Top = 312
    Width = 64
    Height = 13
    Caption = 'Output Prefix:'
  end
  object Button1: TButton
    Left = 432
    Top = 295
    Width = 89
    Height = 26
    Caption = 'Save'
    TabOrder = 0
    OnClick = Button1Click
  end
  object JvFontComboBox1: TJvFontComboBox
    Left = 8
    Top = 24
    Width = 201
    Height = 22
    FontName = 'Abadi MT Condensed'
    ItemIndex = 0
    Options = [foTrueTypeOnly]
    Sorted = True
    TabOrder = 1
    OnChange = FontSettingChanged
  end
  object cmbSize: TComboBox
    Left = 224
    Top = 24
    Width = 41
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 2
    Text = '8'
    OnChange = FontSettingChanged
    Items.Strings = (
      '8'
      '9'
      '10'
      '11'
      '12'
      '14'
      '18'
      '20'
      '24'
      '32')
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 128
    Width = 273
    Height = 153
    Caption = 'Font Style:'
    TabOrder = 3
    object Label5: TLabel
      Left = 172
      Top = 17
      Width = 93
      Height = 13
      Caption = '(Not recommended)'
    end
    object Label6: TLabel
      Left = 172
      Top = 41
      Width = 93
      Height = 13
      Caption = '(Not recommended)'
    end
    object Label4: TLabel
      Left = 8
      Top = 96
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Foreground Color:'
    end
    object Label3: TLabel
      Left = 6
      Top = 124
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Background Color:'
    end
    object chkBold: TCheckBox
      Left = 8
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Bold'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = FontSettingChanged
    end
    object chkItalic: TCheckBox
      Left = 8
      Top = 40
      Width = 73
      Height = 17
      Caption = 'Italic'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 1
      OnClick = FontSettingChanged
    end
    object chkUnderline: TCheckBox
      Left = 88
      Top = 16
      Width = 73
      Height = 17
      Caption = 'Underline'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
      OnClick = FontSettingChanged
    end
    object chkStrikethrough: TCheckBox
      Left = 88
      Top = 40
      Width = 85
      Height = 17
      Caption = 'Strikethrough'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsStrikeOut]
      ParentFont = False
      TabOrder = 3
      OnClick = FontSettingChanged
    end
    object chkAntialias: TCheckBox
      Left = 8
      Top = 72
      Width = 257
      Height = 17
      Caption = 'Antialias (Only works for certain fonts/OSs)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = FontSettingChanged
    end
    object JvColorButton2: TJvColorButton
      Left = 96
      Top = 93
      Width = 49
      Height = 22
      OtherCaption = '&Other...'
      Options = []
      Color = clWhite
      OnChange = FontSettingChanged
    end
    object JvColorButton1: TJvColorButton
      Left = 96
      Top = 120
      Width = 49
      Height = 22
      OtherCaption = '&Other...'
      Options = []
      OnChange = FontSettingChanged
    end
  end
  object edTextToRender: TEdit
    Left = 8
    Top = 72
    Width = 273
    Height = 49
    AutoSize = False
    TabOrder = 4
    Text = 'sample'
    OnChange = FontSettingChanged
  end
  object rbTexture128: TRadioButton
    Left = 80
    Top = 288
    Width = 73
    Height = 17
    Caption = '128x128'
    Checked = True
    TabOrder = 5
    TabStop = True
    OnClick = FontSettingChanged
  end
  object rbTexture256: TRadioButton
    Left = 168
    Top = 288
    Width = 73
    Height = 17
    Caption = '256x256'
    TabOrder = 6
    OnClick = FontSettingChanged
  end
  object Button2: TButton
    Left = 304
    Top = 296
    Width = 105
    Height = 25
    Caption = 'Batch Generate...'
    TabOrder = 7
    OnClick = Button2Click
  end
  object edOutputPrefix: TEdit
    Left = 80
    Top = 308
    Width = 198
    Height = 21
    TabOrder = 8
    Text = 'b_pb_sfxt_bark_'
  end
  object dlgSave: TSaveDialog
    DefaultExt = 'raw'
    Filter = '*.raw|*.raw|All Files (*.*)|*.*'
    Left = 176
    Top = 240
  end
  object JvBrowseForFolderDialog1: TJvBrowseForFolderDialog
    Position = fpFormCenter
    RootDirectory = fdRootFolder
    Left = 528
    Top = 65528
  end
  object SaveBatch: TSaveDialog
    Filter = '*.txt'
    Title = 'Save Batch Settings'
    Left = 224
    Top = 240
  end
end
