unit uFrmSFXTextBuilder;
(*

 Copyright (C) 2004  Jesse Strachman, T. Shaddock Heath

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/**
 *
 * File     :  $RCSfile: uFrmSFXTextBuilder.pas,v $
 * Author(s):  Jesse (archon), Shaddock (archon)
 *
 * @copyright (C) 2004 Archon.
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://www.u6archon.com
 *
 * Main interface for the DS Font Builder
 *
 *----------------------------------------------------------------------------
 *  $Revision: 1.2 $              $Date: 2005-09-10 14:09:42 $
 *----------------------------------------------------------------------------
 *
 */

 Updated 2004-02-23   by Dan
 -Uglified the code by a factor of 3.7
 -Fixed anti-aliasing on the alpha chan if selected.
 -Outputs .raw files instead of 3vil tga.
 -Shows a save dialog instead of saving to app dir

 2004-02-24 Shaddock (Archon)
 - Merged Dan's changes in with the official branch
 - Fixed an issue with the dot being rendered off the edge of the texture.
 - Consolidated the Font Setting Event Handlers
 - Added 24pt and 32pt Font Settings

*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, JvCombobox, JvColorCombo, JvComponent,
  JvColorBox, JvColorBtn, ShellApi, RawFormat, uFrmBatchRender, JvBaseDlg,
  JvBrowseFolder, inifiles;

type
  TfrmFontBuilder = class(TForm)
    Image1: TImage;
    Button1: TButton;
    JvFontComboBox1: TJvFontComboBox;
    cmbSize: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    chkBold: TCheckBox;
    chkItalic: TCheckBox;
    chkUnderline: TCheckBox;
    chkStrikethrough: TCheckBox;
    chkAntialias: TCheckBox;
    Label4: TLabel;
    JvColorButton2: TJvColorButton;
    JvColorButton1: TJvColorButton;
    Label3: TLabel;
    dlgSave: TSaveDialog;
    edTextToRender: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    rbTexture128: TRadioButton;
    rbTexture256: TRadioButton;
    Label9: TLabel;
    Button2: TButton;
    JvBrowseForFolderDialog1: TJvBrowseForFolderDialog;
    SaveBatch: TSaveDialog;
    Label10: TLabel;
    Label11: TLabel;
    edOutputPrefix: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FontSettingChanged(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);

  private
    { Private declarations }
  public
    { Public declarations }
    procedure RenderFont(SaveToFile: boolean; Text: string; Filename: string = '');
    procedure FontToBitmap(bmp: TBitmap; text: string; Flip: boolean);
  end;

  TColorData = array[0..128000] of TRGBQuad;
  pColorData = ^TColorData;

function BitmapFlip(var BitmapIn: TBitmap; var BitmapOut: TBitmap): Boolean;

var
  frmFontBuilder: TfrmFontBuilder;

const
  DOTOFFSET = 1; //# of pixels between right edge of letter and dot
  TITLE = 'SFX-Text Texture Generator';
  VERSION = 'v. 0.1';

implementation

{$R *.DFM}

function MakeItAString(I: Longint): string;

{ Convert any integer type to a string }
var
  S: string[11];
begin
  Str(I, S);
  Result := S;
end;

procedure TfrmFontBuilder.Button1Click(Sender: TObject);
begin
  RenderFont(true, edTextToRender.text);

end;

procedure TfrmFontBuilder.FontToBitmap(bmp: TBitmap; text: string; flip: boolean);
var
  lf: TLogFont;
  bmp2: TBitmap;

  texturesize: integer;


  totalphrasewidth: integer;
  totalphraseheight: integer;
begin


  if rbTexture128.checked then
    textureSize := 128
  else
    textureSize := 256;
  bmp.PixelFormat := pf32bit;
  bmp.Canvas.Font.Name := jvFontComboBox1.FontName;
  bmp.Canvas.Font.Pitch := fpVariable;
  bmp.Canvas.Font.Size := strToIntDef(cmbSize.Text, 8);

  bmp.Width := textureSize;
  bmp.Height := textureSize;

  bmp2 := TRawGraphic.create;
  bmp2.PixelFormat := pf32bit;
  bmp2.Canvas.Font.Name := bmp.Canvas.Font.Name;
  bmp2.Canvas.Font.Pitch := fpVariable;
  bmp2.Canvas.Font.Size := bmp.Canvas.Font.Size;

  bmp2.Canvas.Brush.Color := bmp.Canvas.Brush.Color;
  bmp2.Canvas.FillRect(bmp2.Canvas.ClipRect);
  bmp2.Canvas.Font.Color := bmp.Canvas.Font.Color;


  bmp2.Width := textureSize;
  bmp2.Height := textureSize;

  // set font style
  if chkBold.Checked then
    bmp2.Canvas.Font.Style := bmp2.Canvas.Font.Style + [fsBold];
  if chkItalic.Checked then
    bmp2.Canvas.Font.Style := bmp2.Canvas.Font.Style + [fsItalic];
  if chkUnderline.Checked then
    bmp2.Canvas.Font.Style := bmp2.Canvas.Font.Style + [fsUnderline];
  if chkStrikethrough.Checked then
    bmp2.Canvas.Font.Style := bmp2.Canvas.Font.Style + [fsStrikeout];

  if chkAntialias.Checked then
  begin
    GetObject(bmp2.Canvas.Font.Handle, SizeOf(TLogFont), @lf);
    lf.lfQuality := ANTIALIASED_QUALITY;
    bmp2.Canvas.Font.Handle := CreateFontIndirect(lf);
  end;

  //Here we need to center vertically and horizontally

  totalPhraseWidth := bmp2.canvas.TextWidth(text);
  totalPhraseHeight := bmp2.canvas.TextHeight(text);

  bmp2.canvas.textout((textureSize div 2 - 1) - (totalPhraseWidth div 2), (textureSize div 2 - 1) - (totalPhraseHeight div 2), text);

  if (flip) then
    BitmapFlip(bmp2, bmp)
  else
    bmp.Assign(bmp2);
end;

procedure TfrmFontBuilder.RenderFont(SaveToFile: boolean; Text: string; Filename: string = '');
type
  PRGBQuadArray = ^TRGBQuadArray;
  TRGBQuadArray = array[WORD] of TRGBQuad;

var
  basename: string;
  P: PRGBQuadArray;
  Ptl: PRGBQuadArray;
  y: integer;
  x: integer;

  bmp: TRawGraphic;
  bmp_alpha: TBitmap;
  bmp_display: TRawGraphic;
begin
  //Create the base filename - this follows DS naming conventions
  basename := edOutputPrefix.text + edTextToRender.Text;
  basename := StringReplace(basename, ' ', '', [rfReplaceAll]);
  basename := StringReplace(basename, '"', '', [rfReplaceAll]);
  basename := StringReplace(basename, '*', '', [rfReplaceAll]);
  basename := StringReplace(basename, '?', '', [rfReplaceAll]);
  basename := StringReplace(basename, '!', '', [rfReplaceAll]);
  basename := StringReplace(basename, ',', '', [rfReplaceAll]);
  basename := LowerCase(basename);

  if filename = '' then
    dlgSave.FileName := basename + '.raw'
  else
    dlgSave.filename := filename;

  if SaveToFile and (filename = '') then
  begin
    if not dlgSave.Execute then Exit;
  end;

  bmp := nil;
  bmp_alpha := nil;
  try
    bmp := TRawGraphic.Create;
    bmp.Canvas.Brush.Color := JvColorButton1.Color;
    bmp.Canvas.FillRect(bmp.Canvas.ClipRect);
    bmp.Canvas.Font.Color := JvColorButton2.Color;

    bmp_display := TRawGraphic.Create;
    bmp_display.Canvas.Brush.Color := JvColorButton1.Color;
    bmp_display.Canvas.FillRect(bmp.Canvas.ClipRect);
    bmp_display.Canvas.Font.Color := JvColorButton2.Color;

    bmp_alpha := TBitmap.Create;
    bmp_alpha.Canvas.Brush.Color := clBlack;
    bmp_alpha.Canvas.FillRect(bmp.Canvas.ClipRect);
    bmp_alpha.Canvas.Font.Color := clWhite;


    FontToBitmap(bmp_display, text, false);

    if SaveToFile then
    begin
      FontToBitmap(bmp, text, true);
      FontToBitmap(bmp_alpha, text, true);

      // Set up alpha
      for y := 0 to bmp.Height - 1 do
      begin
        P := bmp.ScanLine[y];
        Ptl := bmp_alpha.ScanLine[y];
        for x := 0 to bmp.Width - 1 do
        begin
          p[x].rgbReserved := (Ptl[x].rgbBlue + Ptl[x].rgbGreen + Ptl[x].rgbRed) div 3;
        end;
      end;
    end;

    //Display our handiwork
    Image1.Canvas.FillRect(Image1.Canvas.ClipRect);
    Image1.Canvas.Draw(0, 0, bmp_display);

    //Save the file.
    if SaveToFile then
    begin
      bmp.SaveToFile(dlgSave.FileName);
    end;
  finally
    if assigned(bmp) then FreeAndNil(bmp);
    if assigned(bmp_alpha) then FreeAndNil(bmp_alpha);
    if assigned(bmp_display) then FreeAndNil(bmp_display);
  end;
end;

procedure TfrmFontBuilder.FormCreate(Sender: TObject);
var
  ini: TMemIniFile;
  pitch : string;
begin
  Caption := TITLE + ' (' + VERSION + ')';


  ini := TMeminifile.Create(ChangeFileExt(application.exename, '.ini'));

  jvFontComboBox1.FontName := ini.ReadString('Preferences', 'Font Name', 'Tahoma');

  pitch :=ini.ReadString('Preferences', 'Font Pitch', '18');
  cmbSize.ItemIndex := cmbSize.Items.IndexOf(pitch);
  chkBold.Checked := ini.Readbool('Preferences', 'Bold', chkbold.Checked);
  chkUnderline.checked := ini.Readbool('Preferences', 'Underline', chkunderline.Checked);
  chkItalic.Checked := ini.Readbool('Preferences', 'Italic', chkitalic.Checked);
  chkStrikethrough.checked := ini.Readbool('Preferences', 'StrikeThrough', chkStrikethrough.Checked);
  chkAntiAlias.Checked := ini.Readbool('Preferences', 'AntiAlias', true);
  rbTexture128.Checked := ini.Readbool('Preferences', '128x128', false);
  rbTexture256.checked := ini.Readbool('Preferences', '256x256', true);
  edOutputPrefix.text := ini.ReadString('Preferences', 'OutputPrefix', edOutputPrefix.text);
  ini.free;







  edTextToRender.Font.name := JvFontComboBox1.FontName;
  RenderFont(False, edTextToRender.text);
end;

procedure TfrmFontBuilder.FontSettingChanged(Sender: TObject);
begin
  edTextToRender.Font.name := JvFontComboBox1.FontName;
  edTextToRender.Font.Size := StrToIntDef(cmbSize.Text, 8);
  RenderFont(False, edTextToRender.text);

end;


//From about.com

function BitmapFlip(var BitmapIn: TBitmap; var BitmapOut: TBitmap): Boolean;
var
  DataIn: pColorData;
  DataOut: pColorData;
  inRow: Integer;
  inCol: Integer;
  vertical: boolean;
  horizontal: boolean;
  //bitmapOut: TBitmap;
begin
  vertical := false;
  horizontal := true;
  //BitmapOut := TBitmap.create;
  Result := False;
  try
    if BitmapIn.PixelFormat <> pf32bit then Exit;
    with BitmapOut do
    begin
      Width := BitmapIn.Width;
      Height := BitmapIn.Height;
      PixelFormat := BitmapIn.PixelFormat;
    end;
    for inRow := 0 to BitmapIn.Height - 1 do
    begin
      DataIn := BitmapIn.Scanline[inRow];
      if Vertical then
      begin
        DataOut := BitmapOut.ScanLine
          [BitmapIn.Height - 1 - inRow];
      end
      else
      begin
        DataOut := BitmapOut.ScanLine[inRow];
      end;
      if Horizontal then
      begin
        for inCol := 0 to BitmapIn.Width - 1 do
          DataOut[inCol] := DataIn
            [BitmapIn.Width - 1 - inCol];
      end
      else
      begin
        for inCol := 0 to BitmapIn.Width - 1 do
          DataOut[inCol] := DataIn[inCol];
      end;
    end;
    Result := True;
    //BitmapIn.Assign(BitmapOut);
//      BitmapIn.Canvas.FillRect(Bitmapout.Canvas.ClipRect);
    //BitmapOut.free;
  except
  end;
end;

procedure TfrmFontBuilder.Button2Click(Sender: TObject);
var
  batchForm: TfrmBatchRender;
  outputDirectory: string;
  filenameconvention: string;
  i: integer;
  nameValue: boolean;

  filename: string;
  rendertext: string;
begin

  batchForm := TfrmBatchRender.Create(self);
  if batchForm.ShowModal = mrOk then
  begin
    JvBrowseForFolderDialog1.Title := 'Select the directory to generate textures to';
    if JvBrowseForFolderDialog1.Execute = true then
    begin
      outputDirectory := JvBrowseForFolderDialog1.Directory;
      filenameconvention := outputDirectory + '\' + batchform.edFileNameConvention.text;
      nameValue := batchForm.ckNameValuePairs.Checked;
      for i := 0 to batchForm.memoBatchRender.Lines.Count - 1 do
      begin
        if nameValue then
        begin
          filename := batchForm.memoBatchRender.Lines.Names[i];
          renderText := batchForm.memoBatchRender.Lines.values[filename];
        end
        else
        begin
          renderText := batchform.memobatchrender.Lines[i];
          filename := StringReplace(batchform.memobatchrender.Lines[i], ' ', '', [rfReplaceAll]);
          filename := StringReplace(filename, '+', '', [rfReplaceAll]);
          filename := StringReplace(filename, '\', '', [rfReplaceAll]);
          filename := StringReplace(filename, '/', '', [rfReplaceAll]);
          filename := StringReplace(filename, '?', '', [rfReplaceAll]);
          filename := StringReplace(filename, '*', '', [rfReplaceAll]);
        end;

        filename := StringReplace(filenameConvention, '%s', filename, [rfIgnoreCase, rfReplaceAll]);


        RenderFont(true, renderText, fileName);
      end;


      //if MessageDlg('Rendering has completed.  Would you like to save this batch for a later run?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      //begin

      //end;

    end;

  end;
  batchForm.free;
end;

procedure TfrmFontBuilder.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  ini: TMemIniFile;
begin
  case MessageDlg('Save Settings?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
    mrYes:
      begin
        ini := TMeminifile.Create(ChangeFileExt(application.exename, '.ini'));
        ini.WriteString('Preferences', 'Font Name', jvFontComboBox1.FontName);
        ini.WriteString('Preferences', 'Font Pitch', cmbSize.text);
        ini.Writebool('Preferences', 'Bold', chkbold.Checked);
        ini.Writebool('Preferences', 'Underline', chkunderline.Checked);
        ini.Writebool('Preferences', 'Italic', chkitalic.Checked);
        ini.Writebool('Preferences', 'StrikeThrough', chkStrikethrough.Checked);
        ini.Writebool('Preferences', 'AntiAlias', chkAntiAlias.Checked);
        ini.Writebool('Preferences', '128x128', rbTexture128.Checked);
        ini.Writebool('Preferences', '256x256', rbTexture256.Checked);
        ini.WriteString('Preferences', 'OutputPrefix', edOutputPrefix.text);
        ini.UpdateFile;
        ini.free;
      end;
    mrCancel: CanClose := false;
  end;

end;

end.

