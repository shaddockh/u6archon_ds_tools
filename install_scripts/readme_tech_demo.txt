Ultima 6 Project
Technical Demo

******************************************************************************
Notes
******************************************************************************

This represents almost three years of technical research and implementation.
This would not have been possible without contributions from many individuals
and groups.  Team Archon is very grateful, and respectfully asks that you take
a moment to read the Credits and Special Thanks sections.

The purpose of this demonstration is to show the public the technical systems
that we are recreating in order to support an Ultima-like game experience.
Please note that a fair number of these systems are in a Beta-like state, and
there are bound to be problems.  Please report any bugs that you find in
the U6 Archon forums, and we will try to get them fixed so that they work
correctly in subsequent public releases.

Thanks again for taking the time to review our work, and we hope you enjoy
what we've wrought.  And remember, as Bill and Ted once said, "Be excellent
to each other."

******************************************************************************
Installation
******************************************************************************

Ensure that you do not have any extra non-standard DS/DSLOA resource files or
modifications installed in your Dungeon Siege Resources Directory.  The U6P
technical demo installs into a separate directory from normal Dungeon Siege
and provides its own custom shortcut so it should not interfere with any
other modifications.

The default location that the U6p Technical Demo will install to is:
c:\program files\Microsoft Games\Dungeon Siege\Mods\u6p_tech_demo

To uninstall, use Windows Add/Remove Programs or the Uninstall shortcut in
the start menu.


******************************************************************************
Credits
******************************************************************************

TEAM ARCHON

For this demo:
--------------

Matt "Sliding Dragon" Hutaff : Founder, Webmaster

Jesse "Zephyr" Strachman : Project Manager, Art Director, Conversation System 
  Design/Code, Skritter, Mapper, 3d Modeler

Martin "Rowindor" Kale : World Building Director, Mapper

Shaddock "Frilly Wumpus" Heath : Coding Director, Mapper, Skritter 
  (Boats, Moongates, Spellcasting, Tips, etc., etc.)

Steven Keys : Musician

Shafali Anand : UI and portrait art

Aaron "Xmen90s" Anderson : Skritter (Custom stats system, Initial Teleport Code, Shrines)

Pamela "Luthien Elfsinger" Strachman : Conversation content

Team members working on an upcoming release:
--------------------------------------------

Riptzen : Mapper (upcoming release)

Aaron "Baalzamon" Taylor : 3d trailer (upcoming release)





******************************************************************************
Special Thanks
******************************************************************************

Team Lazarus
---------------
For moral support, Boat Mesh, Reagent Meshes, Reagent Icons, to 
True Kirika for additional conversation engine coding, and to Risen
Dragon for assistance with UI development

Planet DungeonSiege
---------------
For supporting the DS community and providing us with our web hosting.

Stephanie Legault
---------------
For the original format of the API documentation and extensive research
of the API system

Xaa
---------------
For the skrit tutorials, beta testing the UI Designer, carryable torch 
idea, and providing so much knowledge and insight to the community.

Major Hostility
---------------
For beta testing the UI Designer and building the SFX Tester 
which was used to help create this demo.

Witness
----------------
For providing a fabulous central resource for the community and 
providing insight into inducing unconsciousness.  Also for the 
listen.skrit debug component.

Jordan Russell
----------------
For the Inno Setup Installer 
http://www.jrsoftware.org

Sourceforge.net
----------------
For providing support and source control to the open source
community.

Uncle Ernsie
----------------
For providing answers when there were none to be found and 
for generally supporting the community.

Gas Powered Games
----------------
For providing such a fabulous system for creating new worlds.

Origin Systems and Richard "Lord British" Garriott
----------------
For having the vision to create a world that still fascinates 
and inpsires us decades later.

Last but not Least, The Fans
----------------
Who provide us with the motivation to do crazy things like this.


******************************************************************************
Legal
******************************************************************************

Dungeon Siege is a registered trademark and copyright 2002 by Gas Powered Games, 
Inc. and Microsoft, Inc.

Ultima is a registered trademark of Origin Systems.

Lord British is a registered trademark of Richard Garriott.

All original music contained in this release is copyright 2003-2004 by Steven 
Keys, and is used by permission.  All other rights are reserved on these
compositions, and they may not be reproduced or distributed in any other context
without the express written consent of the composer.
http://www.stevenkeys.com

Original versions of Stones and the U4 Wander theme are Copyright (c) Origin
Systems.

Original UI artwork and NPC portraits are copyright 2003-2004 by Shafali 
Anand, and are used by permission.  All other rights are reserved on these
compositions, and they may not be reproduced or distributed in any other context
without the express written consent of the artist.
http://www.innoken.com

Original Avatar portraits are copyright 2002 by Julio Vanzeler and are used by 
permission.  All other rights are reserved on these compositions, and they may 
not be reproduced or distributed in any other context without the express 
written consent of the artist.

Boat Mesh, Reagent Meshes, and Reagent Icons are copyright 2003 Team Lazarus, 
and are used by permission.  All other rights are reserved on these
compositions, and they may not be reproduced or distributed in any other context
without the express written consent of the artist(s).

All other original artwork or content not specifically mentioned here is
copyright 2003-2004 Team Archon, and they may not be reproduced or distributed 
in any other context without express written consent.

Team Archon does not warrant this software for any particular purpose or use,
and cannot be held liable for any damage caused by the use of this software.
The user consents to the use of this software AT HIS OR HER OWN RISK.


******************************************************************************
Open Source License for Code
******************************************************************************

All code contained in u6p_logic.dsres is considered open source and 
may be used in other work, provided the original author(s) is given credit
and any modifications adhere to the GNU General Public License.  (Link below.)

http://www.gnu.org/copyleft/gpl.html